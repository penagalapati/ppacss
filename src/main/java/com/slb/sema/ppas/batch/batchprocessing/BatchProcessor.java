////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchProcessor.java
//      DATE            :       16-April-2004
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       Implements all common logic for batch data processing.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcommon.ThreadControl;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;


/**
 * This abstract class implements all common logic for all batch data processing.
 * It supports multi-threading and each thread is run within its own instance of the
 * inner class <code>ProcessorThread</code>.
 */
public abstract class BatchProcessor
{
    //-------------------------------------------------------------------------
    // Private constants.
    //-------------------------------------------------------------------------
    /** Constant holding the name of the current class. Value is {@value}. */
    private static final String C_CLASS_NAME     = "BatchProcessor";

    //-------------------------------------------------------------------------
    // Private variables
    //-------------------------------------------------------------------------
    /** The number of batch processor threads to be started by this instance. */
    private int             i_noOfProcessThreads    = 0;

    /** An array of thread controllers, one object for each started processor thread. */
    private ThreadControl[] i_threadControlArr      = null;
    
    /** A finished thread counter. */
    private int             i_finishedThreadCounter = 0;

    /** The input data queue. */
    private SizedQueue      i_inQueue               = null;

    /** The output data queue. */
    private SizedQueue      i_outQueue              = null;

    /** The input data queue used for retry reasons. */
    private SizedQueue      i_retryQueue            = null;
    
    /** The queue currently used by the batch process. */
    private SizedQueue      i_processQueue          = null;

    /** The time the thread should sleep before run the retry queue. */
    private long i_sleepTimeBeforeRetry             = 0;


    //-------------------------------------------------------------------------
    // Protected variables
    //-------------------------------------------------------------------------
    /** The PpasContext object. */
    protected PpasContext     i_ppasContext     = null;

    /** The logger component. */
    protected Logger          i_logger          = null;
    
    /** The start parameters storage. */
    protected Map             i_params          = null;

    /** The batch controller object. */
    protected BatchController i_batchController = null;

    /** The IS API timeout value. */
    protected long            i_isApiTimeout    = BatchConstants.C_DEFAULT_TIMEOUT;
    
    /** The properties used by the batch sub-system. */
    protected PpasProperties  i_properties      = null;
    

    //-------------------------------------------------------------------------
    // Constructors
    //-------------------------------------------------------------------------
    /**
     * Constructs an instance of this <code>BatchProcessor</code> class using the specified
     * batch controller, input data queue and output data queue.
     * 
     * @param p_ppasContext      the PpasContext reference
     * @param p_logger           the logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_params           the start process paramters
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public BatchProcessor(PpasContext     p_ppasContext,
                          Logger          p_logger,
                          BatchController p_batchController,
                          SizedQueue      p_inQueue,
                          SizedQueue      p_outQueue,
                          Map             p_params,
                          PpasProperties  p_properties)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                "Constructing " + C_CLASS_NAME);
        }

        // Init. the instance variables.
        i_ppasContext          = p_ppasContext;
        i_logger               = p_logger;
        i_batchController      = p_batchController;
        i_inQueue              = p_inQueue;
        i_outQueue             = p_outQueue;
        i_params               = p_params;
        i_properties           = p_properties;
        i_noOfProcessThreads   = i_properties.getIntProperty(BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS);
        try
        {
            i_sleepTimeBeforeRetry = i_properties.getLongProperty(BatchConstants.C_TIME_BEFOR_RETRY, 5000);
        }
        catch (NumberFormatException e)
        {
            i_sleepTimeBeforeRetry = 5000;
            e = null;
        }
        
        i_processQueue          = i_inQueue;
        i_finishedThreadCounter = 0;

        // Set the IS API timeout value.
        i_isApiTimeout = p_properties.getLongProperty(BatchConstants.C_TIMEOUT,
                                                      BatchConstants.C_DEFAULT_TIMEOUT);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_END,
                    C_CLASS_NAME,
                    10020,
                    this,
                    "Constructed " + C_CLASS_NAME);
        }
    }

    //-------------------------------------------------------------------------
    // Public instance methods
    //-------------------------------------------------------------------------
    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_start = "start";
    /**
     * Creates and starts the given number of processor threads, that is a number of inner class instances.
     */
    public void start()
    {
        BatchMessage l_msg = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10020,
                this,
                "Entering " + C_METHOD_start);
        }

        if (i_noOfProcessThreads > 0)
        {
            if (i_threadControlArr == null)
            {
                // Create an array of ThreadControl objects.
                i_threadControlArr = new ThreadControl[i_noOfProcessThreads];
            
                // Create each ThreadControl object and its worker thread.
                synchronized (i_threadControlArr)
                {
                    for (int i = 0; i < i_threadControlArr.length; i++)
                    {
                        // Create a ThreadControl instance.
                        i_threadControlArr[i] = new ThreadControl();

                        // Running state must be set here, otherwise the thread will start and then die
                        //directly.
                        i_threadControlArr[i].setRunning(true); 

                        // Create a new instance of the ProcessorThread (inner) class, and set it as the
                        // current ThreadControl's worker thread.
                        i_threadControlArr[i].setWorkerThread(new ProcessorThread(i));

                        // Set the 'paused' flag to false in order to allow thread execution.
                        i_threadControlArr[i].setPaused(false);

                        // Set the thread's state to unknown.
                        i_threadControlArr[i].setState(ThreadControl.C_THREAD_UNKNOWN);

                        // Start this processor thread.
                        i_threadControlArr[i].getWorkerThread().start();
                    }
                }
            }
            else
            {
                // The processing thread(s) has already been created and started.
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10025,
                                this,
                                "The processing thread(s) has already been created and started.");

                // Report the error to the batch controller.
                l_msg = new BatchMessage();
                l_msg.setSendingComponent(BatchMessage.C_COMPONENT_PROCESSOR);
                l_msg.setTargetComponent(BatchMessage.C_COMPONENT_MANAGER);
                l_msg.setRequest(BatchMessage.C_REQUEST_STATUS);
                l_msg.setStatus(BatchMessage.C_STATUS_ERROR);
                if (i_batchController != null)
                {
                    i_batchController.postMessage(l_msg);
                }

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_APPLICATION_DATA,
                        C_CLASS_NAME,
                        10030,
                        this,
                        "Status reported: " + l_msg.getStatus());
                }
            }
        }
        else
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_ERROR,
                            C_CLASS_NAME,
                            10035,
                            this,
                            "Invalid number of processor threads is specified in the property '" +
                            BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS + "': " + i_noOfProcessThreads);

            // Report the error to the batch controller.
            l_msg = new BatchMessage();
            l_msg.setSendingComponent(BatchMessage.C_COMPONENT_PROCESSOR);
            l_msg.setTargetComponent(BatchMessage.C_COMPONENT_MANAGER);
            l_msg.setRequest(BatchMessage.C_REQUEST_STATUS);
            l_msg.setStatus(BatchMessage.C_STATUS_ERROR);
            if (i_batchController != null)
            {
                i_batchController.postMessage(l_msg);
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_APPLICATION_DATA,
                    C_CLASS_NAME,
                    10040,
                    this,
                    "Status reported: " + l_msg.getStatus());
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10045,
                this,
                "Leaving " + C_METHOD_start);
        }
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_stop = "stop";
    /**
     * Stops all existing processor threads.
     */
    public void stop()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10050,
                this,
                "Entering " + C_METHOD_stop);
        }

        for (int i = 0; i < i_threadControlArr.length; i++)
        {
            synchronized (i_threadControlArr[i])
            {
                i_threadControlArr[i].setPaused(false);
                i_threadControlArr[i].setRunning(false);

                // Wake up any thread that is waiting on this object, i.e. paused.
                i_threadControlArr[i].notify();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10060,
                this,
                "Leaving " + C_METHOD_stop);
        }
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_pause = "pause";
    /**
     * Pauses all existing processor threads.
     */
    public void pause()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10070,
                this,
                "Entering " + C_METHOD_pause);
        }

        for (int i = 0; i < i_threadControlArr.length; i++)
        {
            synchronized (i_threadControlArr[i])
            {
                i_threadControlArr[i].setPaused(true);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10080,
                this,
                "Leaving " + C_METHOD_pause);
        }
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_resume = "resume";
    /**
     * Resumes all existing processor threads that have been paused.
     */
    public void resume()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10090,
                this,
                "Entering " + C_METHOD_resume);
        }

        // Re-open the output queue.
        i_outQueue.reopenForWriting();

        for (int i = 0; i < i_threadControlArr.length; i++)
        {
            synchronized (i_threadControlArr[i])
            {
                i_threadControlArr[i].setPaused(false);

                // Wake up all threads that are waiting on this object.
                i_threadControlArr[i].notify();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10100,
                this,
                "Leaving " + C_METHOD_resume);
        }
    }


    //-------------------------------------------------------------------------
    // Protected instance methods
    //-------------------------------------------------------------------------
    
    /**
     * To be overridden by the sub class in order to finishing any outstanding items.
     * Only for BBS batches.
     */
    protected void flush()
    {
        /** Flush not implemented in this class.
         *  Dummy method for some batches.
         */
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_getRecord = "getRecord";
    /**
     * Returns a record from the current process queue.
     * 
     * @return a record from the current process queue.
     * @throws SizedQueueClosedForWritingException  if the current process queue is both empty and closed for
     *                                              writing.
     */
    protected BatchRecordData getRecord() throws SizedQueueClosedForWritingException
    {
        BatchRecordData l_record = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10110,
                this,
                "Entering " + C_METHOD_getRecord);
        }

        // Get a record from the queue currently used by the batch process (indata queue or retry queue).
        l_record = (BatchRecordData)i_processQueue.removeFirst();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10120,
                this,
                "Leaving " + C_METHOD_getRecord);
        }
        return l_record;
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_addRetryRecord = "addRetryRecord";
    /**
     * Adds a record to the retry queue.
     * Returns <code>true</code> if it succeeds to add it to the retry queue, otherwise it returns 
     * <code>false</code> (it could for instance be the retry queue that is processed).
     * Records in the retry queue will be processed once again after that all records in the in data queue
     * has been processed.
     * 
     * @param p_record   the record that should be processed once again.
     * 
     * @return  <code>true</code> if it succeeds to add it to the retry queue, otherwise it returns 
     *          <code>false</code> (it could for instance be the retry queue that is processed).
     */
    protected boolean addRetryRecord(BatchRecordData p_record)
    {
        boolean l_success = true;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10130,
                this,
                "Entering " + C_METHOD_addRetryRecord);
        }

        if (i_retryQueue == null)
        {
            // The retry queue doesn't exist, let's create it.
            try
            {
                i_retryQueue = new SizedQueue("Retry Queue", 100, null);
            }
            catch (SizedQueueInvalidParameterException p_sqIPEx)
            {
                // Failed to create the retry queue.
                p_sqIPEx = null;
//                l_success = false;
            }
        }
        
        // Add the record to retry queue, but avoid to add it a second time.
        if (i_processQueue == i_inQueue)
        {
            try
            {
                if (i_retryQueue != null)
                {
                    p_record.setRetryRecord(true);
                    i_retryQueue.addLast(p_record);
//                    l_success = true;
                }
            }
            catch (SizedQueueClosedForWritingException p_sqCFWEx)
            {
                // Failed to add it to the retry queue.
                p_sqCFWEx = null;
//                l_success = false;
            }
        }
        else
        {
            // It is the retry queue that is processed, indicate no success and no more retry.
//            l_success = false;
            p_record.setRetryRecord(false);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10140,
                this,
                "Leaving " + C_METHOD_addRetryRecord);
        }
        
        return l_success;
    }


    /**
     * Processes the given <code>BatchDataRecord</code> and returns it.
     * If the process succeeded the <code>BatchDataRecord</code> is updated with information
     * needed by the writer component.
     * If the process fails but the record shall be re-processed at the end of this process,
     * the record will be put in the retry queue and this method returns <code>null</code>.
     * 
     * @param p_record  the <code>BatchDataRecord</code>.
     * @return the given <code>BatchDataRecord</code> updated with info for the writer component,
     *         or <code>null</code>.
     * @throws PpasServiceException  if an unrecoverable error occurs while installing a subscriber.
     */
    protected abstract BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException;


    //-------------------------------------------------------------------------
    // Private instance methods
    //-------------------------------------------------------------------------
    /**
     * Returns <code>true</code> if the processor thread with the specified id is paused,
     * otherwise it returns <code>false</code>.
     * 
     * @param p_threadId  the thread id of an existing processor thread.
     * 
     * @return <code>true</code> if the processor thread with the specified id is paused,
     *         otherwise it returns <code>false</code>.
     */
    private boolean isPaused(int p_threadId)
    {
        return i_threadControlArr[p_threadId].isPaused();
    }


    /**
     * Returns <code>true</code> if the processor thread with the specified id is running,
     * otherwise it returns <code>false</code>.
     * 
     * @param p_threadId  the thread id of an existing processor thread.
     * 
     * @return <code>true</code> if the processor thread with the specified id is running,
     *         otherwise it returns <code>false</code>.
     */
    private boolean isRunning(int p_threadId)
    {
        return i_threadControlArr[p_threadId].isRunning();
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_reportState = "reportState";
    /**
     * Reports the current state of the worker thread with the given thread id to the batch controller.
     * 
     * @param p_threadId  the thread id of the calling thread.
     * @param p_state     the current state to be reported.
     */
    private void reportState(int p_threadId, int p_state)
    {
        BatchMessage l_msg     = new BatchMessage();
        boolean      l_postMsg = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10150,
                this,
                "Entering " + C_METHOD_reportState);
        }

        synchronized (i_threadControlArr)
        {
            // Update the state for the current processor thread.
            i_threadControlArr[p_threadId].setState(p_state);
        
            l_msg.setSendingComponent(BatchMessage.C_COMPONENT_PROCESSOR);
            l_msg.setTargetComponent(BatchMessage.C_COMPONENT_MANAGER);
            l_msg.setRequest(BatchMessage.C_REQUEST_STATUS);
        
            // Post error message to batch controller as soon as ANY thread is reporting error.
            if (p_state == ThreadControl.C_THREAD_ERROR)
            {
                l_msg.setStatus(BatchMessage.C_STATUS_ERROR);
            }
            else
            {
                // Check if all threads are in the same state as the given state.
                if (isAllThreadsStateEqual(p_state))
                {
                    switch (p_state)
                    {
                        case ThreadControl.C_THREAD_RUNNING:
                            l_msg.setStatus(BatchMessage.C_STATUS_STARTED);
                            break;

                        case ThreadControl.C_THREAD_STOPPED:
                            l_msg.setStatus(BatchMessage.C_STATUS_STOPPED);
                            break;

                        case ThreadControl.C_THREAD_PAUSED:
                            l_msg.setStatus(BatchMessage.C_STATUS_PAUSED);
                            break;

                        case ThreadControl.C_THREAD_RESUMED:
                            l_msg.setStatus(BatchMessage.C_STATUS_RESUMED);
                            break;

                        case ThreadControl.C_THREAD_FINISHED:
                            l_msg.setStatus(BatchMessage.C_STATUS_FINISHED);
                            break;

                        default:
                            l_postMsg = false;
                            break;
                    }
                }
                else
                {
                    // No, all threads aren't in the same state, do not post any message to the controller.
                    l_postMsg = false;
                }
            }

            // Post message to the batch controller.
            if (l_postMsg)
            {
                if (i_batchController != null)
                {
                    i_batchController.postMessage(l_msg);
                }
            
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_APPLICATION_DATA,
                        C_CLASS_NAME,
                        10155,
                        this,
                        "Status reported: " + l_msg.getStatus());
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10160,
                this,
                "Leaving " + C_METHOD_reportState);
        }
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_isAllThreadsStateEqual = "isAllThreadsStateEqual";
    /**
     * Returns <code>true</code> if all processor thread's state is equal to the given state.
     * 
     * @param p_state  the thread state to compare the processor thread's state against.
     * @return <code>true</code> if all processor thread's state is equal to the given state,
     *                           otherwise it returns <code>false</code>.
     */
    private boolean isAllThreadsStateEqual(int p_state)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10170,
                this,
                "Entering " + C_METHOD_isAllThreadsStateEqual);
        }

        boolean l_retVal = true;
        for (int i = 0; i < i_threadControlArr.length  &&  l_retVal; i++)
        {
            if (i_threadControlArr[i].getState() != p_state)
            {
                l_retVal = false;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10180,
                this,
                "Leaving " + C_METHOD_isAllThreadsStateEqual);
        }

        return l_retVal;
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_storeRecord = "storeRecord";
    /**
     * Stores the given <code>BatchRecordData</code> in the output data queue.
     * 
     * @param p_record  the record to be stored in the output data queue.
     */
    protected void storeRecord(BatchRecordData p_record)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10190,
                this,
                "Entering " + C_METHOD_storeRecord);
        }

        try
        {
            i_outQueue.addLast(p_record);
        }
        catch (SizedQueueClosedForWritingException p_sqCFWExe)
        {
            // No specific handling for this exception, it should never happen.
            p_sqCFWExe = null;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10200,
                this,
                "Leaving " + C_METHOD_storeRecord);
        }
    }


    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_getYesNoParam = "getYesNoParam";

    /**
     * Gets a yes/no string from the parameter map and converts to boolean. The
     * parameter must be in the map.
     * @param p_key Key of parameter.
     * @return Boolean value of the key.
     */
    protected boolean getYesNoParam(
        String                        p_key)
    {
        String                         l_value = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME, 11100, this,
                "Entered " + C_METHOD_getYesNoParam);
        }

        l_value = (String)i_params.get(p_key);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VHIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME, 11190, this,
                "Leaving " + C_METHOD_getYesNoParam);
        }

        return l_value.equals(BatchConstants.C_VALUE_YES);
    } // end of getYesNoParam
    //------------------------------------------------------------------------
    // Private inner class
    //------------------------------------------------------------------------

    /**
     * Inner class <code>ProcessorThread</code>.
     * This inner class is the engine in the batch data processing.
     * It extends the <code>ThreadObject</code> class and uses the certain methods
     * defined in the outer class.
     */
    private class ProcessorThread extends ThreadObject
    {
        //-------------------------------------------------------------------------
        // Private variables
        //-------------------------------------------------------------------------
        /** The current thread id. */
        private int i_threadId = 0;


        //-------------------------------------------------------------------------
        // Constructors
        //-------------------------------------------------------------------------
        /** Constant holding the name of the current class. Value is {@value}. */
        private static final String C_INNER_CLASS_NAME = "ProcessorThread";
        /**
         * Constructs an instance of this <code>ProcessorThread</code> class with the specified
         * thread id.
         * This thread id will be used when communicating with the <code>BatchProcessor</code> object.
         * 
         * @param p_threadId  a unique thread id.
         */
        public ProcessorThread(int p_threadId)
        {
            super();

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_START,
                    C_INNER_CLASS_NAME + "_" + p_threadId,
                    20000,
                    this,
                    "Entering the Constructor");
            }

            i_threadId = p_threadId;

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_END,
                    C_INNER_CLASS_NAME + "_" + p_threadId,
                    20010,
                    this,
                    "Leaving the Constructor");
            }
        }


        //-------------------------------------------------------------------------
        // Public instance methods
        //-------------------------------------------------------------------------

        /** Constant holding the name of this method. Value is (@value). */
        private static final String C_METHOD_doRun = "doRun";
        /**
         * Implements the abstract <code>doRun()</code> method in the super class, <code>ThreadObject</code>,
         * of this <code>ProcessorThread</code> class.
         * @see com.slb.sema.ppas.util.lang.ThreadObject#doRun()
         */
        public void doRun()
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_INNER_CLASS_NAME + "_" + i_threadId,
                    20020,
                    this,
                    "Entering " + C_METHOD_doRun);
            }
            
            BatchRecordData l_record          = null;
            boolean         l_continueProcess = true;
            boolean         l_processFailed   = false;
            boolean         l_processStopped  = false;
            
            // Report 'up and running'.
            BatchProcessor.this.reportState(i_threadId, ThreadControl.C_THREAD_RUNNING);
            
            // Start the process loop.
            while (l_continueProcess)
            {
                // Check if it is ordered to stop.
                // If it is ordered to stop, it will not really stop until the queue is empty and closed
                // for writing, i.e. no more records will be added to the queue.
                if (!BatchProcessor.this.isRunning(i_threadId) &&
                    BatchProcessor.this.i_processQueue.isClosedForWriting() &&
                    BatchProcessor.this.i_processQueue.isEmpty())
                {
                    // Check if this is the last process thread to be stopped.
//                    BatchProcessor.this.i_stoppedThreadCounter++;
//                    if (BatchProcessor.this.i_stoppedThreadCounter == 
//                        BatchProcessor.this.i_noOfProcessThreads)

                    // Update the state for the current processor thread.
//                    i_threadControlArr[i_threadId].setState(ThreadControl.C_THREAD_STOPPED);
//                    if (BatchProcessor.this.isAllThreadsStateEqual(ThreadControl.C_THREAD_STOPPED))
//                    {
//                        // This is the last thread to be stopped, close outdata queue for writing in order
//                        // to inform a reader of this queue that no more records will be added.
//                        BatchProcessor.this.i_outQueue.closeForWriting();
//                    }
                    // It shall be stopped, report state and exit the loop.
                    BatchProcessor.this.reportState(i_threadId, ThreadControl.C_THREAD_STOPPED);
                    l_continueProcess = false;
                    l_processStopped = true;
                    continue;
                }

                // Check if it is ordered to pause.
                // If it is ordered to pause, it will not really pause until the queue is empty and closed
                // for writing, i.e. no more records will be added to the queue (at least not for the moment).
                if (BatchProcessor.this.isPaused(i_threadId) &&
                    BatchProcessor.this.i_processQueue.isClosedForWriting() &&
                    BatchProcessor.this.i_processQueue.isEmpty())
                {
                    // Check if this is the last process thread to be paused.
//                  BatchProcessor.this.i_pausedThreadCounter++;
//                  if (BatchProcessor.this.i_pausedThreadCounter == BatchProcessor.this.i_noOfProcessThreads)

                    // Update the state for the current processor thread.
                    i_threadControlArr[i_threadId].setState(ThreadControl.C_THREAD_PAUSED);
                    if (BatchProcessor.this.isAllThreadsStateEqual(ThreadControl.C_THREAD_PAUSED))
                    {
                        // This is the last thread to be paused, close outdata queue for writing in order
                        // to inform a reader of this queue that no more records will be added for the moment.
                        BatchProcessor.this.i_outQueue.closeForWriting();
                        
                        // Reset the thread counter.
//                        BatchProcessor.this.i_pausedThreadCounter = 0;
                    }
                    synchronized (BatchProcessor.this.i_threadControlArr[i_threadId])
                    {
                        try
                        {
                            // Report state and pause.
                            BatchProcessor.this.reportState(i_threadId, ThreadControl.C_THREAD_PAUSED);
                            BatchProcessor.this.i_threadControlArr[i_threadId].wait();
                        }
                        catch (InterruptedException p_iEx)
                        {
                            // No specific handling of the 'InterruptedException'.
                            p_iEx = null;
                        }
                    }
                    
                    if (BatchProcessor.this.isRunning(i_threadId))
                    {
                        // The process is still running, report resumed to the controller.
                        BatchProcessor.this.reportState(i_threadId, ThreadControl.C_THREAD_RESUMED);
                    }
                    else
                    {
                        // The process shall be stopped, restart the internal process loop.
                        continue;
                    }

                    // Re-open the output queue.
                    if (BatchProcessor.this.i_outQueue.isClosedForWriting())
                    {
                        BatchProcessor.this.i_outQueue.reopenForWriting();
                    }
                }

                // Get a record from the process queue.
                try
                {
                    l_record = BatchProcessor.this.getRecord();
                    // If the returned record was the last record and the queue is the retry queue, then the
                    // queue has to be closed for writing in order to notify any other processor thread
                    // waiting in the 'removeFirst' method of the queue.
                    if (i_processQueue == i_retryQueue  &&  i_processQueue.isEmpty())
                    {
                        i_processQueue.closeForWriting();
                    }
                }
                catch (SizedQueueClosedForWritingException p_sqCFWEx)
                {
                    // If the current queue is the indata queue and the process is neither stopped nor paused
                    // then the retry queue shall be processed (if it exists).
                    if (i_processQueue == i_inQueue               &&
                        BatchProcessor.this.isRunning(i_threadId) &&
                        !BatchProcessor.this.isPaused(i_threadId) &&
                        i_retryQueue != null)
                    {
                        try
                        {
                            Thread.sleep(BatchProcessor.this.i_sleepTimeBeforeRetry);
                        }
                        catch (InterruptedException e)
                        {
                            e = null;
                        }
                        
                        i_processQueue = i_retryQueue;
                        
                        continue;
                    }
                    
                    // No record to process.
                    l_record = null;
                }

                if (l_record != null)
                {
                    // Process the record.
                    try
                    {
                        l_record = BatchProcessor.this.processRecord(l_record);
                    }
                    catch (PpasServiceException p_serviceEx)
                    {
                        // A fatal error has occurred, report to controller and stop processing.
                        BatchProcessor.this.reportState(i_threadId, ThreadControl.C_THREAD_ERROR);
                        l_continueProcess = false;
                        l_processFailed = true;
                        continue;
                    }

                    if (l_record != null  &&  !l_record.isRetryRecord())
                    {
                        // The record has been properly processed, put it in the output data queue.
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10350,
                                this,
                                "DumpRecord: " + l_record.dumpRecord());
                        }

                        BatchProcessor.this.storeRecord(l_record);
                    }
                }
                else
                {
                    // No more records to process.
                    l_continueProcess = false;
                }
            }

            // Don't report status FINISHED if the process either has failed or has been stopped.
            if (!l_processFailed && !l_processStopped)
            {
                BatchProcessor.this.reportState(i_threadId, ThreadControl.C_THREAD_FINISHED);
            }

            BatchProcessor.this.flush();

            // Check if this is the last process thread to be finished.
            // This has to be done by a counter since the processor threads may have different states,
            // for instance one has status error and the others are stopped.
            BatchProcessor.this.i_finishedThreadCounter++;
            if (BatchProcessor.this.i_finishedThreadCounter == BatchProcessor.this.i_noOfProcessThreads)
            {
                BatchProcessor.this.i_outQueue.closeForWriting();
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END,
                    C_INNER_CLASS_NAME + "_" + i_threadId,
                    20030,
                    this,
                    "Leaving " + C_METHOD_doRun);
            }
        } // End of method 'doRun', the current thread will die.


        //-------------------------------------------------------------------------
        // Protected instance methods
        //-------------------------------------------------------------------------
        /**
         * @see com.slb.sema.ppas.util.lang.ThreadObject#getThreadName()
         */
        protected String getThreadName()
        {
//            return "ProcessorThread." + i_threadId;
            return "PThread." + i_threadId;
        }
    } // End of class ProcessorThread

    //------------------------------------------------------------------------
    // End of private inner classes
    //------------------------------------------------------------------------

} // End of public class BatchProcessor
////////////////////////////////////////////////////////////////////////////////
//                       End of file
////////////////////////////////////////////////////////////////////////////////}
