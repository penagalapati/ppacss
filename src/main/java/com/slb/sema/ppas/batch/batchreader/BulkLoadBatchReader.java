////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BulkLoadBatchReader
//      DATE            :       27-July-2004
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AFBulkLoadBatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isil.batchisilservices.AFBulkLoadDbBatchService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
 * The purpose of this <code>BulkLoadBatchReader</code> class is to provide the
 * <code>BulkLoadBatchProcessor</code> component with <code>BulkLoadBatchDataData</code> objects.
 * Since this is a database driven batch process the creation and initial population of the
 * <code>BulkLoadBatchRecordData</code> objects are handled by the dedicated "Batch ISIL Service".
 */
public class BulkLoadBatchReader extends BatchReader
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String   C_CLASS_NAME                    = "BulkLoadBatchReader";
    
//    // Constants needed for the DB Service.
//    /** Number of values in the DB Service array. Value is {@value}. */
//    private static final int      C_NUMBER_OF_PARAMETERS          = 2;
//
//    /** Index for Start Range value in the DB Service array.  Value is {@value}. */
//    private static final int      C_START_RANGE                   = 0;
//
//    /** Index for Stop Range value in the DB Service array.  Value is {@value}. */
//    private static final int      C_END_RANGE                     = 1;

//    /** Message for the log. */
//    private static final String C_PPAS_SERVICE_EXCEPTION_GET_RECORD = 
//        "PpasServiceException when getRecord() ";
//
//    /** Message for the log. */
//    private static final String C_PPAS_SERVICE_EXCEPTION_CREATE_DB_SERVICE = 
//        "PpasServiceException when createDbService() ";

    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** A reference to the dedicated Batch ISIL Service. */
    private AFBulkLoadDbBatchService i_afBulkLoadDbBatchService = null;
//    private SubscrDbBatchService i_subscrDbBatchService = null;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * Constructor for the BulkLoadBatchReader.     
     * @param p_ppasContext PpasContext.
     * @param p_logger      Logger.
     * @param p_controller  Callback reference to BatchController.
     * @param p_inQueue     In queue.
     * @param p_parameters  Start parameters.
     * @param p_properties  Properties defined for BulkLoad.
     */
    public BulkLoadBatchReader(
        PpasContext      p_ppasContext,
        Logger           p_logger,
        BatchController  p_controller,
        SizedQueue       p_inQueue,
        Map              p_parameters,
        PpasProperties   p_properties )
    {
        super(p_ppasContext, p_logger,  p_controller, p_inQueue, p_parameters, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
        init();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTED );
        }

    } // End of constructor

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
   /** 
    * This method call the Batch ISIL Service that returns a BulkLoadBatchDataRecord.
    * No manipulation is required since the underlying database service has knowledge
    * of the record created. The customer id is extracted and used to call
    * addCustomerInprogress().
    * 
    * @return The BulkLoadBatchDataRecord.
    */
    protected BatchRecordData getRecord()
    {
        AFBulkLoadBatchRecordData l_afBulkLoadBatchRecordData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // get one record by calling fetch
        try
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "innan i_afBulkLoadDbBatchService.fetch() - getRecord. " + C_METHOD_getRecord );
            }

            l_afBulkLoadBatchRecordData = (AFBulkLoadBatchRecordData)i_afBulkLoadDbBatchService.fetch();
            
            // Note that fetch can return null. This indicates 'no more records to fetch'.
            if ( l_afBulkLoadBatchRecordData != null )
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10530,
                        this,
                        "after i_afBulkLoadDbBatchService.fetch() - getRecord. " + C_METHOD_getRecord +
                        " record: " + l_afBulkLoadBatchRecordData.dumpRecord());
                }
            }
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10530,
                        this,
                        "after i_afBulkLoadDbBatchService.fetch() - getRecord. " + C_METHOD_getRecord + 
                        " record IS NULL - no more to read.");
                }                                   
            }
        }

        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10531,
                    this,
                    "PpasServiceException - getRecord. " + C_METHOD_getRecord + " " + e.getMsgKey());
                }
                e.printStackTrace();
        }
        
        /*
        transferData(
            l_afBulkLoadBatchRecordData,
            l_bulkLoadBatchDataRecord);
        */

        // Return the record to doRun() in super class
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return l_afBulkLoadBatchRecordData;
        
    } // End of getRecord()

    /**
     * Initialises the Bulk Load Reader.
     */
    protected void init()
    {
        createDbService();
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createDbService = "createDbService";    
    /**
     * Creates the DB Service.
     */
    protected void createDbService()
    {
        PpasRequest l_request      = null;
        PpasSession l_session      = null;
        String l_queryParameters[] = null;
        int l_fetchSize = 0;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createDbService);
        }

//        l_queryParameters = new String[C_NUMBER_OF_PARAMETERS];
//        l_queryParameters[C_START_RANGE] = (String)i_parameters.get(BatchConstants.C_KEY_START_RANGE);
//        l_queryParameters[C_END_RANGE] = (String)i_parameters.get(BatchConstants.C_KEY_END_RANGE);

        l_fetchSize = getFetchSize();

        // Create and open the Batch ISIL Service Component
        l_session = new PpasSession();
        l_session.setContext(i_context);
        
        l_request = new PpasRequest(l_session);
        i_afBulkLoadDbBatchService   = new AFBulkLoadDbBatchService(l_request, i_logger, i_context );

        // Call open on the API component
        try
        {
            i_afBulkLoadDbBatchService.open(l_queryParameters, l_fetchSize );
        }
        catch (PpasServiceException p_ppasServEx)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10531,
                    this,
                    C_METHOD_createDbService + 
                    " -- A PpasServiceException was caught when opening the DB Service,  msg key = " +
                    p_ppasServEx.getMsgKey());
            }
        }

        catch (SecurityException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "SecurityException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "SecurityException - getRecord. " + C_METHOD_createDbService );
            }

            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( "IllegalArgumentException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "IllegalArgumentException - getRecord. " + C_METHOD_createDbService );
            }

            e.printStackTrace();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createDbService);
        }

        return;
    } // End of createDbService()




    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_tidyUp = "tidyUp";    
    /**
     * Inplementation of the abstract super class method.
     */
    protected void tidyUp()
    {
        // Clean up connectivity
        try
        {
            finalise();
        }
        catch (PpasServiceException e)
        {
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "PpasServiceException - tidyUp. " + C_METHOD_tidyUp );
            }

        }
                
        return;
    }  // End of tidyUp()





    /**
     * Cleanup.
     * @throws PpasServiceException Error from closing isapi.
     */
    private void finalise() throws PpasServiceException 
    {
        if ( i_afBulkLoadDbBatchService != null )
        {
            i_afBulkLoadDbBatchService.close();
            i_afBulkLoadDbBatchService = null;
            
        }
        
        return;
    }  // End of finalise()


//    private void getProperties()
//    {
//        i_numberOfThreads       = 
//            i_properties.getIntProperty( BatchConstants.C_NUMBER_OF_READER_THREADS, C_MAXIMUM_OF_THREADS);
//
//        return;
//        
//    }

} // End of BulkLoadBatchReader
