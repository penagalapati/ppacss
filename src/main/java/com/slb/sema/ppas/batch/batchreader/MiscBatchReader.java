////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MiscBatchReader
//      DATE            :       21-June-2004
//      AUTHOR          :       Olivier Duparc
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class implements methods for parsing miscellaneous 
//                              data from file. It extends the abstract class BatchFileReader 
//                              that contributes with several auxilary metods.
//                              The file is processed on a line-per-line basis and 
//                              each line extracted is evaluated and put into a 
//                              MiscBatchDataRecord.
//                              Each read line has the format: 
//                              <Opcode><MSISDN><99xxx>,<99xxx>,...
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.MiscBatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class implements methods for parsing miscellaneous 
 * data from file.
 */
public class MiscBatchReader extends BatchLineFileReader
{
    //-----------------------------------------------------
    //  Class level constant
    //------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "MiscBatchReader";

    // -------------------------
    // Error codes
    // -------------------------

    /** Error code "Invalid record format". Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT    = "01";

    /** Error code "Invalid Action". Value is {@value}. */
    private static final String C_ERROR_INVALID_ACTION_TYPE      = "02";

    /** Error code "Non-numeric MSISDN". Value is {@value}. */
    private static final String C_ERROR_NON_NUMERIC_MSISDN       = "03";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_NON_NUMERIC_FIELD_NUMBER = "05";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_FIELD_NUMBER     = "06";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_FIELD_VALUE_SIZE = "07";


    //-------------------------
    // Action type values.   --
    //-------------------------
    /** Action type for Insert. Value is {@value}. */
    private static final String C_ACTION_TYPE_INS = "I"; 

    /** Action type for Update. Value is {@value}. */
    private static final String C_ACTION_TYPE_UPD = "U"; 

    /** Action type for Delete. Value is {@value}. */
    private static final String C_ACTION_TYPE_DEL = "D"; 

    /** The action type regular expression. */
    private static final String C_ACTION_TYPE_PATTERN =
        "[" + C_ACTION_TYPE_INS + C_ACTION_TYPE_UPD + C_ACTION_TYPE_DEL + "]";


    // -------------------------
    // Positions and lengths. --
    // -------------------------

    /** Action type position. Value is {@value}. */
    private static final int    C_ACTION_TYPE_POS         = 0;
    
    /** Action type length. Value is {@value}. */
    private static final int    C_ACTION_TYPE_LEN         = 1;
    
    /** Msisdn position. Value is {@value}. */
    private static final int    C_MSISDN_POS              = 1;
    
    /** Msisdn length. Value is {@value}. */
    private static final int    C_MSISDN_LEN              = 15;
    
    /** Field number length. Value is {@value}. */
    private static final int    C_FIELD_NUMBER_LEN        = 2;
    
    /** Field number min value. Value is {@value}. */
    private static final int    C_FIELD_NUMBER_MIN_VALUE  = 1;
    
    /** Field number max value. Value is {@value}. */
    private static final int    C_FIELD_NUMBER_MAX_VALUE  = 40;
    
    /** Field Value max length. Value is {@value}. */
    private static final int    C_FIELD_VALUE_MAXLEN      = 30;
    
    /** Number of delimeter in the file. Value is {@value}. */
    private static final int    C_NO_OF_DELIMETER         = 39;
    
    /** Field number/value separator. Value is {@value}. */
    private static final String C_FIELD_SEPARATOR         = ",";
    
    /** The minimum length of an input record, that is, action type delete + MSISDN. */
    private static final int    C_MIN_RECORD_LEN          = C_ACTION_TYPE_LEN + C_MSISDN_LEN;
    
    /** The maximum length of an input record, that is, action type + MSISDN + 40 * (field numbers and
     * values). */
    private static final int    C_MAX_RECORD_LEN          =
        C_MIN_RECORD_LEN + C_NO_OF_DELIMETER + (40 * (C_FIELD_NUMBER_LEN + C_FIELD_VALUE_MAXLEN));

    
    //-------------------------------------------------------------------------
    // Class variables
    //-------------------------------------------------------------------------
    
    /** The trace print out base statement number. */
    private static int c_trace_stmt_number = 12000;
   
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
   
    /** The action type. */
    private String   i_actionType = null;

    /** The MSISDN. */
    private Msisdn   i_msisdn     = null;

    /** The fields (field number + value in each String). */
    private String[] i_fieldArr   = null;


    /**
     * Constructor for StatusChangeBatchReader.
     * 
     * @param p_ppasContext Reference to PpasContext.
     * @param p_logger      reference to the logger.
     * @param p_controller  Reference to the BatchController that creates this object.
     * @param p_queue       Reference to the Queue, where all indata records will be added.
     * @param p_parameters  Reference to a Map holding information e.g. filename.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public MiscBatchReader(PpasContext p_ppasContext,
                           Logger p_logger,
                           BatchController p_controller,
                           SizedQueue p_queue,
                           Map p_parameters,
                           PpasProperties p_properties)
    {
        super( 
            p_ppasContext, 
            p_logger, 
            p_controller, 
            p_queue, 
            p_parameters, 
            p_properties );
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10100,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    } // end of public Constructor MiscBatchReader(.....)

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";
    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchDataRecord record.
     * 
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {
        MiscBatchRecordData l_batchDataRecord = null; // The BatchDataRecord to be stored into the Queue.
        String              l_trc             = null;
        int                 l_fieldNumber     = 0;
        String              l_fieldValue      = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10200,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // Get next line from input file
        if ( this.readNextLine() != null )
        {
            l_batchDataRecord = new MiscBatchRecordData();

            // Store input filename
            l_batchDataRecord.setInputFilename(this.i_inputFileName);
            
            // Store line-number form the file
            l_batchDataRecord.setRowNumber(this.i_lineNumber);
            
            // Store original data record
            l_batchDataRecord.setInputLine(this.i_inputLine);
            
            // Extract and validate data from the read input line.
            if (extractAndValidateData(this.i_inputLine, l_batchDataRecord))
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10300,
                                    this,
                                    "extractAndValidateData(...) was successful!" );
                }
                
                // Update the batch data record with the extracted data.
                l_batchDataRecord.setOperationCode(i_actionType);
                l_batchDataRecord.setMsisdn(i_msisdn);
                if (i_fieldArr != null)
                {
                    for (int i = 0; i < i_fieldArr.length; i++)
                    {
                        l_fieldNumber = Integer.parseInt(i_fieldArr[i].substring(0, C_FIELD_NUMBER_LEN));
                        l_fieldValue  = i_fieldArr[i].substring(C_FIELD_NUMBER_LEN);
                        l_batchDataRecord.setField(l_fieldNumber, l_fieldValue);
                    }
                }
            }
            else
            {
                // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10400,
                        this,
                        "extractAndValidateData(...) failed!" );
                }

                // Flag for corrupt line and set error code if not already set.
                l_batchDataRecord.setCorruptLine( true );
                if ( l_batchDataRecord.getErrorLine() == null )
                {
                    // Other error than specific field errors.
                    l_batchDataRecord.setErrorLine( C_ERROR_INVALID_RECORD_FORMAT + 
                                                    BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                                    l_batchDataRecord.getInputLine() );
                }
                                
            }
            
            l_batchDataRecord.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
            l_batchDataRecord.setNumberOfErrorRecords(super.i_notProcessed);
            if ( super.faultyRecord(i_lineNumber))
            {
                l_batchDataRecord.setFailureStatus(true);
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10340,
                    this,
                    "After setNumberOfSuccessfully...."+ super.i_successfullyProcessed);
            }
        } // end if
        
        if ( l_batchDataRecord != null )
        {
            l_trc = "DumpRecord: " + l_batchDataRecord.dumpRecord();
        }
        else
        {
            l_trc = "l_batchDataRecord is NULL !!!";
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10600,
                            this,
                            l_trc);

            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10700,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return l_batchDataRecord;
        
    } // end of method getRecord()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";
    /**
     * Check if filename is of the valid form: UPDATE_MISC_yyyymmdd_sssss.DAT.
     * @param p_fileName Indata file name.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid( String p_fileName )
    {
        boolean  l_validFileName = false;
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10800,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid + " ==> p_fileName=[" + p_fileName +
                                        "]");
        }

        if (p_fileName != null && 
            (p_fileName.matches(BatchConstants.C_PATTERN_UPDATE_MISC_FILENAME_DAT) ||
             p_fileName.matches(BatchConstants.C_PATTERN_UPDATE_MISC_FILENAME_SCH) ||
             p_fileName.matches(BatchConstants.C_PATTERN_UPDATE_MISC_FILENAME_IPG)))
        {
            l_validFileName = true;

            // Flag that processing is in progress. Rename the file to *.IPG.
            // If the batch is running in recovery mode, it already has the extension .IPG
            if (!i_recoveryMode)

            if (p_fileName.matches(BatchConstants.C_PATTERN_UPDATE_MISC_FILENAME_SCH))
            {
                renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
            }
            else
            {
                renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
            }
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10900,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
        
    } // End of isFileNameValid(.)


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractAndValidateData = "extractAndValidateData";
    /**
     * Extracts and validates the data specified in the input line read from the input file.
     * It returns <code>true</code> if no error is detected, otherwise it returns <code>false</code>.
     * If no error is detected the instance variables 'i_actionType', 'i_msisdn' and 'i_fieldArr' are updated.
     * If any error is detected the instance variables 'i_actionType', 'i_msisdn' and 'i_fieldArr' will all be
     * set to <code>null</code>, and the error line in the <code>MiscBatchRecordData</code> will be set to a
     * proper value.
     * 
     * @param p_inputLine        the input line read from the input file.
     * @param p_batchDataRecord  the current <code>MiscBatchRecordData</code>.
     * 
     * @return <code>true</code> if no error is detected, otherwise it returns <code>false</code>.
     */
    private boolean extractAndValidateData(String p_inputLine, MiscBatchRecordData p_batchDataRecord)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11000,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_extractAndValidateData + 
                            "'" + p_inputLine + "'");
        }

        boolean  l_validData      = true; //Assume valid data.

        // Extract and validate input data.
        if (validateRecord(p_inputLine, p_batchDataRecord) &&
            validateActionType(p_inputLine, p_batchDataRecord) &&
            validateMsisdn(p_inputLine, p_batchDataRecord) &&
            validateFields(p_inputLine, p_batchDataRecord))
        {
            l_validData = true;
        }
        else
        {
            l_validData = false;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11500,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_extractAndValidateData );
        }
        return l_validData;
    }

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_validateRecord = "validateRecord";
    /**
     * Validates the record length.
     * Returns <code>false</code> if the record is either too long or too short, otherwise it returns
     * <code>true</code>.
     * 
     * @param p_inputLine        the input line read from the input file.
     * @param p_batchDataRecord  the current <code>MiscBatchRecordData</code>.
     * 
     * @return  <code>false</code> if the record is either too long or too short, otherwise it returns
     *          <code>true</code>.
     */
    private boolean validateRecord(String p_inputLine, MiscBatchRecordData p_batchDataRecord)
    {
        boolean l_validRecord = true; // Assumes that the record is valid.
        int     l_recordLen   = p_inputLine.length();

        // Validate the record length.
        if (l_recordLen < C_MIN_RECORD_LEN  ||  l_recordLen > C_MAX_RECORD_LEN)
        {
            // Invalid record length!
            trace(this, C_METHOD_validateRecord,
                  "Record is too long or too short: '" + p_inputLine + "',  size = " + l_recordLen + 
                  ". Min size = " + C_MIN_RECORD_LEN + ",  max size = " + C_MAX_RECORD_LEN + ".");

            p_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT +
                                           BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                           p_batchDataRecord.getInputLine());
            l_validRecord = false;
        }

        // Check for missing data in the end of the line.
        if (p_inputLine.endsWith(","))
        {
            // ERROR: Missing data in the end of the line.
            trace(this, C_METHOD_validateRecord,
                  "Missing data in the end of the line (i.e. ending with a comma (',')).");

            p_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT +
                                           BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                           p_batchDataRecord.getInputLine());
            l_validRecord = false;
        }

        return l_validRecord;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_validateActionType = "validateActionType";
    /**
     * Validates the action type.
     * Returns <code>true</code> if the action type is an input, update or delete action, otherwise it returns
     * <code>false</code>.
     * If it returns <code>true</code> the instance variable 'i_actionType' is set to a proper value.
     * 
     * @param p_inputLine        the input line read from the input file.
     * @param p_batchDataRecord  the current <code>MiscBatchRecordData</code>.
     * 
     * @return  <code>true</code> if the action type is an input, update or delete action, otherwise it
     *          returns <code>false</code>.
     */
    private boolean validateActionType(String p_inputLine, MiscBatchRecordData p_batchDataRecord)
    {
        boolean l_validActionType = true; // Assumes that the action type is valid.

        // Validate the action type.
        i_actionType = p_inputLine.substring(C_ACTION_TYPE_POS, C_ACTION_TYPE_LEN);
        if (!i_actionType.matches(C_ACTION_TYPE_PATTERN))
        {
            // Invalid action type, i.e. neither 'insert', 'update' nor 'delete'.
            trace(this, C_METHOD_validateActionType, "Invalid action type specified: '" + i_actionType + "'");
            i_actionType = null;
            p_batchDataRecord.setErrorLine(C_ERROR_INVALID_ACTION_TYPE +
                                           BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                           p_batchDataRecord.getInputLine());
            l_validActionType = false;
        }

        return l_validActionType;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_validateMsisdn = "validateMsisdn";
    /**
     * Validates the MSISDN.
     * Returns <code>false</code> if the MSISDN is non-numerical or has an invalid format according to the 
     * <code>parse(String p_msisdnString)</code> method in the <code>MsisdnFormat</code> class, otherwise it
     * returns <code>true</code>.
     * If it returns <code>true</code> the instance variable 'i_msisdn' is set to a proper value.
     * 
     * @param p_inputLine        the input line read from the input file.
     * @param p_batchDataRecord  the current <code>MiscBatchRecordData</code>.
     * 
     * @return  <code>false</code> if the MSISDN is non-numerical or has an invalid format according to the 
     *          <code>parse(String p_msisdnString)</code> method in the <code>MsisdnFormat</code> class,
     *          otherwise it returns <code>true</code>.
     */
    private boolean validateMsisdn(String p_inputLine, MiscBatchRecordData p_batchDataRecord)
    {
        boolean      l_validMsisdn  = true; // Assumes that the MSISDN is valid.
        String       l_msisdnStr    = null;
        MsisdnFormat l_msisdnFormat = null;


        // Validate the MSISDN.
        l_msisdnStr = p_inputLine.substring(C_MSISDN_POS, (C_ACTION_TYPE_LEN + C_MSISDN_LEN));
        if (!l_msisdnStr.trim().matches(BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC))
        {
            // A non-numerical MSISDN specified.
            trace(this, C_METHOD_validateMsisdn, "A non-numerical MSISDN specified: '" + l_msisdnStr + "'");
            p_batchDataRecord.setErrorLine(C_ERROR_NON_NUMERIC_MSISDN +
                                           BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                           p_batchDataRecord.getInputLine());
            l_validMsisdn = false;
        }
        else
        {
            l_msisdnFormat = super.i_context.getMsisdnFormatInput();
            try
            {
                trace(this, "validateMsisdn", "msisdn string: '" + l_msisdnStr + "'");
                i_msisdn = l_msisdnFormat.parse(l_msisdnStr.trim());
                trace(this, "validateMsisdn", "msisdn:        '" + i_msisdn + "'");
            }
            catch (ParseException p_parseEx)
            {
                // Failed to create a MSISDN, invalid format.
                trace(this, C_METHOD_validateMsisdn,
                      "Failed to create a MSISDN object due to invalid format: '" + l_msisdnStr + "'");
                i_msisdn = null;
                p_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT +
                                               BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                               p_batchDataRecord.getInputLine());
                l_validMsisdn = false;
            }
        }

        return l_validMsisdn;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_validateFields = "validateFields";
    /**
     * Validates the fields specified in the input line.
     * Assumes that the action type already has been validated with a successful result, i.e. the instance
     * variable 'i_actionType' is set to a proper value.
     * Returns <code>false</code> if:
     * <br> * any field is specified when the action type is 'delete'.
     * <br> * no field is specifed when the action type is 'input' or 'update'.
     * <br> * the number of fields is greater than 'C_FIELD_NUMBER_MAX_VALUE'.
     * <br> * the field number is less than 2 chars.
     * <br> * a non-numerical field number is given.
     * <br> * the field number is less than 'C_FIELD_NUMBER_MIN_VALUE' or greater than 
     *        'C_FIELD_NUMBER_MAX_VALUE'.
     * <br> * a field number is specified but no field value.
     * <br> * the length of the field value is greater than 'C_FIELD_VALUE_MAXLEN'.
     * <br>Otherwise it returns <code>true</code>.
     * 
     * If it returns <code>true</code> the instance variable 'i_fieldArr' is set to a proper value.
     * 
     * @param p_inputLine        the input line read from the input file.
     * @param p_batchDataRecord  the current <code>MiscBatchRecordData</code>.
     * 
     * @return  <code>false</code> if:
     *          <br> * any field is specified when the action type is 'delete'.
     *          <br> * no field is specifed when the action type is 'input' or 'update'.
     *          <br> * the number of fields is greater than 'C_FIELD_NUMBER_MAX_VALUE'.
     *          <br> * the field number is less than 2 chars.
     *          <br> * a non-numerical field number is given.
     *          <br> * the field number is less than 'C_FIELD_NUMBER_MIN_VALUE' or greater than 
     *                 'C_FIELD_NUMBER_MAX_VALUE'.
     *          <br> * a field number is specified but no field value.
     *          <br> * the length of the field value is greater than 'C_FIELD_VALUE_MAXLEN'.
     *          <br>Otherwise it returns <code>true</code>.
     */
    private boolean validateFields(String p_inputLine, MiscBatchRecordData p_batchDataRecord)
    {
        boolean  l_validFields          = true; // Assumes that the fields are valid.
        String   l_allFields            = null;
        String[] l_fieldsNumAndValueArr = null;
        
        l_allFields = p_inputLine.substring((C_ACTION_TYPE_LEN + C_MSISDN_LEN), p_inputLine.length());
        if (l_allFields == null  ||  l_allFields.length() == 0)
        {
            // No fields specified in input line.
            if (!i_actionType.equals(C_ACTION_TYPE_DEL))
            {
                // No fields only allowed for action type 'delete', invalid record.
                trace(this, C_METHOD_validateFields,
                      "An input line without fields is only allowed for the 'delete' action type, " +
                      "input line: '" + p_inputLine + "'");
                p_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT +
                                               BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                               p_batchDataRecord.getInputLine());
                l_validFields = false;
            }
        }
        else
        {
            // At least one field is specified.
            if (i_actionType.equals(C_ACTION_TYPE_DEL))
            {
                // Fields are not allowed for action type 'delete', invalid record.
                trace(this, C_METHOD_validateFields,
                      "An input line with fields is not allowed for the 'delete' action type, input line: '" +
                      p_inputLine + "'");
                p_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT +
                                               BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                               p_batchDataRecord.getInputLine());
                l_validFields = false;
            }
        }

        if (l_validFields  &&  !i_actionType.equals(C_ACTION_TYPE_DEL))
        {
            // Create an array of field number + field value Strings.
            l_fieldsNumAndValueArr = l_allFields.split(C_FIELD_SEPARATOR);

            if (l_fieldsNumAndValueArr.length > C_FIELD_NUMBER_MAX_VALUE)
            {
                // Too many fields specified, invalid record format.
                trace(this, C_METHOD_validateFields,
                      "Too many fields are specified, " + l_fieldsNumAndValueArr.length + ". " +
                      "Max number of fields = " + C_FIELD_NUMBER_MAX_VALUE +
                      ". Input line: '" + p_inputLine + "'");
                p_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT +
                                               BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                               p_batchDataRecord.getInputLine());
                l_fieldsNumAndValueArr = null;
                l_validFields = false;
            }
            else
            {
//                i_fieldArr = new String[ l_fieldsNumAndValueArr.length ];
                for (int i = 0; i < l_fieldsNumAndValueArr.length  &&  l_validFields; i++)
                {
                    l_validFields = validateField(l_fieldsNumAndValueArr[i], p_batchDataRecord);
                    if (!l_validFields)
                    {
                        l_fieldsNumAndValueArr = null;
                        break;
                    }
                }
            }
        }

        i_fieldArr = l_fieldsNumAndValueArr;

        return  l_validFields;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_validateField = "validateField";
    /**
     * Validates a single field specified in the input line.
     * Returns <code>false</code> if:
     * <br> * the field number is less than 2 chars.
     * <br> * a non-numerical field number is given.
     * <br> * the field number is less than 'C_FIELD_NUMBER_MIN_VALUE' or greater than 
     *        'C_FIELD_NUMBER_MAX_VALUE'.
     * <br> * a field number is specified but no field value.
     * <br> * the length of the field value is greater than 'C_FIELD_VALUE_MAXLEN'.
     * <br>Otherwise it returns <code>true</code>.
     * 
     * @param p_field            a field number and value record read from the input file.
     * @param p_batchDataRecord  the current <code>MiscBatchRecordData</code>.
     * 
     * @return  <code>false</code> if:
     *          <br> * the field number is less than 2 chars.
     *          <br> * a non-numerical field number is given.
     *          <br> * the field number is less than 'C_FIELD_NUMBER_MIN_VALUE' or greater than 
     *                 'C_FIELD_NUMBER_MAX_VALUE'.
     *          <br> * a field number is specified but no field value.
     *          <br> * the length of the field value is greater than 'C_FIELD_VALUE_MAXLEN'.
     *          <br>Otherwise it returns <code>true</code>.
     */
    private boolean validateField(String p_field, MiscBatchRecordData p_batchDataRecord)
    {
        boolean l_validField     = true; // Assumes that the field is valid.
        String  l_fieldNumberStr = null;
        int     l_fieldNumber    = 0;
        String  l_fieldValue     = null;
        int     l_fieldLen       = 0;

        l_fieldLen = p_field.length();
        if (l_fieldLen < C_FIELD_NUMBER_LEN)
        {
            // Invalid field number.
            trace(this, C_METHOD_validateField,
                "Invalid field number: '" + p_field + "'. It must be " + C_FIELD_NUMBER_LEN + " characters.");
            p_batchDataRecord.setErrorLine(C_ERROR_INVALID_FIELD_NUMBER +
                                           BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                           p_batchDataRecord.getInputLine());
            l_validField = false;
        }
        else
        {
            l_fieldNumberStr = p_field.substring(0, C_FIELD_NUMBER_LEN);
            if (!l_fieldNumberStr.matches(BatchConstants.C_PATTERN_NUMERIC))
            {
                // A non-numeric field number is specified.
                trace(this, C_METHOD_validateField,
                      "A non-numeric field number is specified: '" + l_fieldNumberStr + "'.");
                p_batchDataRecord.setErrorLine(C_ERROR_NON_NUMERIC_FIELD_NUMBER +
                                               BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                               p_batchDataRecord.getInputLine());
                l_validField = false;
            }
            else
            {
                l_fieldNumber = Integer.parseInt(l_fieldNumberStr);
                if (l_fieldNumber < C_FIELD_NUMBER_MIN_VALUE  ||  l_fieldNumber > C_FIELD_NUMBER_MAX_VALUE)
                {
                    // Field number is out of range, invalid field number.
                    trace(this, C_METHOD_validateField,
                          "The field number is out of range: " + l_fieldNumber +
                          ". It must be in the range " + C_FIELD_NUMBER_MIN_VALUE +
                          " - " + C_FIELD_NUMBER_MAX_VALUE + ".");
                    p_batchDataRecord.setErrorLine(C_ERROR_INVALID_FIELD_NUMBER +
                                                   BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                                   p_batchDataRecord.getInputLine());
                    l_validField = false;
                }
            }
        }

        if (l_validField)
        {
            // The field number is valid!
            l_fieldValue = p_field.substring(C_FIELD_NUMBER_LEN, l_fieldLen);
            if (l_fieldValue.length() == 0  ||  l_fieldValue.length() > C_FIELD_VALUE_MAXLEN)
            {
                // Too short or too long field value, invalid field value size.
                trace(this, C_METHOD_validateField,
                      "Too short or too long field value length: " + l_fieldValue.length() +
                      ". The length must be between 1 and " + C_FIELD_VALUE_MAXLEN + " characters. " +
                      "Field value: '" + l_fieldValue + "'.");
                p_batchDataRecord.setErrorLine(C_ERROR_INVALID_FIELD_VALUE_SIZE +
                                               BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                               p_batchDataRecord.getInputLine());
                l_validField = false;
            }
        }

        return l_validField;
    }


    /**
     * Prints trace print outs.
     * 
     * @param p_method         the name of th calling method.
     * @param p_callingObject  the calling <code>Object</code>.
     * @param p_message        the trace message to be printed.
     */
    private static void trace(Object p_callingObject, String p_method, String p_message)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_ALL,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            c_trace_stmt_number++,
                            p_callingObject,
                            p_method + " -- " + p_message);
        }
    }
} // End of MiscBatchReader
