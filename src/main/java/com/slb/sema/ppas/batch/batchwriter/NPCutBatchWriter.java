// //////////////////////////////////////////////////////////////////////////////
//      ASCS : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME : NPCutBatchWriter.java
//      DATE : Aug 17, 2004
//      AUTHOR : Urban Wigstrom
//      REFERENCE : PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT : ATOS ORIGIN 2004
//
//      DESCRIPTION : 
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME      | DESCRIPTION                          | REFERENCE
//----------+-----------+--------------------------------------+----------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
//----------+-----------+--------------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchcontroller.NPCutBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.NPCutBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasNamedSequenceService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/** Creates and writes to output-,error- and recovery files. */
public class NPCutBatchWriter extends BatchWriter
{
    //  ------------------------------------------------------------------------
    //   Private class constants
    //  ------------------------------------------------------------------------
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String      C_CLASS_NAME                                        =
        "NPCutBatchWriter";

    /** Logger printout - missing property for Control Table Update Frequency. Value is {@value}. */
    private static final String      C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY = 
        "Property for CONTROL_TABLE_UPDATE_FREQUENCY not found";

    /** Logger printout - missing property for Report File Directory. Value is {@value}. */
    private static final String      C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY          = 
        "Property for REPORT_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Recovery File Directory. Value is {@value}. */
    private static final String      C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY        = 
        "Property for RECOVERY_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Recovery File Directory. Value is {@value}. */
    private static final String      C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY          = 
        "Property for OUTPUT_FILE_DIRECTORY not found";

    /** Logger printout - missing value in Map for Input File Name. Value is {@value}. */
    private static final String      C_KEY_NOT_FOUND_INPUT_FILENAME                      = 
        "Missing value in the Map for INPUT_FILE_NAME";

    /** Logger printout - cannot open report- or recovery-file. Value is {@value}. */
    private static final String      C_CANNOT_OPEN_RECOVERY_FILE                         = 
        "Cannot open recovery-file";

    /** An instance of <code>PpasNamedSequenceService</code>. */
    private PpasNamedSequenceService i_namedSequenceService = null;

    /** Gets how many records to be written. Gets from <code>p_params</code>. */
    private int                      i_interval             = 0;

    /** Used to count how many successfull records that has been processed. */
    private int                      i_success              = 0;

    /** Used to count how many records that has been processed. */
    private int                      i_processed            = 0;

    /** Report directory - read from properties. */
    private String                   i_reportFileDirectory  = null;

    /** Recovery directory - read from properties. */
    private String                   i_recoveryFileDirectory = null;

    /** Output directory - read from properties. */
    private String                   i_outputFileDirectory   = null;

    /** Input filename - read from properties. */
    private String                   i_inputFileName         = null;

    /** The date retrieved from the input filename. */
    private String                   i_fileDate              = null;

    /** The sequence retrieved from the input filename. */
    private String                   i_fileSequence          = null;

    /**
     * Help variable for recovery mode. If it is the first record that is read add the number of already
     * successfully processed records to the number successfully processed in this run.
     */
    private boolean                  i_firstRecord                          = true;

    /** Help variable for recovery for count the total number of successfully processed records. */
    private int                      i_numberOfSuccessfullyProcessedRecords = 0;

    /** Help variable for recovery for counting the total number of failed records. */
    private int                      i_numberOfFaultyRecords                = 0;

    /** A hashMap used to hold the number of times a file has proccessed without errors for each sdp. */
    private Hashtable                i_sdpOutputMap                         = new Hashtable();

    /** A hashMap used to hold the sequence number for each sdp. */
    private HashMap i_seqMap                                                = new HashMap();
    
    /**The name of the output file. */
    private String i_outputFilename                                         = null;

    /**
     * Constructs a NPCutBatchWriter object.
     * @param p_ppasContext A PPpsCOntext
     * @param p_logger The logger used of the writer.
     * @param p_controller The batch jobs <code>BatchController</code>.
     * @param p_params Holding parameters used of the writer.
     * @param p_outQueue The batch jobs out queue.
     * @param p_properties <code>PpasProperties </code> for the batch subsystem.
     */
    public NPCutBatchWriter(PpasContext p_ppasContext,
            Logger p_logger,
            BatchController p_controller,
            Map p_params,
            SizedQueue p_outQueue,
            PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties);

        String[] l_inputFileArr = null;
        String l_reportFileFullPath = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10001,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        if (this.getProperties())
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10310,
                                this,
                                C_CLASS_NAME + "Got all properties");
            }

            i_namedSequenceService = new PpasNamedSequenceService(null, p_logger, p_ppasContext);
                        

            // Get the date and sequence fields from the input filename
            l_inputFileArr = i_inputFileName.split(BatchConstants.C_PATTERN_FILENAME_COMPONENTS_DELIMITER);
            i_fileDate = l_inputFileArr[2];

            // Get the full path filename for the recovery-file.
            // Shall always exist, named inputfilename and recovery extension.

            l_reportFileFullPath = generateFileName(this.i_reportFileDirectory,
                                                    this.i_inputFileName,
                                                    BatchConstants.C_EXTENSION_TMP_FILE);

            super.i_recoveryFileName = generateFileName(this.i_recoveryFileDirectory,
                                                        this.i_inputFileName,
                                                        BatchConstants.C_EXTENSION_RECOVERY_FILE);

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10320,
                                this,
                                " recoveryFileFullPath= " + super.i_recoveryFileName + " date= " + i_fileDate
                                        + " sequence= " + i_fileSequence);
            }
            try
            {
                super.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(super.i_recoveryFileName));
                super.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_reportFileFullPath));
            }
            catch (IOException e)
            {

                sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                i_logger.logMessage(new LoggableEvent(C_CANNOT_OPEN_RECOVERY_FILE,
                                                      LoggableInterface.C_SEVERITY_ERROR));
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME,
                                    10330,
                                    this,
                                    C_CANNOT_OPEN_RECOVERY_FILE + " " + C_METHOD_getProperties);
                }
                e.printStackTrace();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10002,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";

    /**
     * Does all necessary final processing of the passed record, that is writing to file(s) and/or database.
     * @param p_record The record to process.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void writeRecord(BatchRecordData p_record) throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Enter " + C_METHOD_writeRecord);
        } 
        
        NPCutBatchRecordData l_batchRecordData = (NPCutBatchRecordData)p_record;
        String l_sdpId = l_batchRecordData.getSdpId();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10100,
                            this,
                            "Sdp id= " + l_sdpId + " " + C_METHOD_writeRecord);
        }
        

        final String L_OUTPUT_FILE_KEY = "OUTPUT_SDP" + l_sdpId;
        
        if (i_seqMap.isEmpty() || !i_seqMap.containsKey(l_sdpId))
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10100,
                                this,
                                "Get next sequence number. " + C_METHOD_writeRecord);
            }
            
            try
            {
                i_fileSequence = i_namedSequenceService.getNextSequence(null,
                                                                        BatchConstants.C_NP_SEQUENCE_NAME,
                                                                        super.i_timeout);
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10100,
                                    this,
                                    "New sequnce number. " + i_fileSequence);
                } 

                i_seqMap.put(l_sdpId, i_fileSequence);

                i_outputFilename = i_outputFileDirectory + "/" + "NPLAN_SDP" + l_sdpId
                        + BatchConstants.C_DELIMITER_FILENAME_FIELDS + i_fileSequence
                        + BatchConstants.C_EXTENSION_TMP_FILE; 
            }
            catch (PpasServiceException e1)
            {
                e1.printStackTrace();
            }
        }

        int l_sdpCounter = 0; //Successful record - count and store the counter for this sdp


        // Save number of already processed records for the trailer printout.
        if (this.i_firstRecord)
        {
            this.i_firstRecord = false;
            this.i_numberOfSuccessfullyProcessedRecords = l_batchRecordData
                    .getNumberOfSuccessfullyProcessedRecords();
            this.i_numberOfFaultyRecords = l_batchRecordData.getNumberOfErrorRecords();
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10360,
                            this,
                            "noSuccessfully processed=" + this.i_numberOfSuccessfullyProcessedRecords
                                    + " noFailured=" + this.i_numberOfFaultyRecords);
        }

        //Count every time a file is being processed.
        i_processed++;

        //Check if reject record info exist and if so write to report file.
        if (l_batchRecordData.getErrorLine() != null)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                50200,
                                this,
                                "Enter checks if reject record info exist.");
            }

            super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_batchRecordData.getErrorLine());
            super.i_errors++;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                50260,
                                this,
                                "Leaving checks if reject record info exist. Errors=" + super.i_errors);
            }
        }
        else
        {
            //Count the total number of successed processed files.
            i_success++; 

            //Check if output file info exist and if so write to output file.
            if (l_batchRecordData.getOutputLine() != null)
            {
                if (!super.fileExists(L_OUTPUT_FILE_KEY))
                {
                    super.openFile(L_OUTPUT_FILE_KEY, new File(i_outputFilename));

                    //Initialize the Report Map with 0 successful entries
                    i_sdpOutputMap.put(L_OUTPUT_FILE_KEY, new Integer(0));
                }

                super.writeToFile(L_OUTPUT_FILE_KEY, l_batchRecordData.getOutputLine());

            }

            // Count successed processd files for each sdp
            if (i_sdpOutputMap.containsKey(L_OUTPUT_FILE_KEY))
            {
                l_sdpCounter = ((Integer)i_sdpOutputMap.get(L_OUTPUT_FILE_KEY)).intValue();
                l_sdpCounter++;
            }

            i_sdpOutputMap.put(L_OUTPUT_FILE_KEY, new Integer(l_sdpCounter));
        }

        // If this record failed during the previous run - decrease it otherwise it will be counted
        // twice as a failure record in the baco-table
        if (l_batchRecordData.getFailureStatus())
        {
            this.i_numberOfFaultyRecords--;
        }

        //  Always write to recovery file.
        super.writeToFile(BatchConstants.C_KEY_RECOVERY_FILE, l_batchRecordData.getRecoveryLine());

        if ((i_processed % i_interval) == 0)
        {
            updateStatus();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10110,
                            this,
                            "Leaving " + C_METHOD_writeRecord);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeTrailerRecord = "writeTrailerRecord";
    /**
     * Writes out the trailer record in report files. This will only apply to batches that have an input file.
     * @param p_outcome either SUCCESS if completed the batch or FAILURE if fatal error encountered.
     * @throws IOException If it is not possible to write to the file.
     */

    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10210,
                            this,
                            "Enter " + C_METHOD_writeTrailerRecord + " Outcom=" + p_outcome);
        }

        String l_outputFileKey = null;
        int l_sdpCounter = 0;
        StringBuffer l_tmp = new StringBuffer();

        String l_count = BatchConstants.C_TRAILER_ZEROS
                + (i_success + i_numberOfSuccessfullyProcessedRecords);

        l_count = l_count.substring(l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count
                .length());

        // Generate trailer records for each output file
        for (Enumeration e = i_sdpOutputMap.keys(); e.hasMoreElements();)
        {
            // Get file keys
            l_outputFileKey = (String)e.nextElement();

            // Get number of successed processed files.
            l_sdpCounter = ((Integer)i_sdpOutputMap.get(l_outputFileKey)).intValue();

            // Trailer for the update files to the SDPs - if file has been created
            if (super.fileExists(l_outputFileKey))
            {
                l_tmp.append(BatchConstants.C_RECORD_TYPE_NUMBER_PLAN_CHANGE_TRAILER_RECORD);
                l_tmp.append(BatchConstants.C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2);
                l_tmp.append(l_sdpCounter);
                super.writeToFile(l_outputFileKey, l_tmp.toString());
                
                // Rename output file for SDP
                BatchFile l_outFile = ((BatchFile)i_filePointers.get(l_outputFileKey));
                
                if (l_outFile != null)
                {
                    l_outFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                         BatchConstants.C_EXTENSION_TMP_FILE,
                                         BatchConstants.C_EXTENSION_INDATA_FILE);
                }
                else
                {
                    throw new IOException("TeMP output file does not exist");
                }
            }
        }

        super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_count + p_outcome);

        // Total number of faulty record. Set to flag if the recovery file may be deleted.
        super.i_errors = super.i_errors + i_numberOfFaultyRecords;
        
        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10220,
                            this,
                            "Leaving " + C_METHOD_writeTrailerRecord);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";

    /**
     * Does all necessary final processing such as writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus() throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10310,
                            this,
                            "Enter " + C_METHOD_updateStatus);
        }

        super.flushFiles();
        BatchJobData l_jobData = ((NPCutBatchController)super.i_controller).getKeyedControlRecord();
        l_jobData.setBatchType(BatchConstants.C_JOB_TYPE_NUMBER_PLAN_CUTOVER);
        l_jobData.setNoOfRejectedRec(Integer.toString(super.i_errors + i_numberOfFaultyRecords));
        l_jobData.setNoOfSuccessRec(Integer.toString(i_success + i_numberOfSuccessfullyProcessedRecords));

        super.updateControlInfo(l_jobData);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10320,
                            this,
                            "Leaving " + C_METHOD_updateStatus);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getProperties = "getProperties";

    /**
     * This method tries to read the properies for: Table update frequency and directories for the input-,
     * report- and recovery-files.
     * @return true if all properties were found.
     */
    private boolean getProperties()
    {
        String l_interval = null; // Help variable - finding the update frequency intervall
        boolean l_returnValue = true; // Assume all found

        // Get input filename from the Map
        this.i_inputFileName = (String)this.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);
        if (this.i_inputFileName == null)
        {
            l_returnValue = false;
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage(new LoggableEvent(C_KEY_NOT_FOUND_INPUT_FILENAME,
                                                  LoggableInterface.C_SEVERITY_ERROR));
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10380,
                                this,
                                C_KEY_NOT_FOUND_INPUT_FILENAME + " " + C_METHOD_getProperties);
            }
        }

        // Get the "update frequency interval"
        l_interval = i_properties.getTrimmedProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);

        if (l_interval != null)
        {
            i_interval = Integer.parseInt(l_interval);
        }
        else
        {
            l_returnValue = false;
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage(new LoggableEvent(C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY,
                                                  LoggableInterface.C_SEVERITY_ERROR));
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10390,
                                this,
                                C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY + " "
                                        + C_METHOD_getProperties);
            }
        }

        // Get the Recovery File Directory
        i_recoveryFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);

        if (i_recoveryFileDirectory == null)
        {
            l_returnValue = false;
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage(new LoggableEvent(C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY,
                                                  LoggableInterface.C_SEVERITY_ERROR));
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10410,
                                this,
                                C_PROPERTY_NOT_FOUND_RECOVERY_FILE_DIRECTORY + " " + C_METHOD_getProperties);
            }
        }

        // Get the report file directory
        i_reportFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);

        if (i_reportFileDirectory == null)
        {
            l_returnValue = false;
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage(new LoggableEvent(C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY,
                                                  LoggableInterface.C_SEVERITY_ERROR));
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10420,
                                this,
                                C_PROPERTY_NOT_FOUND_REPORT_FILE_DIRECTORY + " " + C_METHOD_getProperties);
            }
        }

        // Get the report file directory
        i_outputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);

        if (i_outputFileDirectory == null)
        {
            l_returnValue = false;
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage(new LoggableEvent(C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY,
                                                  LoggableInterface.C_SEVERITY_ERROR));
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10420,
                                this,
                                C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY + " " + C_METHOD_getProperties);
            }
        }

        return l_returnValue;

    } // End of method getProperties()

}