////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromoAllocSynchBatchWriter.java
//      DATE            :       Sep 1, 2004
//      AUTHOR          :       Lars Lundberg
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//10/09/07 | K Bond        | Update the success / failure     | PpacLon#3112/12037
//         |               | counters.                        |
//---------+-----------+---+----------------------------------+-----------------
//18/06/08 | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//         |           | rename on completion of batch job.   |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;
//import java.util.NoSuchElementException;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.PromoAllocSynchBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.dataclass.PromoAllocSynchBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for the final processing of the Promotion Plan Allocation Synchronisation,
 * that is, to write any rejected subcriber accounts to a report file.
 */
public class PromoAllocSynchBatchWriter extends BatchWriter
{
    //---------------------------------------------------------------
    //  Class level constants.
    //---------------------------------------------------------------
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                       = "PromoAllocSynchBatchWriter";

//    /** The length of the output file sequence number. Value is {@value}. */
//    private static final int    C_LENGTH_FILE_SEQUENCE_NUMBER      = 5;
    
    /** Logger message -- missing database update interval property. Value is {@value}. */
    private static final String C_DATABASE_UPDATE_INTERVAL_MISSING = 
        "Property for CONTROL_TABLE_UPDATE_FREQUENCY is missing.";

    /** Logger message -- invalid database update interval property. Value is {@value}. */
    private static final String C_DATABASE_UPDATE_INTERVAL_INVALID = 
        "Property for CONTROL_TABLE_UPDATE_FREQUENCY is invalid, i.e. not a numerical value.";

    /** Logger message -- missing report file directory property. Value is {@value}. */
    private static final String C_REPORT_FILE_DIRECTORY_MISSING    = 
        "Property for REPORT_FILE_DIRECTORY is missing.";

    /** File prefix for the output filename. */
    private static final String C_REPORT_FILENAME_PREFIX           = "BATCH_SDP_SYNCH_PROMO_ALLOC";
    
    /** File suffix for the output filename. */
    private static final String C_REPORT_FILENAME_SUFFIX           = BatchConstants.C_EXTENSION_TMP_FILE;

    /** Report file key for open call.  Value is {@value}. */
    private static final String C_KEY_REPORT_FILE                  = BatchConstants.C_KEY_REPORT_FILE;
    

    
    //--------------------------------------------------------
    //  Instance variables.
    //  ------------------------------------------------------ 
    /** The update database interval. */
    private int    i_interval            = 0;

    /** The number of processed records. */
    private int    i_processCnt          = 0;

    /** The number of rejected records that has been processed. */
    private int    i_rejectCnt           = 0;

    /** Report file directory (full path). */
    private String i_reportFileDirectory = null;

//    /** The last processed cust id. */
//    private String i_lastCustId          = null;


    //---------------------------------------------------------------
    //  Constructors.
    //---------------------------------------------------------------
    /**
     * Constructs an instance of this <code>PromoAllocSynchBatchWriter</code> class using the passed
     * parameters.
     *
     * @param p_ppasContext      the <code>PpasContext</code> reference.
     * @param p_logger           the logger
     * @param p_batchController  the batch controller
     * @param p_outQueue         the output data queue
     * @param p_parameters       the process paramters
     * @param p_properties       the <code>PpasProperties</code> for the current process.
     * 
     * @throws IOException  if it fails to oopen the report file.
     */
    public PromoAllocSynchBatchWriter(PpasContext                    p_ppasContext,
                                      Logger                         p_logger,
                                      PromoAllocSynchBatchController p_batchController,
                                      SizedQueue                     p_outQueue,
                                      Map                            p_parameters,
                                      PpasProperties                 p_properties) throws IOException
    {
        super(p_ppasContext, p_logger, p_batchController, p_parameters, p_outQueue, p_properties);

//        String       l_fileSequenceNumber = null;
        String       l_reportFileFullPath = null;
        StringBuffer l_tmpReportFilename  = null;
        String       l_yyyyMMddHHmmss     = null;
        String       l_yyyyMMdd           = null;
        String       l_hhmmss             = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        if (this.setProperties())
        {
            //Create the complete filename for the report file (directory path included).
            l_yyyyMMddHHmmss  = DatePatch.getDateTimeNow().toString_yyyyMMdd_HHmmss();
            l_yyyyMMdd        = l_yyyyMMddHHmmss.split("[-]")[0];
            l_hhmmss          = l_yyyyMMddHHmmss.split("[-]")[1];

            l_tmpReportFilename = new StringBuffer();
            l_tmpReportFilename.append(this.i_reportFileDirectory);
            l_tmpReportFilename.append("/");
            l_tmpReportFilename.append(C_REPORT_FILENAME_PREFIX); 
            l_tmpReportFilename.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
            l_tmpReportFilename.append(l_yyyyMMdd);
            l_tmpReportFilename.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
            l_tmpReportFilename.append(l_hhmmss);
            l_tmpReportFilename.append(C_REPORT_FILENAME_SUFFIX);

            l_reportFileFullPath = l_tmpReportFilename.toString();
           
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10310,
                                this,
                                "Report file full path: '" + l_reportFileFullPath + "'" );
            }

            try
            {
                super.openFile(C_KEY_REPORT_FILE, new File(l_reportFileFullPath));
            }
            catch (IOException p_ioExe)
            {
                
                sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                i_logger.logMessage(new LoggableEvent("Failed to open report file '" + 
                                                      l_reportFileFullPath + "'",
                                                      LoggableInterface.C_SEVERITY_ERROR));
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME,
                                    10330,
                                    this,
                                    "Failed to open report file '" + l_reportFileFullPath + "'");
                }
                throw p_ioExe;
            }
            finally
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10340,
                        this,
                        "Finally - efter openFile..." );
                }
            }    
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    }

    //--------------------------------------------------------
    //  Protected instance methods.
    //-------------------------------------------------------- 
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
    /**
     * Writes the passed record's error info to the report file.
     * 
     * @param p_record  The record that holds the info to be printed.
     * 
     * @throws IOException          if it fails to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void writeRecord(BatchRecordData p_record) throws IOException, PpasServiceException
    {
        PromoAllocSynchBatchRecordData l_record = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10360,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_writeRecord);
        }

        l_record = (PromoAllocSynchBatchRecordData)p_record;
        
        // If any error info exist, write it to file.
        if (l_record.getErrorLine() != null)
        {
            //Write to output file.
            super.writeToFile(C_KEY_REPORT_FILE, l_record.getErrorLine() );
            i_rejectCnt++;
        }

        i_processCnt++;
        if ( (i_processCnt % i_interval) == 0)
        {
            updateStatus();
        }
        
//        i_lastCustId = l_record.getCustomerId();
//        this.i_controller.removeCustomerInProgress(l_record.getCustomerId());

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10370,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_writeRecord);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeTrailerRecord = "writeTrailerRecord";    
    /**
     * Writes out the trailing record to the report file.
     * The trailer record layout is a string composed of the number of successfully processed records as a 6
     * digits string (right adjusted and padded with leading zeros) followed by a 'SUCCESS' text.
     * 
     * @param p_outcome  Whether the batch FAILED (could not reach the end of the file) or SUCCEEDED.
     * 
     * @throws IOException  if it fails to write the trailing record to the report file.
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10360,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_writeTrailerRecord);
        }

        // Set the record counter to the number of successfully processed records, that is total number of
        // processed records minus the number of rejected records.
        String l_count = BatchConstants.C_TRAILER_ZEROS + (i_processCnt - i_rejectCnt);
        l_count =
            l_count.substring(l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count.length());

        super.writeToFile(C_KEY_REPORT_FILE, l_count + p_outcome);
        
        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10370,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_writeTrailerRecord);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";    
    /**
     * Updates 'last processed customer id' in the sub job control table.
     * Also flushes any buffered report file records to the report file.
     * 
     * @throws IOException           if it fails to write report records to the file.
     * @throws PpasServiceException  if it fails to update the 'last processed customer id' in the sub job
     *                               control table. No specific keys are anticipated.
     */
    protected void updateStatus() throws IOException, PpasServiceException
    {
        BatchSubJobData l_subJobData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10360,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        // Flush buffered info to file.
        super.flushFiles();

        // Update 'last processed customer id' in the sub job control table.
        l_subJobData = ((PromoAllocSynchBatchController)super.i_controller).getKeyedControlRecord();
        l_subJobData.setLastProcessedCustId("-1");
        updateSubJobRecordCounters(null,
                Integer.parseInt(l_subJobData.getMasterJsJobId()),
                new PpasDateTime(l_subJobData.getExecutionDateTime()),
                (i_processCnt - i_rejectCnt),
                (i_rejectCnt));
        super.updateControlInfo(l_subJobData);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10370,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_updateStatus);
        }
    }


    //--------------------------------------------------------
    //  Private instance methods.
    //  ------------------------------------------------------ 
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_setProperties = "setProperties";    
    /**
     * This method returns <code>true</code> if it succeeds to set the properties used by this writer.
     * If it fails an error status message is sent to the batch controller and the method will return
     * <code>false</code>. 
     * The properties that are set is the database update frequency and report file directory.
     * @return true if all properties was read successfully
     */
    private boolean setProperties()
    {
        String  l_interval    = null;
        boolean l_returnValue = true;  // Assume success.
        

        // Get the "database update interval".
        l_interval = i_properties.getTrimmedProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        if ( l_interval != null )
        {
            try
            {
                // Set the "database update interval".
                i_interval = Integer.parseInt(l_interval);
            }
            catch (NumberFormatException p_nfExe)
            {
                // The interval property has an invalid value, that is not a numerical value.
                sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                i_logger.logMessage(new LoggableEvent(C_DATABASE_UPDATE_INTERVAL_INVALID,
                                                      LoggableInterface.C_SEVERITY_ERROR));
                l_returnValue = false;
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME,
                                    10390,
                                    this,
                                    C_METHOD_setProperties + " -- " + C_DATABASE_UPDATE_INTERVAL_INVALID);
                }
            }
        }
        else
        {
            // The interval property is missing.
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage(new LoggableEvent(C_DATABASE_UPDATE_INTERVAL_MISSING,
                                                  LoggableInterface.C_SEVERITY_ERROR));
            l_returnValue = false;
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10390,
                                this,
                                C_METHOD_setProperties + " -- " + C_DATABASE_UPDATE_INTERVAL_MISSING);
            }
        }


        // Get the report file directory.
        i_reportFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        if ( i_reportFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage(new LoggableEvent(C_REPORT_FILE_DIRECTORY_MISSING,
                                                  LoggableInterface.C_SEVERITY_ERROR));
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10420,
                                this,
                                C_METHOD_setProperties + " -- " + C_REPORT_FILE_DIRECTORY_MISSING);
            }
        }
                
        return l_returnValue;
        
    } // End of method setProperties()
}
