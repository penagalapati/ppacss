////////////////////////////////////////////////////////////////////////////////
//
//    FILE NAME       :       BatchMaster.java
//    DATE            :       2004
//    AUTHOR          :       Lars Lundberg
//    REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//    COPYRIGHT       :       WM-data 2007
//
//    DESCRIPTION     :       Schedules batch sub-processes for database driven
//                            batches.
//
//////////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
//////////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------------
//05/05/06 | Chris         | Filter Disconnection records by  | PpacLon#2250/8691
//         | Harrison      | Disconnection Reason codes.      | PRD_ASCS_GEN_CA_82
//---------+---------------+----------------------------------+-----------------
//13/03/07 | L. Lundberg   | Correction of a problem reported | PpacLon#2987/11149
//         |               | by the 'FindBugs' tool:          |
//         |               | "l_jsClient could be null and is |
//         |               | guaranteed to be dereferenced in |
//         |               | method submitJob(...)."          |
//---------+---------------+----------------------------------+-----------------
//21/06/07 | Ian James     | Batch Subscriber Segmentaion     | PpacLon#3183/11755
//         |               | change.                          | PRD_ASCS00_GEN_CA_129
//---------+---------------+----------------------------------+-----------------------
//29/06/07 | E Dangoor     | Add disconnection date to output | PpacLon#3195/11774
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchmaster;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.dataclass.BatchJobDataSet;
import com.slb.sema.ppas.common.dataclass.BatchSubJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchSubJobControlDataSet;
import com.slb.sema.ppas.common.dataclass.DisconBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.support.ConfigKey;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.is.isapi.PpasDailySequencesService;
import com.slb.sema.ppas.is.isil.batchisilservices.MasterDbBatchService;
import com.slb.sema.ppas.js.jobrunner.SimpleJavaJob;
import com.slb.sema.ppas.js.jsclient.JsClient;
import com.slb.sema.ppas.js.jscommon.JobActionException;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.js.jscommon.JsRequest;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.support.UtilDateFormat;


/**
 * This <code>BatchMaster</code> class is a derived <code>SimpleJavaJob</code> class.
 * The purpose of this <code>BatchMaster</code> class is to schedule batch sub-jobs for database driven
 * batches and also to carry out concurrency checks (i.e. to avoid starting a batch sub-job if it is already
 * started).
 */
public class BatchMaster extends SimpleJavaJob
{
    //-------------------------------------------------------------------------
    // Private constants.
    //-------------------------------------------------------------------------
    /** Constant holding the name of the current class. Value is {@value}. */
    private static final String C_CLASS_NAME                  = "BatchMaster";
    
    /** Constant holding the job type passed into the Constructor. Value is {@value}. */
    private static final String C_BATCH_MASTER_JOB_TYPE       = "batchMaster";
    
    /** Additional properties layers to load for this <code>BatchMaster</code> process. */
    private static final String C_ADDED_BATCH_MASTER_LAYER    = "batch_mas";
    
    /** The default IS API timout value. Value is {@value}. */
    private static final long   C_DEFAULT_ISAPI_TIMEOUT       = 5000L;

    /** Constant holding a value that indicates an undefined range (start or end) value. */
    private static final long   C_UNDEFINED_RANGE_VALUE       = -1;

    /** Constant holding the batch job 'in progress' status value. */
    private static final char   C_BATCH_JOB_STATUS_INPROGRESS = 'I';

    /** File prefix for the output filename during pre-processing. */
    private static final String C_PREFIX_OUTPUT_FILE_NAME     = "DISC_";
    
    /** File suffix for the output filename during pre-processing. */
    private static final String C_SUFFIX_OUTPUT_FILE_NAME     = ".DAT";
    
    /** The length of the output file sequence number during pre-processing. */
    private static final int    C_LENGTH_FILE_SEQUENCE_NUMBER = 5;
    
    /** Field length of the MSISDN in the output file record. Value is {@value}. */
    private static final int    C_MSISDN_LENGTH               = 15;

    /** Parameter name for date offset in days. Value is {@value}. */
    private static final String C_KEY_DISCONNECTION_OFFSET    = "disconnectionOffset";


    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /** The <code>PpasContext</code> object. */
    private PpasContext             i_ppasContext             = null;

    /** The start parameters storage. */
    private Map                     i_params                  = null;

    /** <code>Logger</code> for the batch master.*/
    private Logger                  i_logger                  = null;

    /** The current master job type. Passed as a Constructor parameter. */
    private String                  i_batchMasterJobType      = null;

    /** The batch job type. Passed as a mapped parameter, e.g. 'BatchDisconnectionMaster'. */
    private String                  i_batchJobType            = null;

    /** The batch sub-job type. Its value depends on the value of the <code>i_batchJobType</code> variable,
     *  e.g. 'batchDisconnect'. */
    private String                  i_batchSubJobType         = null;

    /** The master's js job id. Passed as a Constructor parameter. */
    private long                    i_jsJobId                 = -1;
    
    /** The current number of sub jobs to be submitted (if not a recovery process). */
    private long                    i_numberOfSubJobs         = 0;     

    /** The recovery flag. Passed as a mapped parameter. */
    private String                  i_recoveryFlag            = null;
    
    /** The batch recovery mode indicator. */
    private boolean                 i_recovery                = false;
    
    /** The <code>PpasBatchControlService</code> object reference. */
    private PpasBatchControlService i_ppasBatchControlService = null;

    /** The current execution date and time. */
    private PpasDateTime            i_executionDateTime       = null;
    
    /** The disconnection date parameter (if any is given). */
    private String                  i_disconnectionDate       = null;
    
    /** The maximum time the current process will wait for a response from an IS API. */
    private long                    i_isApiTimeout            = C_DEFAULT_ISAPI_TIMEOUT;
    
    /** The start range parameter (if any is given). Passed as a mapped parameter. */
    private long                    i_passedStartRange        = C_UNDEFINED_RANGE_VALUE;

    /** The end range parameter (if any is given). Passed as a mapped parameter. */
    private long                    i_passedEndRange          = C_UNDEFINED_RANGE_VALUE;

    /** Used to set the bsco_extra_data_1 column in the bsco_batch_sub_control table. */
    private String                  i_extraData1               = null;

    /** The flag to indicate whether to output the disconnection date. */
    private boolean                 i_outputDiscDate           = false;


    //-------------------------------------------------------------------------
    // Constructors.
    //-------------------------------------------------------------------------
    /**
     * Constructs a <code>BatchMaster</code> instance with the passed parameters.
     * 
     * @param p_ppasContext    the <code>PpasContext</code> object.
     * @param p_batchMasterJobType the current job type, in this case the "BatchMaster".
     * @param p_jsJobId        the job id.
     * @param p_params         the start parameters storage.
     * @param p_jobRunnerName  the name of the current <code>JobRunner</code> process.
     * 
     * @throws PpasConfigException  if configuration data is missing or incomplete, or problems to
     *                              locate/load the properties file.
     */
    public BatchMaster(PpasContext p_ppasContext,
                       String      p_batchMasterJobType,
                       String      p_jsJobId,
                       String      p_jobRunnerName,
                       Map         p_params) throws PpasConfigException
    {
        super(p_ppasContext, p_batchMasterJobType, p_jsJobId, p_jobRunnerName, p_params);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                "Constructing " + C_CLASS_NAME);
            
            // Print out the given arguments.
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10005,
                this,
                "job type: " + p_batchMasterJobType + ",  job id: " + p_jsJobId + 
                ",  runner name: " + p_jobRunnerName);
        }
        
        // Set instance variables.
        i_ppasContext        = p_ppasContext;
        i_batchMasterJobType = p_batchMasterJobType;
        i_jsJobId            = Long.parseLong(p_jsJobId);
        i_params             = p_params;

        // Initialise this instance.
        init();
        
        // The BatchMaster should neither be pausible nor stoppable.
        super.setPausable(false);
        super.setStoppable(false);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                "Constructed " + C_CLASS_NAME);
        }
    }


    //-------------------------------------------------------------------------
    // Protected instance methods. 
    //-------------------------------------------------------------------------

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_doJobProcessing = "doJobProcessing";
    /**
     * Processes the initiated master job.
     * The job is initiated when the current <code>BatchMaster</code> instance is created.
     * 
     * @see com.slb.sema.ppas.js.jobrunner.SimpleJavaJob#doJobProcessing()
     * @throws PpasException  Any exception descended from the PpasException class, indicating
     *                        that some error has occured while trying to process the job.
     */
    protected void doJobProcessing() throws PpasException
    {
        int l_jobStatus = JobStatus.C_JOB_EXIT_STATUS_UNDEFINED;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10020,
                this,
                "Entering " + C_METHOD_doJobProcessing);
        }

        // Notify that the job starts.
        super.startDone();
        
        // Check the master job type.
        if (i_batchMasterJobType != null  && i_batchMasterJobType.equals(C_BATCH_MASTER_JOB_TYPE))
        {
            if (i_recovery)
            {
                l_jobStatus = processRecoveryJob();
            }
            else
            {
                // Call preProcessing. If this method throws an exception we should NOT
                // carry on processing but aborting execution.
                try 
                {
                    preProcessing();
                    l_jobStatus = processJob();
                }
                catch (PpasServiceException l_psEx)
                {
                    l_jobStatus = JobStatus.C_JOB_EXIT_STATUS_FAILURE;
                }
//                catch (Exception l_e)
//                {
//                    i_logger.logMessage( new LoggableEvent( "Runtime Exception:" + l_e.getClass().getName(),
//                                                            LoggableInterface.C_SEVERITY_ERROR) );
//
//                    l_jobStatus = JobStatus.C_JOB_EXIT_STATUS_FAILURE;
//                }
                
            }
        }
        
        // Notify that the job is finished.
        super.finishDone(l_jobStatus);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10030,
                this,
                "Leaving " + C_METHOD_doJobProcessing);
        }
    }
    

    //-------------------------------------------------------------------------
    // Private instance methods. 
    //-------------------------------------------------------------------------
    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_init = "init";
    /**
     * Initialises this <code>BatchMaster</code> instance.
     * 
     * @throws PpasConfigException  if any configuration parameter is missing or if the properties
     *                              cannot be loaded.
     */
    private void init() throws PpasConfigException
    {
        String              l_startRangeStr = null;
        String              l_endRangeStr   = null;
        PpasConfigException l_configEx      = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10040,
                this,
                "Entering " + C_METHOD_init);
        }

        // Get the default logger. The default logger is determined by the
        // process name, which in this case is the job runner.
        i_logger = i_ppasContext.getLoggerPool().getLogger();

        // Set the current date and time as the execution date and time.
        i_executionDateTime = DatePatch.getDateTimeNow();

        // Get the disconnection date.
        i_disconnectionDate = getDisconnectionDate();

        // Set the start range and end range parameters.
        l_startRangeStr = (String)i_params.get(BatchConstants.C_KEY_START_RANGE);
        if (l_startRangeStr != null)
        {
            i_passedStartRange = Long.parseLong(l_startRangeStr);
        }
        else
        {
            i_passedStartRange = -1;
        }

        l_endRangeStr = (String)i_params.get(BatchConstants.C_KEY_END_RANGE);
        if (l_endRangeStr != null)
        {
            i_passedEndRange = Long.parseLong(l_endRangeStr);
        }
        else
        {
            i_passedEndRange = -1;
        }

        // Set the recovery mode indicator.
        i_recoveryFlag = (String)i_params.get(BatchConstants.C_KEY_RECOVERY_FLAG);
        if (i_recoveryFlag != null  &&  i_recoveryFlag.equals(BatchConstants.C_VALUE_RECOVERY_MODE_ON))
        {
            i_recovery = true;
        }
        else
        {
            i_recovery = false;
        }

         //  Load additional properties, start with the batch master properties.
        super.i_properties = super.i_properties.getPropertiesWithAddedLayers(C_ADDED_BATCH_MASTER_LAYER);

        // Set the IS API timeout value.
        i_isApiTimeout =
            super.i_properties.getLongProperty(BatchConstants.C_TIMEOUT, C_DEFAULT_ISAPI_TIMEOUT);

       // Set the batch job, the sub-job type and the number of sub jobs.
        i_batchSubJobType = null;
        i_numberOfSubJobs = 0;
        i_batchJobType = (String)i_params.get(BatchConstants.C_KEY_BATCH_JOB_TYPE);
        if (i_batchJobType != null)
        {
            if (i_batchJobType.equals(BatchConstants.C_JOB_TYPE_BATCH_DISCONNECTION))
            {
                i_batchSubJobType = BatchConstants.C_SUB_JOB_TYPE_BATCH_DISCONNECTION;
                i_numberOfSubJobs = 
                    super.i_properties.getLongProperty(BatchConstants.C_NUMBER_OF_DISCONNECT_SUBPROCESSES, 0);
                i_extraData1 = i_disconnectionDate;
                i_outputDiscDate = i_properties.getBooleanProperty(BatchConstants.C_OUTPUT_DISC_DATE);
            }
            else if (i_batchJobType.equals(BatchConstants.C_JOB_TYPE_PROMO_ALLOC_PROVISIONING))
            {
                i_batchSubJobType = BatchConstants.C_SUB_JOB_TYPE_PROMO_ALLOC_PROVISIONING;
                i_numberOfSubJobs = 
                    super.i_properties.getLongProperty(
                        BatchConstants.C_NUMBER_OF_PROMO_ALLOC_PROVISIONING_SUBPROCESSES, 0);
            }
            else if (i_batchJobType.equals(BatchConstants.C_JOB_TYPE_BULK_LOAD_OF_ACCOUNT_FINDER))
            {
                i_batchSubJobType = BatchConstants.C_SUB_JOB_TYPE_BULK_LOAD_OF_ACCOUNT_FINDER;
                
                // Only one (1) sub job shall be started for this batch (CA37 requirement)!
                i_numberOfSubJobs = 1;
            }
            else if (i_batchJobType.equals(BatchConstants.C_JOB_TYPE_BATCH_PROMO_ALLOC_SYNCH))
            {
                i_batchSubJobType = BatchConstants.C_SUB_JOB_TYPE_BATCH_PROMO_ALLOC_SYNCH;
                i_numberOfSubJobs = 
                    super.i_properties.getLongProperty(
                        BatchConstants.C_NUMBER_OF_PROMO_ALLOC_SYNCH_SUBPROCESSES, 0);
            }
            else if (i_batchJobType.equals(BatchConstants.C_JOB_TYPE_AUTO_MSISDN_ROUTING_DELETION))
            {
                i_batchSubJobType = BatchConstants.C_SUB_JOB_TYPE_AUTO_MSISDN_ROUTING_DELETION;
                
                // Only one (1) sub job shall be started for this batch
                i_numberOfSubJobs = 1;
            }
        }

        // Create the PpasBatchControlService object.
        i_ppasBatchControlService = new PpasBatchControlService(null, i_logger, super.getContext());

        if (i_batchJobType == null  ||  i_batchSubJobType == null  ||  i_numberOfSubJobs == -1)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10045,
                    this,
                    "i_batchJobType = " + i_batchJobType + 
                    ",  i_batchSubJobType = " + i_batchSubJobType +
                    ",  i_numberOfSubJobs = " + i_numberOfSubJobs);
            }

            l_configEx = new PpasConfigException(C_CLASS_NAME,
                                                 C_METHOD_init,
                                                 10045,
                                                 null,
                                                 null,
                                                 0,
                                                 ConfigKey.get().missingConfigData(
                                                     BatchConstants.C_KEY_BATCH_JOB_TYPE +
                                                         ", Sub Job Type or Number of Sub Jobs",
                                                     C_CLASS_NAME));
                                 
            i_logger.logMessage(l_configEx);
            throw l_configEx;    
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10050,
                this,
                "Leaving " + C_METHOD_init);
        }
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_getDisconnectionDate = "getDisconnectionDate";
    /**
     * Returns the disconnection date as a <code>String</code> with the format 'yyyymmdd'.
     * If no disconnection date is passed as a mapped parameter the disconnection date is set to current date.
     * The disconnection date could be given as an offset in days that should be subtracted from the current
     * date, i.e. the offset should be >= 0.
     * If an invalid date format is passed as a mapped parameter, i.e. not one of the pre-defined formats in
     * <code>PpasDate</code> class, a <code>PpasConfigException</code> is thrown.
     * 
     * @return  the disconnection date as a <code>String</code> with the format 'yyyymmdd'.
     * 
     * @throws  PpasConfigException  if an invalid date format, i.e. not one of the pre-defined formats
     *          in PpasDate class, a <code>PpasConfigException</code> is thrown.
     */
    private String getDisconnectionDate() throws PpasConfigException
    {
        String                         l_disconDateStr    = null;
        String                         l_disconOffsStr    = null;
        PpasDate                       l_disconDate       = null;
        PpasConfigException            l_configEx         = null;
//        BatchInvalidParameterException l_invalidParamEx   = null;
        int                            l_disconDateOffset = 0;
        boolean                        l_approvedOffset   = true;

        // The supported formats (Ex: 17-Nov-2004, 2004-11-17, 20041117).
        final UtilDateFormat[] L_DATE_FORMAT_ARR = {PpasDate.C_DF_dd_MMM_yyyy,
                                                    PpasDate.C_DF_yyyy_MM_dd,
                                                    PpasDate.C_DF_yyyyMMdd};
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10050,
                            this,
                            "Entering " + C_METHOD_getDisconnectionDate);
        }

        // Get the disconnection date or offset as a mapped parameter.
        // The offset parameter will override the date parameter.
        // If neither a date nor an offset is passed as a mapped parameter the current date is used.
        l_disconOffsStr = (String)i_params.get(C_KEY_DISCONNECTION_OFFSET);
        l_disconDateStr = (String)i_params.get(BatchConstants.C_KEY_DISCONNECTION_DATE);
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10050,
                            this,
                            C_METHOD_getDisconnectionDate + 
                            " -- Mapped disconnection date = '" + l_disconDateStr + "',  " +
                            "Mapped disconnection offset = '" + l_disconOffsStr + "'.");
        }

        if (l_disconOffsStr == null  &&  l_disconDateStr == null)
        {
            // Neither is passed, used current date as disconnection date.
            l_disconDate = DatePatch.getDateTimeNow().getPpasDate();
            l_disconDateStr = l_disconDate.toString_yyyyMMdd();
        }
        else if (l_disconOffsStr != null)
        {
            if (l_disconOffsStr.matches(BatchConstants.C_PATTERN_NUMERIC))
            {
                // Ok, the offset is numerical.
                // Parse it as a decimal integer. If the given offset is too big (i.e. > Integer.MAX_VALUE,
                // which is 7fffffff hex or 2147483647) a PpasConfigException will be thrown.
                try
                {
                    l_disconDateOffset = Integer.parseInt(l_disconOffsStr);
                }
                catch (NumberFormatException e)
                {
                    // The given offset is too big.
                    l_approvedOffset = false;
                    l_disconDate = null;
                }

                if (l_approvedOffset)
                {
                    l_disconDate = DatePatch.getDateTimeNow().getPpasDate();
                    l_disconDate.add(PpasDate.C_FIELD_DATE, (0 - l_disconDateOffset));
                }
            }
        }
        else
        {
            // The disconnection date parameter was given, but not the offset.
            for (int i = 0; i < L_DATE_FORMAT_ARR.length; i++)
            {
                try
                {
                    l_disconDate = new PpasDate(l_disconDateStr, L_DATE_FORMAT_ARR[i]);
                        
                    // The given disconnection date string matched the passed format, no more attempt needed.
                    break;
                }
                catch (PpasSoftwareException p_ppasSoftwareEx)
                {
                    // Given disconnection date string didn't match the passed format.
                    // Try with next format.
                    l_disconDate = null;
                    continue;
                }
            } //End of for - loop.
        }

        if (l_disconDate != null)
        {
            l_disconDateStr = l_disconDate.toString_yyyyMMdd();
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10050,
                                this,
                                C_METHOD_getDisconnectionDate + 
                                " -- ***ERROR: Invalid disconnection date format, '" + 
                                l_disconDateStr + "', is passed as a mapped parameter." +
                                " A 'PpasConfigException' will be thrown.");
            }

            // TODO: Use 'BatchInvalidParameterException' instead of 'PpasConfigException'.
//            l_invalidParamEx =
//                new BatchInvalidParameterException(C_CLASS_NAME,
//                                                   C_METHOD_getDisconnectionDate,
//                                                   10045,
//                                                   this,
//                                                   null,
//                                                   0L,
//                                                   BatchMsg.C_KEY_BATCH_INVALID_PARAMETER,
//                                                   new Object[] {BatchConstants.C_KEY_DISCONNECTION_DATE},
//                                                   LoggableInterface.C_SEVERITY_ERROR);
//          i_logger.logMessage(l_invalidParamEx);
//          throw l_invalidParamEx;

            l_configEx = new PpasConfigException(C_CLASS_NAME,
                                                 "getDisconnectionDate",
                                                 10045,
                                                 null,
                                                 null,
                                                 0,
                                                 ConfigKey.get().missingConfigData(
                                                         C_KEY_DISCONNECTION_OFFSET, C_CLASS_NAME));
            i_logger.logMessage(l_configEx);
            throw l_configEx;    
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10050,
                            this,
                            "Leaving " + C_METHOD_getDisconnectionDate);
        }
        return l_disconDateStr;
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_processJob = "processJob";
    /**
     * Processes a master job and returns the status.
     * 
     * @return  the status of the processed disconnect job.
     * @throws PpasServiceException If the scheduled job encounters a logic problem.
     * @throws JobActionException   If the job cannot be actioned.
     */
    private int processJob() throws JobActionException, PpasServiceException
    {
        int                    l_jobStatus        = JobStatus.C_JOB_EXIT_STATUS_UNDEFINED;
        int                    l_subJobId         = 0;
        long                   l_jsSubJobId       = -1;
        Collection             l_subJobRanges     = null;
        Iterator               l_subJobRangesIter = null;
        long[]                 l_startAndEndRange = null;
        BatchJobData           l_batchJobData     = null;
        BatchSubJobControlData l_batchSubJobControlData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10060,
                            this,
                            "Entering " + C_METHOD_processJob);
        }

        // Get the sub job ranges.
        l_subJobRanges = getSubJobRanges();

        if (l_subJobRanges.size() > 0)
        {
            // Insert a record in the batch master job control table.
            l_batchJobData = new BatchJobData();
            l_batchJobData.setBatchJobStatus(Character.toString(C_BATCH_JOB_STATUS_INPROGRESS));
            l_batchJobData.setBatchType(i_batchJobType);
            l_batchJobData.setExecutionDateTime(i_executionDateTime.toString());
            l_batchJobData.setExtraData1(i_extraData1);
            l_batchJobData.setJsJobId(Long.toString(i_jsJobId));
            l_batchJobData.setNoOfRejectedRec("0");
            l_batchJobData.setNoOfSuccessRec("0");
            l_batchJobData.setOpId(super.getSubmitterOpid());
            l_batchJobData.setSubJobCnt(String.valueOf(i_numberOfSubJobs));
            i_ppasBatchControlService.addJobDetails(null, l_batchJobData, i_isApiTimeout);

            // Submit the sub-jobs.
            l_subJobId = 1;
            l_subJobRangesIter = l_subJobRanges.iterator();
            while (l_subJobRangesIter.hasNext())
            {
                // Get the start and end range.
                l_startAndEndRange = (long[])l_subJobRangesIter.next();

                // Insert a record in the batch sub-job control table.
                l_batchSubJobControlData = new BatchSubJobControlData(i_batchSubJobType,
                                                                      i_executionDateTime,
                                                                      l_subJobId,
                                                                      -1L,
                                                                      i_jsJobId,
                                                                      C_BATCH_JOB_STATUS_INPROGRESS,
                                                                      l_startAndEndRange[0],
                                                                      l_startAndEndRange[1],
                                                                      -1,
                                                                      null,
                                                                      null,
                                                                      super.getSubmitterOpid());
                i_ppasBatchControlService.addSubJobDetails(null, l_batchSubJobControlData, i_isApiTimeout);

                l_jsSubJobId = submitJob(l_startAndEndRange[0],
                                         l_startAndEndRange[1],
                                         i_batchSubJobType,
                                         l_subJobId);

                // Update the batch sub-job control table with the retrieved js job id.
                i_ppasBatchControlService.updateSubJobJsJobId(null,
                                                              l_batchSubJobControlData.getMasterJsJobId(),
                                                              l_batchSubJobControlData.getExecutionDateTime(),
                                                              l_batchSubJobControlData.getSubJobId(),
                                                              l_jsSubJobId,
                                                              i_isApiTimeout);
                l_subJobId++;

                try
                {
                    // In order to make sure that consecutive sub jobs will be given a unique job name
                    // (a sub job name consists of '<node>_<opid>_<timestamp>') a delay for at least one 
                    // second has to be done (to avoid more than one timestamp during the same second).
                    Thread.sleep(1500L);
                }
                catch (InterruptedException p_intEx)
                {
                    // No specific handling of this Exception.
                    p_intEx = null;
                }
            } // End of while loop.
        }

        l_jobStatus = JobStatus.C_JOB_EXIT_STATUS_SUCCESS;
    
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10070,
                this,
                "Leaving " + C_METHOD_processJob);
        }
        return l_jobStatus;
    }




    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_processRecoveryJob = "processRecoveryJob";
    /**
     * Processes a master job and returns the status.
     * 
     * @return  the status of the processed disconnect job.
     * @throws PpasServiceException If the scheduled job encounters a logic problem.
     * @throws JobActionException   If the job cannot be actioned.
     * @throws NumberFormatException If a range is not numeric.
     */
    private int processRecoveryJob() throws JobActionException, NumberFormatException, PpasServiceException
    {
        int             l_jobStatus        = JobStatus.C_JOB_EXIT_STATUS_UNDEFINED;
        Collection      l_subJobRanges     = null;
        Iterator        l_subJobRangesIter = null;
        long[]          l_startAndEndRange = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10080,
                this,
                "Entering " + C_METHOD_processRecoveryJob);
        }

        // Get the recovery job ranges.
        l_subJobRanges = getRecoverySubJobRanges();

        // Submit the sub-jobs.
        l_subJobRangesIter = l_subJobRanges.iterator();
        while (l_subJobRangesIter.hasNext())
        {
            // Get the start and end range.
            l_startAndEndRange = (long[])l_subJobRangesIter.next();

            submitJob(l_startAndEndRange[0],
                      l_startAndEndRange[1],
                      i_batchSubJobType,
                      (int)l_startAndEndRange[2]);
            
            try
            {
                // In order to make sure that consecutive sub jobs will be given a unique job name (a sub job
                // name consists of '<node>_<opid>_<timestamp>') a delay for at least one second has to be
                // done (to avoid more than one timestamp during the same second).
                Thread.sleep(1500L);
            }
            catch (InterruptedException p_intEx)
            {
                // No specific handling of this Exception.
                p_intEx = null;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10090,
                this,
                "Leaving " + C_METHOD_processRecoveryJob);
        }
        return l_jobStatus;
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_getSubJobRanges = "getSubJobRanges";
    /**
     * Returns the sub-job ranges as a <code>Collection</code> of <code>String</code> arrays.
     * A <code>String</code> array consists of two elements: [start_range, end_range].
     * 
     * @return the sub-job ranges as a <code>Collection</code> of <code>String</code> arrays.
     * 
     * @throws PpasServiceException  if the request for highest used customer id fails.
     */
    private Collection getSubJobRanges() throws PpasServiceException
    {
        ArrayList l_subJobRanges       = null;
        long[]    l_startAndEndRange   = null;
        long      l_startRange         = 0;
        long      l_endRange           = 0;
        long      l_rangeSize          = 0;
        long      l_rest               = 0;
        long      l_highestCustId      = 0;
        long      l_totNumberOfCustIds = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10120,
                this,
                "Entering " + C_METHOD_getSubJobRanges);
        }

        l_subJobRanges = new ArrayList();

        if (i_batchSubJobType.equals(BatchConstants.C_SUB_JOB_TYPE_BULK_LOAD_OF_ACCOUNT_FINDER))
        {
            // For a 'Bulk Load of Account Finder' sub job the cust id is not used as an range attribute,
            // and according to the CA37 requirements only one (1) sub job shall be started.
            // As a consequence one (1) dummy range is created and returned.
            l_subJobRanges.add(new long[]{0L, 0L});
            return l_subJobRanges;
        }

        if (i_numberOfSubJobs == 0)
        {
            // No sub jobs are configured to be started.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10122,
                                this,
                                "Number of sub jobs is zero (0), i.e. no sub jobs will be submitted.");
            }
            l_subJobRanges.clear();
            return l_subJobRanges;
        }

        if (i_passedStartRange != C_UNDEFINED_RANGE_VALUE)
        {
            l_startRange = i_passedStartRange;
        }

        if (i_passedEndRange != C_UNDEFINED_RANGE_VALUE)
        {
            l_highestCustId = i_passedEndRange;
        }
        else
        {
            l_highestCustId = i_ppasBatchControlService.getHighestUsedCustId(null, i_isApiTimeout);
        }

        if (l_highestCustId == -1)
        {
            // The database is empty, i.e. no subscribers are yet installed. No sub jobs to start.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10122,
                                this,
                                "The database is probably emty, i.e. no sub jobs will be submitted.");

                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_END,
                                C_CLASS_NAME,
                                10124,
                                this,
                                "Leaving " + C_METHOD_getSubJobRanges);
            }
            return l_subJobRanges;
        }

        // Calculate the total number of cust ids (including the start cust id).
        l_totNumberOfCustIds = l_highestCustId - l_startRange + 1;

        // Calculate the number of sub jobs (i.e. check if the configured number of sub jobs is too big).
        i_numberOfSubJobs =
            (i_numberOfSubJobs > l_totNumberOfCustIds  ?  l_totNumberOfCustIds : i_numberOfSubJobs);

        // Calculate the range (number of cust ids) for each sub job.
        l_rangeSize = l_totNumberOfCustIds / i_numberOfSubJobs;
        
        // Calculate the 'rest' number of cust ids.
        l_rest = l_totNumberOfCustIds % i_numberOfSubJobs;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10126,
                            this,
                            "start cust id = " + l_startRange + ",  highest cust id = " + l_highestCustId +
                            ",  tot number of cust ids = " + l_totNumberOfCustIds + 
                            ",  number of sub jobs = " + i_numberOfSubJobs +
                            ",  range size = " + l_rangeSize + ",  rest = " + l_rest);

        }

        for (int i = 0; i < i_numberOfSubJobs; i++)
        {
            l_startAndEndRange = new long[2];

            // Calculate the end range for the current sub job.
            l_endRange = l_startRange + l_rangeSize - 1;
            if (l_rest > 0)
            {
                l_endRange++;
                l_rest--;
            }

            // Put start and end range into the String array, and add the String array into the ArrayList.
            l_startAndEndRange[0] = l_startRange;
            l_startAndEndRange[1] = l_endRange;
            l_subJobRanges.add(l_startAndEndRange);
            
            // Set the next start range.
            l_startRange = l_endRange + 1;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10130,
                this,
                "Leaving " + C_METHOD_getSubJobRanges);
        }
        
        return l_subJobRanges;
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_getRecoverySubJobRanges = "getRecoverySubJobRanges";
    /**
     * Returns the sub-job ranges and the corresponding sub job id as a <code>Collection</code> of 
     * <code>String</code> arrays when running in recovery mode.
     * A sub-job range <code>String</code> array  in recovery mode consists of three elements:
     * [start_range, end_range, current_sub_job_id].
     * 
     * @return  the sub-job ranges and the corresponding sub job id as a <code>Collection</code> of 
     *          <code>String</code> arrays.
     * @throws PpasServiceException If the scheduled job encounters a logic problem.
     */
    private Collection getRecoverySubJobRanges() throws PpasServiceException
    {
        ArrayList                 l_subJobRanges              = null;
        long[]                    l_startAndEndRange          = null;
        BatchJobDataSet           l_batchJobDataSet           = null;
        BatchJobData              l_batchJobData              = null;
        BatchSubJobControlDataSet l_batchSubJobControlDataSet = null;
        BatchSubJobControlData[]  l_batchSubJobControlDataArr = null;
        BatchSubJobControlData    l_batchSubJobControlData    = null;
        boolean                   l_subJobsCompleted          = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10140,
                this,
                "Entering " + C_METHOD_getRecoverySubJobRanges);
        }

        l_subJobRanges = new ArrayList();

        l_batchJobDataSet = i_ppasBatchControlService.getLastRunJobDetails(null,
                                                                           i_batchJobType,
                                                                           i_isApiTimeout);

        // Check if any batch job data was found.
        if (l_batchJobDataSet == null  ||  l_batchJobDataSet.getDataSet().size() == 0)
        {
            // No batch job data was found, return an empty Collection.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10140,
                                this,
                                "No last master job details was found, nothing to do.");
            }
            return l_subJobRanges;
        }

        // Get the batch job data, it should only be one object in the batch job data set.
        l_batchJobData = (BatchJobData)l_batchJobDataSet.getDataSet().elementAt(0);
        if (l_batchJobData.getBatchJobStatus().equals(BatchConstants.C_BATCH_STATUS_COMPLETED))
        {
            // This batch job is already completed, nothing more to do.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10140,
                                this,
                                "This master job was already completed, nothing to do.");
            }
            return l_subJobRanges;
        }

        // Get info about the sub-jobs that corresponds to the current master job.
        l_batchSubJobControlDataSet = 
            i_ppasBatchControlService.getAllSubJobsDetails(null,
                                                           Long.parseLong(l_batchJobData.getJsJobId()),
                                                           i_isApiTimeout);
        if (l_batchSubJobControlDataSet == null  ||  l_batchSubJobControlDataSet.getDataSet().size() == 0)
        {
            // No batch sub-jobs were found, throw a PpasServiceException(?).
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10140,
                                this,
                                "No sub jobs were found, nothing to do.");
            }
            return l_subJobRanges;
        }

        l_batchSubJobControlDataArr =
            new BatchSubJobControlData[ l_batchSubJobControlDataSet.getDataSet().size() ];
        l_batchSubJobControlDataSet.getDataSet().toArray(l_batchSubJobControlDataArr);

        l_subJobsCompleted = true;
        for (int i = 0; i < l_batchSubJobControlDataArr.length; i++)
        {
            l_batchSubJobControlData = l_batchSubJobControlDataArr[i];
            if (l_batchSubJobControlData.getStatus() != BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0))
            {
                // A not completed sub-job is found.
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10141,
                        this,
                        "A not completed sub-job is found, js job id: " + 
                        l_batchSubJobControlData.getJsJobId());
                }
                l_subJobsCompleted = false;
                l_startAndEndRange = new long[3];

                // Use original start- and stop- range, the SQL statement will select the
                // correct records anyway.
                // The "lastPrecessedCustId" cannot be trusted if more than on processor
                // threads are used.
                l_startAndEndRange[0] = l_batchSubJobControlData.getStartCustId();
                l_startAndEndRange[1] = l_batchSubJobControlData.getEndCustId();
                l_startAndEndRange[2] = l_batchSubJobControlData.getSubJobId();
                l_subJobRanges.add(l_startAndEndRange);
            }
        }

        if (l_subJobsCompleted)
        {
            // No un-completed sub jobs found, assume batch is completed.
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10145,
                    this,
                    "No un-completed sub jobs found, assume batch is completed.");
            }

            l_batchJobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_COMPLETED);
            i_ppasBatchControlService.updateJobDetails(null, l_batchJobData, i_isApiTimeout);
        }
        else
        {
            // There is at least one sub job to be recovered.

            // Since we need the disconnection date as a sub job parameter the 'i_disconnectionDate' variable
            // is set to the saved (in the database) disconnection date. 
            i_disconnectionDate = l_batchJobData.getExtraData1();

            // Since we need the old execution date as a sub job parameter the 'i_executionDateTime' variable
            // is set to the saved (in the database) execution date. 
            i_executionDateTime = new PpasDateTime(l_batchJobData.getExecutionDateTime());
            
            // Since we need to pass the current recovered batch job's js job id when submitting a sub job
            // the value of the 'i_jsJobId' variable has to be retrieved from the 'BatchJobData' object.
            i_jsJobId = Long.parseLong(l_batchJobData.getJsJobId());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10150,
                this,
                "Leaving " + C_METHOD_getRecoverySubJobRanges);
        }
        return l_subJobRanges;
    }


    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_submitJob = "submitJob";
    /**
     * Submits the job with the given job type, start and end range, and returns the js job id for the
     * submitted job.
     * 
     * @param p_startRange  the start range.
     * @param p_endRange    the end range.
     * @param p_subJobType  the job type.
     * @param p_subJobId    the sub job id.
     *
     * @return   the js job id for the submitted job.
     * 
     * @throws JobActionException    if it fails to submit the job.
     * @throws PpasServiceException  if it fails to submit the job.
     */
    private long submitJob(long p_startRange, long p_endRange, String p_subJobType, int p_subJobId)
        throws JobActionException, PpasServiceException
    {
        JsClient  l_jsClient     = null;
        JsRequest l_jsRequest    = null;
        HashMap   l_params       = null;
        String    l_recoveryFlag = null;
        String    l_jsJobIdStr   = null;
        long      l_jsJobId      = -1;
        String    l_opId         = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10160,
                this,
                "Entering " + C_METHOD_submitJob);
        }

        // Print out given parameters.
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10162,
                this,
                "Start range = " + p_startRange + ",  endRange = " + p_endRange + 
                ",  subJobType = " + p_subJobType + ",  subJobId = " + p_subJobId);
        }

        l_jsClient = super.getJsClient();
        if (l_jsClient == null)
        {
            PpasSoftwareException l_ppasSwEx =
                new PpasSoftwareException(C_CLASS_NAME,
                                          C_METHOD_submitJob,
                                          10164,
                                          this,
                                          null,
                                          0L,
                                          SoftwareKey.get().invalidVariableValue("JsClient", "null"));
            i_logger.logMessage(l_ppasSwEx);
            throw l_ppasSwEx;
        }

        // Get the op id.
        l_opId = super.getSubmitterOpid();

        // Create the JsRequest object.
        l_jsRequest = new JsRequest(l_opId, super.getSubmittersNode());

        // Create the map object, and add all necessary parameters.
        l_params    = new HashMap();
        l_params.put(BatchConstants.C_KEY_BATCH_JOB_TYPE, p_subJobType);
        l_params.put(BatchConstants.C_KEY_START_RANGE, Long.toString(p_startRange));
        l_params.put(BatchConstants.C_KEY_END_RANGE, Long.toString(p_endRange));
        l_params.put(BatchConstants.C_KEY_EXECUTION_DATE_TIME, i_executionDateTime.toString());
        l_params.put(BatchConstants.C_KEY_DISCONNECTION_DATE, i_disconnectionDate);
        l_params.put(BatchConstants.C_KEY_SUB_JOB_ID, Integer.toString(p_subJobId));
        l_params.put(BatchConstants.C_KEY_MASTER_JS_JOB_ID, Long.toString(i_jsJobId));
        l_recoveryFlag = 
            i_recovery  ?  BatchConstants.C_VALUE_RECOVERY_MODE_ON : BatchConstants.C_VALUE_RECOVERY_MODE_OFF;
        l_params.put(BatchConstants.C_KEY_RECOVERY_FLAG, l_recoveryFlag);

        // Print out the parameters submitted to the sub job.
        Set l_entrySet = l_params.entrySet();
        Iterator l_entryIter = l_entrySet.iterator();
        while (l_entryIter.hasNext())
        {
            Map.Entry l_entry = (Entry)l_entryIter.next();
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10166,
                    this,
                    "key: '" + l_entry.getKey() + "',  value: '" + l_entry.getValue() + "'");
            }
        }

//            l_jsJobId = l_jsClient.scheduleJob(l_jsRequest, p_subJobType, l_params);
        // A work-around since the above method throws a 'java.lang.NullPointerException'.
        // The execution date and time needs to be in the past.
        PpasDateTime l_execDateTime = DatePatch.getDateTimeNow();
        l_execDateTime.add(PpasDate.C_FIELD_HOUR, -1);
        l_jsJobIdStr = l_jsClient.scheduleJob(l_jsRequest, p_subJobType, l_params, l_execDateTime, true);
        l_jsJobId = Long.parseLong(l_jsJobIdStr);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10170,
                this,
                "Leaving " + C_METHOD_submitJob + ", Scheduled js job id: " + l_jsJobId);
        }
        
        return l_jsJobId;
    }
    
    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_preProcessing = "preProcessing";
    /**
     * Performs pre-processing before sub-jobs are submitted.
     * 
     * @throws PpasServiceException  if pre-processing fails.
     */
    private void preProcessing()
        throws PpasServiceException
    {
        MasterDbBatchService  l_isApi                 = null;
        DisconBatchRecordData l_record                = null;
        BatchJobDataSet       l_batchJobDataSet       = null;
        BatchJobData          l_batchJobData          = null;
        PpasDateTime          l_lastExecutionDateTime = null;
        StringBuffer          l_tmp                   = null;
        StringBuffer          l_outputLine            = null;
        String                l_sequenceNumber        = null;
        String                l_outputFileDirectory   = null;
        String                l_outputFileFullPath    = null;
        BufferedWriter        l_fileWriter            = null;
        int                   l_counter               = 0;
        String                l_disconnectionCodes    = null;
        boolean               l_recordFiltered        = false;
        PpasDateTime          l_discDate              = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10170,
                this,
                "Entering " + C_METHOD_preProcessing);
        }
        
        //Verify that this is BatchDisconnectionMaster. i_batchSubJobType
        if ((i_batchSubJobType != null)
            && (!i_batchSubJobType.equals(BatchConstants.C_SUB_JOB_TYPE_BATCH_DISCONNECTION)))
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END,
                    C_CLASS_NAME,
                    10170,
                    this,
                    "Leaving " + C_METHOD_preProcessing + " no pre-processing required for " +
                    i_batchSubJobType );
            }
            // Pre-processing not required - return.
            return;
        }
        
        // Get file sequence number to use.
        l_sequenceNumber = getFileSequenceNumber();
        if (l_sequenceNumber == null)
        {
            throw new PpasServiceFailedException( C_CLASS_NAME,
                                                  C_METHOD_preProcessing,
                                                  10000,
                                                  this,
                                                  null,
                                                  0,
                                                  ServiceKey.get().generalFailure());
        }
        // Get the output file directory
        l_outputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);
        
        // Create the full path filename for the output-file 
        l_tmp = new StringBuffer();
        l_tmp.append(l_outputFileDirectory);
        l_tmp.append("/");
        l_tmp.append(C_PREFIX_OUTPUT_FILE_NAME);        
        l_tmp.append(DatePatch.getDateTimeNow().toString_yyyyMMdd());
        l_tmp.append("_");
        l_tmp.append(l_sequenceNumber );
        l_tmp.append(C_SUFFIX_OUTPUT_FILE_NAME);

        l_outputFileFullPath = l_tmp.toString();
        
        //Create the file based on the absolute filename
        try
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END,
                    C_CLASS_NAME,
                    10170,
                    this,
                    "Create file " + l_outputFileFullPath + " in " + C_METHOD_preProcessing);
            }
            l_fileWriter = new BufferedWriter(new FileWriter(l_outputFileFullPath));
        }
        catch (IOException l_ie)
        {
            i_logger.logMessage(new LoggableEvent("BatchMaster:preProcessing: Exception "
                    + "couldn't create the file " + l_ie, LoggableInterface.C_SEVERITY_ERROR));
            if (l_fileWriter != null)
            {
                l_fileWriter = null;

            }
            throw new PpasServiceFailedException( C_CLASS_NAME,
                                                  C_METHOD_preProcessing,
                                                  10000,
                                                  this,
                                                  null,
                                                  0,
                                                  ServiceKey.get().generalFailure(),
                                                  l_ie);
        }
        
        // Get details from last run. 
        l_batchJobDataSet = i_ppasBatchControlService.getLastRunJobDetails(null,
                                                                           i_batchJobType,
                                                                           i_isApiTimeout);
        // Check if any batch job data was found.
        if (l_batchJobDataSet == null  ||  l_batchJobDataSet.getDataSet().size() == 0)
        {
            // No batch job data was found.
            // BatchMaster disconnection has never been run before - use dummy date.
            l_lastExecutionDateTime = new PpasDateTime("19900101 00:00:00");
        }
        else
        {
            // Get the batch job data, it should only be one object in the batch job data set.
            l_batchJobData = (BatchJobData)l_batchJobDataSet.getDataSet().elementAt(0);
            // Get the executionDateTime from that run.
            l_lastExecutionDateTime = new PpasDateTime(l_batchJobData.getExecutionDateTime());
        }

        //Instantiate batch ISIL passing those two times
        l_isApi = initISAPI(l_lastExecutionDateTime, i_executionDateTime);
        if (l_isApi == null)
        {
            throw new PpasServiceFailedException( C_CLASS_NAME,
                                                  C_METHOD_preProcessing,
                                                  10000,
                                                  this,
                                                  null,
                                                  0,
                                                  ServiceKey.get().generalFailure());
        }

        // Get list of Disconnection Reason codes to use for filtering records.
        l_disconnectionCodes = i_properties.getTrimmedProperty(
                                                    BatchConstants.C_FILTER_DISCONNECTION_REASON_CODES);
        

        //Loop the result and write to file
        try //This 'try' clause is introduced to make it possible to add a 'finally' clause.
        {
            do
            {
                // Get the next record - null indicates no more to fecth 
                try
                {
                    l_record = (DisconBatchRecordData)l_isApi.fetch();
                }
                catch (PpasServiceException l_ppasServEx)
                {
                    throw new PpasServiceFailedException( C_CLASS_NAME,
                                                          C_METHOD_preProcessing,
                                                          10000,
                                                          this,
                                                          null,
                                                          0,
                                                          ServiceKey.get().generalFailure(),
                                                          l_ppasServEx);
                }
                if (l_record == null)
                {
                    break;
                }

                // Filter any record that has a Disconnection Reason code that
                // matches the criteria specified in the properties file.
                l_recordFiltered = false;

                if ((l_disconnectionCodes != null) &&
                	(l_record.getDisconnectReason() != null) &&
                    (l_disconnectionCodes.indexOf(l_record.getDisconnectReason()) != -1))
                {
                    l_recordFiltered = true;
                }
                    
                if (!l_recordFiltered)
                {
                    // Okay, we got a record - format and write to output file.
                    try
                    {
                        l_outputLine = new StringBuffer();
                        // Add the MSISDN...
                        l_outputLine.append(l_record.getMsisdn());
                        
                        // ...pad with space...
                        for(int i = 0; i < (C_MSISDN_LENGTH - l_record.getMsisdn().length()); i++ )
                        {
                            l_outputLine.append(" ");
                        } 

                        // ...add disconnection reason...
                        l_outputLine.append(l_record.getDisconnectReason());

                        if (i_outputDiscDate)
                        {
                            // ...add disconnection date...
                            l_discDate = new PpasDateTime(l_record.getDisconnectionDate());
                            l_outputLine.append(l_discDate.toString_yyyyMMddHHmmss());
                        }
                        
                        // ...and write to file.
                        l_fileWriter.write(l_outputLine.toString());
                        l_fileWriter.newLine();
                        
                        // Don't flush every time. 100 interval is resonable.
                        l_counter++;
                        if ( (l_counter % 100) == 0)
                        {
                            l_fileWriter.flush();
                        }
                    }
                    catch (IOException l_ioEx)
                    {
                        throw new PpasServiceFailedException( C_CLASS_NAME,
                                                              C_METHOD_preProcessing,
                                                              10000,
                                                              this,
                                                              null,
                                                              0,
                                                              ServiceKey.get().generalFailure(),
                                                              l_ioEx);
                    }
                }
            }
            while(true);
        }
        finally // This 'finally' clause is introduced to make it possible to close the IS API object (and
        {       // restore the used JDBC connection to the connection pool) even if an Exception is thrown.
            if (l_isApi != null)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_END,
                                    C_CLASS_NAME,
                                    10170,
                                    this,
                                    C_METHOD_preProcessing + " -- Close the IS API (restore the JDBC conn.)");
                }
                l_isApi.close();
                l_isApi = null;
            }
        }
        
        //Close the file
        try
        {
            l_fileWriter.flush();
            l_fileWriter.close();
        }
        catch (IOException l_ioEx)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END,
                    C_CLASS_NAME,
                    10170,
                    this,
                    "Exception caught when flushing/closing file in " + C_METHOD_preProcessing);
            }
            throw new PpasServiceFailedException( C_CLASS_NAME,
                                                  C_METHOD_preProcessing,
                                                  10000,
                                                  this,
                                                  null,
                                                  0,
                                                  ServiceKey.get().generalFailure(),
                                                  l_ioEx);
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10170,
                this,
                "Leaving " + C_METHOD_preProcessing + " : " +
                l_counter + " records processed");
        }
        
        return;
    }
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_initISAPI = "initISAPI";    
    /**
     * This method instantiate and opens the BatchISIL Service.
     * 
     * @param p_startDate    the start date to use for query in CUDI.
     * @param p_stopDate     the stop date to use for query in CUDI.
     *
     * @return   the MasterDbBatchService used to fecth from CUDI.
     */
    private MasterDbBatchService initISAPI(PpasDateTime p_startDate, PpasDateTime p_stopDate)
    {
        PpasRequest          l_request         = null;
        PpasSession          l_session         = null;
        MasterDbBatchService l_isApi           = null;
        String               l_tmpFetchSize    = null;
        int                  l_fetchSize       = 20; // Use 20 as default if not configured
        String[]             l_inputParameters = new String[] {p_startDate.toString(), p_stopDate.toString()};
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                "Entering " + C_METHOD_initISAPI + " with " + 
                p_startDate.toString() + " and " + p_stopDate.toString());
        }

        // Get the fetch-size from property
        l_tmpFetchSize = i_properties.getProperty(BatchConstants.C_FETCH_ARRAY_SIZE);
        if ( l_tmpFetchSize != null )
        {
            l_fetchSize = Integer.valueOf(l_tmpFetchSize).intValue();
        }
        
        // Create and open the Batch ISIL Service Component
        l_session = new PpasSession();
        l_session.setContext(this.i_ppasContext);
        
        l_request = new PpasRequest(l_session);
        l_isApi   = new MasterDbBatchService(l_request, i_logger, i_ppasContext );

        // Call open on the API component
        try
        {
            l_isApi.open(l_inputParameters, l_fetchSize );
        }
        catch (PpasServiceException e)
        {
            
            i_logger.logMessage( e );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "PpasSqlException: " + C_METHOD_initISAPI );
            }
            l_isApi = null;
            e.printStackTrace();
        }
        catch (SecurityException e)
        {
            
            i_logger.logMessage( new LoggableEvent( "SecurityException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "SecurityException: " + C_METHOD_initISAPI );
            }
            l_isApi = null;
            e.printStackTrace();
        }
        catch (IllegalArgumentException e)
        {
           
            i_logger.logMessage( new LoggableEvent( "IllegalArgumentException",
                                 LoggableInterface.C_SEVERITY_ERROR) );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10530,
                    this,
                    "IllegalArgumentException: " + C_METHOD_initISAPI );
            }
            l_isApi = null;
            e.printStackTrace();
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_initISAPI);
        }

        return l_isApi;
    } // End of init()

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_getFileSequenceNumber = "getFileSequenceNumber";
    /**
     * Returns a file sequence number which is obtained from the database.
     * 
     * @return a file sequence number obtained from the database.
     */
    private String getFileSequenceNumber()
    {
        String                    l_seqNumberStr              = null;
        StringBuffer              l_tmpSeqNumber              = null;
        long                      l_seqNumber                 = 0;
        long                      l_timeout                   = 0;
        String                    l_batchJobType              = null;
        PpasDate                  l_currentDate               = null;
        PpasDailySequencesService l_ppasDailySequencesService = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10440,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getFileSequenceNumber);
        }

        l_currentDate  = DatePatch.getDateToday();
        l_batchJobType = i_batchSubJobType;
        l_timeout      = super.i_properties.getLongProperty(BatchConstants.C_TIMEOUT);
        l_ppasDailySequencesService = 
            new PpasDailySequencesService(null, this.i_logger, this.i_ppasContext);

        try
        {
            l_seqNumber = 
                l_ppasDailySequencesService.getNextSequenceNumber(null,
                                                                  l_timeout,
                                                                  l_batchJobType,
                                                                  l_currentDate);
            l_seqNumberStr = String.valueOf(l_seqNumber);
            l_tmpSeqNumber = new StringBuffer();
            for ( int i = 0; i < C_LENGTH_FILE_SEQUENCE_NUMBER - l_seqNumberStr.length(); i++ )
            {
                l_tmpSeqNumber.append("0");
            }
            l_tmpSeqNumber.append(l_seqNumberStr);
            l_seqNumberStr = l_tmpSeqNumber.toString();
        }
        catch (PpasServiceException p_ppasServExe)
        {
            l_seqNumberStr = null;
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10460,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getFileSequenceNumber +
                " return sequence " + l_seqNumberStr);
        }
        return l_seqNumberStr;
    }

} // End of public class BatchMaster
////////////////////////////////////////////////////////////////////////////////
//                     End of file
////////////////////////////////////////////////////////////////////////////////}

