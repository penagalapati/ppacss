////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromPlChBatchProcessor.java
//      DATE            :       Unknown
//      AUTHOR          :       Unknown
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       This class is responsible for the actual Promotion insert or update in the
//                              batch process.
//
////////////////////////////////////////////////////////////////////////////////
//          CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 08/12/05 | K Goswami  | Changed DateTime to Date        | PpacLon#1058 
//          |            |                                 | CrRr_1058_7237_PromoAllocData.doc
//----------+------------+---------------------------------+--------------------
// 19.01.06 | MAGray     | Change to accept  boolean when  | PpacLon#1954/7831
//          |            | adding                          |
//----------+------------+---------------------------------+-------------------
// 20/05/10 | Siva Sanker| Promotion Plan change should not| PpacBan#3688/13752
//          | Reddy      | be changed for a NPC cut over   |
//          |            | MSISDN                          |
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchreader.PromPlChBatchReader;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.CustPromoAllocData;
import com.slb.sema.ppas.common.dataclass.CustPromoAllocDataSet;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.PromPlChBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasCustPromoAllocService;
import com.slb.sema.ppas.is.isapi.PpasService;
import com.slb.sema.ppas.is.isil.primitiveservice.CustPromoAllocPrimitiveService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for the actual Promotion insert or update in the batch process.
 */
public class PromPlChBatchProcessor extends BatchProcessor
{
    //-------------------------------------------------------------------------
    // Private constants.
    //-------------------------------------------------------------------------
    /** Constant holding the name of this class. Value is {@value}. */
    private static final String       C_CLASS_NAME                = "PromPlChBatchProcessor";

    /**
     * Constant holding the flag indicating whether the promotion should start immediately.
     * Value is {@value}.
     */
    private static final boolean      C_BATCH_START_NOW_FLAG      = false;

    /**
     * Constant holding the flag indicating whether the promotion should end immediately.
     *  Value is {@value}.
     */
    private static final boolean      C_BATCH_END_NOW_FLAG        = false;

    /** Error code. Value is {@value}. */
    public static final String        C_ERROR_PROM_SYNCH_REQUIRED = "05";

    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /** The <code>IS API</code> reference. */
    private PpasCustPromoAllocService i_ppasCustPromoAllocService = null;

    /** The <code>Session</code> object. */
    private PpasSession               i_ppasSession               = null;

    /**
     * Constructs an instance of this <code>PromPlChBatchProcessor</code> class using the specified batch
     * controller, input data queue and output data queue.
     * @param p_ppasContext the PpasContext reference
     * @param p_logger the logger
     * @param p_batchController the batch controller
     * @param p_inQueue the input data queue
     * @param p_outQueue the output data queue
     * @param p_params the start process paramters
     * @param p_properties <code>PpasProperties </code> for the batch subsystem.
     */
    public PromPlChBatchProcessor(PpasContext p_ppasContext,
            Logger p_logger,
            BatchController p_batchController,
            SizedQueue p_inQueue,
            SizedQueue p_outQueue,
            Map p_params,
            PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);

        PpasRequest l_ppasRequest = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10000,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_ppasSession = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest = new PpasRequest(i_ppasSession);
        i_ppasCustPromoAllocService = new PpasCustPromoAllocService(l_ppasRequest, p_logger, p_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10010,
                            this,
                            "Constructed " + C_CLASS_NAME);
        }
    }

    /** Constant holding the name of this method. Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";

    /**
     * Processes the given <code>BatchDataRecord</code> and returns it. If the process succeeded the
     * <code>BatchDataRecord</code> is updated with information needed by the writer component before
     * returning it. If the process fails but the record shall be re-processed at the end of this process, the
     * record will be put in the retry queue and this method returns <code>null</code>.
     * @param p_record the <code>BatchDataRecord</code>.
     * @return the given <code>BatchDataRecord</code> updated with info for the writer component, or
     * <code>null</code>.
     * @throws PpasServiceException if an unrecoverable error occurs while installing a subscriber.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        PromPlChBatchRecordData l_promPlChBatchRecordData = null;
        CustPromoAllocDataSet l_custPromoAllocDataSet = null;
        String l_opId = null;
        String l_custId = null;
        PpasRequest l_ppasRequest = null;
        String l_promoPlanCode = null;
        PpasDateTime l_promoStartDateTime = null;
        PpasDateTime l_promoEndDateTime = null;
        OldPromoPlan l_oldPromoPlan = null;
        Msisdn       l_msisdn       = null;
        PpasAccountService l_ppasAccountService = null;
        BasicAccountData  l_basicAccountData= null;
        long         l_custStatusFlags = 0;
        long         l_timeout         = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10020,
                            this,
                            "Entering " + C_METHOD_processRecord);
        }

        l_promPlChBatchRecordData = (PromPlChBatchRecordData)p_record;

        if (!l_promPlChBatchRecordData.isCorruptLine())
        {
            l_opId = i_batchController.getSubmitterOpid();
            l_custId = l_promPlChBatchRecordData.getCustId();
            l_ppasRequest = new PpasRequest(i_ppasSession, l_opId, l_custId);           

            try
            {   
                l_msisdn = l_promPlChBatchRecordData.getMsisdn();
                
                l_ppasAccountService = new PpasAccountService(l_ppasRequest, i_logger, i_ppasContext);
                
                // Do not retrieve subordinates or disconnected subscribers.
                l_custStatusFlags = PpasService.C_FLAG_BLOCK_TRANS_ALL;
                
                l_basicAccountData = l_ppasAccountService.getBasicData(
                        l_ppasRequest, l_custStatusFlags, l_timeout);
                
                //To check if the msisdn is Number Plan Cutovered (NPC) 
                if(!l_msisdn.equals(l_basicAccountData.getMsisdn()))
                {    
                    // Create 'warning' level exception, log and throw it.
                    PpasServiceException p_psExe = new PpasServiceFailedException
                                        (C_CLASS_NAME,
                                         C_METHOD_processRecord,
                                         12557,
                                         this,
                                         l_ppasRequest,
                                         0,
                                         ServiceKey.get().msisdnNotExists(l_msisdn));                        
                    i_logger.logMessage (p_psExe);
                    throw (p_psExe);
                }
                
                l_custPromoAllocDataSet = i_ppasCustPromoAllocService
                        .getAllPromos(l_ppasRequest,
                                      i_isApiTimeout,
                                      CustPromoAllocPrimitiveService.C_FLAG_IGNORE_EXPIRED);

                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10025,
                                    this,
                                    "Promotion Set Returned:  " + l_custPromoAllocDataSet.toVerboseString());
                }

                l_promoPlanCode = l_promPlChBatchRecordData.getPromoPlanCode();
                l_promoStartDateTime = l_promPlChBatchRecordData.getPromoStartDateTime();
                l_promoEndDateTime = l_promPlChBatchRecordData.getPromoEndDateTime();

                l_oldPromoPlan = checkIfUpdatedPlan(l_custPromoAllocDataSet,
                                                    l_promoPlanCode,
                                                    l_promoStartDateTime);

                if (l_oldPromoPlan != null)
                {
                    // Promotion is updated.
                    i_ppasCustPromoAllocService.updatePromo(l_ppasRequest,
                                                            i_isApiTimeout,
                                                            l_promoPlanCode,
                                                            l_oldPromoPlan.getPromoStartDateTime(),
                                                            l_promoEndDateTime,
                                                            l_oldPromoPlan.getPromoPlanCode(),
                                                            l_oldPromoPlan.getPromoStartDateTime(),
                                                            l_oldPromoPlan.getPromoEndDateTime(),
                                                            C_BATCH_START_NOW_FLAG,
                                                            C_BATCH_END_NOW_FLAG);
                }
                else
                {
                    // Promotion is inserted.
                    i_ppasCustPromoAllocService
                            .addPromo(l_ppasRequest,
                                      i_isApiTimeout,
                                      l_promoPlanCode,
                                      l_promoStartDateTime.getPpasDate(),
                                      l_promoEndDateTime.getPpasDate(),
                                      false);
                }
            }
            catch (PpasServiceException l_psExe)
            {
                if (l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_END_DATE_BEFORE_START_DATE)
                        || l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_DATE_BEFORE_TODAYS_DATE)
                        || l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_DATE_CANNOT_BE_BLANK)
                        || l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_DATE_OUTSIDE_PROMOTION_LIMITS)
                        || l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_PROMO_ALLOC_OVERLAP_NOT_ALLOWED))
                {
                    l_promPlChBatchRecordData.setErrorCode(PromPlChBatchReader.C_ERROR_INVALID_DATES);
                }
                else
                {
                    if (l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_NOT_MASTER)
                            || l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_ACCOUNT_NOT_EXISTS)
                            || l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_INVALID_INPUT_CUSTID)
                            || l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_DISCONNECTED_ACCOUNT)
                            || l_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS))
                    {
                        l_promPlChBatchRecordData.setErrorCode(PromPlChBatchReader.C_ERROR_INVALID_MSISDN);
                    }
                    else
                    {
                        if (l_psExe.getMsgKey()
                                .equals(PpasServiceMsg.C_KEY_PROMOTION_SYNCHRONIZATION_REQUIRED))
                        {
                            l_promPlChBatchRecordData.setErrorCode(C_ERROR_PROM_SYNCH_REQUIRED);
                        }

                        else
                        {
                            throw l_psExe;
                        }
                    }
                }
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10030,
                            this,
                            "Leaving " + C_METHOD_processRecord);
        }

        return l_promPlChBatchRecordData;
    }

    /** Method name used for calls to Middleware. Value is {@value}. */
    private static final String C_METHOD_checkIfUpdatedPlan = "checkIfUpdatedPlan";

    /**
     * Checks if the batch Promotion Plan Change is an update or an insert. Searches the CustPromoAllocDataSet
     * for the Promotion Code and start date. If there is match, batch operation is an update and the old
     * promotion values are returned, otherwise the operation is an insert and null is returned.
     * @param p_custPromoAllocDataSet Set of Promotion Plans.
     * @param p_promoPlanCode Promotion Plan Code.
     * @param p_promoStartDateTime Promotion start date.
     * @return Values of old Promotion Plan if there is one, otherwise null.
     */
    protected OldPromoPlan checkIfUpdatedPlan(CustPromoAllocDataSet p_custPromoAllocDataSet,
                                              String p_promoPlanCode,
                                              PpasDateTime p_promoStartDateTime)
    {
        OldPromoPlan l_oldPromoPlan = null;
        Vector l_custAllocV;
        boolean l_found = false;
        CustPromoAllocData l_currAlloc = null;
        PpasDate l_currAllocDate = null;
        PpasDate l_batchInputDate = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11700,
                            this,
                            "Entered " + C_METHOD_checkIfUpdatedPlan);
        }

        l_batchInputDate = p_promoStartDateTime.getPpasDate();
        l_custAllocV = p_custPromoAllocDataSet.getDataSetV();

        for (int l_index = 0; (l_index < l_custAllocV.size()) && (!l_found); l_index++)
        {
            l_currAlloc = (CustPromoAllocData)l_custAllocV.elementAt(l_index);
            l_currAllocDate = l_currAlloc.getStartDateTime().getPpasDate();

            if (l_currAlloc.getPromotion().equals(p_promoPlanCode)
                    && l_currAllocDate.equals(l_batchInputDate))
            {
                l_found = true;
            }
        }

        if (l_found)
        {
            l_oldPromoPlan = new OldPromoPlan(l_currAlloc.getPromotion(),
                                              l_currAlloc.getStartDateTime(),
                                              l_currAlloc.getEndDateTime());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11790,
                            this,
                            "Leaving " + C_METHOD_checkIfUpdatedPlan);
        }

        return l_oldPromoPlan;
    } // end of checkIfUpdatedPlan

    /**
     * Inner class.
     */
    private class OldPromoPlan
    {
        //---------------------------------------------------------------------
        // Class level constants
        //---------------------------------------------------------------------

        /** Constant holding the name of this class. Value is {@value}. */
        private static final String C_INNER_CLASS_NAME         = "OldPromoPlan";

        //---------------------------------------------------------------------
        // Instance attributes
        //---------------------------------------------------------------------
        /** Promotion plan code. */
        private String                      i_promoPlanCode      = null;

        /** Promotion start date. */
        private PpasDateTime                i_promoStartDateTime = null;

        /** Promotion end date. */
        private PpasDateTime                i_promoEndDateTime   = null;

        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------
        /**
         * Creates an OldPromoPlan with the old promotion values.
         * @param p_promoPlanCode Promotion plan code.
         * @param p_promoStartDateTime Promotion start date.
         * @param p_promoEndDateTime Promotion end date.
         */
        public OldPromoPlan(String p_promoPlanCode,
                            PpasDateTime p_promoStartDateTime,
                            PpasDateTime p_promoEndDateTime)

        {
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_START,
                                C_INNER_CLASS_NAME,
                                11000,
                                this,
                                "Constructing " + C_INNER_CLASS_NAME);
            }

            i_promoPlanCode = p_promoPlanCode;
            i_promoStartDateTime = p_promoStartDateTime;
            i_promoEndDateTime = p_promoEndDateTime;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VHIGH,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_END,
                                C_INNER_CLASS_NAME,
                                11010,
                                this,
                                "Constructed " + C_INNER_CLASS_NAME);
            }

        }

        //---------------------------------------------------------------------
        // Instance methods
        //---------------------------------------------------------------------

        /**
         * Returns the PromoPlanCode.
         * @return the PromoPlanCode.
         */
        public String getPromoPlanCode()
        {
            return (i_promoPlanCode);
        }

        /**
         * Returns the PromoStartDate.
         * @return the PromoStartDate.
         */
        public PpasDateTime getPromoStartDateTime()
        {
            return (i_promoStartDateTime);
        }

        /**
         * Returns the PromoEndDate.
         * @return the PromoEndDate.
         */
        public PpasDateTime getPromoEndDateTime()
        {
            return (i_promoEndDateTime);
        }

    } // end of Inner class OldPromoPlan
}
// End of public class PromPlChBatchProcessor
////////////////////////////////////////////////////////////////////////////////
//                        End of file
////////////////////////////////////////////////////////////////////////////////}
