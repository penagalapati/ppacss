////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchKey.Java
//      DATE            :       24-Jul-2005
//      AUTHOR          :       MAGray
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Maker of Batch exception Keys.
//
////////////////////////////////////////////////////////////////////////////////////////
//              CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                           | REFERENCE
//----------+---------------+---------------------------------------+-------------------
//26/07/06  | Chris         | Added batchInvalidEndDate()           | PpacLon#2329/9464
//          | Harrison      | batchInvalidNextTierPromotionId()     | PRD_ASCS_GEN_CA_096
//          |               | batchInvalidSyfgParameter()           | 
//----------+---------------+---------------------------------------+-------------------
//21/08/06  | Chris         | Review amendments.                    | PpacLon#2329/9771
//          | Harrison      |                                       |
//----------+---------------+---------------------------------------+-------------------
//20/03/07  | Lars L.       | Improved Synch. batch error handling. | PpacLon#2076/7827
//          |               | One not used method is removed:       |
//          |               | batchInvalidDivisionTableId(...).     |
//          |               | Two new methods are added:            |
//          |               | batchInvalidDedicatedAccount(...) and |
//          |               | batchInvalidExpiryDate(...).          |
//----------+---------------+---------------------------------------+-------------------
//26/04/07  | Lars L.       | One method is removed:                | PpacLon#3033/11279
//          |               | batchInvalidIndataFilenameExtension() |
//          |               | Two new methods are added:            |
//          |               | batchInvalidIndataFilename(...)       |
//          |               | batchIllegalIndataFilenameExt(...)    |
//----------+---------------+---------------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.exceptions;

import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.ExceptionKey;
import com.slb.sema.ppas.common.exceptions.ExceptionKeyMaker;

/** Maker of Batch exception Keys. */
public class BatchKey extends ExceptionKeyMaker
{
    /** Instance of the key maker. */
    private static BatchKey i_keyMaker = new BatchKey(); 

    /** Standard constructor - do nothing but stop others doing it. */
    private BatchKey()
    {
        // Do nothing.
    }

    /** Get the instance of this class.
     * 
     * @return Instance of this class.
     */
    public static BatchKey get()
    {
        return i_keyMaker;
    }

    /** Invalid parameter.
    * 
    * @param p_name Paramater name.
    * @return Key representing this exception.
    */
    public BatchExceptionKey batchInvalidParameter(String p_name)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_PARAMETER, new Object[]{p_name});
    }

    /** Record wrong length.
     * 
     * @param p_expectedLength Expected length of record.
     * @param p_actualLength   Actual length of record.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchWrongRecordLength(int p_expectedLength, int p_actualLength)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_WRONG_RECORD_LENGTH,
                                     new Object[]{new Integer(p_expectedLength),
                                                   new Integer(p_actualLength)});
    }

    /** Parsing of MSISDN failed.
     * 
     * @param p_msisdn Mobile Number.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchParsingOfMsisdnFailed(String p_msisdn)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_PARSING_OF_MSISDN_FAILED,
                                     new Object[]{p_msisdn});
    }

    /** Unexpected non-alphanumeric string (so might not look good in a file).
     * 
     * @param p_string Odd looking string.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchNotAlphaNumeric(String p_string)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_NOT_ALPHANUMERIC,
                                     new Object[]{p_string});
    }

    /** Date format error.
     * 
     * @param p_date    Date that is attempting to be read.
     * @param p_pattern Date pattern.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchDateFormatError(String p_date, String p_pattern)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_DATE_FORMAT_ERROR,
                                     new Object[]{p_date, p_pattern});
    }

    /** Account could not be found.
     * 
     * @param p_msisdn Mobile Number.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchFailedToGetAccountData(Msisdn p_msisdn)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_FAILED_TO_GET_ACCOUNT_DATA,
                                     new Object[]{p_msisdn});
    }

    /** Promotion id not recognised.
     * 
     * @param p_promoId Promotion plan identifier.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchPromIdNotRecognised(String p_promoId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_PROM_ID_NOT_RECOGNISED,
                                     new Object[]{p_promoId});
    }

    /** Currency not recognised.
     * 
     * @param p_currency The currency code.
     * @param p_config   The configuration ("promotion plan" or "refill profile").
     * @param p_id       The id of the current configuration.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchCurrencyNotRecognised(String p_currency, String p_config, String p_id)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_CURRENCY_NOT_RECOGNISED,
                                     new Object[]{p_currency, p_config, p_id});
    }

    /**
     * The given service class is not configured in ASCS.
     * 
     * @param p_serviceClass     The service class.
     * @param p_divisionTableId  The division table id.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchServiceClassNotRecognised(String p_serviceClass, String p_divisionTableId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_SERVICE_CLASS_NOT_RECOGNISED,
                                     new Object[]{p_serviceClass, p_divisionTableId});
    }

    /**
     * Division id does not correspond to a configured division details table.
     * 
     * @param p_divisionTableId  The division table id.
     * @param p_promoId          The promotion plan identifier.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidDivisionId(String p_divisionTableId, String p_promoId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_DIVISION_ID,
                                     new Object[]{p_divisionTableId, p_promoId});
    }

    /**
     * Invalid dedicated account.
     * 
     * @param p_dedAccountId     The invalid dedicated account id.
     * @param p_serviceClassId   The service class id.
     * @param p_divisionTableId  The division table id.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidDedicatedAccount(String p_dedAccountId,
                                                          String p_serviceClassId,
                                                          String p_divisionTableId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_DEDICATED_ACCOUNT,
                                     new Object[]{p_dedAccountId, p_serviceClassId, p_divisionTableId});
    }

    /** Invalid End Date.
     * 
     * @param p_endDate   The end date.
     * @param p_startDate The start date.
     * @param p_promoId   The promotion plan identifier.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidEndDate(String p_endDate, String p_startDate, String p_promoId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_END_DATE,
                                    new Object[]{p_endDate, p_startDate, p_promoId});
    }

    /**
     * An invalid (negative) relative expiry date is found for the given dedicated account that corresponds
     * to the given division table.
     * 
     * @param p_relExpiryDate    The relative expiry date.
     * @param p_dedAccountId     The invalid dedicated account id.
     * @param p_divisionTableId  The division table id.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidExpiryDate(String p_relExpiryDate,
                                                    String p_dedAccountId,
                                                    String p_divisionTableId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_EXPIRY_DATE,
                                    new Object[]{p_relExpiryDate, p_dedAccountId, p_divisionTableId});
    }

    /** Invalid Next Tier Promotion Id.
     * 
     * @param p_nextTierPromotionId  The next tier promotion id.
     * @param p_promoPlanId          The current promotion plan id.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidNextTierPromotionId(String p_nextTierPromotionId,
                                                             String p_promoPlanId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_NEXT_TIER_PROMOTION_ID,
                                    new Object[]{p_nextTierPromotionId, p_promoPlanId});
    }

    /** Invalid Syfg Parameter.
     * 
     * @param p_destination  The syfg parameter destination.
     * @param p_name The syfg parameter name.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidSyfgParameter(String p_destination, String p_name)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_SYFG_PARAMETER,
                                    new Object[]{p_destination, p_name});
    }


    /** Failed to insert a new record into the BACO_BATCH_CONTROL table.
     * 
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchFailedInsertIntoControlTable()
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_FAILED_INSERT_INTO_CONTROL_TABLE, new Object[]{});
    }

    /**
     * Failed to get data from the BACO_BATCH_CONTROL table.
     * 
     * @param p_execDateTime  the execution date & time.
     * @param p_jsJobId       the (unique) js job id.
     * 
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchFailedToGetDataFromControlTable(String p_execDateTime, String p_jsJobId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_FAILED_GET_CONTROL_DATA,
                                     new Object[]{p_execDateTime, p_jsJobId});
    }

    /**
     * Failed to update data in the BACO_BATCH_CONTROL table.
     * 
     * @param p_execDateTime  the execution date & time.
     * @param p_jsJobId       the (unique) js job id.
     * 
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchFailedToUpdateDateControlTable(String p_execDateTime, String p_jsJobId)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_FAILED_UPDATE_CONTROL_DATA,
                                     new Object[]{p_execDateTime, p_jsJobId});
    }


    /** A non-numerical property.
     * 
     * @param p_name   Parameter name.
     * @param p_value  Parameter value.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchNonNumericalProp(String p_name, String p_value)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_NON_NUMERICAL_PROP, new Object[]{p_name, p_value});
    }

    /** A too big numerical property.
     * 
     * @param p_name   Parameter name.
     * @param p_value  Parameter value.
     * @param p_maxVal Max allowed parameter value.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchTooBigNumericalProp(String p_name, String p_value, String p_maxVal)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_TOO_BIG_VALUE_PROP,
                                     new Object[]{p_name, p_value, p_maxVal});
    }

    /** A too low numerical property.
     * 
     * @param p_name   Parameter name.
     * @param p_value  Parameter value.
     * @param p_minVal Min allowed parameter value.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchTooLowNumericalProp(String p_name, String p_value, String p_minVal)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_TOO_LOW_VALUE_PROP,
                                     new Object[]{p_name, p_value, p_minVal});
    }

    /** A mandatory property is missing.
     * 
     * @param p_name   Parameter name.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchMissingMandatoryProp(String p_name)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_MISSING_MANDATORY_PROP, new Object[]{p_name});
    }


    /** A non-numerical parameter.
     * 
     * @param p_name   Parameter name.
     * @param p_value  Parameter value.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchNonNumericalParam(String p_name, String p_value)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_NON_NUMERICAL_PARAM, new Object[]{p_name, p_value});
    }

    /** A too big numerical parameter.
     * 
     * @param p_name   Parameter name.
     * @param p_value  Parameter value.
     * @param p_maxVal Max allowed parameter value.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchTooBigNumericalParam(String p_name, String p_value, String p_maxVal)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_TOO_BIG_VALUE_PROP,
                                     new Object[]{p_name, p_value, p_maxVal});
    }

    /** A mandatory parameter is missing.
     * 
     * @param p_name   Parameter name.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchMissingMandatoryParam(String p_name)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_MISSING_MANDATORY_PARAM, new Object[]{p_name});
    }


    /** The given input data filename has an invalid prefix.
     * 
     * @param p_filename       Input data filename.
     * @param p_validPrefixes  Valid prefixes as a comma separated list.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidIndataFilenamePrefix(String p_filename, String p_validPrefixes)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_FILENAME_PREFIX,
                                     new Object[]{p_filename, p_validPrefixes});
    }


    /**
     * The given input data filename is invalid, it does NOT match the valid filename regular expression.
     * 
     * @param p_filename    The input data filename.
     * @param p_validRegEx  The regular expression that defines a valid input data filename.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidIndataFilename(String p_filename, String p_validRegEx)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_INDATA_FILENAME,
                                     new Object[]{p_filename, p_validRegEx});
    }


    /** The given input data filename has an invalid body.
     * 
     * @param p_filename           Input data filename.
     * @param p_validBodyPatterns  Valid body patterns as a comma separated list.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidIndataFilenameBody(String p_filename, String p_validBodyPatterns)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_FILENAME_BODY,
                                     new Object[]{p_filename, p_validBodyPatterns});
    }

    /** The given input data file is invalid.
     * 
     * @param p_indataFilePath  The absolute input data file path.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidIndataFile(String p_indataFilePath)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_INDATA_FILE,
                                     new Object[]{p_indataFilePath});
    }

    /** The required recovery file is invalid.
     * 
     * @param p_recoveryFilePath  The absolute recovery file path.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchInvalidRecoveryFile(String p_recoveryFilePath)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_INVALID_RECOVERY_FILE,
                                     new Object[]{p_recoveryFilePath});
    }

    /** Failed to change the input data file extension to the "in progress" extension.
     * 
     * @param p_indataFilePath         The input data file absolute path.
     * @param p_indataFileInProgrPath  The input data file "in progress" absolute path.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchFailedToRenameIndataFile(String p_indataFilePath,
                                                           String p_indataFileInProgrPath)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_FAILED_RENAME_INDATA_FILE,
                                     new Object[]{p_indataFilePath, p_indataFileInProgrPath});
    }

    /** Failed to create or open the file.
     * 
     * @param p_filePath  The file path.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchFailedToCreateOrOpenFile(String p_filePath)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_FAILED_TO_CREATE_OR_OPEN_FILE,
                                     new Object[]{p_filePath});
    }

    /** Failed to read from the file.
     * 
     * @param p_filePath  The file path.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchFailedToReadFromFile(String p_filePath)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_FAILED_TO_READ_FROM_FILE, new Object[]{p_filePath});
    }

    /** Failed to close the file.
     * 
     * @param p_filePath  The file path.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchFailedToCloseFile(String p_filePath)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_FAILED_TO_CLOSE_FILE, new Object[]{p_filePath});
    }

    /** The given input data filename has an illegal extension when a recovery file exists.
     * 
     * @param p_filenameExt    the illegal filename extension.
     * @param p_inProgressExt  the "in progress" filename extension.
     * @return Key representing this exception.
     */
    public BatchExceptionKey batchIllegalIndataFilenameExt(String p_filenameExt, String p_inProgressExt)
    {
        return new BatchExceptionKey(BatchMsg.C_KEY_BATCH_ILLEGAL_INPUT_FILENAME_EXT,
                                     new Object[]{p_filenameExt, p_inProgressExt});
    }


    /** Inner class representing a key of a Batch Exception. */ 
    public class BatchExceptionKey extends ExceptionKey
    {
        /** Standard constructor.
         * 
         * @param p_exceptionKey Key of this exception.
         * @param p_parameters   Parameters for this instance of the exception.
         */
        private BatchExceptionKey(String p_exceptionKey, Object[] p_parameters)
        {
            super(p_exceptionKey, p_parameters);
        }
    }
}
