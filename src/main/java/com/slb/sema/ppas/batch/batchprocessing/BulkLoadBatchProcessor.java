////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BulkLoadBatchProcessor
//      DATE            :       4-June-2004
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.BulkLoadBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AFBulkLoadBatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchElementResponseData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchResponseData;
import com.slb.sema.ppas.common.dataclass.MsisdnRoutingData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasMsisdnRoutingService;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for calling the appropriate IS API method to perform the
 * update of the account finder. This is acheived by calling the business service 
 * PpasBulkLoadAFService, method addSubscriber().
 */
public class BulkLoadBatchProcessor extends BatchProcessor
{
    //--------------------------------------------------------------------------
    //--  Class level constant.                                               --
    //--------------------------------------------------------------------------
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "BulkLoadBatchProcessor";
    

    //--------------------------------------------------------------------------
    //--  Class level variables.                                              --
    //--------------------------------------------------------------------------

    /** The trace print out base statement number. */
    private static int c_traceStatementNumber = 12000;


    //--------------------------------------------------------------------------
    //--  Instance variables.                                                 --
    //--------------------------------------------------------------------------

    /** The maximum chunk size. */
    private int    i_chunkSize                = 0;

    /** The chunk buffer. */
    private Vector i_chunk                    = null;
    
    /** The maximum number of parallell inner class threads. */
    private int    i_maxThreads               = 0;

    /** Signaling object. */
    private Object i_signalObj                = new Object();
    
    /** A <code>Vector</code> that contains references to the living inner class process threads. */
    private Vector i_livingIsProcessThreadVec = null;
    
    /** The inner class thread id counter. */
    private int    i_threadIdCnt              = 0;


    //--------------------------------------------------------------------------
    //--  Constructors.                                                       --
    //--------------------------------------------------------------------------

    /**
     * Constructs an instance of this <code>BulkLoadBatchProcessor</code> class using the specified
     * parameters.
     *
     * @param p_ppasContext      the PpasContext reference
     * @param p_logger           the Logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_params           the start process parameters
     * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
     */
    public BulkLoadBatchProcessor(PpasContext             p_ppasContext,
                                  Logger                  p_logger,
                                  BulkLoadBatchController p_batchController,
                                  SizedQueue              p_inQueue,
                                  SizedQueue              p_outQueue,
                                  Map                     p_params,
                                  PpasProperties          p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10001,
                            this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }


        i_threadIdCnt = 0;
        i_chunkSize  = p_properties.getIntProperty(BatchConstants.C_MAX_CHUNK_SIZE, 200);
        i_maxThreads = p_properties.getIntProperty(BatchConstants.C_MAX_NUMBER_OF_THREADS, 10);
        trace(this, "Constructor", "chunck size = " + i_chunkSize);
        trace(this, "Constructor", "max threads = " + i_maxThreads);
        
        i_livingIsProcessThreadVec = new Vector(i_maxThreads);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTED );
        }

    }


    //--------------------------------------------------------------------------
    //--  Protected instance methods.                                         --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";        
    /**
     * Processes the given <code>BatchDataRecord</code> but will always return a 'null' object.
     * The passed <code>BatchDataRecord</code> will instead be stored in an internal buffer in order to be
     * processed when the buffer reaches a configurable number of stored records.
     * 
     * @param p_record  the <code>BatchDataRecord</code>.
     * 
     * @return always returns a <code>null</code> object.
     * 
     * @throws PpasServiceException  if a fatal error occurs.
     */
    public BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        IsProcessor l_isProcessor = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11020,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }

        if (i_chunk == null)
        {
            i_chunk = new Vector();
        }

        trace(this, C_METHOD_processRecord, "Current i_chunk size     = " + i_chunk.size());
        trace(this, C_METHOD_processRecord, "Max allowed chunk size   = " + i_chunkSize);
        trace(this, C_METHOD_processRecord, "Number of living threads = " + i_maxThreads);
        trace(this, C_METHOD_processRecord, "Max no of threads        = " + i_maxThreads);

        if (!p_record.isCorruptLine())
        {
            i_chunk.add(p_record);

            if (i_chunk.size() == i_chunkSize)
            {
                if (i_livingIsProcessThreadVec.size() >= i_maxThreads)
                {
                    // Max number of threads already started, wait before starting a new thread.
                    trace(this, C_METHOD_processRecord, "Synch. before 'Wait to start new thread'.");
                    synchronized (i_signalObj)
                    {
                        try
                        {
                            trace(this, C_METHOD_processRecord, "Wait to start new thread.");
                            i_signalObj.wait();
                        }
                        catch (InterruptedException p_intEx)
                        {
                            // Nothing to do.
                            p_intEx = null;
                        }
                    }
                }
                // Construct and start the inner class.
                trace(this, C_METHOD_processRecord, "Start new thread with id = " + i_threadIdCnt);
                l_isProcessor = new IsProcessor(i_threadIdCnt++, i_chunk);
                l_isProcessor.start();
                i_livingIsProcessThreadVec.add(l_isProcessor);
                i_chunk = null;
            }
        }
        else
        {
            // A corrupt line, store it directly in the output queue.
            trace(this, "processRecord", "Corrupt line, store record immediately.");
            super.storeRecord(p_record);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_processRecord);
        }

        return null;
    } // End of processRecord()


    //--------------------------------------------------------------------------
    //--  Protected methods.                                                  --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_flush = "flush";        
    /**
     * Flushes the chunk buffer.
     * This method is called from the super class ProcessorThread just before it will end, which means that
     * this method shouldn't return until all living 'IsProcessor' Threads have ended.
     */
    protected void flush()
    {
        IsProcessor l_isProcessor = null;
        boolean     l_continue    = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11020,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_flush);
        }

        // Check that there is a chunk of un-processed records.
        if (i_chunk != null  &&  i_chunk.size() > 0)
        {
            // We don't bother to check if the max number of process threads will be exceeded since we are
            // going to create only one new process thread.
            trace(this, C_METHOD_flush, "Create and start a processing thread with id = " + i_threadIdCnt);
            l_isProcessor = new IsProcessor(i_threadIdCnt++, i_chunk);
            l_isProcessor.start();
            i_livingIsProcessThreadVec.add(l_isProcessor);
        }

        trace(this, C_METHOD_flush, "Wait for all living processing thread...");
        l_continue = true;
        while (l_continue)
        {
            try
            {
                trace(this, C_METHOD_flush, "Get the first living processing thread.");
                l_isProcessor = (IsProcessor)i_livingIsProcessThreadVec.firstElement();
            }
            catch (NoSuchElementException p_nseEx)
            {
                // Ok, no more threads to join.
                trace(this, C_METHOD_flush, "No more living processing thread to join.");
                l_continue = false;
                
                // Start the loop all over again.
                continue;
            }

            try
            {
                trace(this, C_METHOD_flush, "Join the processing thread '" +
                                            l_isProcessor.getThreadName() + "'");
                l_isProcessor.join(0L);
            }
            catch (InterruptedException p_intEx)
            {
                // No specific handling.
                p_intEx = null;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_flush);
        }
    }


    //--------------------------------------------------------------------------
    //--  Private methods.                                                    --
    //--------------------------------------------------------------------------

    /**
     * Prints trace print outs.
     * 
     * @param p_method         the name of th calling method.
     * @param p_callingObject  the calling <code>Object</code>.
     * @param p_message        the trace message to be printed.
     */
    private static void trace(Object p_callingObject, String p_method, String p_message)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_ALL,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            c_traceStatementNumber++,
                            p_callingObject,
                            "TRACE: " + p_method + " -- " + p_message);
        }
    }

    
    //--------------------------------------------------------------------------
    //--  Inner classes.                                                      --
    //--------------------------------------------------------------------------

    /**
     * The purpose of this inner class, <code>IsProcessor</code>, is to process the 
     * <code>MsisdnRoutingBatchRecordData</code> records stored in the chunk buffer passed in to the
     * Constructor.
     */
    private class IsProcessor extends ThreadObject
    {
        //----------------------------------------------------------------------
        //--  Class level constant.                                               --
        //----------------------------------------------------------------------

        /** Class name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_INNER_CLASS_NAME                          = 
            "BulkLoadBatchProcessor.IsProcessor";

        /** The error code for a service response timeout.  Value is {@value}. */
        private static final String C_ERROR_CODE_SERVICE_RESPONSE_TIMEOUT       = "03";
        
        /** The error code for 'MSISDN not exists'.  Value is {@value}. */
        private static final String C_ERROR_CODE_MSISDN_NOT_EXISTS              = "17";
        
        /** The error code for an unexpected PpasServiceException.  Value is {@value}. */
        private static final String C_ERROR_CODE_UNEXPECTED_EXCEPTION           = "99";
        
        
        //----------------------------------------------------------------------
        //--  Instance variables.                                             --
        //----------------------------------------------------------------------

        /** The current thread id. */
        private int i_threadId = 0;

        /** The IS API. */
        private PpasMsisdnRoutingService i_isApi = null;

        /** The incoming chunk buffer. */
        private Vector    i_chunkBuffer          = null;
        
        /** The IS API output buffer. */
        private ArrayList i_outputBuffer         = null;


        //----------------------------------------------------------------------
        //--  Constructors.                                                   --
        //----------------------------------------------------------------------

        /**
         * Constructs an <code>IsProcessor</code> instance using the given chunk buffer.
         * 
         * @param p_threadId     a unique thread id.
         * @param p_chunkBuffer  a <code>Vector</code> that contains a number of 
         *                       <code>MsisdnRoutingBatchRecordData</code> records to be processed.
         */
        public IsProcessor(int p_threadId, Vector p_chunkBuffer)
        {
            super();

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_START,
                                C_INNER_CLASS_NAME,
                                11000,
                                this,
                                BatchConstants.C_CONSTRUCTING);
            }

            i_threadId = p_threadId;
            i_isApi = new PpasMsisdnRoutingService(null,
                                                   BulkLoadBatchProcessor.super.i_logger,
                                                   BulkLoadBatchProcessor.super.i_ppasContext);
            this.i_chunkBuffer = p_chunkBuffer;
            i_outputBuffer = new ArrayList(p_chunkBuffer.size());

            trace(this, "Constructor", getThreadName() + ": is constructed.");

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_END,
                                C_INNER_CLASS_NAME,
                                11000,
                                this,
                                BatchConstants.C_CONSTRUCTED);
            }
        }


        //----------------------------------------------------------------------
        //--  Public methods.                                                 --
        //----------------------------------------------------------------------

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_doRun = "doRun";        
        /**
         * This method is called from the active <code>Thread</code> in the super class.
         */
        public void doRun()
        {
            AFBulkLoadBatchRecordData  l_record                  = null;
            MsisdnRoutingData          l_routingData             = null; // The IS API input data object.
            BatchResponseData          l_responseData            = null; // The IS API response data object.
            Collection                 l_responseCollection      = null;
            BatchElementResponseData[] l_batchElemResponsDataArr = null;
            BatchRecordData            l_batchRecordData         = null;
            PpasServiceException       l_ppasServiceException    = null;
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_INNER_CLASS_NAME,
                                11020,
                                this,
                                BatchConstants.C_ENTERING + C_METHOD_doRun);
            }

            trace(this, C_METHOD_doRun, 
                  getThreadName() + ": Scan through passed i_chunkBuffer, size = " + i_chunkBuffer.size());
            for (int i = 0; i < this.i_chunkBuffer.size(); i++)
            {
                l_record = (AFBulkLoadBatchRecordData)this.i_chunkBuffer.elementAt(i);
                l_routingData = new MsisdnRoutingData(l_record.getMsisdn(), l_record.getSdpId());
                this.i_outputBuffer.add(l_routingData);
            }
            
            try
            {
                trace(this, C_METHOD_doRun, getThreadName() + ": Call the IS API, timeout = " +
                      BulkLoadBatchProcessor.super.i_isApiTimeout + " ms");
                l_responseData = this.i_isApi.afBulkLoad(
                                                 null,
                                                 BulkLoadBatchProcessor.super.i_isApiTimeout,
                                                 this.i_outputBuffer);
            }
            catch (PpasServiceException p_psExe)
            {
                trace(this, C_METHOD_doRun,
                            getThreadName() + ": A PpasServiceException is caught: " + p_psExe.getMessage() + 
                            ",  key = " + p_psExe.getMsgKey());
                // Any 'PpasServiceException' thrown directly by the IS API is treated as a fatal error,
                // i.e. the 'PpasServiceException' is logged and the error line will be set for all
                // 'BatchRecordData' objects in the 'i_chunkBuffer'.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10024,
                                    this,
                                    "A PpasServiceException is caught: " + p_psExe.getMessage() + 
                                    ",  key = " + p_psExe.getMsgKey());
                }

                // Log the 'PpasServiceException'.
                BulkLoadBatchProcessor.super.i_logger.logMessage(p_psExe);
            
                // Set the error line for all 'BatchRecordData' records in the 'i_chunkBuffer' and put them
                // into the output buffer.
                for (int i = 0; i < this.i_chunkBuffer.size(); i++)
                {
                    l_record = (AFBulkLoadBatchRecordData)this.i_chunkBuffer.elementAt(i);
                    this.mapException(l_record, p_psExe);

                    // Store the 'BatchRecordData' in the output queue.
                    BulkLoadBatchProcessor.super.storeRecord(
                                                           (BatchRecordData)this.i_chunkBuffer.elementAt(i));
                }
                
                // Finish up the current thread.
                trace(this, C_METHOD_doRun, getThreadName() + ": Finish up the current thread.");
                // Remove this instance from the 'living IsProcess Thread Vector'.
                BulkLoadBatchProcessor.this.i_livingIsProcessThreadVec.remove(this);
                
                // Notify any waiting thread.
                synchronized (i_signalObj)
                {
                    BulkLoadBatchProcessor.this.i_signalObj.notify();
                }

                // Return from this method.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_END,
                                    C_INNER_CLASS_NAME,
                                    11130,
                                    this,
                                    BatchConstants.C_LEAVING + C_METHOD_doRun + 
                                    ", since a 'PpasServiceException' was caught.");
                }
                return;
            }

            trace(this, C_METHOD_doRun, "Response from IS API received, all success = " + 
                                        l_responseData.getAllSuccess());
            if (l_responseData.getAllSuccess())
            {
                // Store all the processed records directly in the output queue.
                for (int i = 0; i < this.i_chunkBuffer.size(); i++)
                {
                    BulkLoadBatchProcessor.super.storeRecord(
                                                            (BatchRecordData)this.i_chunkBuffer.elementAt(i));
                }
            }
            else
            {
                // Scan through the responses in order to update the corresponding BatchRecordData's error
                // line for an erroneous request before storing it into the output queue.
                l_responseCollection = l_responseData.getData();
                l_batchElemResponsDataArr = new BatchElementResponseData[ l_responseCollection.size() ];
                l_responseCollection.toArray(l_batchElemResponsDataArr);
                trace(this, C_METHOD_doRun,
                      "l_batchElemResponsDataArr.length = " + l_batchElemResponsDataArr.length); 
                for (int i = 0; i < l_batchElemResponsDataArr.length; i++)
                {
                    // Get the current 'BatchRecordData' object.
                    l_batchRecordData = (BatchRecordData)this.i_chunkBuffer.elementAt(i);

                    // Check if any Exception was raised during the process.
                    l_ppasServiceException = l_batchElemResponsDataArr[i].getException();
                    if (l_ppasServiceException != null)
                    {
                        // The current request failed, update the corresponding BatchRecordData's error line.
                        this.mapException(l_batchRecordData, l_ppasServiceException);
                    }

                    // Store the 'BatchRecordData' object into the output buffer.
                    BulkLoadBatchProcessor.super.storeRecord(l_batchRecordData);
                }
            }
            
            // Finish up the current thread.
            trace(this, C_METHOD_doRun, getThreadName() + ": Finish up the current thread.");
            // Remove this instance from the 'living IsProcess Thread Vector'.
            BulkLoadBatchProcessor.this.i_livingIsProcessThreadVec.remove(this);
            
            // Notify any waiting thread.
            synchronized (i_signalObj)
            {
                BulkLoadBatchProcessor.this.i_signalObj.notify();
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_END,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                BatchConstants.C_LEAVING + C_METHOD_doRun);
            }
        }


        //----------------------------------------------------------------------
        //--  Protected methods.                                              --
        //----------------------------------------------------------------------

        /**
         * Returns the name of this thread.
         * 
         * @return  the name of this thread.
         */
        protected String getThreadName()
        {
            return "IsProcessor: Thread-" + i_threadId;
        }
        

        //----------------------------------------------------------------------
        //--  Private methods.                                                --
        //----------------------------------------------------------------------

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_mapException = "mapException";        
        /**
         * Updates the passed <code>BatchRecordData</code> object's error line with an error code that 
         * corresponds to the passed <code>PpasServiceException</code>'s message key.
         * 
         * @param p_record      the <code>BatchRecordData</code>.
         * @param p_ppasServEx  the <code>PpasServiceException</code>.
         */
        private void mapException(BatchRecordData p_record, PpasServiceException p_ppasServEx)
        {
            StringBuffer l_errorLine         = null;
            boolean      l_mappingDone       = false;
            String[][]   l_errorCodeMappings = null;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                BatchConstants.C_ENTERING + C_METHOD_mapException);
            }

            // Set-up the message key - error code mapping array.
            l_errorCodeMappings = new String[][]{
                // Exceptions thrown by the method itself.
                {PpasServiceMsg.C_KEY_SERVICE_RESPONSE_TIMEOUT, C_ERROR_CODE_SERVICE_RESPONSE_TIMEOUT},
                
                // Exceptions included in the 'BatchElementResponseData' object.
                {PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS, C_ERROR_CODE_MSISDN_NOT_EXISTS}
            };

            l_errorLine = new StringBuffer(BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                           p_record.getInputLine());

            trace(this, C_METHOD_mapException, "l_errorCodeMappings.length = " + l_errorCodeMappings.length);
            // Look for matching mapping through the whole error code mapping array or until a matching
            // mapping is done.
            for (int i = 0; (i < l_errorCodeMappings.length  &&  !l_mappingDone); i++)
            {
                if (p_ppasServEx.getMsgKey().equals(l_errorCodeMappings[i][0]))
                {
                    l_errorLine.insert(0, l_errorCodeMappings[i][1]);
                    l_mappingDone = true;
                    trace(this, C_METHOD_mapException,
                          "matching mapping found, key = " + l_errorCodeMappings[i][0] +
                          ",  error code = " + l_errorCodeMappings[i][1]);
                }
            }
            if (!l_mappingDone)
            {
                // None of the expected mapping matches the given message key.
                trace(this, "", "An unexpected PpasServiceException is detected: " + p_ppasServEx);
                l_errorLine.insert(0,
                    C_ERROR_CODE_UNEXPECTED_EXCEPTION + " (" + p_ppasServEx.getMsgKey() + ")");
            }

            p_record.setErrorLine(l_errorLine.toString());

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_END,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                BatchConstants.C_LEAVING + C_METHOD_mapException);
            }
        }
    }
} // End of class BulkLoadBatchProcess
