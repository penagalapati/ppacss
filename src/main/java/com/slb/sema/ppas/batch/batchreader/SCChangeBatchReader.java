////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SCChangeBatchReader.java 
//      DATE            :       Jun 23, 2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.SCChangeBatchRecordData;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
 * This class is responsible for implementation of the method getRecord, 
 * which is declared as abstract in super class BatchFileReader.  
 * For each read line from file an SCChangeBatchRecordData is created and 
 * stored in the in-queue.  
 */
public class SCChangeBatchReader extends BatchLineFileReader
{
    
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "SCChangeBatchReader";
    
    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT = "01";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_NON_NUMERIC_MSISDN    = "02";
               
    /** Field length of MSISDN to be installed. Value is {@value}. */
    private static final int    C_MSISDN_LENGTH               = 15;

    /** Field length of old service class to be installed. Value is {@value}. */
    private static final int    C_OLD_SERVICE_CLASS_LENGTH    = 4;   

    /** Field length of new service class to be installed. Value is {@value}. */
    private static final int    C_NEW_SERVICE_CLASS_LENGTH    = 4;
    
    /**The maximum allowed length of a record line in the file. */
    private static final int    C_EXPECTED_RECORD_LENGTH      = C_MSISDN_LENGTH + C_OLD_SERVICE_CLASS_LENGTH 
                                                                + C_NEW_SERVICE_CLASS_LENGTH;

    // Index in the batch record array
    /** Number of fields in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NUMBER_OF_FIELDS            = 3;
    
    /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_MSISDN                      = 0;
    
    /** Index for the field Master MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_OLD_SERVICE_CLASS           = 1;
    
    /** Index for the field SCP ID in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NEW_SERVICE_CLASS           = 2;
    
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    
    /** The BatchDataRecord to be sent into the Queue. */
    private SCChangeBatchRecordData i_batchDataRecord = null;
    
    /**
     * Constructor for SubInstBatchReader.
     * Information about msisdn and status s retrieved from the indata filename.
     * @param p_controller  Reference to the BatchController that creates this object.
     * @param p_ppasContext Reference to PpasContext.
     * @param p_logger      reference to the logger.
     * @param p_parameters  Reference to a Map holding information e.g. filename.
     * @param p_inQueue     Reference to the Queue, where all indata records will be added.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public SCChangeBatchReader(
        PpasContext p_ppasContext,
        Logger p_logger,
        BatchController p_controller,
        SizedQueue p_inQueue,
        Map p_parameters,
        PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_inQueue, p_parameters, p_properties);
  
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME );
        }
        
        
    }

    /**
     * Get method for the created BatchDataRecord.
     * @return BatchDataRecord
     */
    public BatchRecordData getBatchDataRecord()
    {
        return this.i_batchDataRecord;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchDataRecord record.
     * 
     * @return - null if "No more records to read"
     */
    protected BatchRecordData getRecord()
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // Get next line from input file
        i_batchDataRecord = null;

        if ( super.readNextLine() != null )
        {
            i_batchDataRecord = new SCChangeBatchRecordData();

            // Store input filename
            i_batchDataRecord.setInputFilename( this.i_inputFileName );
            
            // Store line-number form the file
            i_batchDataRecord.setRowNumber( this.i_lineNumber );

            // Store original data record
            i_batchDataRecord.setInputLine( this.i_inputLine );

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10342,
                    this,
                    "getRecord : innan extractLineAndValidate" );
            }

            // Extract data from line
            if (!extractLineAndValidate())
            {
               // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10343,
                        this,
                        "extractLineAndValid failed!" );
                }

                i_batchDataRecord.setCorruptLine(true);
                
                if ( i_batchDataRecord.getErrorLine() == null )
                {
                    // Other error than specific field errors
                    i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT +
                                                   BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                                   i_batchDataRecord.getInputLine());
                }
            }
            
            i_batchDataRecord.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
            i_batchDataRecord.setNumberOfErrorRecords(super.i_notProcessed);
            if ( super.faultyRecord(i_lineNumber))
            {
                i_batchDataRecord.setFailureStatus(true);
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10340,
                    this,
                    "After setNumberOfSuccessfully...." + super.i_successfullyProcessed);
            }

        } // end if
        
        
        if ( i_batchDataRecord != null )
        {

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10350,
                    this,
                    "DumpRecord: " + i_batchDataRecord.dumpRecord());
            }
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10350,
                    this,
                    "i_batchDataRecord is NULL!!!!!");
            }            
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_batchDataRecord;
        
    } // end of method getRecord()
    

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Check if filename is of the valid form.
     * @param p_fileName Filename to be tested.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid( String p_fileName )
    {
        boolean  l_validFileName          = false;   // Help variable - return value. Assume filename is OK.
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
        }

        if (p_fileName != null
            && (p_fileName.matches(BatchConstants.C_PATTERN_BATCH_CHG_SERV_FILENAME_DAT)
                || p_fileName.matches(BatchConstants.C_PATTERN_BATCH_CHG_SERV_FILENAME_SCH)
                || p_fileName.matches(BatchConstants.C_PATTERN_BATCH_CHG_SERV_FILENAME_IPG)))
        {
            l_validFileName = true;
        }
        
        // Check if recovery mode
        if ( l_validFileName && !this.i_recoveryMode )
        {
            // Flag that processing is in progress. Rename the file to *.IPG.
            // If the batch is running in recovery mode, it already has the extension .IPG
            if (p_fileName.matches(BatchConstants.C_PATTERN_BATCH_CHG_SERV_FILENAME_SCH))
            {
                renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
            }
            else
            {
                renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_INDATA_FILE,
                                 BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
            }
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10410,
                    this,
                    "After rename, inputFullPathName=" + this.i_fullPathName );
            }              
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10410,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    

    /**
     * This method extracts information from the batch data record. The record must look like:
     * [MSISDN][MSISDN][SDP ID][Agent][Promotion Plan][Account Group][Service Offering]
     * The line number will be used to check against the recovery file if the batch mode is
     * recovery.
     * All fields are checked to be alpha numberic.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        MsisdnFormat l_msisdnFormat = null;
        Msisdn       l_tmpMsisdn    = null; // Help variable to convert from string to Msisdn object.
        String[]     l_recordFields = new String[C_NUMBER_OF_FIELDS]; // Help array to keep the input line elements

        boolean l_validData = true; // return value - assume it is valid

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10360,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate);
        }


        //Check max record length.
        if (i_inputLine.length() != C_EXPECTED_RECORD_LENGTH)
        {
            i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                    + BatchConstants.C_DELIMITER_REPORT_FIELDS + super.i_inputLine);
            l_validData = false;
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10365,
                                this,
                                "Row: ]" + i_inputLine 
                                         + "[ wrong record length " + " l_lineNumber= " + i_lineNumber
                                         + " " + i_batchDataRecord.getErrorLine() + " length= "
                                         + i_inputLine.length());
            }
        }
        else
        {
            initializeRecordValidation(i_inputLine, l_recordFields, i_batchDataRecord);

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10361,
                                this,
                                "ExtractLineAndValidate innan IF getfields.....]"+i_inputLine+"[");
            }

            // Get the record fields and check if it is numeric/alphanumberic
            //   Start End #Field Format check Errorcode
            if (getField(0,
                         C_MSISDN_LENGTH,
                         C_MSISDN,
                         BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC,
                         C_ERROR_NON_NUMERIC_MSISDN)
                    && getField(C_MSISDN_LENGTH,
                                C_OLD_SERVICE_CLASS_LENGTH,
                                C_OLD_SERVICE_CLASS,
                                BatchConstants.C_PATTERN_NUMERIC,
                                C_ERROR_INVALID_RECORD_FORMAT)
                    && getField(C_OLD_SERVICE_CLASS_LENGTH,
                                C_NEW_SERVICE_CLASS_LENGTH,
                                C_NEW_SERVICE_CLASS,
                                BatchConstants.C_PATTERN_NUMERIC,
                                C_ERROR_INVALID_RECORD_FORMAT))
            {
                // Both MSISDN and new Sevice class and old service class must be specified.
                if (l_recordFields[C_MSISDN] == null || 
                    l_recordFields[C_NEW_SERVICE_CLASS] == null ||
                    l_recordFields[C_OLD_SERVICE_CLASS] == null)
                {
                    // Field not specified
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_START,
                                        C_CLASS_NAME,
                                        10365,
                                        this,
                                        "Row: ]" + i_inputLine 
                                                 + "[ is an INVALID record" + " l_lineNumber   ="
                                                 + i_lineNumber + "\n" + " l_msisdn   ="
                                                 + l_recordFields[C_MSISDN] + "\n"
                                                 + " l_new_service_class   ="
                                                 + l_recordFields[C_NEW_SERVICE_CLASS] + "\n"
                                                 + " l_old_service_class   ="
                                                 + l_recordFields[C_OLD_SERVICE_CLASS]);
                    }

                    i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                            + BatchConstants.C_DELIMITER_REPORT_FIELDS + super.i_inputLine);
                    i_batchDataRecord.setCorruptLine(true);
                    
                    l_validData = false;
                }
            }
            else
            {
                // Illegal field format detected
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10365,
                                    this,
                                    "Row: ]" + i_inputLine + "[ is a INVALID record" 
                                             + " l_lineNumber     ="
                                             + i_lineNumber + "\n" + " l_msisdn         ="
                                             + l_recordFields[C_MSISDN] + "\n" + " l_oldServiceClass="
                                             + l_recordFields[C_OLD_SERVICE_CLASS] + "\n"
                                             + " l_newServiceClass ="
                                             + l_recordFields[C_NEW_SERVICE_CLASS]);
                }

                // Flag for corrupt line
                l_validData = false;
            }
        }
        // Store information into the BatchDataRecord
        // If some of the fields were invalid their value will be NULL.

        // Convert the MSISDN string (master and subordinate) to a Msisdn object
        // This may give an ParseException - if so flag currupt line!
        if (l_validData)
        {
            l_msisdnFormat = this.i_context.getMsisdnFormatInput();
            try
            {
                l_tmpMsisdn = l_msisdnFormat.parse(l_recordFields[C_MSISDN]);
                i_batchDataRecord.setMsisdn(l_tmpMsisdn);
            }
            catch (ParseException e)
            {
                l_validData = false;

                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10366,
                                    this,
                                    "Row: " + i_lineNumber + " ]" + i_inputLine + "[ has invalid MSISDN");
                }
            }
        }
        if (l_validData)
        {
            try
            {
                i_batchDataRecord.setOldServiceClass(new ServiceClass(Integer
                        .parseInt(l_recordFields[C_OLD_SERVICE_CLASS])));

                i_batchDataRecord.setNewServiceClass(new ServiceClass(Integer
                        .parseInt(l_recordFields[C_NEW_SERVICE_CLASS])));
            }
            catch (NumberFormatException e)
            {
                l_validData = false;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10370,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate);
        }

        return l_validData;

    } // end of method extractLineAndValidate(.)
}