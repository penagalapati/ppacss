////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       DisconBatchWriterUT.java 
//DATE            :       Sep 17, 2004
//AUTHOR          :       Urban Wigstrom
//REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//COPYRIGHT       :       WM-data 2006
//
//DESCRIPTION     :       Unit test for DisconBatchWriter.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+--------------------
//DD/MM/YY | <name>        | <brief description of change>    | <reference>
//---------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////

package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.DisconBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchSubJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.dataclass.DisconBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**Unit test for DisconBatchWriter. */
public class DisconBatchWriterUT extends BatchTestCaseTT
{
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String          C_CLASS_NAME       = "DisconBatchWriterUT";

    /** Reference to SubInstBatch controller object. */
    private static DisconBatchWriter     c_disconBatchWriter;

    /** Reference to subInstBatch controller object. */
    private static DisconBatchController c_disconBatchController;
    
    /** The date when the file is processed. */
    private static final String          C_FILE_DATE        = DatePatch.getDateToday().toString_yyyyMMdd();

    /** The batch job name. Value is {@value}. */
    private static final String          C_BATCH_JOB_NAME   = "DisconBatchWriterUT";
    
    /** The master js job id. */
    private static final long            C_MASTER_JS_JOB_ID = 1;

    /** The start range. */
    private static final long            C_START_RANGE      = 1;

    /** The end range. */
    private static final long            C_END_RANGE        = 1;

    /** The sub job id. */
    private static final int             C_SUB_JOB_ID       = 1;

    /** The js job id. */
    private static final long            C_JS_JOB_ID        = 10001;

    /** Reference to PpsBatchControlService. */
    private PpasBatchControlService      i_controlService   = null;

    /**
     * Create an instanse of DisconBatchWriterUT.
     * @param p_name This unit tests class name.
     */
    public DisconBatchWriterUT(String p_name)
    {
        super(p_name, "batch_dis");
        createContext();
    }

    /**
     * Test writeRecord(). Begins with creating a instans in table <code>table baco_batch_control</code>.
     * Creates a <code>SubInstBatchRecordData</code> with a error line set. Updates the table 
     * <code>baco_batch_control</code> by calling how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if
     * the row <code>baco_failur</code> has been updated.
     * Set the value for error line in the object <code>SubInstBatchRecordData</code> to null and updates
     * the table <code>baco_batch_control</code> by calling <code>updateStatus()</code> how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if the 
     * row <code>baco_success</code> has been updated.
     * So by test <code>writeRecord()</code> even the methods <code>updateStatus()<code> and
     * <code>updateControlInfo(...)</code> is being tested.
     */
    public void testWriteRecord()
    {
        beginOfTest("testWriteRecord");
        DisconBatchRecordData l_dataRecord =
            new DisconBatchRecordData("0832000000", "19", "BZ", new PpasDateTime(getToday()));
 
        BatchSubJobData    l_batchData1              = new BatchSubJobData();
        BatchSubJobControlData l_batchSubJobControlData_added     = null;
        BatchSubJobControlData l_batchSubJobControlData_retrieved = null;
        String             l_outputLine              = "1 OUT";
        PpasDateTime       l_executionDateTime       = null;
        int                l_subJobId                = -1;
        long               l_masterJsJobId           = -1;
        String             l_actualText              = null;
        String             l_lastDailySequenceNumber = null;
        PpasDate           l_currentDate             = null;
        String             l_outputFilename          = null;
        String             l_fullOuputFilename       = null;
  
        try
        {
            i_controlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);
            l_batchData1 = c_disconBatchController.getKeyedControlRecord();
            l_masterJsJobId = Long.parseLong(l_batchData1.getMasterJsJobId());
            l_executionDateTime = new PpasDateTime(l_batchData1.getExecutionDateTime());
            l_subJobId = Integer.parseInt(l_batchData1.getSubJobId());
            
            //Create a BatchSubJobControlData object and save it in table baco_batch_control.
            l_batchSubJobControlData_added = 
                new BatchSubJobControlData(C_BATCH_JOB_NAME,
                                           l_executionDateTime,
                                           l_subJobId,
                                           C_JS_JOB_ID,
                                           l_masterJsJobId,
                                           'I',
                                           C_START_RANGE,
                                           C_END_RANGE,
                                           -1,
                                           " ",
                                           " ",
                                           c_ppasRequest.getOpid());

            
            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                i_controlService.addSubJobDetails(c_ppasRequest, l_batchSubJobControlData_added, C_TIMEOUT);

                l_dataRecord.setOutputLine(l_outputLine);
                c_disconBatchWriter.writeRecord(l_dataRecord);
                c_disconBatchWriter.flushFiles();

                l_batchSubJobControlData_retrieved = i_controlService.getSubJobDetails(c_ppasRequest,
                                                                                       l_masterJsJobId,
                                                                                       l_executionDateTime,
                                                                                       l_subJobId,
                                                                                       C_TIMEOUT);
                
                assertTrue("Not correct values in the BSCO_BATCH_SUB_CONTROL table.",
                           l_batchSubJobControlData_retrieved.equals(l_batchSubJobControlData_added));
            }
            catch (PpasServiceException e)
            {
                failedTestException(e);
            }  
            
            //Get the output text from the output logfile.
            l_currentDate = DatePatch.getDateToday();
            l_lastDailySequenceNumber = 
                getPaddedSequenceNumber(C_BATCH_JOB_NAME, l_currentDate);
            l_outputFilename = "DISC_" + C_FILE_DATE + "_" + l_lastDailySequenceNumber + ".TMP";
            say("DisconBatchWriterUT.testWriteRecord() -- output filename: '" + l_outputFilename + "'");
            l_actualText = getValueFromLogFile(l_outputFilename,
                                               BatchConstants.C_EXTENSION_TMP_FILE,
                                               BatchConstants.C_OUTPUT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_INDATA_FILE,
                        ("No file found".equals(l_actualText)));
            assertEquals("Failure: Wrong data in the recovery file", l_outputLine, l_actualText);

            // Delete the output file.
            c_disconBatchWriter.tidyUp(); // Need to call this method to close the output file.
            l_fullOuputFilename = c_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);
            l_fullOuputFilename += "/" + l_outputFilename;
            say("DisconBatchWriterUT.testWriteRecord() -- full output filename: '" +
                l_fullOuputFilename + "'");
            File l_outputFile = new File(l_fullOuputFilename);
            say("DisconBatchWriterUT.testWriteRecord() -- output file is deleted: " + l_outputFile.delete()); 
        }
        catch (IOException e)
        {
            failedTestException(e);
        }

        endOfTest();
    }

    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(DisconBatchWriterUT.class);
    }

    /**
     * Create context neccessary for this test.
     */
    private void createContext()
    {
        DisconBatchRecordData l_data =
            new DisconBatchRecordData("0832000001", "99", "DE", new PpasDateTime(getToday()));
        Hashtable   p_params                  = new Hashtable();
        SizedQueue  l_outQueue                = null;
        String      l_executionDateTime       = DatePatch.getDateTimeNow().toString();

        //The update frequency needs to be 1 for the test to work.
        c_properties.setProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY, "1");
        
        try
        {
            l_outQueue = new SizedQueue("BatchWriter", 1, null);

            l_outQueue.addFirst(l_data);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            failedTestException(e);
        }
        catch (SizedQueueClosedForWritingException e)
        {
            failedTestException(e);
        }
        
        p_params.put(BatchConstants.C_KEY_BATCH_JOB_TYPE, C_BATCH_JOB_NAME);
        p_params.put(BatchConstants.C_KEY_EXECUTION_DATE_TIME, l_executionDateTime);        
        p_params.put(BatchConstants.C_KEY_MASTER_JS_JOB_ID, Long.toString(C_MASTER_JS_JOB_ID));
        p_params.put(BatchConstants.C_KEY_START_RANGE,Long.toString(C_START_RANGE));
        p_params.put(BatchConstants.C_KEY_END_RANGE, Long.toString(C_END_RANGE));     
        p_params.put(BatchConstants.C_KEY_SUB_JOB_ID, Integer.toString(C_SUB_JOB_ID));   
                
        try
        {
            c_disconBatchController = new DisconBatchController(c_ppasContext,
                                                                C_BATCH_JOB_NAME,
                                                                Long.toString(C_JS_JOB_ID),
                                                                "DisconBatchWriterUT",
                                                                p_params);

            c_disconBatchWriter = new DisconBatchWriter(c_ppasContext,
                                                        c_logger,
                                                        c_disconBatchController,
                                                        p_params,
                                                        l_outQueue,
                                                        c_properties);
        }
        catch (PpasConfigException e)
        {
            failedTestException(e);
        }
    }

    /**
     * Returns the last daily sequence number for the given sequence name and date as a 5 characters right 
     * adjusted <code>String</code> padded with zeroes.
     * 
     * @param p_sequenceName  the current sequence name.
     * @param p_sequenceDate  the used sequence date.
     * @return  the last daily sequence number for the given sequence name and date as a 5 characters right
     *          adjusted <code>String</code> padded with zeroes.
     */
    private String getPaddedSequenceNumber(String p_sequenceName, PpasDate p_sequenceDate)
    {
        StringBuffer l_seqNoStr    = null;
        String       l_tmpSeqNoStr = null;
        int          l_lastSeqNo   = getLastDailySequenceNumber(p_sequenceName, p_sequenceDate);
        
        if (l_lastSeqNo != -1)
        {
            l_tmpSeqNoStr = Integer.toString(l_lastSeqNo);
            l_seqNoStr = new StringBuffer(l_tmpSeqNoStr);
            for (int i = 1; i <= 5; i++)
            {
                if (i > l_tmpSeqNoStr.length())
                {
                    l_seqNoStr.insert(0, "0");
                }
            }
        }
        
        return l_seqNoStr.toString();
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getLastDailySequenceNumber = "getLastDailySequenceNumber";
    /**
     * Returns the last daily sequence number stored in the database table DASE_DAILY_SEQUENCE for the given
     * sequence name and date.
     * 
     * @param p_sequenceName  the current sequence name.
     * @param p_sequenceDate  the used sequence date.
     * 
     * @return  the last daily sequence number stored in the database table DASE_DAILY_SEQUENCE for the given
     *          sequence name and date.
     */
    private int getLastDailySequenceNumber(String p_sequenceName, PpasDate p_sequenceDate)
    {
        int           l_lastSeqNo = -1;
        SqlString     l_selectStr = null;
        JdbcResultSet l_resultSet = null;

        l_selectStr = new SqlString(100, 2,
                                    "SELECT dase_sequence_value " +
                                    "FROM   dase_daily_sequence " +
                                    "WHERE  dase_sequence_name = {0} " +
                                    "AND    dase_sequence_date = {1}");
        l_selectStr.setStringParam(0, p_sequenceName);
        l_selectStr.setDateParam(1, p_sequenceDate);

        JdbcConnection l_conn = null;
        try
        {  
            l_conn = getConnection(C_CLASS_NAME, C_METHOD_getLastDailySequenceNumber, 10100);

            JdbcStatement statement = l_conn.createStatement(C_CLASS_NAME,
                                                             C_METHOD_getLastDailySequenceNumber,
                                                             14010,
                                                             this,
                                                             c_ppasRequest);

            // Execute the select transaction.
            l_resultSet = statement.executeQuery(C_CLASS_NAME,
                                                 C_METHOD_getLastDailySequenceNumber,
                                                 14020,
                                                 this,
                                                 c_ppasRequest,
                                                 l_selectStr);

            if (l_resultSet.next(14030))
            {
                l_lastSeqNo = l_resultSet.getInt(14040, 1);
            }
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        finally
        {
            if (l_resultSet != null)
            {
                try
                {
                    l_resultSet.close(14070);
                }
                catch (PpasSqlException p_ppasSqlEx)
                {
                    // No specific Exception handling.
                    p_ppasSqlEx = null;
                }
                l_resultSet = null;
            }
            putConnection(l_conn);
        }

        return l_lastSeqNo;
    }
}
