////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SCChangeBatchProcessor.java 
//      DATE            :       Jun 23, 2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
// 20/05/10 | Siva Sanker   | Service Class Change should not  | PpacBan#3688/13752
//          | Reddy         | be changed for a NPC Cut over     |
//          |               | MSISDN                           |
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.SCChangeBatchRecordData;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**Handling the batch data processing for Service Class Change. */
public class SCChangeBatchProcessor extends BatchProcessor
{

    //-------------------------------------------------------------------------
    // Class constants.
    //-------------------------------------------------------------------------
    /** Constant holding the name of this class. Value is {@value}. */
    private static final String C_CLASS_NAME                           = "SCChangeBatchProcessor";

    /** Constant holding the error code for other errors. Value is {@value}. */
    private static final String C_ERROR_CODE_OTHER_ERROR               = "00";

    /** Constant holding the error code for an invalid MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_MSISDN            = "02";

    /** Constant holding the error code for a not available MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_MSISDN_NOT_INSTALLED      = "03";

    /** Constant holding the error code for an bad account type. Value is {@value}. */
    private static final String C_ERROR_CODE_BAD_ACCOUNT_TYPE          = "04";

    /** Constant holding the error code for an inactive account. Value is {@value}. */
    private static final String C_ERROR_CODE_SUBSCRIBER_TEMP_BLOCKED   = "05";

    /** Constant holding the error code for an invalid old service class. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_OLD_SERVICE_CLASS = "06";

    /** Constant holding the error code for an invalid new service class. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_NEW_SERVICE_CLASS = "07";
       
    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    /** The <code>IS API</code> reference. */
    private PpasAccountService i_ppasAccountService = null;

    /** The <code>Session</code> object. */
    private PpasSession        i_ppasSession        = null;
   
    /**
     * Constructs an instance of this <code>SubInstBatchProcessor</code> class using the specified
     * batch controller, input data queue and output data queue.
     *
     * @param p_ppasContext      the PpasContext reference
     * @param p_logger           the logger
     * @param p_batchController  the batch controller
     * @param p_inQueue          the input data queue
     * @param p_outQueue         the output data queue
     * @param p_params           the start process paramters
     * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
     */
    public SCChangeBatchProcessor(
        PpasContext p_ppasContext,
        Logger p_logger,
        BatchController p_batchController,
        SizedQueue p_inQueue,
        SizedQueue p_outQueue,
        Map p_params,
        PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);
        
                
        PpasRequest l_ppasRequest = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                "Entering the Constructor");
        }

        i_ppasSession = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest = new PpasRequest(i_ppasSession);
        i_ppasAccountService = new PpasAccountService(l_ppasRequest, p_logger, p_ppasContext);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                "Leaving the Constructor");
        }
    }

    /** Constant holding the name of this method. Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";
    /**
     * Processes the given <code>BatchDataRecord</code> and returns it.
     * If the process succeeded the <code>BatchDataRecord</code> is updated with information
     * needed by the writer component before returning it.
     * If the process fails but the record shall be re-processed at the end of this process,
     * the record will be put in the retry queue and this method returns <code>null</code>.
     * 
     * @param p_record  the <code>BatchDataRecord</code>.
     * @return the given <code>BatchDataRecord</code> updated with info for the writer component,
     *         or <code>null</code>.
     * @see BatchProcessor#processRecord(BatchRecordData)
     * @throws PpasServiceException  if an unrecoverable error occurs while installing a subscriber.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        String                  l_opId            = null;
        Msisdn                  l_msisdn          = null;
        PpasRequest             l_ppasRequest     = null;
        ServiceClass            l_oldServiceClass = null;
        ServiceClass            l_newServiceClass = null;
        SCChangeBatchRecordData l_scChangeRecord  = null;
        String                  l_recoveryLine    = null;
        StringBuffer            l_tmpRecoveryLine = null;
        StringBuffer            l_tmpErrorLine    = null;
        BasicAccountData        l_basicAccountData= null;
        long                    l_custStatusFlags = 0;
        String                  l_custId          = null;
        long                    l_timeout         = 0;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10020,
                this,
                "Entering " + C_METHOD_processRecord);
        }
        
        l_scChangeRecord = (SCChangeBatchRecordData)p_record;
        
        
        //Prepare recovery line - shall always be created
        l_tmpRecoveryLine = new StringBuffer();
        l_tmpRecoveryLine.append(l_scChangeRecord.getRowNumber());
        l_tmpRecoveryLine.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);

        if (!l_scChangeRecord.isCorruptLine())
        {
            l_opId            = super.i_batchController.getSubmitterOpid();
            l_msisdn          = l_scChangeRecord.getMsisdn();
            l_ppasRequest     = new PpasRequest(i_ppasSession, l_opId, l_msisdn);
            l_oldServiceClass = l_scChangeRecord.getOldServiceClass();
            l_newServiceClass = l_scChangeRecord.getNewServiceClass();
            
            // Do not retrieve subordinates or disconnected subscribers.
            l_custStatusFlags = PpasService.C_FLAG_BLOCK_TRANS_ALL;
            
            try
            {
                //Fetch the cust_id of the msisdn
                l_custId = (i_ppasAccountService.getBasicData(
                        l_ppasRequest,
                        l_custStatusFlags,
                        l_timeout)).getCustId();
                
                if(l_custId != null || l_custId != "" )
                {
                    l_ppasRequest = new PpasRequest(i_ppasSession, l_opId,
                                                    l_msisdn, l_custId);
                    
                    l_basicAccountData = i_ppasAccountService.getBasicData(
                            l_ppasRequest, l_custStatusFlags, l_timeout);
                    
                    //To check if the msisdn is Number Plan Cutovered (NPC)
                    if(!l_msisdn.equals(l_basicAccountData.getMsisdn()))
                    {    
                        // Create 'warning' level exception, log and throw it.
                        PpasServiceException p_psExe = new PpasServiceFailedException
                                            (C_CLASS_NAME,
                                             C_METHOD_processRecord,
                                             12557,
                                             this,
                                             l_ppasRequest,
                                             0,
                                             ServiceKey.get().msisdnNotExists(l_msisdn));                        
                        i_logger.logMessage (p_psExe);
                        throw (p_psExe);
                     }   
                }
                
                // Change the service class.
                i_ppasAccountService.changeServiceClass(
                    l_ppasRequest,
                    super.i_isApiTimeout,
                    l_oldServiceClass,
                    l_newServiceClass);
                
                l_tmpRecoveryLine.append(BatchConstants.C_SUCCESSFULLY_PROCESSED);               
            }
            catch (PpasServiceException p_psExe)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10030,
                        this,
                        "PpasServiceException is caught: " + p_psExe.getMessage() + 
                        ",  key: " + p_psExe.getMsgKey());
                }
                
                l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);
                
                l_tmpErrorLine = new StringBuffer();
               
                //If not available/non-existent MSISDN, set error line in batch record.
                if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_AVAILABLE)
                        || p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS)
                        || p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_DISCONNECTED_ACCOUNT))
                {
                    l_tmpErrorLine.append(C_ERROR_CODE_MSISDN_NOT_INSTALLED);
                    
                }
                else
                {
                    //If the MSISDM is invalid, set error line in batch record.
                    if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_INVALID_INPUT_MSISDN))
                    {
                        l_tmpErrorLine.append(C_ERROR_CODE_INVALID_MSISDN);
                    }
                    else
                    {
                        //If the account type is subordinate, set error line in batch record.
                        if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_GET_MASTER_IS_SUBORDINATE))
                        {
                            l_tmpErrorLine.append(C_ERROR_CODE_BAD_ACCOUNT_TYPE);
                        }
                        else
                        {
                            //If the subscriber is temprary blocked, set error line in batch record.
                            if (p_psExe.getMsgKey().equals(PpasServiceMsg.C_KEY_CUST_IS_TEMP_BLOCKED))
                            {
                                l_tmpErrorLine.append(C_ERROR_CODE_SUBSCRIBER_TEMP_BLOCKED);
                            }
                            else
                            {
                                //If the old service class is invalid, set error line in batch record.
                                if (p_psExe.getMsgKey()
                                        .equals(PpasServiceMsg.C_KEY_INVALID_VALUE_OLD_SERV_CLASS))
                                {
                                    l_tmpErrorLine.append(C_ERROR_CODE_INVALID_OLD_SERVICE_CLASS);
                                    
                                    if (PpasDebug.on)
                                    {
                                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                        PpasDebug.C_APP_SERVICE,
                                                        PpasDebug.C_ST_TRACE,
                                                        C_CLASS_NAME,
                                                        11030,
                                                        this,
                                                        "Invalid old sevice class " + p_psExe.getMsgKey());
                                    }
                                }
                                else
                                {
                                    //if the new service class is invalid, set error line in batch record.
                                    if (p_psExe.getMsgKey()
                                            .equals(PpasServiceMsg.C_KEY_INVALID_VALUE_NEW_SERV_CLASS))
                                    {
                                        l_tmpErrorLine.append(C_ERROR_CODE_INVALID_NEW_SERVICE_CLASS);

                                        if (PpasDebug.on)
                                        {
                                            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                            PpasDebug.C_APP_SERVICE,
                                                            PpasDebug.C_ST_TRACE,
                                                            C_CLASS_NAME,
                                                            11030,
                                                            this,
                                                            "Invalid new sevice class " +
                                                            p_psExe.getMsgKey());
                                        }
                                    }
                                    else
                                    {
                                        if (p_psExe.getLoggingSeverity() == PpasServiceException.C_SEVERITY_ERROR)
                                        { 
                                            if (PpasDebug.on)
                                            {
                                                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                                                PpasDebug.C_APP_SERVICE,
                                                                PpasDebug.C_ST_TRACE,
                                                                C_CLASS_NAME,
                                                                11080,
                                                                this,
                                                                "A severity error " + p_psExe.getMsgKey());
                                            }
                                            
                                            throw p_psExe;
                                        }
                                                                               
                                        l_tmpErrorLine.append(C_ERROR_CODE_OTHER_ERROR);
                                    }
                                }
                            }
                        }
                    }
                }
                
                l_tmpErrorLine.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                l_tmpErrorLine.append(l_scChangeRecord.getInputLine());
                l_scChangeRecord.setErrorLine(l_tmpErrorLine.toString());               
            } //end catch
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    11110,
                    this,
                    C_METHOD_processRecord + " NOT PROCESSED " );
            }
            
            l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);
        }

        // Always create a recovery line.
        l_scChangeRecord.setRecoveryLine(l_tmpRecoveryLine.toString());
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                12070,
                this,
                "Recobvery line= " + l_recoveryLine + " " + l_scChangeRecord.getRecoveryLine());
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10030,
                this,
                "Leaving " + C_METHOD_processRecord);
        }
        
        return l_scChangeRecord;
    }
}
