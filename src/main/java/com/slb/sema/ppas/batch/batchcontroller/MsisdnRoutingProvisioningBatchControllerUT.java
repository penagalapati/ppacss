////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MsisdnRoutingProvisioningBatchControllerUT.java 
//      DATE            :       02-March-2007
//      AUTHOR          :       Marianne T�rnqvist
//      REFERENCE       :       
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Unit test for MsisdnRoutingProvisioningBatchController.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// 15/03/07 | M. T�rnqvist  | End-to-end tests are introduced. | PpacLon#2859/11085
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcommon.BatchTestDataTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/**
 * Class to unit test the Subscriber Install Status Change batch.
 */
/** Unit tests the Miscellaneous DataUpload batch controller. */
public class MsisdnRoutingProvisioningBatchControllerUT extends BatchTestCaseTT
{
    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                = "MsisdnRoutingProvisioningBatchControllerUT";

    /** The name of the Status Change batch. */
    private static final String C_MSISDN_ROUTING_BATCH_NAME = BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_PROVISIONING;

    /** The name of the test data template file to be used for the succesful insert test case. */
    private static final String C_FILENAME_SUCCESSFUL       = "MSISDN_LOAD_SUCCESSFUL.DAT";

    /** The name of the test data template file to be used for the test case with an errornous inputfile. */
    private static final String C_FILENAME_ERROR            = "MSISDN_LOAD_ERRORS.DAT";

    /** The name of the test data template file to be used for the test case with an errornous inputfile. */
    private static final String C_FILENAME_INVALID_SDP      = "MSISDN_LOAD_INVALID_SDP.DAT";

    /** Starting index for routing MSISDNs. */
    private static int     i_msisdnIndex;
    
    /** Flag to show if routing MSISDN shall be created, before starting the RoutingDeletion batch. */
    private static boolean i_generateRoutingMsisdns;
    
    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs a <code>MsisdnRoutingProvisioningBatchControllerUT</code> instance to be used for
     * unit tests of the <code>MsisdnRoutingProvisioningBatchController</code> class.
     * 
     * @param p_name  the name of the current test.
     */
    public MsisdnRoutingProvisioningBatchControllerUT(String p_name)
    {
        super(p_name);
    }


    // =========================================================================
    // == Public unit test method(s).                                         ==
    // =========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testMsisdnRoutingProvisioning_Successful =
        "testMsisdnRoutingProvisioning_Successful";
    /**
     * Performs a fully functional test of a successful 'MSISDN Routing Provisioning'.
     * A test file with a valid name containing 10 valid Msisdn Routing Provisioning 
     * records are created and placed in the batch indata directory. The MSISDN Routing 
     * Provisioning batch is started in order to process the test file and update the
     * MSISDN table.
     *
     * @ut.when        A test file containing 10 valid status change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        10 new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There wouldn't be any recovery file.
     *                 A report file will be created which will contain only one
     *                 success trailing record.
     *                 The test file will be renamed with a new extension, DNE. 
     * 
     * @ut.attributes  +f
     */
    public void testMsisdnRoutingProvisioning_Successful()
    {
    	final int      L_NO_SUCCESSFUL_RECORDS = 10;
    	final int      L_NO_ERRORNOUS_RECORDS  = 0;
        final String[] L_REPORT_ROWS           = {getTrailerRecord(L_NO_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_ROWS         = {};

        beginOfTest(C_METHOD_testMsisdnRoutingProvisioning_Successful);

        
        MsisdnRoutingProvisioningTestDataTT l_testDataTT =
            new MsisdnRoutingProvisioningTestDataTT( CommonTestCaseTT.c_ppasRequest,
                                                     UtilTestCaseTT.c_logger,
                                                     CommonTestCaseTT.c_ppasContext,
                                                     C_MSISDN_ROUTING_BATCH_NAME,
                                                     c_batchInputDataFileDir,
                                                     C_FILENAME_SUCCESSFUL,
                                                     JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                                     BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                                     L_NO_SUCCESSFUL_RECORDS,
                                                     L_NO_ERRORNOUS_RECORDS );

        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows(L_RECOVERY_ROWS );

        // Starting index for Routing MSISDNs that shall be deleted. (Index 0 - belongs to an installed
        // subscriber, that has been used to get a "starting MSISDN" number in this test.
        i_msisdnIndex = 1;
        
        // Generate 10 Routing MSISDNs to be deleted by the Routing Deletion batch.
        i_generateRoutingMsisdns = true;
        
        completeFileDrivenBatchTestCase( l_testDataTT );

        endOfTest();
    }


    
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testMsisdnRoutingProvisioning_Error =
        "testMsisdnRoutingProvisioning_Error";
    /**
     * Performs a fully functional test of an errournous 'MSISDN Routing Provisioning'.
     * A test file with a valid name containing invalid Msisdn Routing Provisioning 
     * records are created and placed in the batch indata directory. The MSISDN Routing 
     * Provisioning batch is started in order to process the test file and update the
     * MSISDN table.
     *
     * @ut.when        A test file containing invalid status change records is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        No new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There will be no recovery file.
     *                 A report file will be created which will containing error code rows and
     *                 a trailing record saying that no successful statusChanges have been made.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testMsisdnRoutingProvisioning_Error()
    {
    	final int      L_NO_SUCCESSFUL_RECORDS = 0;
    	final int      L_NO_ERRORNOUS_RECORDS  = 7;
    	
        final String[] L_REPORT_ROWS = { "01",   //Invalid record format
                                         "01",   //Invalid record format
                                         "02",   //Non-numeric MSISDN
                                         "01",   //Invalid record format
                                         "01",   //Invalid record format
                                         "01",   //Invalid record format
                                         "14",   // MSISDN not in block range
                                         getTrailerRecord(L_NO_SUCCESSFUL_RECORDS) };
        
        final String[] L_RECOVERY_ROWS = {};

        beginOfTest( C_METHOD_testMsisdnRoutingProvisioning_Error );

        MsisdnRoutingProvisioningTestDataTT l_testDataTT =
            new MsisdnRoutingProvisioningTestDataTT( CommonTestCaseTT.c_ppasRequest,
                                                     UtilTestCaseTT.c_logger,
                                                     CommonTestCaseTT.c_ppasContext,
                                                     C_MSISDN_ROUTING_BATCH_NAME,
                                                     c_batchInputDataFileDir,
                                                     C_FILENAME_ERROR,
                                                     JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                                     BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                                     L_NO_SUCCESSFUL_RECORDS,
                                                     L_NO_ERRORNOUS_RECORDS );
        
        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows( L_RECOVERY_ROWS );
        
        // Index for MSISDN to be used in the indata file. For this test there will only be one MSISDN
        // belonging to an installed subscriber. This will generate errorcode = 12
        i_msisdnIndex = 0;
    
        // For this test shall no Routing Msisdn be generated, only error codes will be tested.
        i_generateRoutingMsisdns = false;

        completeFileDrivenBatchTestCase( l_testDataTT );

        endOfTest();
    }

    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testMsisdnRoutingProvisioning_InvalidSDP =
        "testMsisdnRoutingProvisioning_InvalidSDP";
//    /**
//     * Performs a fully functional test of an errournous 'MSISDN Routing Provisioning'.
//     * A test file with a valid name containing an invalid Msisdn Routing Provisioning 
//     * record is created and placed in the batch indata directory. The MSISDN Routing 
//     * Provisioning batch is started in order to process the test file and update the
//     * MSISDN table.
//     *
//     * @ut.when        A test file containing invalid a Routing Provisioneing record is
//     *                 created and placed in the batch indata directory.
//     *
//     * @ut.then        No new record will be inserted in the MSISDN table.
//     *                 A new record will be inserted in the BACO_BATCH_CONTROL
//     *                 table.
//     *                 There will be no recovery file.
//     *                 A report file will be created which will containing error code rows and
//     *                 a trailing record saying that no successful statusChanges have been made.
//     *                 The test file will be renamed with a new extension. 
//     * 
//     * @ut.attributes  +f
//     */
//    public void testMsisdnRoutingProvisioning_InvalidSDP()
//    {
//    	final int      L_NO_SUCCESSFUL_RECORDS = 0;
//    	final int      L_NO_ERRORNOUS_RECORDS  = 1;
//    	
//        final String[] L_REPORT_ROWS = { "16",   // Invalid SDP ID
//                                         getTrailerRecord(L_NO_SUCCESSFUL_RECORDS) };
//        
//        final String[] L_RECOVERY_ROWS = {};
//
//        beginOfTest( C_METHOD_testMsisdnRoutingProvisioning_InvalidSDP );
//
//        MsisdnRoutingProvisioningTestDataTT l_testDataTT =
//            new MsisdnRoutingProvisioningTestDataTT( CommonTestCaseTT.c_ppasRequest,
//                                                     UtilTestCaseTT.c_logger,
//                                                     CommonTestCaseTT.c_ppasContext,
//                                                     C_MSISDN_ROUTING_BATCH_NAME,
//                                                     c_batchInputDataFileDir,
//                                                     C_FILENAME_INVALID_SDP,
//                                                     JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
//                                                     BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
//                                                     L_NO_SUCCESSFUL_RECORDS,
//                                                     L_NO_ERRORNOUS_RECORDS );
//        
//        l_testDataTT.defineReportRows( L_REPORT_ROWS );
//        l_testDataTT.defineRecoveryRows( L_RECOVERY_ROWS );
//        
//        // Index for MSISDN to be used in the indata file. For this test there will only be one MSISDN
//        // belonging to an installed subscriber. This will generate errorcode = 12
//        i_msisdnIndex = 1;
//    
//        // For this test shall no Routing Msisdn be generated, only error codes will be tested.
//        i_generateRoutingMsisdns = true;
//
//        completeFileDrivenBatchTestCase( l_testDataTT );
//
//        endOfTest();
//    }


    // =========================================================================
    // == Public class method(s).                                             ==
    // =========================================================================
    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite( MsisdnRoutingProvisioningBatchControllerUT.class );
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println( "Parameters are: " + p_args );
        junit.textui.TestRunner.run( suite() );
    }
    

    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /**
     * Returns an instance of the <code>MsisdnRoutingProvisioningBatchController</code> class.
     * 
     * @param p_batchTestDataTT  the current batch test data object, which in this case should be
     *                           an instance of the <code>MsisdnRoutingProvisioningTestDataTT</code> class.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController( BatchTestDataTT p_batchTestDataTT )
    {
        MsisdnRoutingProvisioningTestDataTT l_testDataTT = null;
        
        l_testDataTT = (MsisdnRoutingProvisioningTestDataTT)p_batchTestDataTT;
        
        return createMsisdnRoutingProvisioningBatchController( l_testDataTT.getTestFilename() );
    }


    /**
     * Returns the required additional properties layers for the status change batch.
     * 
     * @return the required additional properties layers for the misc data upload batch.
     */
    protected String getPropertiesLayers()
    {
        return "batch_mrp";
    }


    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();

        // Set number of processor threads to one. This is needed to ensure that the data rows are
        // processed in the same order as they are written in the input data file,
        // i.e. to avoid race conditions.
        c_ppasContext.getProperties().setProperty( BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS, "1" );

    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }


    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Returns an instance of the <code>MsisdnRoutingProvisioningBatchController</code> class.
     * 
     * @param p_testFilename  the name of the input data test file.
     * 
     * @return an instance of the <code>MsisdnRoutingProvisioningBatchController</code> class.
     */
    private MsisdnRoutingProvisioningBatchController createMsisdnRoutingProvisioningBatchController( String p_testFilename )
    {
        MsisdnRoutingProvisioningBatchController l_MsisdnRoutingProvisioningBatchController = null;
        HashMap                                  l_batchParams                              = null;

        l_batchParams = new HashMap();
        l_batchParams.put( BatchConstants.C_KEY_INPUT_FILENAME, p_testFilename );
        
        try
        {
            l_MsisdnRoutingProvisioningBatchController =
                new MsisdnRoutingProvisioningBatchController( CommonTestCaseTT.c_ppasContext,
                                                              BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE,
                                                              super.getJsJobID(BatchTestCaseTT.C_BATCH_CONTROL_TABLE_PREFIX),
                                                              C_CLASS_NAME,
                                                              l_batchParams );
        }
        catch (Exception l_Ex)
        {
            sayTime( "***ERROR: Failed to create a 'MsisdnRoutingProvisioningBatchController' instance!" );
            super.failedTestException( l_Ex );
        }

        return l_MsisdnRoutingProvisioningBatchController;
    }


    //==========================================================================
    // Inner class(es).
    //==========================================================================
    private class MsisdnRoutingProvisioningTestDataTT extends BatchTestDataTT
    {
        //======================================================================
        // Private constants(s).
        //======================================================================
        /** The test data filename prefix. */
        private static final String C_PREFIX_TEST_DATA_FILENAME = "MSISDN_LOAD";

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_MSISDN  = "<MSISDN>";

        /** The regular expression used to find the 'MSISDN' template data tag. */
        private static final String C_REGEXP_DATA_TAG_MSISDN    = "^(" + C_TEMPLATE_DATA_TAG_MSISDN + ").*$";

        /** SQL to perform a "false" disconnection - to get errorcode 08. */
        private static final String C_DELETE_MSIS_MSISDN =
        	"DELETE FROM msis_msisdn WHERE msis_mobile_number = {0}";

        // =====================================================================
        //  Private attribute(s).
        // =====================================================================
        /** The name of the template test file. */
        private String  i_templateTestFilename = null;

        /** The 'MSISDN' template data tag <code>Pattern</code> object. */
        private Pattern i_msisdnDataTagPattern = null;

        // =====================================================================
        //  Constructor(s).
        // =====================================================================
        /**
         * Constructs an <code>MsisdnRoutingProvisioningTestDataTT</code> using the given parameters.
         * 
         * @param p_ppasRequest  The <code>PpasRequest</code> object.
         * @param p_logger       The <code>Logger</code> object.
         * @param p_ppasContext  The <code>PpasContext</code> object.
         */
        private MsisdnRoutingProvisioningTestDataTT( PpasRequest p_ppasRequest,
                                                     Logger      p_logger,
                                                     PpasContext p_ppasContext,
                                                     String      p_batchName,
                                                     File        p_batchInputFileDir,
                                                     String      p_templateTestFilename,
                                                     int         p_expectedJobExitStatus,
                                                     char        p_expectedBatchJobControlStatus,
                                                     int         p_expectedNoOfSuccessRecords,
                                                     int         p_expectedNoOfFailedRecords )
        {
            super( p_ppasRequest,
                   p_logger,
                   p_ppasContext,
                   p_batchName,
                   p_batchInputFileDir,
                   p_expectedJobExitStatus,
                   p_expectedBatchJobControlStatus,
                   p_expectedNoOfSuccessRecords,
                   p_expectedNoOfFailedRecords );

            i_templateTestFilename  = p_templateTestFilename;

            // Create 'Pattern' object for template data tags.
            // NOTE: The data tag will be placed in matcher group 1 if it is found in the template record.
            //       See method '' for the usage of the 'Pattern' object.
            i_msisdnDataTagPattern = Pattern.compile( C_REGEXP_DATA_TAG_MSISDN );
            
        }


        //==========================================================================
        // Protected method(s).
        //==========================================================================
        

        /**
         * Creates a test file.
         * @throws PpasServiceException  if it fails to create a test file.
         */
        protected void createTestFile() throws PpasServiceException, IOException
        {
            createTestFile( C_PREFIX_TEST_DATA_FILENAME,
                            BatchTestDataTT.C_SUFFIX_ORDINARY_TEST_DATA_FILENAME,
                            i_templateTestFilename,
                            C_CLASS_NAME );
        }


        /**
         * Replaces any found data tag in the given data string by real data and returns the resulting string.
         * 
         * @param p_dataStr  the data string.
         * 
         * @return  the resulting string after data tags have been replaced by real data.
         */
        protected String replaceDataTags( String p_dataRow )
        {
            final int    L_DATA_TAG_GROUP_NUMBER = 1;

            StringBuffer l_dataRowSB             = null;
            Matcher      l_matcher               = null;
            String       l_tmpMsisdn       = null;
            int          l_tagBeginIx            = 0;
            int          l_tagEndIx              = 0;
            
            String       l_tmpRow                = p_dataRow;
            
            
            say("**START replaceDataTags : ]"+p_dataRow+"[");
            if (p_dataRow != null)
            {
                l_dataRowSB = new StringBuffer( l_tmpRow );
                l_matcher   = i_msisdnDataTagPattern.matcher( l_tmpRow );
                if (l_matcher.matches())
                {
                    l_tagBeginIx = l_matcher.start( L_DATA_TAG_GROUP_NUMBER );
                    l_tagEndIx   = l_matcher.end( L_DATA_TAG_GROUP_NUMBER );
                    l_tmpMsisdn  = (String)getSubscriberData( i_msisdnIndex++ );
                    l_dataRowSB.replace( l_tagBeginIx, l_tagEndIx, l_tmpMsisdn );
                }
            }

            return l_dataRowSB.toString();
        }


        /**
         * Verifies that the ASCS database has been properly updated.
         * 
         * @throws PpasServiceException if it fails to get the ASCS database info.
         */
        protected void verifyAscsDatabase() throws PpasServiceException
        {
        	say("*** start verify database ***");
            PpasAdditionalInfoService l_additionalInfoServ = null;
            AdditionalInfoData        l_additionalInfoData = null;
            BasicAccountData          l_basicAccountData   = null;
            String                    l_msisdn             = null;
            StringBuffer              l_tmpSelect             = null;
            String                    l_sql              = null;
            

            l_tmpSelect = new StringBuffer("select * from msis_msisdn where msis_mobile_number in (");
            
            for ( int i = 0; i < getNumberOfTestSubscribers(); i++ )
            {
            	l_msisdn = ((String)getSubscriberData(i)).trim();
                say("########### MTR - verifyAscsDatabase ############### i: " + i + " , l_msisdn=" + l_msisdn);
                l_tmpSelect.append("'");
                l_tmpSelect.append(l_msisdn);
                l_tmpSelect.append("',");
            }
            int l_last = l_tmpSelect.length();
            l_tmpSelect = l_tmpSelect.replace(l_last-1, l_last, ")");
            l_sql = l_tmpSelect.toString();
            say("***verifyAscsDatabase*** sql: ]"+l_sql+"[");
            try
            {
            JdbcResultSet l_resultSet = sqlQuery(new SqlString(200,0,l_sql));
            while ( l_resultSet.next(100))
            {
            	String l_tmpMsisdn = l_resultSet.getString(101, "MSIS_MOBILE_NUMBER");
                String l_status    = l_resultSet.getString(102, "MSIS_STATUS");
                String l_sdpId     = l_resultSet.getString(103, "MSIS_SDP_ID");
                say("msisdn: "+l_tmpMsisdn + " status: "+l_status + " sdp: " + l_sdpId);
                assertEquals("VerifyDatabase - unexpected status ", l_status, "AV");
            }
            }
            catch (Exception l_Ex)
            {
            	say("exeption verifyAscsDatabase:"+l_Ex.getMessage());
            }
            finally
            {
            	say("finally verifyAscsDatabase");
            }
            say("*** end verify database ***");
        }

        /**
         * Verifies that the report file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyReportFile() throws IOException
        {
        	int    l_noGeneratedReportRows      = 0;
    		int    l_noExpectedRowsInReportFile = 0;
    		Vector l_reportData                 = null;
//    		String l_tmpTrailer                 = null;
    		
        	say("*** start verifyReportFile *** ");
        	
        	l_reportData = readNewFile( c_batchReportDataFileDir, 
        			                    i_testFilename, 
 			                            "DAT", 
                                        "RPT" );
        	
        	say("***verifyReportFile*** expected no success= " + i_expectedNoOfSuccessRecords + " expected no failed= " + i_expectedNoOfFailedRecords);
        	if ( i_expectedNoOfFailedRecords == 0 )
        	{
        		// SUCCESS expected!
        		// All successful - only one row (the trailing record) in the result file
        		assertEquals( "***verifyReportFile*** Wrong number of rows in the report file.",
        				      1, l_reportData.size() );
        		
//                l_tmpTrailer = i_expectedReportData[0];
//        		say("***verifyReportFile*** **MTR** first element: ]"+ (((String)l_reportData.firstElement())+"[ expected : ]"+(l_tmpTrailer))+"[") ;

          		assertTrue( "***verifyReportFile*** Trailer string not as expected : ",
                            ((String)l_reportData.firstElement()).equals(i_expectedReportData[0]));
        	}
        	else
        	{      		
        		say("###verifyReportFile### report file has failed records..");
        		
        		l_noGeneratedReportRows      = l_reportData.size();  // Rows with error codes and trailing record
        		l_noExpectedRowsInReportFile = i_expectedReportData.length;
        		say("###verifyReportFile### noGeneratedReportRows:"+l_noGeneratedReportRows+" noExp:"+l_noExpectedRowsInReportFile);
        		assertTrue( ("Report file has wrong number of rows expected:" + l_noExpectedRowsInReportFile +
        				    " found:" + l_noGeneratedReportRows),
        				    l_noGeneratedReportRows==l_noExpectedRowsInReportFile );
        		
        		for ( int i = 0; i < l_noExpectedRowsInReportFile; i++ )
        		{
        			say("###verifyReportFile### i:"+i+" len="+i_expectedReportData[i].length());
        			String l_errorCode = ((String)l_reportData.get(i)).substring(0,i_expectedReportData[i].length());
        			say("###verifyReportFile###, expected row in reportFile :]" + l_errorCode +
        					   "[ found : ]" + i_expectedReportData[i] + "[");
        			assertTrue( "Unexpected row in reportFile :" + l_errorCode+
        					   " but was :" + i_expectedReportData[i], 
        					   l_errorCode.equals(i_expectedReportData[i]));
        		}
        	}
        	say("***verifyReportFile*** *** end verifyReportFile ***");
        }

        /**
         * Verifies that the recovery file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRecoveryFile() throws IOException
        {
        	// No recovery file generated from this batch
        }


        /**
         * Verifies that the input file has been renamed.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRenaming() throws IOException
        {
        	say("*** start verifyRenaming *** ");

        	File   l_file             = null;
        	String l_newInputFileName = constructFileName( c_batchInputDataFileDir, 
                                                           i_testFilename, 
                                                           "DAT", 
                                                           "DNE" );        	
            l_file = new File(l_newInputFileName);
            
            if (l_file.exists())
            {
                say("FILE EXIST ***" + l_file);
            }
            else
            {
            	say( "File DOES NOT EXIST : " + l_file);
            	throw new IOException();            	
            }
        	say("*** end verifyRenaming ***");
        }
        
        ////////////////////////////////////////////////////////////////////////////
        // Redefined methods
        ////////////////////////////////////////////////////////////////////////////
        /**
         * Creates and returns a test data filename. This batch uses a special filename
         * style: MSISDN_LOAD_yyyymmddhhmmss.DAT
         * 
         * @param p_testDataFilenamePrefix  the test data filename prefix.
         * @param p_utName                  the name of the UT test (for instance "MiscDataUploadBatchUT").
         * 
         * @return the test data filename.
         */
        protected String createTestDataFilename(String p_prefix, String p_suffix, String p_utName)
            throws PpasServiceException
        {
        	PpasDateTime l_dateTime         = DatePatch.getDateTimeNow();        	
            StringBuffer l_testDataFilename = null;

            l_testDataFilename = new StringBuffer(p_prefix);
            l_testDataFilename.append(C_DELIM_TEST_DATA_FILENAME);
            l_testDataFilename.append(C_DATE_TEST_DATA_FILENAME.toString_yyyyMMdd());
            l_testDataFilename.append( constructFilledString("00", "", l_dateTime.getHours(),   2) );
            l_testDataFilename.append( constructFilledString("00", "", l_dateTime.getMinutes(), 2) );
            l_testDataFilename.append( constructFilledString("00", "", l_dateTime.getSeconds(), 2) );

            l_testDataFilename.append(p_suffix);

            return l_testDataFilename.toString();
        }

        
        //==========================================================================
        // Private method(s).
        //==========================================================================
        private String getTestFilename()
        {
            return super.i_testFilename;
        }
        
        /**
         * Installs test subscribers.
         * @throws PpasServiceException  if it fails to install test subscribers.
         */
        protected void installTestSubscribers() throws PpasServiceException
        {
        	final String L_MSISDN_FILLER = "000";
        	final int    L_FILLER_LENGTH = L_MSISDN_FILLER.length();

           	BasicAccountData l_subscriber  = null;
           	String           l_tmpMsisdnPrefix = null;
           	String           l_tmpLastDigits   = null;
           	int              l_endingDigits    = 0;
           	String           l_firstMsisdn = null;
           	SqlString        l_tmpDelete   = null;
           	
        	// Install one subscriber - will be used to get starting #MSISDN and
           	// to test to delete an assign customer
        	l_subscriber = installLocalTestSubscriber(null);
        	addSubscriberData(l_subscriber);	
            l_firstMsisdn = getFormattedMsisdn(0);
            
            // Store the MSISDN String instead of BasicAccountData
            removeSubscriberData(0);
//            say("***installTestSubscribers*** l_firstMsisdn:"+l_firstMsisdn);
            addSubscriberData(l_firstMsisdn); 

            // Remove this pre-installed MSISDN from from the database
            l_tmpDelete = new SqlString( 110, 1, C_DELETE_MSIS_MSISDN );
            l_tmpDelete.setStringParam( 0, l_firstMsisdn.trim() );
            
            int noRowsInserted = sqlUpdate( l_tmpDelete );
            
            
//            say("**MTR** SQL: ]" + l_tmpDelete.toString() + "[");


            int noRowsDeleted = sqlUpdate(l_tmpDelete);
//            say("noRowsDeleted: " + noRowsDeleted);

            if ( i_generateRoutingMsisdns )
            {
           	    say("================= GENERATE 10 TEST MSISDNs !!! ================");
        	    // Create a MSISDN prefix - exclude the last 3 digits
                l_tmpMsisdnPrefix = l_firstMsisdn.substring(0, l_firstMsisdn.length() - L_FILLER_LENGTH );
                l_tmpLastDigits   = l_firstMsisdn.substring(l_firstMsisdn.length() - L_FILLER_LENGTH );
               	
               	l_endingDigits = (new Integer(l_tmpLastDigits)).intValue() + 1;
                
                
            	// Create 10 MSISDN strings
            	for ( int i = l_endingDigits; i < l_endingDigits+10; i++ )
            	{
            		String l_tmpMsisdn   = constructFilledString(L_MSISDN_FILLER, l_tmpMsisdnPrefix, i, L_FILLER_LENGTH);
            		say("i:"+i+" ]"+l_tmpMsisdn+"[");
                    addSubscriberData(l_tmpMsisdn);
            	}
            }
            else
            {
            	say("================= DON'T GENERATE MSISDNS !!! ================");
            }

        }

    } // End of inner class 'MsisdnRoutingProvisioningTestDataTT'.
    
    
} // End of class 'MsisdnRoutingProvisioningBatchControllerUT'.