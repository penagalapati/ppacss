////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       BatchReader.java
//      DATE            :       16-April-2004
//      AUTHOR          :       Urban Wigstrom / Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Implements all common logic for batch reading
//                              top-level functionallity.      
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcommon.ThreadControl;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.ServiceKey;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;

/**
 * This abstract class implements all common logic for batch reading.
 */
public abstract class BatchReader
{

    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String   C_CLASS_NAME         = "BatchReader";

    /** Default number of threads in BatchReader.  Value is {@value}. */
    protected static final int    C_MAXIMUM_OF_THREADS = 1;

    /** Inner class name constant used in calls to middleware.  Value is {@value}. */
    private static final String   C_INNER_CLASS_NAME   = ".ReaderThread";
    
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------

    /** Number of threads threads the reader component will start and run. */
    protected int              i_numberOfThreads       = 1;

    /** Array of reference to thread-control objects (one per thread). */
    private ThreadControl[]    i_threadControl         = null;

    /** Reference to the controller component. */
    private BatchController    i_controller            = null;

    /** Reference to the controller component. */
    private SizedQueue         i_inQueue               = null;

    /** Reference to the PpasContext component. */
    protected PpasContext      i_context               = null;

    /** Reference to the paramters. */
    protected Map              i_parameters            = null;
    
    /** Reference to the logger component. */
    protected Logger           i_logger                = null;

    /** PpasProperties for the batch subsystem. */
    protected PpasProperties   i_properties            = null;

    /** Name of the directory for the error log-files. */
    protected String           i_errorFileDirectory    = null;

    /** Name of the directory for the in-data files. */
    protected String           i_inputFileDirectory    = null;

    /** Name of the directory for the output files . */
    protected String           i_outputFileDirectory   = null;

    /** Name of the directory for the recovery files. */
    protected String           i_recoveryFileDirectory = null;
    
    /** Flag for recovery mode. */
    protected boolean          i_recoveryMode          = false;
    
    /**
     * Maximum time to wait to get a JDBC connection from the pool. Default is 
     * <code>BatchConstants.C_DEFAULT_TIMEOUT</code>.
     */
    protected long             i_timeout            = 0;

    /**
     * Constructor for the BatchReader class.
     * @param p_controller  reference to BatchControl
     * @param p_ppasContext the batch context
     * @param p_logger      reference to the logger
     * @param p_parameters  reference to the paramters
     * @param p_inQueue     in queue
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public BatchReader(
        PpasContext      p_ppasContext,
        Logger           p_logger,
        BatchController  p_controller,
        SizedQueue       p_inQueue,
        Map              p_parameters,
        PpasProperties   p_properties)
    {
        super();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
        i_controller = p_controller;
        i_context    = p_ppasContext;
        i_logger     = p_logger;
        i_properties = p_properties;
        i_parameters = p_parameters;
        i_inQueue    = p_inQueue;
        
        String l_jdbcConnTimeoutLong = String.valueOf(i_context.getAttribute(
                                           "com.slb.sema.ppas.support.PpasContext.connResponseTimeout"));

        i_timeout = (l_jdbcConnTimeoutLong != null) ? Long.valueOf(l_jdbcConnTimeoutLong).longValue() :
                                                      BatchConstants.C_DEFAULT_TIMEOUT;
        
        getProperties();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

    } // end of public constructor BatchReader(....)



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_start = "start";
    /**
     * Starts a specified number of BatchReaders.
     */
    public void start()
    {
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10020,
                this,
                BatchConstants.C_ENTERING + C_METHOD_start);
        }
        
        if ( i_numberOfThreads >  0 &&
             i_numberOfThreads <= C_MAXIMUM_OF_THREADS )
        {
            if ( i_threadControl == null)
            {
            
                // Create an array of ThreadControl - thread objects 
                i_threadControl = new ThreadControl[i_numberOfThreads];

                synchronized (i_threadControl)
                {
                    for (int i = 0; i < i_numberOfThreads; i++)
                    {
                        i_threadControl[i] = new ThreadControl();
                    
                        // Must be set here otherwise thread will start and then die directly
                        i_threadControl[i].setRunning(true);

                        // Create new instance of inner class passing thread/object ID...
                        i_threadControl[i].setWorkerThread((new BatchReader.ReaderThread(i)));

                        // ... and start the thread within it
                        i_threadControl[i].getWorkerThread().start();

                        // Set paused flag to allow thread execution
                        i_threadControl[i].setPaused(false);

                        // Set thread status to unknown
                        i_threadControl[i].setState(ThreadControl.C_THREAD_UNKNOWN);
                    }
                }
            }
            else
            {
                // The processing thread(s) has already been created and started.
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10025,
                                this,
                                "The processing thread(s) has already been created and started.");

                // Report the error to the batch controller.
                this.sendBatchMessage(BatchMessage.C_STATUS_ERROR);

            }
        }
        else
        // Illegal number of reader threads -- ONLY ONE can be used!
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_ERROR,
                            C_CLASS_NAME,
                            10035,
                            this,
                            "Invalid number of processor threads is specified in the property '" +
                            BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS + " : " + i_numberOfThreads);

            // Report the error to the batch controller.
            sendBatchMessage(BatchMessage.C_STATUS_ERROR);

        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10030,
                this,
                BatchConstants.C_LEAVING + C_METHOD_start);
        }

        return;

    } // end of method start()



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_stop = "stop";
    /**
     * Stops all existing working BatchReaders.
     */
    public void stop()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10040,
                this,
                BatchConstants.C_ENTERING + C_METHOD_stop);
        }

        // Flag that queue is stopped for writing
//        this.i_inQueue.closeForWriting();
        
        for (int i = 0; i < i_numberOfThreads; i++)
        {
            synchronized (i_threadControl[i])
            {
                i_threadControl[i].setPaused(false);
                i_threadControl[i].setRunning(false);
                i_threadControl[i].notify();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10050,
                this,
                BatchConstants.C_LEAVING + C_METHOD_stop);
        }

        return;

    } // end of method stop()



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_pause = "pause";
    /**
     * Temporary halt all existing working BatchReaders.
     */
    public void pause()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10060,
                this,
                BatchConstants.C_ENTERING + C_METHOD_pause);
        }

        // Flag that the in queue is paused
//        this.i_inQueue.closeForWriting();
        
        for (int i = 0; i < i_numberOfThreads; i++)
        {
            synchronized (i_threadControl[i])
            {
                i_threadControl[i].setPaused(true);
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10070,
                this,
                BatchConstants.C_LEAVING + C_METHOD_pause);
        }

        return;

    } // end of method pause()



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_resume = "resume";
    /**
     * Resumes temporary paused BatchReaders.
     */
    public void resume()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10080,
                this,
                BatchConstants.C_ENTERING + C_METHOD_resume);
        }

        // Flag that the in queue is open for writing
        this.i_inQueue.reopenForWriting();
        
        for (int i = 0; i < i_numberOfThreads; i++)
        {
            synchronized (i_threadControl[i])
            {
                i_threadControl[i].setPaused(false);
                i_threadControl[i].notify();
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10090,
                this,
                BatchConstants.C_LEAVING + C_METHOD_resume);
        }

        return;

    } // end - method resume()


    /** Returns the Controller. 
     * @return A reference to the BatchController. */
    public BatchController getController()
    {
        return( i_controller );
    }


    /**
     * Returns the pause state of a thread.
     * @param p_threadId Thread number.
     * @return           <code>true</code> is paused or <code>false</code> is not paused.
     */
    private boolean isPaused(int p_threadId)
    {
        return i_threadControl[p_threadId].isPaused();
    }



    /**
     * Returns the running state of a thread.
     * @param p_threadId Thread number.
     * @return <code>true</code> is running else <code>false</code>.
     */
    private boolean isRunning(int p_threadId)
    {
        return i_threadControl[p_threadId].isRunning();
    }



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_reportState = "reportState";
    /**
     * Constructs a BatchMessage for state change of a thread.
     * @param p_threadId Internal thread id.
     * @param p_state    The thread state e.g. Running, pause..
     */
    public synchronized void reportState(int p_threadId, int p_state)
    {
//        BatchMessage l_message = null; // Message to send to the controller.

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10100,
                this,
                BatchConstants.C_ENTERING + C_METHOD_reportState);
        }

        // Update the status for this thread
        i_threadControl[p_threadId].setState(p_state);

        // Since there only will be one reader thread we don't need to loop.  
        // TODO - if more than one Reader thread then loop!      
        switch (i_threadControl[p_threadId].getState())
        {
            case ThreadControl.C_THREAD_ERROR :
                this.sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                break;

            case ThreadControl.C_THREAD_RUNNING :
                this.sendBatchMessage(BatchMessage.C_STATUS_STARTED);
                break;

            case ThreadControl.C_THREAD_FINISHED :
                this.sendBatchMessage(BatchMessage.C_STATUS_FINISHED);
                break;

            case ThreadControl.C_THREAD_PAUSED :
                this.sendBatchMessage(BatchMessage.C_STATUS_PAUSED);
                break;

            case ThreadControl.C_THREAD_RESUMED :
                this.sendBatchMessage(BatchMessage.C_STATUS_RESUMED);
                break;

            case ThreadControl.C_THREAD_STOPPED :
                this.sendBatchMessage(BatchMessage.C_STATUS_STOPPED);
                break;

            default :
                // TODO UNKNOWN STATE detected when creating batchMessage
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10110,
                        this,
                        "ReportState : unknown state=" + i_threadControl[p_threadId].getState());
                }

                break;

        }


        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10110,
                this,
                BatchConstants.C_LEAVING + C_METHOD_reportState);
        }

        return;

    } // end of method reportState(..)



    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchDataRecord record.
     * 
     * @return The BatachDataRecord otherwise NULL if "No more records to read".
     */
    protected abstract BatchRecordData getRecord();



    /**
     * This method is invoked during start-up.
     */
    protected abstract void init();



    /**
     * This method is invoked before termination.
     */
    protected abstract void tidyUp();



    /**
     * Creates a BatchMessage for a thread in the BatchReader.
     * @param p_state - the thread state.
     * @return - BatchMessage.
     */
    protected BatchMessage sendBatchMessage( int p_state )
    {
        BatchMessage l_message = new BatchMessage();

        l_message.setSendingComponent(BatchMessage.C_COMPONENT_READER);
        l_message.setTargetComponent(BatchMessage.C_COMPONENT_MANAGER);
        l_message.setRequest(BatchMessage.C_REQUEST_STATUS);
        l_message.setStatus(p_state);

        if (i_controller != null)
        {
            i_controller.postMessage(l_message);
        }
 
        return l_message;

    } // end of private method createBatchMessage(.)



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_addToQueue = "addToQueue";
    /**
     * Adds a record to the batch queue.
     * @param p_record Record to be added to the queue.
     * @throws SizedQueueClosedForWritingException If trying to add records to a queue which has been closed.
     */
    private void addToQueue(BatchRecordData p_record) throws SizedQueueClosedForWritingException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10120,
                this,
                BatchConstants.C_ENTERING + C_METHOD_addToQueue);
        }

        // Might throw an exeption to caller if the queue is closed for writing
        this.i_inQueue.addLast( p_record );

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10130,
                this,
                BatchConstants.C_LEAVING + C_METHOD_addToQueue);
        }

        return;

    } // end of private method addToQueue(.)



    /**
     * This method retrieves the batch properties and 
     * stores them in instance variables.
     */
    private void getProperties()
    {
        i_errorFileDirectory    = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        i_inputFileDirectory    = i_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
        i_numberOfThreads       = 
            i_properties.getIntProperty( BatchConstants.C_NUMBER_OF_READER_THREADS, C_MAXIMUM_OF_THREADS);
        i_outputFileDirectory   = i_properties.getTrimmedProperty(BatchConstants.C_OUTPUT_FILE_DIRECTORY);
        i_recoveryFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);

        return;
    } // end of method getProperties()

    /**
     * Gets the Fetch Size for the DB Service.
     * @return The Fetch Size for the DB Service.
     */
    protected int getFetchSize()
    {
        int l_fetchSize = 0;
        String l_tmpFetchSize = null;
        
        l_tmpFetchSize = i_properties.getProperty(BatchConstants.C_FETCH_ARRAY_SIZE);
        if ( l_tmpFetchSize != null )
        {
            l_fetchSize = Integer.valueOf(l_tmpFetchSize).intValue();
        }
        
        return l_fetchSize;
    } // end of getFetchSize

    /**
     * @return <code>true</code> when it is recovery mode otherwise <code>false</code>.
     */
    protected boolean isRecovery()
    {
        return i_recoveryMode;
    }

    /**
     * This inner class is the engine in the batch data reading.
     * It extends ThreadObject and uses the methods defined in its outer class.
     */
    private class ReaderThread extends ThreadObject
    {
        /** Internal thread id. */
        private int i_threadId;

        /**
         * Constructor for BatchReader.ReaderThread.
         * @param p_threadId Thread identifier to be used when communicating with
         * the BatchReader object.
         * NOTE - currently design only allows one thread to be started, i.e. only one
         * instance of RaderThread will be created.
         */
        public ReaderThread(int p_threadId)
        {
            super();
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_START,
                    C_CLASS_NAME + C_INNER_CLASS_NAME,
                    10200,
                    this,
                    BatchConstants.C_CONSTRUCTING);
            }

            i_threadId = p_threadId;

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_CONFIN_END,
                    C_CLASS_NAME + C_INNER_CLASS_NAME,
                    10210,
                    this,
                    BatchConstants.C_CONSTRUCTED);
            }

        } // end of public constructor ReadThread(.)



        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_doRun = "doRun";
        /**
         * The reader thread will constantly loop within this method doRun()
         * and get records to store in the queue until it is told to stop.
         * The thread can be paused and thereafter resumed without losing data
         * or state.
         */
        public void doRun()
        {
            boolean l_continueToRead = true;
            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME + C_INNER_CLASS_NAME,
                    10220,
                    this,
                    BatchConstants.C_ENTERING + C_METHOD_doRun);
            }

            try
            {
                // OK - we're up and runing - report
                BatchReader.this.reportState(i_threadId, ThreadControl.C_THREAD_RUNNING);

                while ( l_continueToRead )
                {
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME + C_INNER_CLASS_NAME,
                            10221,
                            this,
                            "doRun: while loop start");
                    }
                    
                    // Check if this thread is ordered to stop
                    if (!BatchReader.this.isRunning(i_threadId))
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME + C_INNER_CLASS_NAME,
                                10222,
                                this,
                                "doRun: ordered to stop!");
                        }

                        // Close indata queue for writing, i.e. notify the processor component that the
                        // reader will not put any more records into the queue.
                        BatchReader.this.i_inQueue.closeForWriting();

                        // Report change in status ....
                        BatchReader.this.reportState(i_threadId, ThreadControl.C_THREAD_STOPPED);

                        // .. and break out
                        l_continueToRead = false;
                        continue;
                    }

                    // Check if this thread is ordered to pause
                    if (BatchReader.this.isPaused(i_threadId))
                    {
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME + C_INNER_CLASS_NAME,
                                10223,
                                this,
                                "doRun: ordered to pause!");
                        } 
                        
                        // Close indata queue for writing, i.e. notify the processor component that the
                        // reader will not put any more records into the queue until it is resumed.
                        BatchReader.this.i_inQueue.closeForWriting();
                        
                        // Report change in status ...
                        synchronized (BatchReader.this.i_threadControl[i_threadId])
                        {
                            BatchReader.this.reportState(i_threadId, ThreadControl.C_THREAD_PAUSED);

                            // ...and wait for resume (or stop) to be called
                            BatchReader.this.i_threadControl[i_threadId].wait();
                        }
                        
                        BatchReader.this.reportState(i_threadId, ThreadControl.C_THREAD_RESUMED);
                        BatchReader.this.i_inQueue.reopenForWriting();

                    }
                    else
                    {
                        // Do the work
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME + C_INNER_CLASS_NAME,
                                10223,
                                this,
                                "doRun: readRecords...");
                        }                        
                        if (!readRecords())
                        {
                            // No more records to read!
                            if (PpasDebug.on)
                            {
                                PpasDebug.print(
                                    PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME + C_INNER_CLASS_NAME,
                                    10223,
                                    this,
                                    "doRun: NO MORE RECORDS TO READ");
                            } 
                            BatchReader.this.reportState(i_threadId, ThreadControl.C_THREAD_FINISHED) ;
                            l_continueToRead = false;                      
                            continue;
                        }
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME + C_INNER_CLASS_NAME,
                                10223,
                                this,
                                "doRun: read one more record...");
                        }                        

                    }

                } // end of while

                // Call finalise (in first sub-class
                BatchReader.this.tidyUp();

                // Close the in-wueue for writing
                BatchReader.this.i_inQueue.closeForWriting();

                //Thread is finished and will now die!
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_START,
                        C_CLASS_NAME + C_INNER_CLASS_NAME,
                        10223,
                        this,
                        "doRun: thread will die " + getThreadName());
                }

            }

            // All other exceptions will case thread to die
            // (The wait() has been interrupted.)
            catch (InterruptedException e)
            {
                PpasServiceFailedException l_servE = null;
                BatchReader.this.reportState(i_threadId, ThreadControl.C_THREAD_ERROR);
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_END,
                        C_CLASS_NAME + C_INNER_CLASS_NAME,
                        10230,
                        this,
                        this.getThreadName() + " " + C_METHOD_doRun + e +
                        " Exception msg:" + e.getMessage() );
                }
                                
                l_servE = new PpasServiceFailedException(C_CLASS_NAME, 
                                                   C_METHOD_doRun, 
                                                   10440, 
                                                   this, 
                                                   null, 
                                                   0, 
                                                   ServiceKey.get().interruptedWaitOrSleep(),
                                                   e);
                
                BatchReader.this.i_logger.logMessage(l_servE);
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END,
                    C_CLASS_NAME + C_INNER_CLASS_NAME,
                    10230,
                    this,
                    this.getThreadName() + BatchConstants.C_LEAVING + C_METHOD_doRun );
            }
        } // end of method doRun()

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_readRecords = "readRecords";
        /**
         * Reads a record and adds them to the queue. If the queue is full, this
         * thread will wait in the queue until there is space left for the record to
         * be added.
         * @return <code>true</code> if more records to read else it returns <code>false</code>.
         */
        private boolean readRecords()
        {
            BatchRecordData l_record            = null;
            boolean         l_moreRecordsToRead = false;  // assume last one..

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME + C_INNER_CLASS_NAME,
                    10240,
                    this,
                    this.getThreadName() + BatchConstants.C_ENTERING + C_METHOD_readRecords );
            }

            // Call getRecord ( in the second sub-class)
            l_record = BatchReader.this.getRecord();
            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME + C_INNER_CLASS_NAME,
                    10241,
                    this,
                    this.getThreadName() + " Has read a record " + l_record );
            }
            
            if (l_record != null)
            {
                try
                {
                    // Store the record in queue - if queue is full the thread will
                    // wait in the queue until space is available.
                    BatchReader.this.addToQueue(l_record);
                }

                catch (SizedQueueClosedForWritingException e)
                {
                    // This situation may only happen if the reader component is running with more than
                    // one thread.
                    // It is the reader itself that controls the setting of the 'ClosedForWriting'
                    // flag in the queue.
                    // If it should happen anyway - report to the controller and then die.
                    
                    // Set condition for the thread to stop and die.
                    l_moreRecordsToRead = false;
                    
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME + C_INNER_CLASS_NAME,
                            10245,
                            this,
                            this.getThreadName() + "SizedQueueCLosedForWritinException " +
                            C_METHOD_readRecords );
                    } 
                }

                l_moreRecordsToRead = true;

            }
            else
            {
                // No more records to read, report finished
//                BatchReader.this.reportState(i_threadId, ThreadControl.C_THREAD_FINISHED);
                l_moreRecordsToRead = false;
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_END,
                        C_CLASS_NAME + C_INNER_CLASS_NAME,
                        10245,
                        this,
                        "**** No more records to read - STOP! ****" );
                } 

            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END,
                    C_CLASS_NAME + C_INNER_CLASS_NAME,
                    10250,
                    this,
                    this.getThreadName() + BatchConstants.C_LEAVING + C_METHOD_readRecords +
                    " moreRecordsToRead= " + l_moreRecordsToRead );
            }

            return l_moreRecordsToRead;

        } // end of private method readRecords()



        /**
         * Returns the name to use for the thread name of the thread this object
         * is running in.
         * 
         * @return the unique thread name of the thread running in this object.
         */
        protected String getThreadName()
        {
            return "Reader_" + this.i_threadId;
        }


    } // end of inner class BatchReader.ReaderThread
} // end of class BatchReader
