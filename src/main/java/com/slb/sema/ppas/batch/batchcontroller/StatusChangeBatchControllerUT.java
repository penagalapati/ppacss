////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       StatusChangeBatchControllerUT
//      DATE            :       22-June-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WMdata 2007
//
//      DESCRIPTION     :       This is the test program for the Status Change batch.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// 02/03/07 | M.T�rnqvist   | adapt UT batch end-to-end test   | <reference>
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcommon.BatchTestDataTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/**
 * Class to unit test the Subscriber Install Status Change batch.
 */
/** Unit tests the Miscellaneous DataUpload batch controller. */
public class StatusChangeBatchControllerUT extends BatchTestCaseTT
{
    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                = "StatusChangeBatchControllerUT";

    /** The name of the Status Change batch. */
    private static final String C_STATUS_CHANGE_BATCH_NAME  = "BatchStatusChange";

    /** The name of the test data template file to be used for the succesful insert test case. */
    private static final String C_FILENAME_SUCCESSFUL       = "STATUS_CHANGE_SUCCESSFUL.DAT";

    /** The name of the test data template file to be used for the test case with an errornous inputfile. */
    private static final String C_FILENAME_ERROR            = "STATUS_CHANGE_ERRORS.DAT";

    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs a <code>StatusChangeBatchControllerUT</code> instance to be used for
     * unit tests of the <code>StatusChangeBatchController</code> class.
     * 
     * @param p_name  the name of the current test.
     */
    public StatusChangeBatchControllerUT(String p_name)
    {
        super(p_name);
    }


    // =========================================================================
    // == Public unit test method(s).                                         ==
    // =========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testStatusChangeBatch_Successful =
        "testStatusChangeBatch_Successful";
    /**
     * Performs a fully functional test of a successful 'status change'.
     * A test file with a valid name containing 3 valid status change record is
     * created and placed in the batch indata directory. The status change
     * batch is started in order to process the test file and update the MSISDN table
     *  table, CUST_COMMENTS table
     *
     * @ut.when        A test file containing 3 valid status change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        2 new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There wouldn't be any recovery file.
     *                 A report file will be created which will contain only one
     *                 success trailing record.
     *                 The test file will be renamed with a new extension, DNE. 
     * 
     * @ut.attributes  +f
     */
    public void testStatusChangeBatch_Successful()
    {
    	final int      L_NO_SUCCESSFUL_RECORDS = 3;
    	final int      L_NO_ERRORNOUS_RECORDS  = 0;
        final String[] L_REPORT_ROWS           = {getTrailerRecord(L_NO_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_ROWS         = {};

        beginOfTest(C_METHOD_testStatusChangeBatch_Successful);

        StatusChangeTestDataTT l_testDataTT =
            new StatusChangeTestDataTT( CommonTestCaseTT.c_ppasRequest,
                                        UtilTestCaseTT.c_logger,
                                        CommonTestCaseTT.c_ppasContext,
                                        C_STATUS_CHANGE_BATCH_NAME,
                                        c_batchInputDataFileDir,
                                        C_FILENAME_SUCCESSFUL,
                                        JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                        BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                        L_NO_SUCCESSFUL_RECORDS,
                                        L_NO_ERRORNOUS_RECORDS );

        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows(L_RECOVERY_ROWS );

        completeFileDrivenBatchTestCase( l_testDataTT );

        endOfTest();
    }


    
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testStatusChangeBatch_Error =
        "testStatusChangeBatch_Error";
    /**
     * Performs a fully functional test of a successful 'status change'.
     * A test file with a valid name containing 7 invalid status change record is
     * created and placed in the batch indata directory. The status change
     * batch is started in order to process the test file and update the MSISDN table,
     * CUST_COMMENTS table
     *
     * @ut.when        A test file containing 7 invalid status change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        2 new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There will be a recovery file, 7 rows indicating errors.
     *                 A report file will be created which will contain 7 error code rows and
     *                 a trailing record saying that no successful statusChanges have been made.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testStatusChangeBatch_Error()
    {
    	final int      L_NO_SUCCESSFUL_RECORDS = 0;
    	final int      L_NO_ERRORNOUS_RECORDS  = 7;
    	
        final String[] L_REPORT_ROWS = { "01",   //Invalid record format
                                         "01", 
                                         "02",   //Non-numeric MSISDN
                                         "03",   //Invalid status
                                         "04",   //MSISDN not installed on ASCS
                                         "04",
                                         "08",   //MSISDN is already disconnected
                                         getTrailerRecord(L_NO_SUCCESSFUL_RECORDS) };
        
        final String[] L_RECOVERY_ROWS = { "1,ERR",
                                           "2,ERR",
                                           "3,ERR",
                                           "4,ERR",
                                           "5,ERR",
                                           "6,ERR",
                                           "7,ERR" };

        beginOfTest( C_METHOD_testStatusChangeBatch_Error );

        StatusChangeTestDataTT l_testDataTT =
            new StatusChangeTestDataTT( CommonTestCaseTT.c_ppasRequest,
                                        UtilTestCaseTT.c_logger,
                                        CommonTestCaseTT.c_ppasContext,
                                        C_STATUS_CHANGE_BATCH_NAME,
                                        c_batchInputDataFileDir,
                                        C_FILENAME_ERROR,
                                        JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                        BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                        L_NO_SUCCESSFUL_RECORDS,
                                        L_NO_ERRORNOUS_RECORDS );
        
        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows( L_RECOVERY_ROWS );

        completeFileDrivenBatchTestCase( l_testDataTT );

        endOfTest();
    }



    // =========================================================================
    // == Public class method(s).                                             ==
    // =========================================================================
    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite( StatusChangeBatchControllerUT.class );
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println( "Parameters are: " + p_args );
        junit.textui.TestRunner.run( suite() );
    }
    

    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /**
     * Returns an instance of the <code>StatusChangeBatchController</code> class.
     * 
     * @param p_batchTestDataTT  the current batch test data object, which in this case should be
     *                           an instance of the <code>StatusChangeTestDataTT</code> class.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController( BatchTestDataTT p_batchTestDataTT )
    {
        StatusChangeTestDataTT l_testDataTT = null;
        
        l_testDataTT = (StatusChangeTestDataTT)p_batchTestDataTT;
        
        return createStatusChangeBatchController( l_testDataTT.getTestFilename() );
    }


    /**
     * Returns the required additional properties layers for the status change batch.
     * 
     * @return the required additional properties layers for the misc data upload batch.
     */
    protected String getPropertiesLayers()
    {
        return "batch_sta";
    }


    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();

        // Set number of processor threads to one. This is needed to ensure that the data rows are
        // processed in the same order as they are written in the input data file,
        // i.e. to avoid race conditions.
        c_ppasContext.getProperties().setProperty( BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS, "1" );

    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }


    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Returns an instance of the <code>StatusChangeBatchController</code> class.
     * 
     * @param p_testFilename  the name of the input data test file.
     * 
     * @return an instance of the <code>StatusChangeBatchController</code> class.
     */
    private StatusChangeBatchController createStatusChangeBatchController( String p_testFilename )
    {
        StatusChangeBatchController l_statusChangeBatchController = null;
        HashMap                     l_batchParams                 = null;

        l_batchParams = new HashMap();
        l_batchParams.put( BatchConstants.C_KEY_INPUT_FILENAME, p_testFilename );
        
        try
        {
            l_statusChangeBatchController =
                new StatusChangeBatchController( CommonTestCaseTT.c_ppasContext,
                                                 BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE,
                                                 super.getJsJobID(BatchTestCaseTT.C_BATCH_CONTROL_TABLE_PREFIX),
                                                 C_CLASS_NAME,
                                                 l_batchParams );
        }
        catch (Exception l_Ex)
        {
            sayTime( "***ERROR: Failed to create a 'StatusChangeBatchController' instance!" );
            super.failedTestException( l_Ex );
        }

        return l_statusChangeBatchController;
    }


    //==========================================================================
    // Inner class(es).
    //==========================================================================
    private class StatusChangeTestDataTT extends BatchTestDataTT
    {
        //======================================================================
        // Private constants(s).
        //======================================================================
        /** The test data filename prefix. */
        private static final String C_PREFIX_TEST_DATA_FILENAME = "INSTALL_STATUS";

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_MSISDN  = "<MSISDN>";

        /** The regular expression used to find the 'MSISDN' template data tag. */
        private static final String C_REGEXP_DATA_TAG_MSISDN    = "^(" + C_TEMPLATE_DATA_TAG_MSISDN + ").*$";

        /** SQL to perform a "false" disconnection - to get errorcode 08. */
        private static final String C_UPDATE_MSISDN_TO_DISCONNECT_IN_PROGRESS =
        	"UPDATE cust_mast SET cust_status = 'P' WHERE cust_mobile_number = '<MSISDN>'";

        // =====================================================================
        //  Private attribute(s).
        // =====================================================================
        /** The name of the template test file. */
        private String  i_templateTestFilename = null;

        /** The 'MSISDN' template data tag <code>Pattern</code> object. */
        private Pattern i_msisdnDataTagPattern = null;

        // =====================================================================
        //  Constructor(s).
        // =====================================================================
        /**
         * Constructs an <code>StatusChangeTestDataTT</code> using the given parameters.
         * 
         * @param p_ppasRequest  The <code>PpasRequest</code> object.
         * @param p_logger       The <code>Logger</code> object.
         * @param p_ppasContext  The <code>PpasContext</code> object.
         */
        private StatusChangeTestDataTT( PpasRequest p_ppasRequest,
                                        Logger      p_logger,
                                        PpasContext p_ppasContext,
                                        String      p_batchName,
                                        File        p_batchInputFileDir,
                                        String      p_templateTestFilename,
                                        int         p_expectedJobExitStatus,
                                        char        p_expectedBatchJobControlStatus,
                                        int         p_expectedNoOfSuccessRecords,
                                        int         p_expectedNoOfFailedRecords )
        {
            super( p_ppasRequest,
                   p_logger,
                   p_ppasContext,
                   p_batchName,
                   p_batchInputFileDir,
                   p_expectedJobExitStatus,
                   p_expectedBatchJobControlStatus,
                   p_expectedNoOfSuccessRecords,
                   p_expectedNoOfFailedRecords );

            i_templateTestFilename  = p_templateTestFilename;

            // Create 'Pattern' object for template data tags.
            // NOTE: The data tag will be placed in matcher group 1 if it is found in the template record.
            //       See method '' for the usage of the 'Pattern' object.
            i_msisdnDataTagPattern = Pattern.compile( C_REGEXP_DATA_TAG_MSISDN );
            
        }


        //==========================================================================
        // Protected method(s).
        //==========================================================================
        

        /**
         * Creates a test file.
         * @throws PpasServiceException  if it fails to create a test file.
         */
        protected void createTestFile() throws PpasServiceException, IOException
        {
            super.createTestFile( C_PREFIX_TEST_DATA_FILENAME,
                                  BatchTestDataTT.C_SUFFIX_ORDINARY_TEST_DATA_FILENAME,
                                  i_templateTestFilename,
                                  C_CLASS_NAME );
        }


        /**
         * Replaces any found data tag in the given data string by real data and returns the resulting string.
         * 
         * @param p_dataStr  the data string.
         * 
         * @return  the resulting string after data tags have been replaced by real data.
         */
        protected String replaceDataTags( String p_dataRow )
        {
            final int    L_DATA_TAG_GROUP_NUMBER = 1;
            final String L_SPECIAL_MSISDN_TAG    = "<MSISDN2>";

            StringBuffer l_dataRowSB             = null;
            Matcher      l_matcher               = null;
            String       l_formattedMsisdn       = null;
            int          l_tagBeginIx            = 0;
            int          l_tagEndIx              = 0;
            
            String       l_tmpRow                = p_dataRow;
            int          l_index                 = 0;
            
            
            say("**START replaceDataTags : ]"+p_dataRow+"[");
            if (p_dataRow != null)
            {
            	if ( p_dataRow.indexOf( L_SPECIAL_MSISDN_TAG )==0 )
            	{
            		l_tmpRow = p_dataRow.replaceAll( "2", "" );
            		l_index  = 1;
            		say("**MTR** special row: ]"+l_tmpRow+"[");
            	}
                l_dataRowSB = new StringBuffer( l_tmpRow );
                l_matcher   = i_msisdnDataTagPattern.matcher( l_tmpRow );
                if (l_matcher.matches())
                {
                    l_tagBeginIx      = l_matcher.start( L_DATA_TAG_GROUP_NUMBER );
                    l_tagEndIx        = l_matcher.end( L_DATA_TAG_GROUP_NUMBER );
                    l_formattedMsisdn = getFormattedMsisdn( l_index );
                    l_dataRowSB.replace( l_tagBeginIx, l_tagEndIx, l_formattedMsisdn );
                }
            }

            return l_dataRowSB.toString();
        }


        /**
         * Verifies that the ASCS database has been properly updated.
         * 
         * @throws PpasServiceException if it fails to get the ASCS database info.
         */
        protected void verifyAscsDatabase() throws PpasServiceException
        {
        	say("*** start verify database ***");
            PpasAdditionalInfoService l_additionalInfoServ = null;
            AdditionalInfoData        l_additionalInfoData = null;
            BasicAccountData          l_basicAccountData   = null;

            l_additionalInfoServ = new PpasAdditionalInfoService( super.i_ppasRequest, 
                                                                  super.i_logger, 
                                                                  super.i_ppasRequest.getContext() );
            for ( int i = 0; i < getNumberOfTestSubscribers(); i++ )
            {
            	l_basicAccountData = (BasicAccountData)super.getSubscriberData(i);
                say("########### MTR - verifyAscsDatabase ############### i: " + i + " , l_basicAccountData=" + l_basicAccountData);
                super.i_ppasRequest.setBasicAccountData( l_basicAccountData );
                l_additionalInfoData = l_additionalInfoServ.getAdditionalInfo( super.i_ppasRequest, 30000L );
                
//                assertNotNull("***ERROR: Failed to validate the ASCS database, no additional info was returned.",
//                              l_additionalInfoData);
                sayTime("l_additionalInfoData = [" + l_additionalInfoData + "]");

//                sayTime("Number of updated fields = [" + l_additionalInfoData.getFieldCount() + "]");

//                assertEquals("***ERROR: The ASCS database has NOT been properly updated, wrong field value.",
//                             "Test case: Successful update.", l_additionalInfoData.getMiscFieldV().get(0));
            }
            
            say("*** end verify database ***");
        }

        /**
         * Verifies that the report file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyReportFile() throws IOException
        {
        	int    l_noGeneratedReportRows      = 0;
    		int    l_noExpectedRowsInReportFile = 0;
    		Vector l_reportData                 = null;
    		String l_tmpTrailer                 = null;
    		
        	say("*** start verifyReportFile *** ");
        	
        	l_reportData = readNewFile( c_batchReportDataFileDir, 
        			                    i_testFilename, 
 			                            "DAT", 
                                        "RPT" );
        	
        	say("***verifyReportFile*** expected no success= " + i_expectedNoOfSuccessRecords + " expected no failed= " + i_expectedNoOfFailedRecords);
        	if ( i_expectedNoOfFailedRecords == 0 )
        	{
        		// SUCCESS expected!
        		// All successful - only one row (the trailing record) in the result file
        		assertEquals( "***verifyReportFile*** Wrong number of rows in the report file.",
        				      1, l_reportData.size() );
        		
                l_tmpTrailer = i_expectedReportData[0];
        		say("***verifyReportFile*** **MTR** first element: ]"+ (((String)l_reportData.firstElement())+"[ expected : ]"+(l_tmpTrailer))+"[") ;

          		assertTrue( "***verifyReportFile*** Trailer string not as expected : ",
                            ((String)l_reportData.firstElement()).equals(l_tmpTrailer));
        	}
        	else
        	{      		
        		say("###verifyReportFile### report file has failed records..");
        		
        		l_noGeneratedReportRows      = l_reportData.size();  // Rows with error codes and trailing record
        		l_noExpectedRowsInReportFile = i_expectedReportData.length;
        		say("###verifyReportFile### noGeneratedReportRows:"+l_noGeneratedReportRows+" noExp:"+l_noExpectedRowsInReportFile);
        		assertTrue( ("Report file has wrong number of rows expected:" + l_noExpectedRowsInReportFile +
        				    " found:" + l_noGeneratedReportRows),
        				    l_noGeneratedReportRows==l_noExpectedRowsInReportFile );
        		
        		for ( int i = 0; i < l_noExpectedRowsInReportFile; i++ )
        		{
        			say("###verifyReportFile### i: len="+i_expectedReportData[i].length());
        			String l_errorCode = ((String)l_reportData.get(i)).substring(0,i_expectedReportData[i].length());
        			say("###verifyReportFile###, expected row in reportFile :]" + l_errorCode +
        					   "[ found : ]" + i_expectedReportData[i] + "[");
        			assertTrue( "Unexpected row in reportFile :" + l_errorCode+
        					   " but was :" + i_expectedReportData[i], 
        					   l_errorCode.equals(i_expectedReportData[i]));
        		}
        	}
        	say("***verifyReportFile*** *** end verifyReportFile ***");
        }

        /**
         * Verifies that the recovery file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRecoveryFile() throws IOException
        {
        	Vector l_recoveryData = null;
        	String l_found        = null;
			String l_expected     = null;
			
        	say("*** start verifyRecoveryFile ***");
        	try
        	{
            	l_recoveryData = readNewFile( c_batchRecoveryDataFileDir, 
            			                      i_testFilename, 
            			                      "DAT", 
            			                      "RCV" );
            	
            	say("expected no success= " + i_expectedNoOfSuccessRecords + "expected no failed= " + i_expectedNoOfFailedRecords);
            	if ( i_expectedNoOfFailedRecords == 0 )
            	{
            		// File should not exist
            		// SUCCESS expected!
            		// All successful - only one row in the result file
            		say("**MTR no recovery file should be found size:" + l_recoveryData.size());
            		assertEquals( "No recovery record should be found.", 
            				      0, l_recoveryData.size());            		
            	}
            	else
            	{
            		say("###verifyReportFile### failure records created");
            		for ( int i = 0; i < l_recoveryData.size(); i++)
            		{
            			l_found    = (String)l_recoveryData.get(i);
            			l_expected = i_expectedRecoveryData[i];
            			assertTrue( "VerifyRecoveryFile failed, found:" + l_found +
            					    " expected:" + l_expected, 
            					    l_found.equals(l_expected));
            		}            		
            	}
        	}
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught "+i_exep.getMessage());
        	}
        	finally
        	{
        		say("FINALLY - recovery file has been checked");
        	}
        	say("*** end verifyRecoverfyFile ***");
        }


        /**
         * Verifies that the input file has been renamed.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRenaming() throws IOException
        {
        	say("*** start verifyRenaming *** ");

        	File   l_file             = null;
        	String l_newInputFileName = constructFileName( c_batchInputDataFileDir, 
                                                           i_testFilename, 
                                                           "DAT", 
                                                           "DNE" );        	
            l_file = new File(l_newInputFileName);
            
            if (l_file.exists())
            {
                say("FILE EXIST ***" + l_file);
            }
            else
            {
            	say( "File DOES NOT EXIST : " + l_file);
            	throw new IOException();            	
            }
        	say("*** end verifyRenaming ***");
        }
        
        
        
        //==========================================================================
        // Private method(s).
        //==========================================================================
        private String getTestFilename()
        {
            return super.i_testFilename;
        }
        
        /**
         * Installs test subscribers.
         * @throws PpasServiceException  if it fails to install test subscribers.
         */
        protected void installTestSubscribers() throws PpasServiceException
        {
        	BasicAccountData l_subscriber = null;
        	Msisdn           l_msisdn     = null;
        	String           l_disconnect = null;
        	String           l_tmpMsisdn  = null;
        	
        	// Create 2 standalone subscribers 
        	l_subscriber = installGlobalTestSubscriber(null);
            addSubscriberData(l_subscriber);
        	l_subscriber = installGlobalTestSubscriber(null);
            addSubscriberData(l_subscriber);
            
            // Update the second MSISDN so that the batch treat it as an already disconnected one.
            l_msisdn     = l_subscriber.getMsisdn();
            l_disconnect = C_UPDATE_MSISDN_TO_DISCONNECT_IN_PROGRESS.replaceAll(C_TEMPLATE_DATA_TAG_MSISDN,
            		                                                            l_msisdn.toString());
            say("**MTR** SQL: ]" + l_disconnect + "[");
            
            l_tmpMsisdn  = getFormattedMsisdn(1).trim();
            l_disconnect = C_UPDATE_MSISDN_TO_DISCONNECT_IN_PROGRESS.replaceAll(C_TEMPLATE_DATA_TAG_MSISDN, 
            		                                                            l_tmpMsisdn);
            say("**MTR** SQL using getFormattedMsisdn: ]" + l_disconnect + "[");
            
            int noRowsUpdated = sqlUpdate(new SqlString(l_disconnect.length(),0,l_disconnect));
            say("noRowsUpdated: " + noRowsUpdated);
        }

    } // End of inner class 'StatusChangeTestDataTT'.
    
    
} // End of class 'StatusChangeBatchControllerUT'.