////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MsisdnRoutingDeletionBatchReader
//      DATE            :       27-October-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class is responsible for implementation of the
//                              method getRecord, which is declared as abstract in 
//                              super class BatchFileReader. For each read line from
//                              file an RoutingBatchDataRecord is created and stored
//                              in the in-queue.
//
//                              Each read line has the format: <MSISDN>.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.MsisdnRoutingBatchRecordData;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
* The reader class for the MsisdnRoutingDeletion batch.
* Input file, has two mandatory fields: MSISDN(15)
*/
public class MsisdnRoutingDeletionBatchReader extends BatchLineFileReader
{
    //-----------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                  = "MsisdnRoutingDeletionBatchReader";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT = "01";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_NON_NUMERIC_MSISDN    = "02";
           
    // Index in the batch record array
    /** Number of fields in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NUMBER_OF_FIELDS            = 1;

    /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_MSISDN                      = 0;

    /** Record length. Value is [@value). */
    private static final int    C_RECORD_LENGTH               = BatchConstants.C_LENGTH_MSISDN;

    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------

    /** The BatchDataRecord to be sent into the Queue. */
    private MsisdnRoutingBatchRecordData i_batchDataRecord = null;



    /**
    * Constructor for MsisdnRoutingDeletionBatchReader.
    * 
    * @param p_controller  Reference to the BatchController that creates this object.
    * @param p_ppasContext Reference to PpasContext.
    * @param p_logger      reference to the logger.
    * @param p_parameters  Reference to a Map holding information e.g. filename.
    * @param p_queue       Reference to the Queue, where all indata records will be added.
    * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
    */
    public MsisdnRoutingDeletionBatchReader( PpasContext      p_ppasContext,
                                             Logger           p_logger,
                                             BatchController  p_controller,
                                             SizedQueue       p_queue,
                                             Map              p_parameters,
                                             PpasProperties   p_properties)
    {
        super( p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties );

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

        return;

    } // end of public Constructor SubInstBatchReader(.....)





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    

    /**
    * This method extracts information from the batch data record. The record must look like:
    * [Line number];[MSISDN][MSISDN][SDP ID][Agent][Promotion Plan][Account Group][Service Offering]
    * The line number will be used to check against the recovery file if the batch mode is
    * recovery.
    * All fields are checked to be alpha numberic.
    * @return <code>true</code> when extracted fields are valid else <code>false</code>.
    */
    private boolean extractLineAndValidate()
    {
        MsisdnFormat l_msisdnFormat        = null;
        Msisdn       l_tmpMsisdn           = null;      // Help variable to convert from string to Msisdn object.
        String[]     l_recordFields        = new String[C_NUMBER_OF_FIELDS];   // Help array to keep the input line elements

        boolean      l_validData           = true;  // return value - assume it is valid
        StringBuffer l_tmpErrorLine        = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10320,
                this,
                BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate );
        }

        // Check record length
        if ( i_inputLine.length() == C_RECORD_LENGTH )
        {
            
            initializeRecordValidation( i_inputLine, l_recordFields, i_batchDataRecord );

            // Get the record fields and check if it is numeric/alphanumberic
            //            Start End                             #Field    Format check                      Errorcode
            if ( getField(0,    BatchConstants.C_LENGTH_MSISDN, C_MSISDN, BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_NON_NUMERIC_MSISDN ) )
            { 
                // Both fields are manatory
                if ( l_recordFields[C_MSISDN] == null )
                {
                    l_validData = false;
                }
                
                else
                {
                    // Got a valid record
                    
                    // Convert the MSISDN string to a Msisdn object
                    // This may give an ParseException - if so flag corrupt line!
                    l_msisdnFormat = i_context.getMsisdnFormatInput();
                    try
                    {
                        l_tmpMsisdn = l_msisdnFormat.parse( l_recordFields[C_MSISDN] );
                        this.i_batchDataRecord.setMsisdn(l_tmpMsisdn);                
                    }
                    catch (ParseException e)
                    {
                        l_validData = false;
                        if ( this.i_batchDataRecord.getErrorLine() == null )
                        {
                            this.i_batchDataRecord.setErrorLine(C_ERROR_NON_NUMERIC_MSISDN);                
                        }
                        if (PpasDebug.on)
                        {
                            PpasDebug.print(
                                PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10370,
                                this,
                                "Row: " + i_lineNumber + " ]" + i_inputLine + "[ has invalid MSISDN" );
                        }
                        e.printStackTrace();
                    }
                } // end - valid record
                             
            } // end - getField OK
            else 
            {
                l_validData = false;
            }
        }
        else
        { 
            // Not a valid record length
            l_validData = false;
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME,
                    10370,
                    this,
                    "*** illegal record length *** Row: " + i_lineNumber + " ]" +
                    i_inputLine + "[ has invalid MSISDN" );
            }
        }
        
        if ( !l_validData )
        {
            // Illegal field format detected
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_START,
                    C_CLASS_NAME,
                    10360,
                    this,
                    "Row: ]" + i_inputLine + "[ is a INVALID record\n" +
                            " l_lineNumber     =" + i_lineNumber + "\n" +
                            " l_msisdn         =" + l_recordFields[C_MSISDN]);
            }
                                                                
            // Flag for corrupt line
            if ( this.i_batchDataRecord.getErrorLine() == null)
            {
                l_tmpErrorLine = new StringBuffer();
                l_tmpErrorLine.append(C_ERROR_INVALID_RECORD_FORMAT);
                l_tmpErrorLine.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                l_tmpErrorLine.append(this.i_batchDataRecord.getInputLine());
                this.i_batchDataRecord.setErrorLine( l_tmpErrorLine.toString());
            }
            this.i_batchDataRecord.setCorruptLine(true);
            
        } // end else - illegal field format detected
            
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10390,
                this,
                BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate );
        }

        return l_validData;

    } // End of method extractLineAndValidate(.)





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
    * This method shall contain all logic necessary to create 
    * and populate the BatchDataRecord record.
    * 
    * @return - null if "No more records to read"
    */
    protected BatchRecordData getRecord()
    {
        StringBuffer l_tmp      = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // Get next line from input file
        i_batchDataRecord = null;

        if ( readNextLine() != null )
        {
            i_batchDataRecord = new MsisdnRoutingBatchRecordData();

            // Store input filename
            i_batchDataRecord.setInputFilename( i_inputFileName );
            
            // Store line-number form the file
            i_batchDataRecord.setRowNumber( i_lineNumber );
            
            // Store original data record
            i_batchDataRecord.setInputLine( i_inputLine );
            
            // Extract data from line
            if ( !extractLineAndValidate() )
            {
                // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10420,
                        this,
                        "*** READER *** extractLineAndValid failed! ");
                }

                // Flag for corrupt line and set error code
                i_batchDataRecord.setCorruptLine( true );
                if ( i_batchDataRecord.getErrorLine() == null )
                {
                    // Other error than specific field errors
                    l_tmp = new StringBuffer();
                    l_tmp.append(C_ERROR_INVALID_RECORD_FORMAT);
                    l_tmp.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                    l_tmp.append(i_batchDataRecord.getInputLine());
                    i_batchDataRecord.setErrorLine( l_tmp.toString() );
                }
                                
            }
            
//            i_batchDataRecord.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
//            i_batchDataRecord.setNumberOfErrorRecords(super.i_notProcessed);
//            if ( super.faultyRecord(i_lineNumber))
//            {
//                i_batchDataRecord.setFailureStatus(true);
//            }
            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10420,
                    this,
                    "*** READER *** before leaving extractLineAndValid " + i_batchDataRecord.dumpRecord());
            }

        } // end if

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10360,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_batchDataRecord;

    } // end of method getRecord()





    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
    * Check if filename is of the valid form: MSISDN_DELETE_yyyymmddhhmiss.DAT /IPG.
    * @param p_fileName Indata file name.
    * @return <code>true</code> if filename is valid else <code>false</code>.
    */
    protected boolean isFileNameValid( String p_fileName )
    {                
        boolean  l_validFileName = true;       // Help variable - return value. Assume filename is OK

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10470,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid + " " + p_fileName);
        }

        if ( p_fileName != null ) 
        {
            if ( p_fileName.matches(BatchConstants.C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_DAT) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_SCH) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_IPG) )
            {
                // OK Filename: MSISDN_DELETE_yyyymmddhhmiss.*
                // Rename the file to .IPG to indicate that processing is in progress.
                // If it is recovery mode, the file already got the "in_progress_extension"
                if ( !i_recoveryMode )
                {
                    // Flag that processing is in progress. Rename the file to *.IPG.
                    // If the batch is running in recovery mode, it already has the extension .IPG
                    if (p_fileName.matches(BatchConstants.C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_SCH))
                    {
                        renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                    }
                    else
                    {
                        renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                    }
                                      
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10480,
                            this,
                            "After rename, inputFullPathName=" + i_fullPathName );
                    }
                                   
                }

            } // End if - valid filename!

            else
            {
                // invalid fileName
                l_validFileName = false;
            }
                    
        } // end - if fileName != null

        else
        {
            l_validFileName = false; 
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10490,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid + " " + l_validFileName);
        }

        return l_validFileName;

    } // End of isFileNameValid(.)

} // End of MsisdnRoutingDeletionBatchReader