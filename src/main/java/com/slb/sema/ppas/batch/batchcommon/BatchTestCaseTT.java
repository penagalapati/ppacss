////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       BatchTestCaseTT.java
//  DATE            :       2004
//  AUTHOR          :       Lars Lundberg
//  REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//  COPYRIGHT       :       WM-data 2007
//
//  DESCRIPTION     :       A super class for batch sub-system Unit Test classes.
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//11/05/06 | Chris         | Added disconnectTestSubscriber   | PpacLon#2250/8691
//         | Harrison      | futureDisconnectTestSubscriber   |
//---------+---------------+----------------------------------+----------------
//26/04/07 | Lars Lundberg | Removed methods:                 | PpacLon#3033/11279
//         |               | createBatchController(...)       |
//         |               | waitForExitStatus(...)           |
//         |               | The construction of the JsRequest|
//         |               | object is changed in the         |
//         |               | startBatch(...) method.          |
//         |               | A lot of comments are changed.   |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcommon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.businessconfig.cache.BusinessConfigCache;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.DisconnectData;
import com.slb.sema.ppas.common.dataclass.DisconnectRequestData;
import com.slb.sema.ppas.common.dataclass.Market;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.ServiceClass;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.sql.JdbcConnection;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.JdbcStatement;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.idgenerator.PpasIdGeneratorException;
import com.slb.sema.ppas.common.support.idgenerator.SequentialIdManager;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasInstallService;
import com.slb.sema.ppas.is.isapi.PpasMsisdnRoutingService;
import com.slb.sema.ppas.is.isc.IsClient;
import com.slb.sema.ppas.is.iscommon.IsContext;
import com.slb.sema.ppas.is.isil.dbservice.DbServiceTT;
import com.slb.sema.ppas.js.jscommon.JobActionException;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.js.jscommon.JsRequest;
import com.slb.sema.ppas.util.file.FileSearch;
import com.slb.sema.ppas.util.file.FileSearchException;

/**
 * This <code>BatchTestCaseTT</code> is a super class for all batch sub-system Unit Test classes.
 * It contains various convenient methods.
 */
public class BatchTestCaseTT extends CommonTestCaseTT
{
    // =========================================================================
    // == Protected constant(s).                                              ==
    // =========================================================================
    /** The batch control data table name prefix. */
    protected static final String C_BATCH_CONTROL_TABLE_PREFIX = "BACO";

    /** Short timeout for batch processing. */
    protected static final long   C_BATCH_SHORT_TIMEOUT        = 3000L;

    /** Timeout for batch processing. */
    protected static final long   C_BATCH_PROCESSING_TIMEOUT   = 5000L;

    /** Default timeout amount. */
    protected static final long   C_DEFAULT_TIMEOUT            = 10000L;

    /** The batch successfully completed status. */
    protected static final char   C_BATCH_STATUS_COMPLETED     = 'C';
    
    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** Class name. */
    private static final String               C_CLASS_NAME     = "BatchTestCaseTT";

    /** MSISDN Routing Service. */
    private static PpasMsisdnRoutingService   c_routeService   = null;

    /** MSISDN Install Service. */
    private static PpasInstallService         c_installService = null;


    // =========================================================================
    // == Protected class attribute(s).                                       ==
    // =========================================================================
    /**
     * DbserviceTT instance requested for the queries on the Db and the creation of subscribers.
     */
    protected static DbServiceTT c_dbService                = null;

    /** The batch input data files directory as a <code>File</code> object. */
    protected static File        c_batchInputDataFileDir    = null;
    
    /** The batch report data files directory as a <code>File</code> object. */
    protected static File        c_batchReportDataFileDir   = null;
    
    /** The batch rerecovery data files directory as a <code>File</code> object. */
    protected static File        c_batchRecoveryDataFileDir = null;

    // =========================================================================
    // == Private class variable(s).                                          ==
    // =========================================================================
    /** First initialisation. */
    private static boolean  c_init      = false;

    /** The input data filename date. */
    private static PpasDate c_fileDate  = new PpasDate("19990101", PpasDate.C_DF_yyyyMMdd);

    /** The input data filename sequence number. */
    private static int      c_fileSeqNo = 1;


    // =========================================================================
    // == Protected instance variable(s).                                     ==
    // =========================================================================
    /** Number of record in each tests files. */
    protected int i_numberOfRecords = 0;
 
    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs a <code>BatchTestCaseTT</code> instance with the given name.
     * 
     * @param p_name  the name of the current UT test.
     */
    public BatchTestCaseTT(String p_name)
    {
        this(p_name, null);
    }
    

    /**
     * Constructs a <code>BatchTestCaseTT</code> instance with the given name.
     * 
     * @param p_name  the name of the current UT test.
     * @param p_batchPropertyFile Name of the relevant batch propertis file (no suffix).
     */
    public BatchTestCaseTT(String p_name, String p_batchPropertyFile)
    {
        super(p_name);

        init(p_batchPropertyFile);
    }
    

    // =========================================================================
    // == Public method(s).                                                   ==
    // =========================================================================
    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_execSqlUpdate = "execSqlQuery";
    /**
      * Executes a SQL query.
      *
      * @param p_sql The SQL statement to execute
      * @throws PpasSqlException If an error occurs when issuing an SQL statement.
      */
    public void execSqlUpdate(SqlString  p_sql)
        throws PpasSqlException
    {
        JdbcStatement  l_statement = null;
        JdbcConnection l_conn      = null;
        
        try
        {
            l_conn      = getConnection(C_CLASS_NAME, C_METHOD_execSqlUpdate, 20000);
            l_statement = l_conn.createStatement(C_CLASS_NAME, C_METHOD_execSqlUpdate, 20010, this, null);
            l_statement.executeQuery(C_CLASS_NAME, C_METHOD_execSqlUpdate, 20020, this, null, p_sql);
        }
        finally
        {
            putConnection(l_conn);
        }
    } // End of public method execSqlQuery()


    /**
     * Returns the trailer record for the report file.
     * 
     * @param p_numberOfSuccessfulRecords  the number of the successfully processed records.
     * @return trailer record
     */
    public String getTrailerRecord(int p_numberOfSuccessfulRecords)
    {
    	String l_tmp = null;
		
		l_tmp = BatchConstants.C_TRAILER_ZEROS + p_numberOfSuccessfulRecords;
		l_tmp = l_tmp.substring( l_tmp.length()-BatchConstants.C_TRAILER_ZEROS.length() );
		
        return l_tmp + BatchConstants.C_TRAILER_SUCCESS;
    }

    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /**
     * Initializes the 'PpasContext' object with required attributes when running a batch.
     */
    protected void initContext()
    {
        IsClient l_isClient = null;

        // Set the 'SequentialIdManager' attribute on the context.
        if (CommonTestCaseTT.c_ppasContext.getAttribute("IssRequestIdGenerator") == null)
        {
            try
            {
                c_ppasContext.setAttribute( IsContext.C_ATTRIB_ISSREQUESTIDGENERATOR,
                                            new SequentialIdManager( "iss_request",
                                                                     c_ppasContext,
                                                                     c_logger));
            }
            catch (Exception l_Ex)
            {
                sayTime("***ERROR: Failed to create a 'SequentialIdManager' instance.");
                CommonTestCaseTT.failedTestException(l_Ex);
            }
        }

        // Set the logger pool attribute on the context.
        c_ppasContext.setLoggerPool(c_loggerPool);    

        // Set the 'IsClient' attribute on the context.
        try
        {
            l_isClient = new IsClient(c_logger, c_ppasContext.getProperties(), c_instManager);
            c_ppasContext.setAttribute("IsClient", l_isClient);
        }
        catch (PpasConfigException l_Ex)
        {
            sayTime("***ERROR: Failed to create an 'IsClient' instance.");
            CommonTestCaseTT.failedTestException(l_Ex);
        }
    }


    /**
     * Returns the required additional properties layers.
     * To be overridden by the sub-class in order to return the specific
     * required additional properties layers.
     * 
     * @return the required additional properties layers.
     */
    protected String getPropertiesLayers()
    {
        return "batch";
    }


    
    /**
     * Returns the actual <code>BatchController</code> object.
     * To be overridden by the sub-class in order to return the specific
     * <code>BatchController</code> object.
     * 
     * @param p_batchTestDataTT  the current batch test data object.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController(BatchTestDataTT p_batchTestDataTT)
    {
        return null;
    }


    protected void setUp()
    {
        String l_addedPropertiesLayers    = null;
        String l_inputDataFileDirStr      = null;
        String l_reportDataFileDirStr     = null;
        String l_recoveryDataFileDirStr   = null;
        File   l_inputDataFileDir         = null;
        File   l_reportDataFileDir        = null;
        File   l_recoveryDataFileDir      = null;

        super.setUp();

        // Load additional properties layers.
        l_addedPropertiesLayers = getPropertiesLayers();
        super.sayTime("Load the '" + l_addedPropertiesLayers + "' additional properties layer.");
        try
        {
            CommonTestCaseTT.c_properties =
                CommonTestCaseTT.c_properties.getPropertiesWithAddedLayers(l_addedPropertiesLayers);
        }
        catch (PpasConfigException l_ppasConfigEx)
        {
            super.sayTime("***ERROR: Failed to read one of the properties files that was found when loading" +
                          " the '" + l_addedPropertiesLayers + "' additional properties layers.");
                super.failedTestException(l_ppasConfigEx);
        }

        if (c_batchInputDataFileDir == null)
        {
            l_inputDataFileDirStr =
                CommonTestCaseTT.c_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
            super.assertNotNull("***ERROR: The mandatory batch input file directory property '" +
                                BatchConstants.C_INPUT_FILE_DIRECTORY + "' is not set.",
                                l_inputDataFileDirStr);
        
            l_inputDataFileDir = new File(l_inputDataFileDirStr);
            super.assertTrue("***ERROR: The batch input data files directory doesn't exist or " +
                             "isn't a directory or isn't readable.",
                             (l_inputDataFileDir.exists() &&
                              l_inputDataFileDir.isDirectory() &&
                              l_inputDataFileDir.canRead()));
    
            c_batchInputDataFileDir = l_inputDataFileDir;
            super.sayTime("The batch input data file directory = '" +
                          c_batchInputDataFileDir.getAbsolutePath() + "'");
        }
        
        if (c_batchReportDataFileDir == null)
        {
            l_reportDataFileDirStr =
                CommonTestCaseTT.c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
            super.assertNotNull("***ERROR: The mandatory batch report file directory property '" +
                                BatchConstants.C_REPORT_FILE_DIRECTORY + "' is not set.",
                                l_reportDataFileDirStr);
        
            l_reportDataFileDir = new File(l_reportDataFileDirStr);
            super.assertTrue("***ERROR: The batch report data files directory doesn't exist or " +
                             "isn't a directory or isn't readable.",
                             (l_reportDataFileDir.exists() &&
                              l_reportDataFileDir.isDirectory() &&
                              l_reportDataFileDir.canRead()));
    
            c_batchReportDataFileDir = l_reportDataFileDir;
            super.sayTime("The batch report data file directory = '" +
                          c_batchReportDataFileDir.getAbsolutePath() + "'");
        }

        if (c_batchRecoveryDataFileDir == null)
        {
            l_recoveryDataFileDirStr =
                CommonTestCaseTT.c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
            super.assertNotNull("***ERROR: The mandatory batch recovery file directory property '" +
                                BatchConstants.C_RECOVERY_FILE_DIRECTORY + "' is not set.",
                                l_recoveryDataFileDirStr);
        
            l_recoveryDataFileDir = new File(l_recoveryDataFileDirStr);
            super.assertTrue("***ERROR: The batch recovery data files directory doesn't exist or " +
                             "isn't a directory or isn't readable.",
                             (l_recoveryDataFileDir.exists() &&
                              l_recoveryDataFileDir.isDirectory() &&
                              l_recoveryDataFileDir.canRead()));
    
            c_batchRecoveryDataFileDir = l_recoveryDataFileDir;
            super.sayTime("The batch recovery data file directory = '" +
                          c_batchRecoveryDataFileDir.getAbsolutePath() + "'");
        }

    }


    /**
	 * Creates and returns a filename with the default layout with the given
	 * prefix and extension.
	 * <br>The default layout is <prefix><date>_<seq_no>.<extension>.
	 * <br>+ The <date> has the format yyyymmdd, for instance 19000101.
	 * <br>+ The <seq_no> consists of five digits, right adjusted filled with
	 *       zeroes (0), for instance 00001.
	 *
	 * @param p_filenamePrefix     the filename prefix.
	 * @param p_filenameExtension  the filename extension.
	 * 
	 * @return a filename with the default layout with the given prefix and extension.
	 */
	protected String createDefaultIndataFilename(String p_filenamePrefix, String p_filenameExtension)
	{
	    StringBuffer l_indataFilenameBuf = null;
	
	    l_indataFilenameBuf = new StringBuffer(p_filenamePrefix);
	    l_indataFilenameBuf.append(c_fileDate.toString_yyyyMMdd());
	    l_indataFilenameBuf.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
	    l_indataFilenameBuf.append(adjustString("" + c_fileSeqNo, 5, '0', true));
	    l_indataFilenameBuf.append('.');
	    l_indataFilenameBuf.append(p_filenameExtension);
	
	    // Increment file date and seq. no.
	    if (c_fileSeqNo == 99999)
	    {
	        c_fileSeqNo = 1;
	        c_fileDate.add(PpasDate.C_FIELD_DATE, 1);
	    }
	    else
	    {
	        c_fileSeqNo++;
	    }
	
	    return l_indataFilenameBuf.toString();
	}


	/**
     * Runs a complete file driven batch test case.
     * 
     * This test case performes the following steps:
     *  1. Install test subscribers if necessary, either only locally in the ASCS database or globally
     *     on the SDP (i.e. the Emulator).
     *
     *  2. Create a test file (with known data).
     *     A test file with a describing name (for instance UPDATE_MISC_SUCCESSFUL_INSTALL.DAT)
     *     should be stored in the specific test data directory and copied to the batchfiles/data
     *     directory (this is done in the build.xml file as ant commands).
     *     The test file could contain specific data tags which should be replaced by real data.
     *     The test file should be renamed to a name required by the actual batch (for instance
     *     UPDATE_MISC_20000101_00001.DAT).
     *
     *  3. Create an instance of the actual BatchController class.
     *
     *  4. Start the actual batch.
     *
     *  5. Wait for the batch to finish.
     *
     *  6. Verify that the exit status is equal to the expected exit status.
     *
     *  7. Verify that the BACO_BATCH_CONTROL table has been updated as expected.
     *
     *  8. Verify that the report file (*.rpt) has been created with the expected name and contents.
     *  
     *  9. Verify that a recovery file has been or not has been stored in the batchfiles/recovery
     *     directory. A successful batch should remove any created recovery file.
     *     If a recovery file has been stored its name and contents should be verified in this step.
     *  
     * 10. Verify that the ASCS database has been updated as expected.
     *
     * 11. Verify that the input data test file has been renamed as expected.
     *
     * @param p_batchTestDataTT  the current batch test data object.
     */
    protected void completeFileDrivenBatchTestCase(BatchTestDataTT p_batchTestDataTT)
    {
        BatchController l_batchController   = null;
        int             l_initialExitStatus = -1;


        // 1. Install test subscriber(s).
        sayTime("Install test subscriber(s).");
        try
        {
            p_batchTestDataTT.installTestSubscribers();
        }
        catch (PpasServiceException l_ppasServEx)
        {
            sayTime("***ERROR: Failed to install test subscriber(s).");
            super.failedTestException(l_ppasServEx);
        }

        // 2. Create a test file.
        sayTime("Create a test file.");
        try
        {
            p_batchTestDataTT.createTestFile();
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to create a test data file.");
            super.failedTestException(l_Ex);
        }

        // 3. Creates an instance of the actual BatchController class.
        sayTime("Create an instance of the actual BatchController class.");
        l_batchController = createBatchController(p_batchTestDataTT);

        // 4 Start the batch.
        sayTime("Start the '" + l_batchController.getClass().getName() + "' batch.");
        l_initialExitStatus = l_batchController.getExitStatus();
        try
        {
            startBatch(l_batchController);
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to start the '" + p_batchTestDataTT.i_batchName + "' batch.");
            super.failedTestException(l_Ex);
        }

        // 5. Wait for the batch to finish properly.
        sayTime("Wait for the '" + p_batchTestDataTT.i_batchName + "' to finish properly.");
        waitForBatchToFinish(l_batchController, p_batchTestDataTT.i_batchName, l_initialExitStatus);

        // 6. Verify that the exit status is equal to the expected exit status.
        sayTime("Verify that the exit status is equal to the expected exit status: " +
                p_batchTestDataTT.i_expectedJobExitStatus + " (= " +
                JobStatus.mapExitStatusToTxt(p_batchTestDataTT.i_expectedJobExitStatus) + ").");
        
        say("**MTR*** p_batchTestDataTT.i_expectedJobExitStatus="+p_batchTestDataTT.i_expectedJobExitStatus+
        		          " l_batchController.getExitStatus()="+l_batchController.getExitStatus());
        
        assertEquals("***ERROR: The batch has finished properly but with an unexpected exit status.",
                     p_batchTestDataTT.i_expectedJobExitStatus, l_batchController.getExitStatus());

        // 7. Verify that the batch control data (BACO_BATCH_CONTROL table) has been updated as expected.
        sayTime("Verify that the batch control data (BACO_BATCH_CONTROL table) has been updated as expected.");
        try
        {
            verifyBatchControlData(l_batchController, p_batchTestDataTT);
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to verify that the batch control data has been updated as expected.");
            super.failedTestException(l_Ex);
        }

        
        // 8. Verify that the report file (*.rpt) has been created and updated as expected.
        sayTime("Verify that the report file (*.rpt) has been created and updated as expected.");
        try
        {
            p_batchTestDataTT.verifyReportFile();
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to verify that the report file has been updated as expected.");
            super.failedTestException(l_Ex);
        }

        
        // 9. Verify that the recovery file (*.rcv) has been created and updated as expected.
        sayTime("Verify that the recovery file (*.rcv) has been created and updated as expected.");
        try
        {
            p_batchTestDataTT.verifyRecoveryFile();
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to verify that the recoveryfile has been created and updated as expected.");
            super.failedTestException(l_Ex);
        }

        // 10. Verify that the ASCS database has been updated as expected.
        try
        {
            p_batchTestDataTT.verifyAscsDatabase();
        }
        catch (PpasServiceException l_Ex)
        {
            sayTime("***ERROR: Failed to verify that the ASCS database has been properly updated.");
            super.failedTestException(l_Ex);
        }
        
        // 11. Verify that the input data test file has been renamed as expected.
        try
        {
        	p_batchTestDataTT.verifyRenaming();
        }
        catch (IOException l_Ex)
        {
        	sayTime("***ERROR: Failed to verify that the inputfile has been properly updated.");        	
        	super.failedTestException(l_Ex);
        }

    }


    /**
     * Starts the batch process by calling the <code>requestStart(JsRequest)</code> method on the passed 
     * <code>BatchController</code> object.
     * A <code>JobActionException</code> will be thrown if it fails to start the batch.
     * 
     * @param p_batchController  the batch controller object.
     * 
     * @throws JobActionException  if it fails to start the batch.
     */
    protected void startBatch(BatchController p_batchController) throws JobActionException
    {
        JsRequest l_jsRequest = null;

        l_jsRequest = new JsRequest("BatchUT", "BatchTestCaseTT", "BatchUTTestNode");

        p_batchController.requestStart(l_jsRequest);
    }


    /**
     * Waits for the batch to finish.
     * 
     * @param p_batchController    the batch controller object.
     * @param p_batchName          the name of the current batch (to be used in an error message).
     * @param p_initialExitStatus  the initial exit status.
     */
    protected void waitForBatchToFinish(BatchController p_batchController,
                                        String          p_batchName,
                                        int             p_initialExitStatus)
    {
        // LALU: Maybe pick these values from properties?
        final long L_MAX_WAITING_TIME = 180; // sec
        final long L_POLL_INTERVAL    = 5;   // sec

        boolean l_batchExitsProperly = false;
        long    l_repeatCnt          = L_MAX_WAITING_TIME / L_POLL_INTERVAL;

        for (int l_ix = 0; l_ix < l_repeatCnt; l_ix++)
        {
            if (p_batchController.getExitStatus() != p_initialExitStatus)
            {
                l_batchExitsProperly = true;
                break;
            }

            sayTime("The '" + p_batchName + "' batch has not yet finished properly, " +
                    "wait " + L_POLL_INTERVAL + " sec before checking the exit status once again...");
            try
            {
                Thread.sleep(L_POLL_INTERVAL * 1000);
            }
            catch (InterruptedException l_intEx)
            {
                // Nothing to do ...
                l_intEx = null;
            }
        } //End of 'for' loop.

        // Check if the batch has finished properly or if the max waiting time has been exceeded.
        if (!l_batchExitsProperly)
        {
            String l_msg = "***ERROR: The '" + p_batchName + "' batch " +
                           "hasn't finished properly within the max waiting time: " +
                           L_MAX_WAITING_TIME + " sec.";
            sayTime(l_msg);
            fail(l_msg);
        }
    }


    /**
     * Verifies that the actual batch control data stored in the BACO_BATCH_CONTROL table is
     * equal to the expected batch control data.
     * The key to the database entry will be obtained by the given <code>BatchController</code> object.
     * The expected batch control data will be obtained by the given <code>AbstractBatchTestDataTT</code>
     * object.
     * 
     * @param p_batchController  the <code>BatchController</code> object.
     * @param p_batchTestDataTT  the <code>BatchTestDataTT</code> object.
     * 
     * @throws Exception  if it fails to get the actual batch job control data.
     */
    protected void verifyBatchControlData(BatchController p_batchController,
                                          BatchTestDataTT p_batchTestDataTT)
        throws Exception
    {
        BatchJobControlData     l_expectedBatchJobControlData = null;
        BatchJobControlData     l_actualBatchJobControlData   = null;

        l_actualBatchJobControlData = p_batchTestDataTT.getActualBatchJobControlData(p_batchController);
        assertNotNull("***ERROR: verifyBatchControlData -- " +
                      "Failed to get the actual batch job control data.");
        sayTime("The actual batch control data: [" + l_actualBatchJobControlData + "]");

        l_expectedBatchJobControlData = p_batchTestDataTT.getExpectedBatchJobControlData(p_batchController);
        assertNotNull("***ERROR: verifyBatchControlData -- " +
                      "Failed to get the expected batch job control data.");
        sayTime("The expected batch control data: [" + l_expectedBatchJobControlData + "]");

        // Verify the actual batch control data is equal to the expected batch control data.
        assertEquals("***ERROR: verifyBatchControlData -- " +
                     "The actual batch job control data is NOT equal to the expected batch job control data.",
                     l_expectedBatchJobControlData, l_actualBatchJobControlData);
    }


    /**
     * Waits for the batch to finish with the expected exit status.
     * NB: If it fails to start the batch the complete test will fail
     * (i.e. an <code>AssertionFailedError</code> will be thrown).
     * 
     * @param p_batchController     the batch controller object.
     * @param p_batchName           the name of the batch, to be used in the error message(s).
     * @param p_expectedExitStatus  the expected exit status.
     * @param p_sleepTime           the waiting time in ms.
     * @param p_repeatCnt           the number of times to wait the given waiting time.
     */
    protected void waitForExitStatus(BatchController p_batchController,
                                     String          p_batchName,
                                     int             p_expectedExitStatus,
                                     long            p_sleepTime,
                                     int             p_repeatCnt)
    {
        boolean l_batchExitsProperly = false;
        int     l_initialStatus      = p_batchController.getExitStatus();
        long[]  l_maxWaitingTime     = getDuration(p_sleepTime * p_repeatCnt);
        String  l_maxWaitingTimeStr  = l_maxWaitingTime[2] + " min(s), " +
                                       l_maxWaitingTime[1] + " sec(s), " +
                                       l_maxWaitingTime[0] + " millisec(s)";


        sayTime("Wait for the batch to exit, maximum waiting time is " + l_maxWaitingTimeStr);
         for (int l_ix = 0; l_ix < p_repeatCnt; l_ix++)
        {
            try
            {
                Thread.sleep(p_sleepTime);
            }
            catch (InterruptedException l_intEx)
            {
                // Nothing to do ...
                l_intEx = null;
            }
            if (p_batchController.getExitStatus() != l_initialStatus)
            {
                l_batchExitsProperly = true;
                break;
            }
        }

        // Check if the the batch is finished.
        if (!l_batchExitsProperly)
        {
            String l_msg = "***ERROR: The batch " + p_batchName + " didn't exit within the max waiting time: "
                            + l_maxWaitingTimeStr;
            sayTime(l_msg);
            fail(l_msg);
        }

        // Check if the the batch has finished with the expected exit status.
        assertEquals("***ERROR: The batch has finished but with an unexpected exit status.",
                     p_expectedExitStatus, p_batchController.getExitStatus());
    }


    /**
     * Creates one test subscriber locally in the ASCS database.
     * NB: No subscriber info will be communicated to the SDP (i.e. the Emulator).
     * Returns a <code>BasicAccountData</code> object which will contain the
     * test subscriber data.
     * If it fails to insert the test subscriber <code>PpasServiceException</code> will be thrown.
     * 
     * @return  a <code>BasicAccountData</code> object which will contain the test subscriber data.
     */
    protected BasicAccountData createTestSubscriber()
    {
        BasicAccountData l_basicAccountData = null;

        try
        {
            l_basicAccountData = c_dbService.installTestSubscriber(null);
        }
        catch (PpasServiceException l_ppasServEx)
        {
            sayTime("***ERROR: Failed to create and install a subscriber.");
            failedTestException(l_ppasServEx);
        }

        return l_basicAccountData;
    }


    /**
     * Disconnects one test subscriber locally in the ASCS database.
     * NB: No subscriber info will be communicated to the SDP (i.e. the Emulator).
     * Returns a <code>DisconnectRequestData</code> object 
     * If it fails to disconnect the test subscriber the complete test will fail
     * (i.e. an <code>AssertionFailedError</code> will be thrown).
     * 
     * @param p_msisdn Mobile number to be disconnected.
     * @param p_disconnectionReasonCode Reason for the disconnection.
     * @return  A <code>DisconnectRequestData</code> object which will contain the
     *          test subscriber disconnection data.
     */
    protected DisconnectRequestData disconnectTestSubscriber(
            Msisdn      p_msisdn,
            String      p_disconnectionReasonCode)
    {
        DisconnectRequestData l_disconnectRequestData = null;

        try
        {
            l_disconnectRequestData = c_dbService.disconnectTestSubscriber(
                                                    p_msisdn,
                                                    p_disconnectionReasonCode);
        }
        catch (PpasServiceException l_ppasServEx)
        {
            sayTime("***ERROR: Failed to disconnect a subscriber.");
            failedTestException(l_ppasServEx);
        }
        catch (PpasSqlException l_ppasSqlEx)
        {
            sayTime("***ERROR: Failed to disconnect a subscriber.");
            failedTestException(l_ppasSqlEx);
        }
        catch (PpasIdGeneratorException l_ppasIdGeneratorEx)
        {
            sayTime("***ERROR: Failed to disconnect a subscriber.");
            failedTestException(l_ppasIdGeneratorEx);
        }

        return l_disconnectRequestData;
    }

    /**
     * Future disconnects one test subscriber locally in the ASCS database.
     * NB: No subscriber info will be communicated to the SDP (i.e. the Emulator).
     * Returns a <code>DisconnectRequestData</code> object 
     * If it fails to disconnect the test subscriber the complete test will fail
     * (i.e. an <code>AssertionFailedError</code> will be thrown).
     * 
     * @param p_msisdn Mobile number to be disconnected.
     * @param p_disconnectionReasonCode Reason for the disconnection.
     * @return  A <code>DisconnectRequestData</code> object which will contain the
     *          test subscriber disconnection data.
     */
    protected DisconnectData futureDisconnectTestSubscriber(
            Msisdn      p_msisdn,
            String      p_disconnectionReasonCode)
    {
        DisconnectData  l_disconnectData = null;

        try
        {
            l_disconnectData = c_dbService.futureDisconnectTestSubscriber(
                                                    p_msisdn,
                                                    p_disconnectionReasonCode);
        }
        catch (PpasServiceException l_ppasServEx)
        {
            sayTime("***ERROR: Failed to disconnect a subscriber.");
            failedTestException(l_ppasServEx);
        }
        catch (PpasSqlException l_ppasSqlEx)
        {
            sayTime("***ERROR: Failed to disconnect a subscriber.");
            failedTestException(l_ppasSqlEx);
        }

        return l_disconnectData;
    }


    /**
     * Returns the given string adjusted to the requested length by padding it
     * out with the given pad character.
     * The string is also right or left adjusted according to the given adjustment
     * indicator.
     * NB: If the passed string is longer than the requested length the complete
     *     string will be returned without any adjustments.
     * 
     * @param p_string           the string to adjust.
     * @param p_requestedLength  the requested length of the adjusted string.
     * @param p_padChar          the character used to pad the given string to
     *                           the requested length.
     * @param p_rightAdjust      if <code>true</code> the string is right adjusted,
     *                           otherwise it is left adjusted.
     * 
     * @return the given string, adjusted to requested length padded with the given
     *         character.
     */
    protected String adjustString(String  p_string,
                                  int     p_requestedLength,
                                  char    p_padChar,
                                  boolean p_rightAdjust)
    {
        int          l_length         = 0;
        StringBuffer l_adjustedString = new StringBuffer(p_string);

        if (p_string.length() < p_requestedLength)
        {
            l_length = p_requestedLength - p_string.length();

            for (int i = 0; i < l_length; i++)
            {
                if (p_rightAdjust)
                {
                    l_adjustedString.insert(0, p_padChar);
                }
                else
                {
                    l_adjustedString.append(p_padChar);
                }
            }
        }

        return l_adjustedString.toString();
    }


    /**
     * Create a new file.
     * @param   p_fileName The absolut file name.
     * @param   p_lines An array of the lines that will be written to the file.
     */
    protected void createNewFile(String p_fileName, String[] p_lines)
    {        
        File l_file = new File(p_fileName);
        String l_line = null;
     
        if (l_file.exists())
        {
            l_file.delete();
        }
                    
        // Create the File object
        BufferedWriter l_tmpFile = null;
                 
        try
        {
            l_tmpFile = new BufferedWriter(new FileWriter(p_fileName));
            for (int i = 0; i < p_lines.length; i++)
            {
                l_line = p_lines[i];
                l_tmpFile.write(l_line);
                l_tmpFile.newLine();
            }
            
            l_tmpFile.flush();
            l_tmpFile.close();
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
    }

   

    /**
     * Gets a new dummy JsJob ID.
     * @return the job id
     * @throws PpasSqlException when encountering problem reading from the database
     */
    protected String getJsJobID() throws PpasSqlException
    {
        SqlString     l_sqlStr = 
                         new SqlString(100, 0,
                                       "SELECT NVL((MAX(BACO_JS_JOB_ID)+1), 1) FROM BACO_BATCH_CONTROL");
        JdbcResultSet l_res = sqlQuery(l_sqlStr);
        String        l_jobID = null;
    
        if (l_res.next(10010))
        {
            l_jobID = "" + l_res.getInt(10020, 1);
        }
        
        l_res.close(10030);
        
        return l_jobID;
    }
    
    /**
     * Delete a  file.
     * @param   p_fileName The absolute file name.
     * @return boolean returns <code>true</code> if the file wasy deleted. <code>false</code> otherwise.
     */
    protected static boolean deleteFile(String p_fileName)
    {  
        boolean l_isDeleted = false;
        File l_file = new File(p_fileName);
     
        if (l_file.exists())
        {
            l_isDeleted = l_file.delete();
        }
        
        return l_isDeleted;
    }

    /**
     * Enables the console log for all debug verbosity levels, all application classes and all debug
     * statement types.
     */
    protected void enableConsoleLogAll()
    {
        PpasDebug.on = true;
        PpasDebug.setDebugToConsole(true);
        PpasDebug.setEnabledAppMask(PpasDebug.C_APP_ALL);
        PpasDebug.setEnabledLevel(PpasDebug.C_LVL_VLOW);
        PpasDebug.setEnabledStmtMask(PpasDebug.C_ST_ALL);
    }

    /**
     * Enables the log for the given debug verbosity levels, all application classes and all debug
     * statement types.
     * All debug printouts are stored in a file called 'debug.out'.
     * 
     * @param p_debugLevel      the requested debug verbosity level.
     * @param p_debugToConsole  if <code>true</code> the debug printouts are displayed on a console GUI.
     */
    protected void enableConsoleLog(int p_debugLevel, boolean p_debugToConsole)
    {
        PpasDebug.on = true;
        PpasDebug.setDebugToConsole(p_debugToConsole);
        PpasDebug.setEnabledLevel(p_debugLevel);
        PpasDebug.setEnabledAppMask(PpasDebug.C_APP_ALL);
        PpasDebug.setEnabledStmtMask(PpasDebug.C_ST_ALL);
    }
    
    /**
     * Reads the logfile and compares the context of the file with
     * <code>p_expectedText</code>.
     * 
     * @param p_filename The name of the log file.
     * @param p_fileExtension The file extension.
     * @param p_fileDirectory The directory where the file is located.
     * @return The value found in the log file or a string "No file found" if the file was not found.       
     */
    protected String getValueFromLogFile(String p_filename, String p_fileExtension, String p_fileDirectory)
    {
        String l_dir = c_properties.getTrimmedProperty(p_fileDirectory);

        FileSearch fileSearch   = new FileSearch();
        String l_actualText     = "No file found";
        File l_loggFile         = null;

        try
        {
            //Fetch all files that ends with <code>p_fileExtension</code> in the directory who's path 
            //is set in l_dir
            Vector l_vec = fileSearch.getFileList(new File(l_dir), p_fileExtension, (short)0);

            for(Enumeration e = l_vec.elements(); e.hasMoreElements();)
            {
                l_loggFile = (File)e.nextElement();

                if (l_loggFile.getName().equals(p_filename))
                {
                    //Read the context of the file to l_buffReader
                    BufferedReader l_buffReader = new BufferedReader(new FileReader(l_loggFile));
                    
                    try
                    {
                        l_actualText = l_buffReader.readLine();
                    }
                    finally
                    {
                        l_buffReader.close();
                    }

                    break;
                }
            }
        }
        catch (FileSearchException e)
        {
            failedTestException(e);
        }
        catch (FileNotFoundException e)
        {
            failedTestException(e);
        }
        catch (IOException e)
        {
            failedTestException(e);
        }
        
        return l_actualText;
    }

    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Initialize the different variables requested for the UT.
     * 
     * @param p_batchPropertyFile Name f the file contraining relevant batch properties.
     */
    private void init(String p_batchPropertyFile)
    {
        JdbcConnection      l_connection          = null;
        BusinessConfigCache l_businessConfigCache = null;
        IsClient            l_isClient            = null;

        if (!c_init)
        {
            try
            {
                c_properties = c_properties.getPropertiesWithAddedLayers("batch_mas");
                
                if (p_batchPropertyFile != null)
                {
                    c_properties = c_properties.getPropertiesWithAddedLayers(p_batchPropertyFile);
                }
            }
            catch (PpasConfigException e)
            {
                failedTestException(e);
            }
            
            c_jdbcPool.setMinFree(2);

            // Set IS Logger to enable batch to create an ESI client :) 
            System.setProperty("com.slb.sema.ppas.util.LoggerPool.LoggerPool.loggers", "default, esiLogger");
            System.setProperty("com.slb.sema.ppas.util.Handler.esiFile.severities", "31");
            System.setProperty("com.slb.sema.ppas.util.Handler.esiFile.directory", 
                               "${ASCS_LOCAL_ROOT}/log/esi");
            System.setProperty("com.slb.sema.ppas.util.Handler.esiFile.filename", "esi");
            System.setProperty("com.slb.sema.ppas.util.Handler.esiFile.type", "file");
            System.setProperty("com.slb.sema.ppas.util.Logger.esiLogger.handlers", "esiFile");
            System.setProperty("com.slb.sema.ppas.util.LoggerPool.LoggerPool.loggers", "esiLogger");

            // Wait 65 sec. for JDBCPool.
            if (!c_jdbcPool.isAvailable(65000))
            {
                fail("BatchTestCaseTT.init -- ***ERROR: JDBCPool is not available (waiting 65 sec.).");
            }

            // Get JDBC Connection.
            l_connection = getConnection(C_CLASS_NAME, "init", 10100);
            if (null == l_connection)
            {
                fail("BatchTestCaseTT.init -- ***ERROR: Could not get JDBC Connection from the JDBCPool:" + 
                     c_jdbcPool.toVerboseString());
            }
 
            // Load the business config. cache.
            try
            {
                l_businessConfigCache = 
                    (BusinessConfigCache)c_ppasContext.getAttribute("BusinessConfigCache");
                l_businessConfigCache.loadAll(c_ppasRequest, l_connection);
            }
            catch (PpasSqlException e)
            {
                failedTestException(e);
            }
            finally
            {
                // Put the JDBC Connection back to the JDBCPool.
                putConnection(l_connection);
            }

            try
            {
                c_ppasContext.setAttribute( IsContext.C_ATTRIB_ISSREQUESTIDGENERATOR,
                                            new SequentialIdManager( "iss_request",
                                                                     c_ppasContext,
                                                                     c_logger));

                if (c_dbService == null)
                {
                    c_dbService = new DbServiceTT(c_ppasRequest, c_logger);
                }
                
                // Put the loggerpool onto the context so that jobs can access it.
                c_ppasContext.setLoggerPool(c_loggerPool);    
                
                l_isClient = new IsClient(c_logger, c_ppasContext.getProperties(), c_instManager);
            }
            catch (PpasException e)
            {
                failedTestException(e);
            }
            
            // Put the IsClient onto the context so that jobs can access it.       
            c_ppasContext.setAttribute("IsClient", l_isClient);

            c_routeService   = new PpasMsisdnRoutingService(null, c_logger, c_ppasContext);
            c_installService = new PpasInstallService(null, c_logger, c_ppasContext);
            
            setAllDatePatch(getNow());  // Ensure consistent date/time across processes.
            
            c_init = true;
        }
    }
    
    
    
    // MTR - test
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Install a simple test subscriber.
     * 
     * @param p_msisdn           MSISDN of new subscriber.
     * @param p_masterMsisdn     MSISDN of account holder or <code>null</code> if a master.
     * @param p_market           Market on which this subscriber shoul dbe installed.
     * @param p_serviceClass     Service class on which to install. Use <code>null</code> for suubordinates
     *                           or to use default class.
     * @param p_initialPromoPlan Initial Promotion Plan.
     * @return Details of the subscriber.
     */
    private BasicAccountData installBasicSubscriber(Msisdn                p_msisdn,
                                                    Msisdn                p_masterMsisdn,
                                                    Market                p_market,
                                                    ServiceClass          p_serviceClass,
                                                    String                p_initialPromoPlan)
    {
        String           l_sdpId        = DbServiceTT.C_DEFAULT_SDP;
        BasicAccountData l_master       = null;
        boolean          l_isSub        = false;
        Market           l_market       = p_market;
        ServiceClass     l_serviceClass = p_serviceClass;
        Msisdn           l_msisdn       = p_msisdn;
        
        if (l_market == null)
        {
            l_market = new Market(DbServiceTT.C_DEFAULT_SRVA, DbServiceTT.C_DEFAULT_SLOC);
        }
        
        if (p_masterMsisdn != null && (l_msisdn == null || !p_masterMsisdn.equals(l_msisdn)))
        {
            l_master = getBad(p_masterMsisdn,0);
            
            l_sdpId = l_master.getScpId();
            
            l_isSub = true;
        }

        if (l_serviceClass == null && !l_isSub)
        {
            l_serviceClass = DbServiceTT.C_DEFAULT_INSTALL_SERVICE_CLASS;
        }
        
        if (l_msisdn == null)
        {
            l_msisdn = getNextMsisdn(l_market, l_sdpId);
        }
        
        try
        {
            c_routeService.addMsisdnRouting(null, C_DEFAULT_TIMEOUT, l_msisdn, l_sdpId);
            c_ppasRequest = createRequestWithMsisdn(l_msisdn);
            
            c_installService.installSubscriber(c_ppasRequest,
                                               C_DEFAULT_TIMEOUT,
                                               l_market,
                                               p_masterMsisdn,
                                               c_dbService.getAgent(l_market),
                                               l_sdpId,
                                               l_isSub ? null : l_serviceClass,
                                               l_isSub ? null : p_initialPromoPlan,
                                               c_dbService.getDefaultAccountGroupId(),
                                               c_dbService.getDefaultServiceOfferings(),
                                               false, // Over-ride quarantine
                                               c_dbService.getDefaultUssdEocnId(),
                                               PpasAccountService.C_CLEAR_TEMPORARY_BLOCK,
                                               true,  // no promo use default
                                               PpasInstallService.C_INSTALL_TYPE_STANDARD);
        }
        catch (PpasServiceException e)
        {
            failedTestException(e);
        }
        
        return getAccount(l_msisdn);
    }

    /**
     * Installs a local test subscriber and returns the corresponding <code>BasicAccountData</code> object.
     * It is only installed in the ASCS database and not on the SDP/Emulator.
     * If the given <code>BasicAccountData</code> object is <code>null</code> a standalone
     * subscriber is installed, otherwise a subordinate subscriber to the given master is installed.
     * 
     * @param p_master  the master subscriber's <code>BasicAccountData</code> object
     *                  (if this is <code>null</code> a standalone subscriber will be installed).
     * @return the <code>BasicAccountData</code> object for the installed subscriber.
     * @throws PpasServiceException  if it fails to create the requested number of test subscribers.
     */
    protected BasicAccountData installLocalTestSubscriber(BasicAccountData p_master) throws PpasServiceException
    {
    	return c_dbService.installTestSubscriber(p_master);
    }
    
    
    /**
     * Create a new subscriber (using all defaults) and activate it.
     * 
     * @param p_master  the master subscriber's <code>BasicAccountData</code> object
     *                  (if this is <code>null</code> a standalone subscriber will be installed).
     * @return a BasicAccountData object holding data for the new user
     */
    protected BasicAccountData installGlobalTestSubscriber(BasicAccountData p_master)
    {
    	Msisdn l_msisdn = (p_master == null ? null : p_master.getMsisdn());
        return installBasicSubscriber(null, l_msisdn, null, null, DbServiceTT.C_DEFAULT_INSTALL_PROMO_PLAN_ID);
    }
    
    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
 
    /** Get basic details of an account - waiting a reasonable time for installation.
     * 
     * @param p_msisdn MSISDN identifying the account.
     * @return Basic details of the account.
     */
    protected BasicAccountData getAccount(Msisdn p_msisdn)
    {
        final int C_MAX_WAITING_TIME = 20; // seconds
        final int C_SNOOZE_TIME      = 1;  // seconds

        BasicAccountData l_bad = null;
        
        for (int i = 0; i < (C_MAX_WAITING_TIME/C_SNOOZE_TIME); i++)
        {
            l_bad = getBad(p_msisdn,0);
            assertNotNull("***ERROR: Failed to get the 'BasicAccountData' for subscriber '" + p_msisdn + "'",
                          l_bad);
            
            if (l_bad.getCustStatus() != 'I')
            {
                break;
            }
            
            snooze(C_SNOOZE_TIME);  // Make sure account is installed.
        }

        // Check if the subscriber has been properly installed.
        assertTrue("***ERROR: The test subscriber '" + p_msisdn + "' is not properly installed.",
                   (l_bad.getCustStatus() != 'I'));

        return l_bad;
    }

    /** Get next MSISDN for a given market and SDP.
     * 
     * @param p_market Market for the MSISDN.
     * @param p_sdp Target SDP of account.
     * @return Next available MSISDN in a given market and SDP.
     */
    protected Msisdn getNextMsisdn(Market p_market, String p_sdp)
    {
        Msisdn l_msisdn = null;
        
        try
        {
            l_msisdn = c_dbService.getNextFreeMsisdn(p_market, p_sdp);
        }
        catch (PpasServiceFailedException e)
        {
            failedTestException(e);
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        
        return l_msisdn;
    }

    /**
     * Get details of an account.
     * 
     * @param p_msisdn Mobile number of the account to get.
     * @param p_flags see {@link com.slb.sema.ppas.is.isil.sqlservice.AccountSqlService#checkSubscriberStatus}
     * @return a BasicAccountData object holding data for the user.
     */
    protected BasicAccountData getBad(Msisdn p_msisdn, long p_flags)
    {
        BasicAccountData l_basicAccountData = null;
        try
        {
            l_basicAccountData = c_dbService.getBad(p_msisdn, p_flags);
        }
        catch (PpasServiceException e)
        {
            failedTestException(e);
        }
        catch (PpasSqlException e)
        {
            failedTestException(e);
        }
        
        return l_basicAccountData;
    }

    /**
     * Help method to construct a right adjusted string
     * <prefix><filler string><lastDigits> The last 2 parts will consist of
     * 'p_noDigits' characters.
     * Example: constructFilledString( "000", "00832000", 2, 3) will give a string
     * like: 00832000002
     * @param p_filler character to be placed to the left
     * @param p_prefix starting string 
     * @param p_lastDigits
     * @param p_noDigits
     * @return a right adjusted string.
     * 
     * @deprecated
     */
    protected String constructFilledString( String p_filler,
    		                                String p_prefix,
    		                                int    p_lastDigits,
    		                                int    p_noDigits)
    {

    	String l_tmpEndingDigits = p_filler + p_lastDigits;
    	int    l_newEndingLength = l_tmpEndingDigits.length();
    	
    	l_tmpEndingDigits        = l_tmpEndingDigits.substring(l_newEndingLength-p_noDigits);
    	
    	return (p_prefix + l_tmpEndingDigits);
    }
}
