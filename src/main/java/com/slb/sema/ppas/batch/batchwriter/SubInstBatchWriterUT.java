// /////////////////////////////////////////////////////////////////////////////
//      ASCS            : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       : SubInstBatchWriterUT.java
//      DATE            : 2004-maj-24
//      AUTHOR          : Urban Wigstrom
//      REFERENCE       : PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       : ATOS ORIGIN 2004
//
//      DESCRIPTION     : See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Hashtable;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcontroller.SubInstBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.SubInstBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.is.isapi.PpasBatchControlService;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueClosedForWritingException;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/** Unit test for SubInstBatchWriter. */

public class SubInstBatchWriterUT extends BatchTestCaseTT
{

    /** Reference to SubInstBatch controller object. */
    private static SubInstBatchWriter     c_subInstBatchWriter;

    /** Reference to subInstBatch controller object. */
    private static SubInstBatchController c_subInstBatchController;

    /** Reference to PpsBatchControlService. */
    private PpasBatchControlService       i_controlService;

    /** A short file name vid extension .DAT. */
    private static final String           C_FILE_SHORT_NAME_DAT = "INSTALL_20040615_00003.DAT";

    /** A short file name vid extension .RPT. */
    private static final String           C_FILE_SHORT_NAME_ERR = "INSTALL_20040615_00003.RPT";

    /** A short file name vid extension .RCV. */
    private static final String           C_FILE_SHORT_NAME_RCV = "INSTALL_20040615_00003.RCV";

    /** A short file name vid extension .IPG. */
    //    private static final String C_FILE_SHORT_NAME_IPG = "INSTALL_20040615_00002.IPG";
    /** A long file name vid extension .DAT. */
    //    private static final String C_FILE_LONG_NAME_DAT = "INSTALL_0101_1234_20040615_00003.DAT";
    /** A long file name vid extension .IPG. */
    //    private static final String C_FILE_LONG_NAME_IPG = "INSTALL_0101_1234_20040615_00002.IPG";
    /** The file date. */
    private static final String           C_FILE_DATE           = "20040615";

    /** The sequence number. */
    private static final int              C_SEQ_NO              = 3;

    /** The service class. */
    private static final String           C_SERVICE_CLASS       = "0001";

    /** The service location. */
    private static final String           C_SERVICE_LOCATION    = "01";

    /** The sevice area. */
    private static final String           C_SERVICE_AREA        = "01";

    /** The sub job counter. */
    private static final int              C_SUB_JOB_COUNT       = -1;

    /**
     * The constructor for the class SubInstBatchWriterUT.
     * @param p_name Name of the test.
     */
    public SubInstBatchWriterUT(String p_name)
    {
        super(p_name);
    }

    /** Tests getRecord(). */
    public void testGetRecord()
    {
        super.beginOfTest("testGetRecord");
        BatchRecordData l_dataRecord;
 
        try
        {
            l_dataRecord = c_subInstBatchWriter.getRecord();

            assertNotNull("Data Record is Null", l_dataRecord);
            assertTrue("Data Record is not of type SubInstBatchRecordData",
                       (l_dataRecord instanceof SubInstBatchRecordData));
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }
        super.endOfTest();
    }

    /** Tests both openFile(...) and fileExist(...). */
    public void testOpenFileAndFileExists()
    {
        super.beginOfTest("testOpenFileAndFileExists");
        String l_directory = c_subInstBatchWriter.i_properties
                .getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
        try
        {
            c_subInstBatchWriter.openFile("1", new File(l_directory + "//" + C_FILE_SHORT_NAME_DAT));
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        //If the assert method don't fails then the openFile and fileExist metohds works.
        assertTrue(c_subInstBatchWriter.fileExists("1"));

        super.endOfTest();
    }

    /** Tests fileExist(...) when the file don't exist. */
    public void testFileExists()
    {
        super.beginOfTest("testFileExists");

        assertFalse(c_subInstBatchWriter.fileExists("2"));

        super.endOfTest();
    }
    
    /**
     * Test writeRecord(). Begins with creating a instans in table <code>table baco_batch_control</code>.
     * Creates a <code>SubInstBatchRecordData</code> with a error line set. Updates the table
     * <code>baco_batch_control</code> by calling how calls <code>updateControlInfo(...)</code>. Gets the
     * updated row from the table and check if the row <code>baco_failur</code> has been updated. Set the
     * value for error line in the object <code>SubInstBatchRecordData</code> to null and updates the table
     * <code>baco_batch_control</code> by calling <code>updateStatus()</code> how calls
     * <code>updateControlInfo(...)</code>. Gets the updated row from the table and check if the 
     * row <code>baco_success</code> has been updated.
     * So by test <code>writeRecord()</code> even the methods <code>updateStatus()</code> and
     * <code>updateControlInfo(...)</code> is being tested.
     */

    public void testWriteRecord()
    {
        super.beginOfTest("testWriteRecord");
        SubInstBatchRecordData l_dataRecord = new SubInstBatchRecordData();

        BatchJobControlData l_batchDataFromDB = null;
        BatchJobControlData l_batchData1 = null;
        String l_expectedKey = null;
        String l_actualKey = null;
        String l_errorLine = "2 ERR";
        String l_recoveryLine = "1 REC";
        PpasDateTime l_executionDateTime = null;
        long l_jsJobId = -1;
        String l_actualText = null;
        
        try
        {
            i_controlService = new PpasBatchControlService(c_ppasRequest, c_logger, c_ppasContext);

            //Create a BatchJobData object and save it in table baco_batch_control.
            l_batchData1 = c_subInstBatchController.getKeyedControlRecord();
            l_executionDateTime = l_batchData1.getExecutionDateTime();
            l_jsJobId = l_batchData1.getJsJobId();            
            l_batchData1.setJobType(BatchConstants.C_JOB_TYPE_BATCH_INSTALLATION);
            l_batchData1.setStatus((BatchConstants.C_BATCH_STATUS_INPROGRESS).charAt(0));
            l_batchData1.setSubJobCnt(C_SUB_JOB_COUNT);
            l_batchData1.setFileDate(new PpasDate(C_FILE_DATE, PpasDateTime.C_DF_yyyyMMdd));
            l_batchData1.setSeqNo(C_SEQ_NO);
            l_batchData1.setNoOfSuccessfullyRecs(0L);
            l_batchData1.setNoOfRejectedRecs(0L);
            l_batchData1.setExtraData1(C_SERVICE_CLASS);
            l_batchData1.setExtraData2(C_SERVICE_AREA);
            l_batchData1.setExtraData3(C_SERVICE_LOCATION);
            l_batchData1.setOpId(super.C_DEFAULT_OPID);
            
            //Test when there is one error line set.
            try
            {
                //Updates the table baco_batch_control.
                i_controlService.addJobDetails(c_ppasRequest, l_batchData1, C_TIMEOUT);

                l_dataRecord.setErrorLine(l_errorLine);
                l_dataRecord.setRecoveryLine((l_recoveryLine));
                c_subInstBatchWriter.writeRecord(l_dataRecord);

                l_batchDataFromDB = i_controlService.getJobDetails(c_ppasRequest,
                                                                   l_batchData1.getJsJobId(),
                                                                   l_batchData1.getExecutionDateTime(),
                                                                   C_TIMEOUT);

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 0.
                l_expectedKey = l_batchData1.getJsJobId() + 
                                l_batchData1.getExecutionDateTime().toString_yyyyMMddHHmmss() + 
                                "1" + 
                                "0";

                l_actualKey = l_batchDataFromDB.getJsJobId() + 
                              l_batchDataFromDB.getExecutionDateTime().toString_yyyyMMddHHmmss() +
                              l_batchDataFromDB.getNoOfRejectedRecs() + 
                              l_batchDataFromDB.getNoOfSuccessfullyRecs();

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);
            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
            
            //Test that the method has written to report file and recovery file.
           //Get the error text from the error logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_SHORT_NAME_ERR,
                                                     BatchConstants.C_EXTENSION_REPORT_FILE,
                                                     BatchConstants.C_REPORT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_REPORT_FILE,
                        ("No file found".equals(l_actualText)));

            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);

//          ** -------------------------------------------- **
//          ** Recovery file is deleted by tidyUp()         **
//          ** This testcase is not possible to run anymore **
//          ** -------------------------------------------- **
//            //Get the recovery text from the recovery logfile.
//            l_actualText = super.getValueFromLogFile(C_FILE_SHORT_NAME_RCV,
//                                                     BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                                                     BatchConstants.C_RECOVERY_FILE_DIRECTORY);
//
//            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_RECOVERY_FILE,
//                        ("No file found".equals(l_actualText)));
//            assertEquals("Failure: Wrong data in the recovery file", l_recoveryLine, l_actualText);

            //Test when there is no error line set.
            try
            {
                l_dataRecord.setErrorLine(null);
                c_subInstBatchWriter.writeRecord(l_dataRecord);

                l_batchDataFromDB = i_controlService.getJobDetails(c_ppasRequest,
                                                                   l_jsJobId,
                                                                   l_executionDateTime,
                                                                   C_TIMEOUT);

                l_actualKey = l_batchDataFromDB.getJsJobId() + 
                              l_batchDataFromDB.getExecutionDateTime().toString_yyyyMMddHHmmss() +
                              l_batchDataFromDB.getNoOfRejectedRecs() + 
                              l_batchDataFromDB.getNoOfSuccessfullyRecs();

                //JsJobID + executionDateTime + BACO_ERROR = 1 + BACO_SUCSESS = 1.
                l_expectedKey = l_batchData1.getJsJobId() + 
                                l_batchData1.getExecutionDateTime().toString_yyyyMMddHHmmss() + 
                                1 + 
                                1;

                assertEquals("Not correct values in the database", l_expectedKey, l_actualKey);

            }
            catch (PpasServiceException e)
            {
                super.failedTestException(e);
            }
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        super.endOfTest();
    }
    
    /**
     * Tests writeToFile(). It begins with writing an error line and a recovery line to file. Then it gets the
     * text from the error logfile and the recovey logfile and compare this text with the text we expected to
     * find.
     */
    public void testWriteToFile()
    {
        super.beginOfTest("testWriteToFile");
        SubInstBatchRecordData l_dataRecord = new SubInstBatchRecordData();

        String l_errorLine = "2 ERR";
        String l_recoveryLine = "1 REC";
        String l_actualText = null;
        
        String l_dir = c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        String l_fileReportName = l_dir + "/" + C_FILE_SHORT_NAME_ERR;
  
        String l_dir2 = c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        String l_fileRecoveryName = l_dir2 + "/" + C_FILE_SHORT_NAME_RCV;
        
        // Make sure that all files are closed.
        c_subInstBatchWriter.tidyUp();
        
        try
        {
            //If the Report file allready exist delete it and create an empty file.
            c_subInstBatchWriter.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_fileReportName));

            //If the Recovery file allready exist delete it and create an empty file.
            c_subInstBatchWriter.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(l_fileRecoveryName));

            // deleteFiles();

            l_dataRecord.setErrorLine(l_errorLine);
            l_dataRecord.setRecoveryLine((l_recoveryLine));
            //Write to the files
            c_subInstBatchWriter.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_dataRecord.getErrorLine());
            c_subInstBatchWriter.writeToFile(BatchConstants.C_KEY_RECOVERY_FILE, l_dataRecord
                    .getRecoveryLine());
            //Close the files after writing.
            c_subInstBatchWriter.tidyUp();

            //Get the error text from the error logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_SHORT_NAME_ERR,
                                                     BatchConstants.C_EXTENSION_REPORT_FILE,
                                                     BatchConstants.C_REPORT_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_REPORT_FILE,
                        ("No file found".equals(l_actualText)));

            assertEquals("Failure: Wrong data in the error file", l_errorLine, l_actualText);

            //Get the recovery text from the recovery logfile.
            l_actualText = super.getValueFromLogFile(C_FILE_SHORT_NAME_RCV,
                                                     BatchConstants.C_EXTENSION_RECOVERY_FILE,
                                                     BatchConstants.C_RECOVERY_FILE_DIRECTORY);

            assertFalse("Failure: Found no file with extention " + BatchConstants.C_EXTENSION_RECOVERY_FILE,
                        ("No file found".equals(l_actualText)));
            assertEquals("Failure: Wrong data in the recovery file", l_recoveryLine, l_actualText);
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }

        super.endOfTest();
    }

       /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(SubInstBatchWriterUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to run all the tests in this
     * class.
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();
        createContext();
    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }

    /**
     * Creates context needed for the unit test.
     */
    private void createContext()
    {
        super.c_writeUtText = true;
        SizedQueue l_outQueue = null;
        SubInstBatchRecordData l_batchRecordData = new SubInstBatchRecordData();
        
        //The update frequency needs to be 1 for the test to work.
        super.c_properties.setProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY, "1");
 
        try
        {
            l_outQueue = new SizedQueue("BatchWriter", 1, null);

            l_outQueue.addFirst(l_batchRecordData);
        }
        catch (SizedQueueInvalidParameterException e)
        {
            super.failedTestException(e);
        }
        catch (SizedQueueClosedForWritingException e)
        {
            super.failedTestException(e);
        }
        
        if (c_subInstBatchWriter == null)
        {
            Hashtable l_params = new Hashtable();
            l_params.put(BatchConstants.C_KEY_INPUT_FILENAME, C_FILE_SHORT_NAME_DAT);
            l_params.put(BatchConstants.C_KEY_SERVICE_AREA, "0001");
            l_params.put(BatchConstants.C_KEY_SERVICE_LOCATION, "0001");
            l_params.put(BatchConstants.C_KEY_SERVICE_CLASS, "01");

            try
            {
                this.deleteFiles();
                c_subInstBatchController = new SubInstBatchController(super.c_ppasContext,
                                                                      BatchConstants.C_JOB_TYPE_BATCH_INSTALLATION,
                                                                      super.getJsJobID(),
                                                                      "SupInstBatchWriterUT",
                                                                      l_params);

                c_subInstBatchWriter = new SubInstBatchWriter(super.c_ppasContext,
                                                              super.c_logger,
                                                              c_subInstBatchController,
                                                              l_params,
                                                              l_outQueue,
                                                              super.c_properties);

            }
            catch (PpasException e)
            {
                failedTestException(e);
            }
            catch (RemoteException e)
            {
                super.failedTestException(e);
            }
            catch (IOException e)
            {
                super.failedTestException(e);
            }
        }
    }
    
    /**Make sure that there are no old files. */
    private void deleteFiles()
    {        
        String l_dir = c_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);
        String l_fileReportName = l_dir + "//" + C_FILE_SHORT_NAME_ERR;
  
        String l_dir2 = c_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);
        String l_fileRecoveryName = l_dir2 + "//" + C_FILE_SHORT_NAME_RCV;
        
        try
        {
            // Make sure that all files are closed.
            if(c_subInstBatchWriter != null) 
            {
                c_subInstBatchWriter.tidyUp();
                
//              If the Report file allready exist delete it and create an empty file.
                System.out.println(super.deleteFile(l_fileReportName));
                c_subInstBatchWriter.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_fileReportName));

                //If the Recovery file allready exist delete it and create an empty file.
                System.out.println(super.deleteFile(l_fileRecoveryName));
                c_subInstBatchWriter.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(l_fileRecoveryName));
            }
            else
            {
                super.deleteFile(l_fileReportName);
                super.deleteFile(l_fileRecoveryName);                
            }
        }
        catch (IOException e)
        {
            super.failedTestException(e);
        }   
    }
}
