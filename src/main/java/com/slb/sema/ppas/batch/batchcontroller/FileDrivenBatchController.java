// //////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       FileDrivenBatchController.java
//  DATE            :       26-Apr-2007
//  AUTHOR          :       Lars Lundberg
//  REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//  COPYRIGHT       :       WM-data 2007
//
//  DESCRIPTION     :       This class implements the common logic for controlling
//                          and managing a file driven batch process.
//
// //////////////////////////////////////////////////////////////////////////////
// CHANGE HISTORY
// //////////////////////////////////////////////////////////////////////////////
// DATE | NAME | DESCRIPTION | REFERENCE
// ----------+---------------+----------------------------------+----------------
// DD/MM/YY  | <name>        | <brief description of            | <reference>
//           |               | change>                          |
// ----------+---------------+----------------------------------+----------------
// 03/05/07  | Lars Lundberg | Debug printouts are added.       | PpacLon#3033/11433
// ----------+---------------+----------------------------------+----------------
// //////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.exceptions.BatchConfigException;
import com.slb.sema.ppas.batch.exceptions.BatchException;
import com.slb.sema.ppas.batch.exceptions.BatchInvalidParameterException;
import com.slb.sema.ppas.batch.exceptions.BatchKey;
import com.slb.sema.ppas.batch.exceptions.BatchServiceException;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDate;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasSoftwareException;
import com.slb.sema.ppas.common.support.SoftwareKey;

/**
 * This <code>FileDrivenBatchController</code> class implements the common logic
 * for controlling and managing a file driven batch process.
 * It is declared <code>abstract</code> in order to be inherited by any batch specific
 * file driven <code>***BatchController</code> sub-class.
 */
public abstract class FileDrivenBatchController extends BatchController
{
    // =========================================================================
    // == Protected constant(s).                                              ==
    // =========================================================================
    /** The regular expression that defines a valid file date.
     *  NB: This regular expression defines only one capturing group that will contain the complete date. */
    protected static final String C_REG_EX_VALID_FILE_DATE        =
        "(\\d{4}(?:(?:0[1-9])|(?:1[0-2]))(?:(?:0[1-9])|(?:[12][0-9])|(?:3[01])))";

    /** The regular expression that defines a valid file sequence number.
     *  NB: This regular expression defines only one capturing group that will contain the sequence number. */
    protected static final String C_REG_EX_VALID_FILE_SEQNO       = "(\\d{5})";

    /** The regular expression that defines a valid filename extension delimiter.
     *  NB: This regular expression does not define any capturing group. */
    protected static final String C_REG_EX_VALID_EXTENSION_DELIM  = "(?:\\.)";

    /** The regular expression that defines the valid default filename extension.
     *  NB: This regular expression defines only one capturing group that will contain the extension. */
    protected static final String C_REG_EX_VALID_EXTENSION        = "(DAT|SCH|IPG)";

    /** The regular expression that defines a valid default filename.
     *  NB: This regular expression defines three (3) capturing groups:
     *      group 1: the file date.
     *      group 2: the file sequence number.
     *      group 3: the filename extension.
     */
    protected static final String C_REG_EX_VALID_DEFAULT_FILENAME = C_REG_EX_VALID_FILE_DATE       + "_" +
                                                                    C_REG_EX_VALID_FILE_SEQNO      +
                                                                    C_REG_EX_VALID_EXTENSION_DELIM +
                                                                    C_REG_EX_VALID_EXTENSION;

    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME = "FileDrivenBatchController";


    // =========================================================================
    // == Protected attribute(s).                                             ==
    // =========================================================================
    /** The file date (extracted from the input data filename). */
    protected PpasDate i_fileDate     = null;

    /** The file sequence number (extracted from the input data filename). */
    protected int      i_fileSeqNo    = -1;

    /** The file sub-sequence number (extracted from the input data filename). */
    protected int      i_fileSubSeqNo = -1;

    /** The extra data 1 (extracted from the input data filename). */
    protected String   i_extraData1   = null;

    /** The extra data 2 (extracted from the input data filename). */
    protected String   i_extraData2   = null;

    /** The extra data 3 (extracted from the input data filename). */
    protected String   i_extraData3   = null;

    /** The extra data 4 (extracted from the input data filename). */
    protected String   i_extraData4   = null;

    /** The filename extension not including the dot (extracted from the input data filename). */
    protected String   i_extension    = null;


    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs a <code>FileDrivenBatchController</code> instance using the given parameters.
     * 
     * @param p_ppasContext    The <code>PpasContext</code> object.
     * @param p_jobType        The type of job, i.e. the kind of batch (e.g. BatchInstall).
     * @param p_jobId          A unique js job id that identifies this job.
     * @param p_jobRunnerName  The name of the job runner in which this job is running.
     * @param p_params         The parameters to use when running this batch.
     * 
     * @throws PpasConfigException   if configuration data is missing or incomplete (for instance if one of
     *                               the properties files that was found had an error when trying to read it.
     * @throws BatchConfigException  if a mandatory property is missing or
     *                               if a property has a bad value (isn't numerical when it should be or
     *                               if the value of a numerical property is too big).
     */
    public FileDrivenBatchController(PpasContext p_ppasContext,
                                     String      p_jobType,
                                     String      p_jobId,
                                     String      p_jobRunnerName,
                                     Map         p_params)
    {
        super(p_ppasContext, p_jobType, p_jobId, p_jobRunnerName, p_params);
    }


    // =========================================================================
    // == Protected abstract method(s).                                       ==
    // == These methods must be implemented by a sub-class of this class.     ==
    // =========================================================================
    /**
     * This method should return the current batch job type.
     * It is declared as <code>abstract</code> in order to force a sub-class
     * of this super class to return the current batch job type.
     * 
     * @return the current batch job type.
     */
    protected abstract String getBatchJobType();


    /**
     * This method should return a regular expression that defines a valid input data filename.
     * @return a regular expression that defines a valid input data filename.
     */
    protected abstract String getValidFilenameRegEx();


    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_addControlInformation = "addControlInformation";
    /**
     * Inserts a new batch control record into the <code>BACO_BATCH_CONTROL</code> table.
     * 
     * @throws BatchServiceException  if it fails to insert a new record into the database.
     */
    protected void addControlInformation() throws BatchServiceException
    {
        BatchJobControlData   l_batchJobControlData = null;
        BatchServiceException l_batchServEx         = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10000, this,
                            BatchConstants.C_ENTERING + C_METHOD_addControlInformation);
        }

        // Create and populate a 'BatchJobControlData' object.
        l_batchJobControlData = new BatchJobControlData(i_executionDateTime, Long.parseLong(getJobId()));
        l_batchJobControlData.setJobType(getBatchJobType());
        l_batchJobControlData.setStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS.charAt(0));
        l_batchJobControlData.setSubJobCnt(-1);
        l_batchJobControlData.setNoOfSuccessfullyRecs(0);
        l_batchJobControlData.setNoOfRejectedRecs(0);
        String l_opid = (super.getSubmitterOpid() != null  ?  super.getSubmitterOpid() : "");
        l_batchJobControlData.setOpId(l_opid);
        
        // Insert a batch job control record in the database.
        try
        {
            super.i_batchContService.addJobDetails(null, l_batchJobControlData, i_timeout);
        }
        catch (PpasServiceException l_ppasServEx)
        {
            // ERROR: Failed to insert a new record into the batch control table,
            //        create and throw a 'BatchServiceException'.
            l_batchServEx = new BatchServiceException(C_CLASS_NAME,
                                                      C_METHOD_addControlInformation,
                                                      10027,
                                                      this,
                                                      null,
                                                      0L,
                                                      BatchKey.get().batchFailedInsertIntoControlTable(),
                                                      l_ppasServEx);
            super.i_logger.logMessage(l_batchServEx);
            throw l_batchServEx;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10000, this,
                            "Leaving " + C_METHOD_addControlInformation);
        }
    }


    /**
     * Updates the batch job control table with the file info attributes.
     * 
     * @throws BatchServiceException  if it fails to update the database with the file info attributes.
     */
    protected void updateBatchControlData() throws BatchServiceException
    {
        BatchJobControlData   l_batchJobControlData = null;
        BatchServiceException l_batchServEx         = null;

        // Get the batch job control data stored in the database.
        try
        {
            l_batchJobControlData = super.i_batchContService.getJobDetails(null,
                                                                           Long.parseLong(getJobId()),
                                                                           i_executionDateTime,
                                                                           i_timeout);
        }
        catch (PpasServiceException l_ppasServEx)
        {
            // ERROR: Failed to get stored control details about the current batch job,
            //        create and throw a 'BatchServiceException'.
            l_batchServEx = new BatchServiceException(C_CLASS_NAME,
                                                      C_METHOD_addControlInformation,
                                                      10027,
                                                      this,
                                                      null,
                                                      0L,
                                                      BatchKey.get().batchFailedToGetDataFromControlTable(
                                                             i_executionDateTime.toString_yyyyMMdd_HHcmmcss(),
                                                             getJobId()),
                                                      l_ppasServEx);
            super.i_logger.logMessage(l_batchServEx);
            throw l_batchServEx;
        }

        // Update the batch job control data.
        String l_extraData1 = ((i_extraData1 != null  &&  i_extraData1.length() > 10) ?
                                i_extraData1.substring(0, 10) : i_extraData1);
        String l_extraData2 = ((i_extraData2 != null  &&  i_extraData2.length() > 10) ?
                                i_extraData2.substring(0, 10) : i_extraData2);
        String l_extraData3 = ((i_extraData3 != null  &&  i_extraData3.length() > 30) ?
                                i_extraData3.substring(0, 30) : i_extraData3);
        String l_extraData4 = ((i_extraData4 != null  &&  i_extraData4.length() > 30) ?
                                i_extraData4.substring(0, 30) : i_extraData4);

        l_batchJobControlData.setFileDate(i_fileDate);
        l_batchJobControlData.setSeqNo(i_fileSeqNo);
        l_batchJobControlData.setSubSeqNo(i_fileSubSeqNo);
        l_batchJobControlData.setExtraData1(l_extraData1);
        l_batchJobControlData.setExtraData2(l_extraData2);
        l_batchJobControlData.setExtraData3(l_extraData3);
        l_batchJobControlData.setExtraData4(l_extraData4);

        try
        {
            super.i_batchContService.updateJobDetails(null, l_batchJobControlData, i_timeout);
        }
        catch (PpasServiceException l_ppasServEx)
        {
            // ERROR: Failed to update a record into the batch control table,
            //        create and throw a 'BatchServiceException'.
            l_batchServEx = new BatchServiceException(C_CLASS_NAME,
                                                      C_METHOD_addControlInformation,
                                                      10027,
                                                      this,
                                                      null,
                                                      0L,
                                                      BatchKey.get().batchFailedToUpdateDateControlTable(
                                                             i_executionDateTime.toString_yyyyMMdd_HHcmmcss(),
                                                             getJobId()),
                                                      l_ppasServEx);
            super.i_logger.logMessage(l_batchServEx);
            throw l_batchServEx;
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_verifyPreConditions = "verifyPreConditions";
    /**
     * Returns <code>true</code> if the pre-conditions are fulfilled, otherwise <code>false</code>. <br>
     * The pre-conditions are: <br>
     * 1. The input filename parameter has to be defined and it must have a pre-defined format. <br>
     * 2. If a recovery file exists then the input filename must have the "in progress" extension. <br>
     * 3. The input filename must correspond to an existing and readable file. <br>
     * 
     * @return <code>true</code> if the pre-conditions are fulfilled, otherwise <code>false</code>.
     */
    protected boolean verifyPreConditions()
    {
        boolean l_preCondFulfilled = true;
        String  l_inputFilename    = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10000, this,
                            BatchConstants.C_ENTERING + C_METHOD_verifyPreConditions);
        }

        try
        {
            // 1. Check the input filename parameter.
            //    It's a mandatory parameter and it must have a pre-defined format.
            l_inputFilename = getInputFilename();
            verifyInputFilenameFormat(l_inputFilename);

            // 2. Check if a recovery file exists.
            //    If it does, the filename must have the "in progress" extension.
            verifyRecoveryMode(l_inputFilename);


            // 3. The input filename must correspond to an existing and readable file.
            verfiyInputFile(l_inputFilename);
        }
        catch (BatchException l_bipEx)
        {
            l_preCondFulfilled = false;
        }
        catch (PpasSoftwareException l_ppasSWEx)
        {
            l_preCondFulfilled = false;
        }
        finally
        {
            // Always update the batch control data table.
            try
            {
                updateBatchControlData();
            }
            catch (BatchServiceException l_Ex)
            {
                l_preCondFulfilled = false;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10000, this,
                            BatchConstants.C_LEAVING + C_METHOD_verifyPreConditions +
                            "; l_preCondFulfilled = " + l_preCondFulfilled);
        }
        return l_preCondFulfilled;
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_verifyInputFilenameFormat = "verifyInputFilenameFormat";
    /**
     * This method should return <code>true</code> if the filename is valid,
     * otherwise it returns <code>false</code>.
     * If the filename is valid this method is expected to update the various
     * file info instance attributes such as file date, file sequence number etc.
     * and also to update the batch control table with this attributes.
     * 
     * @param p_inputFilename  the name of the input data file.
     * @throws BatchConfigException   if the given input data filename has an invalid format.
     */
    protected void verifyInputFilenameFormat(String p_inputFilename) throws BatchConfigException
    {
        String  l_filenameRegEx;
        Pattern l_validFilenamePattern;
        Matcher l_filenameMatcher;

        l_filenameRegEx = getValidFilenameRegEx();
        try
        {
            l_validFilenamePattern = Pattern.compile(l_filenameRegEx);
        }
        catch (PatternSyntaxException l_patternSyntaxEx)
        {
            // ERROR: The regular expression that defines a valid input data filename is invalid.
            //        NB: This is a RuntimeException.
            String l_errorMsg = "\"The regular expression that defines a valid input data filename has " +
                                "an invalid syntax: '" + l_filenameRegEx + "'\"";
            PpasSoftwareException l_ppasSWEx =
                new PpasSoftwareException(C_CLASS_NAME,
                                          C_METHOD_verifyInputFilenameFormat,
                                          10000,
                                          this,
                                          null,
                                          0L,
                                          SoftwareKey.get().unexpectedExceptionInclMessage(l_errorMsg),
                                          l_patternSyntaxEx);
            i_logger.logMessage(l_ppasSWEx);
            i_extraData4 = "Unexpected software problem.";
            throw l_ppasSWEx;
        }

        l_filenameMatcher = l_validFilenamePattern.matcher(p_inputFilename);
        if (l_filenameMatcher.matches())
        {
            String[] l_controlDataAttribArr = new String[l_filenameMatcher.groupCount()];
            for (int l_ix = 1; l_ix <= l_filenameMatcher.groupCount(); l_ix++)
            {
                l_controlDataAttribArr[l_ix - 1] = l_filenameMatcher.group(l_ix);
            }
            setBatchControlDataAttribs(l_controlDataAttribArr);
        }
        else
        {
            // ERROR: The given input data filename is invalid, it doesn't match the valid filename reg. ex.
            BatchConfigException l_batchConfigException =
                new BatchConfigException(C_CLASS_NAME,
                                         C_METHOD_verifyInputFilenameFormat,
                                         10000,
                                         this, null, 0L,
                                         BatchKey.get().batchInvalidIndataFilename(p_inputFilename,
                                                                                   l_filenameRegEx));
            i_logger.logMessage(l_batchConfigException);
            i_extraData4 = "Invalid indata filename.";
            throw l_batchConfigException;
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_setBatchControlDataAttribs = "setBatchControlDataAttribs";
    /**
     * Sets the batch control data (instance) attributes.
     * NB: This is the default implementation which assumes that the file date is the first attribute,
     *     the sequence number is the second attribute and the filename extension is the third attribute.
     *     If this is not the case according to the regular expression that defines a valid input data
     *     filename, then this method must be overridden in order for the sub-class to do its own
     *     implementation.
     * 
     * @param p_controlDataAttribArr  the batch control data attributes as a <code>String</code> array.
     */
    protected void setBatchControlDataAttribs(String[] p_controlDataAttribArr)
    {
        final int L_FILE_DATE_IX          = 0;
        final int L_FILE_SEQNO_IX         = 1;
        final int L_FILENAME_EXTENSION_IX = 2;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10000, this,
                            "Entering " + C_METHOD_setBatchControlDataAttribs);
        }

        if (p_controlDataAttribArr != null)
        {
            if (p_controlDataAttribArr.length > 0)
            {
                // Set the file date. The default file date syntax is: yyyymmdd (example: 20070423).
                i_fileDate = new PpasDate(p_controlDataAttribArr[L_FILE_DATE_IX], PpasDate.C_DF_yyyyMMdd);
            }

            if (p_controlDataAttribArr.length > 1)
            {
                // Set the file sequence number.
                i_fileSeqNo = Integer.parseInt(p_controlDataAttribArr[L_FILE_SEQNO_IX]);
            }

            if (p_controlDataAttribArr.length > 2)
            {
                // Set the file sequence number.
                i_extension = p_controlDataAttribArr[L_FILENAME_EXTENSION_IX];
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10000, this,
                            "Leaving " + C_METHOD_setBatchControlDataAttribs);
        }
    }


    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_getInputFilename = "getInputFilename";
    /**
     * Returns the input data filename runtime parameter.
     * If the parameter is missing a <code>BatchInvalidParameterException</code> is thrown.
     * 
     * @return the input data filename runtime parameter.
     * @throws BatchInvalidParameterException  if the input data filename runtime parameter is missing.
     */
    private String getInputFilename() throws BatchInvalidParameterException
    {
        String l_inputFilename = (String)super.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);
        if (l_inputFilename == null)
        {
            // ERROR: The input filename parameter is missing.
            BatchInvalidParameterException l_batchInvalidParameterEx =
                new BatchInvalidParameterException(C_CLASS_NAME,
                                                   C_METHOD_getInputFilename,
                                                   10010,
                                                   this, null, 0L,
                                                   BatchKey.get().batchMissingMandatoryParam(
                                                                        BatchConstants.C_KEY_INPUT_FILENAME));
            super.i_logger.logMessage(l_batchInvalidParameterEx);
            i_extraData4 = "Missing indata filename param.";
            throw l_batchInvalidParameterEx;
        }
        return l_inputFilename;
    }

    /**
     * Returns the recovery file as a <code>File</code> object.
     * NB: This method doesn't check for the existens of the recovery file.
     * 
     * @param p_inputDataFilename  the name of the input data file.
     * 
     * @return the recovery file as a <code>File</code> object.
     */
    private File getRecoveryFile(String p_inputDataFilename)
    {
        // Local constant(s).
        final String L_DEFVAL_RECOVERY_FILE_DIR = "${ASCS_LOCAL_ROOT}/batchfiles/recovery";
         
        // Local variable(s).
        StringBuffer l_recoveryFileSB;
        int          l_extensionIx;
        File         l_recoveryFile;
    
        String l_recoveryFileDir = i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY,
                                                                    L_DEFVAL_RECOVERY_FILE_DIR);
        l_recoveryFileDir += (l_recoveryFileDir.endsWith(File.separator) ? "" : File.separator);
        l_recoveryFileSB = new StringBuffer(l_recoveryFileDir);
        l_extensionIx = p_inputDataFilename.lastIndexOf('.');
        l_recoveryFileSB.append(p_inputDataFilename.substring(0, l_extensionIx));
        l_recoveryFileSB.append(BatchConstants.C_EXTENSION_RECOVERY_FILE);
        l_recoveryFile = new File(l_recoveryFileSB.toString());

        return l_recoveryFile;
    }


     /**
         * Returns the input data file as a <code>File</code> object. NB: This method doesn't check for the
         * existens of the input data file.
         * @param p_inputDataFilename the name of the input data file.
         * @return the input data file as a <code>File</code> object.
         */
    private File getInputDataFile(String p_inputDataFilename)
    {
        // Local constant(s).
        final String L_DEFVAL_INPUT_DATA_FILE_DIR = "${ASCS_LOCAL_ROOT}/batchfiles/data";

        // Local variable(s).
        StringBuffer l_inputDataFileSB;
        File l_inputDataFile;

        String l_inputDataDir = i_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY,
                                                                L_DEFVAL_INPUT_DATA_FILE_DIR);
        l_inputDataDir += (l_inputDataDir.endsWith(File.separator) ? "" : File.separator);
        l_inputDataFileSB = new StringBuffer(l_inputDataDir);
        l_inputDataFileSB.append(p_inputDataFilename);
        l_inputDataFile = new File(l_inputDataFileSB.toString());

        return l_inputDataFile;
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_verifyRecoveryMode = "verifyRecoveryMode";

    /**
     * Verifies that if a recovery file exists the input data filename extension must be the "in progress"
     * extension.
     * 
     * @param p_inputFilename the name of the input data file.
     * @throws BatchConfigException  if a recovery file exists but the input data filename extension is NOT
     *                               the "in progress" extension.
     */
    private void verifyRecoveryMode(String p_inputFilename) throws BatchConfigException
    {
        // Check if a recovery file exists.
        File l_recoveryFile = getRecoveryFile(p_inputFilename);
        if (l_recoveryFile.exists())
        {
            // Check if the input filename extension is the "in progress" extension.
            String l_extension = (i_extension != null ? "." + i_extension : null);
            if (l_extension == null || !l_extension.equals(BatchConstants.C_EXTENSION_IN_PROGRESS_FILE))
            {
                // ERROR: The input filename extension is NOT the "in progress" extension which it must
                // be when a recovery file exists.
                BatchConfigException l_batchConfigEx =
                    new BatchConfigException(C_CLASS_NAME,
                                             C_METHOD_verifyRecoveryMode,
                                             10020,
                                             this, null, 0L,
                                             BatchKey.get().batchIllegalIndataFilenameExt(
                                                                l_extension,
                                                                BatchConstants.C_EXTENSION_IN_PROGRESS_FILE));
                super.i_logger.logMessage(l_batchConfigEx);
                i_extraData4 = "Not 'in progress' extension.";
                throw l_batchConfigEx;
            }
        }
    }


    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_verfiyInputFile = "verfiyInputFile";
    /**
     * Verfies that the input data filename corresponds to a normal file that exists and is readable.
     * 
     * @param p_inputFilename  the name of the input data file.
     * @throws BatchConfigException  if the input data file doesn't exist, is not a normal file or
     *                               is not readable.
     */
    private void verfiyInputFile(String p_inputFilename) throws BatchConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10000, this,
                            BatchConstants.C_ENTERING + C_METHOD_verfiyInputFile);
        }

        // The input filename must correspond to an existing and readable file.
        File l_inputDataFile = getInputDataFile(p_inputFilename);
        if (!l_inputDataFile.exists() || !l_inputDataFile.isFile() || !l_inputDataFile.canRead())
        {
            // ERROR: The input data file doesn't exist, is not a normal file or is not readable.
            BatchConfigException l_batchConfigEx = 
                new BatchConfigException(C_CLASS_NAME,
                                         C_METHOD_verfiyInputFile,
                                         10020, this, null, 0L,
                                         BatchKey.get().batchInvalidIndataFile(
                                                                          l_inputDataFile.getAbsolutePath()));
            super.i_logger.logMessage(l_batchConfigEx);
            i_extraData4 = "Invalid input data file.";
            throw l_batchConfigEx;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_HIGH, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10000, this,
                            BatchConstants.C_LEAVING + C_METHOD_verfiyInputFile);
        }
    }
}
