////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       AutoMsisdnRoutDelBatchController
//  DATE            :       04 August 2006
//  AUTHOR          :       Ian James
//  REFERENCE       :       PRD_ASCS00_GEN_CA_098
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// 19/09/06 | Ian James     | Resolved comflicts between work  | PpacsLon#2451/9994
//          |               | area and database.               |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.AutoMsisdnRoutDelProcessor;
import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchreader.AutoMsisdnRoutDelBatchReader;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.AutoMsisdnRoutDelBatchWriter;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.featurelicence.FeatureLicence;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDateTime;
import com.slb.sema.ppas.common.support.PpasDebug;

/**
 * This class is the main entry point for starting Automated MSISDn Routing Deletion. Typically it 
 * is instantiated by the JobRunner.
 */
public class AutoMsisdnRoutDelBatchController extends BatchController
{
    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME           = "AutoMsisdnRoutDelBatchController";

    /** Specification of additional properties layers to load for this batch job type. Value is {@value}. */
    private static final String C_ADDED_CONFIG_LAYERS  = "batch_amd";

    /** Master batch control table. Value is {@value}. */
    private static final String C_MASTER_JOB_CONTROL_TABLE = "BACO_BATCH_CONTROL";
    
    /** Sub-batch control table. Value is {@value}. */
    private static final String C_SUB_JOB_CONTROL_TABLE    = "BSCO_BATCH_SUB_CONTROL";
    
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** Master job scheduler job identifier. */
    private String              i_masterJsJobId        = null;

    /** Sub job scheduler job identifier. */
    private String              i_subJobId             = null;

    /** Execution date/time of process. */
    private String              i_executionDateAndTime = null;

    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * @param p_context A <code>PpasContext</code> object.
     * @param p_jobType The type of job (e.g. BatchInstall).
     * @param p_jobId The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_parameters A Map holding parameters to use when executing this job.
     * @throws PpasConfigException If configuration data is missing or incomplete.
     * @throws PpasServiceException If feature is not licensed.
     */
    public AutoMsisdnRoutDelBatchController(PpasContext p_context,
                                            String p_jobType,
                                            String p_jobId,
                                            String p_jobRunnerName,
                                            Map p_parameters)
            throws PpasConfigException, PpasServiceException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_parameters);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10500,
                            this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        p_context.validateFeatureLicence(FeatureLicence.C_FEATURE_CODE_BATCH_AUTO_MSISDN_ROUT_DEL);

        init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10010,
                            this,
                            BatchConstants.C_CONSTRUCTED);
        }
        return;

    } // End of constructor AutoMsisdnRoutDelBatchController(.....)

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";

    /**
     * This method will instantiate the correct reader-class and passes on the required parameters and also a
     * reference to itself. The later is used for the reader-component to report back the
     * controller-component. The method is called from the super-class inner class method doRun() when start
     * is ordered.
     * @return Reference to a AutoMsisdnRoutDelBatchReader.
     */
    public BatchReader createReader()
    {
        AutoMsisdnRoutDelBatchReader l_reader = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10040,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_createReader);
        }

        // Instantiate the relevant type of reader and pass reference to controller
        l_reader = new AutoMsisdnRoutDelBatchReader(super.i_ppasContext,
                                                    super.i_logger,
                                                    this,
                                                    super.i_inQueue,
                                                    super.i_params,
                                                    super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10050,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_createReader);
        }

        return l_reader;

    } // End of createReader()

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";

    /**
     * This method will instantiate the correct writer-class and passes on the required parameters and also a
     * reference to itself. The later is used for the writer-component to report back the
     * controller-component. The method is called from the super-class inner class method doRun() when start
     * is ordered.
     * @return Reference to a AutoMsisdnRoutDelBatchWriter.
     * @throws IOException If the writer cannot be created.
     */
    public BatchWriter createWriter()
            throws IOException
    {
        AutoMsisdnRoutDelBatchWriter l_writer = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10040,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_createWriter);
        }

        l_writer = new AutoMsisdnRoutDelBatchWriter(super.i_ppasContext,
                                                    super.i_logger,
                                                    this,
                                                    super.i_params,
                                                    super.i_outQueue,
                                                    super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10050,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_createWriter);
        }

        return l_writer;

    } // End of createWriter()

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";

    /**
     * This method will instantiate the correct processor-class and passes on the required parameters and also
     * a reference to itself. The later is used for the processor-component to report back the
     * controller-component. The method is called from the super-class inner class method doRun() when start
     * is ordered.
     * @return Reference to a AutoMsisdnRoutDelProcessor.
     */
    public BatchProcessor createProcessor()
    {
        AutoMsisdnRoutDelProcessor l_processor = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10040,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_createProcessor);
        }

        l_processor = new AutoMsisdnRoutDelProcessor(super.i_ppasContext,
                                                     super.i_logger,
                                                     this,
                                                     super.i_inQueue,
                                                     super.i_outQueue,
                                                     super.i_params,
                                                     super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10050,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_createProcessor);
        }

        return l_processor;

    } // End of createProcessor()

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_init = "init";

    /**
     * Initialise.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10100,
                this,
                BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);        
        super.init();


        // Retrieve parameters from the Map
        i_masterJsJobId        = (String)super.i_params.get(BatchConstants.C_KEY_MASTER_JS_JOB_ID);
        i_executionDateAndTime = (String)super.i_params.get(BatchConstants.C_KEY_EXECUTION_DATE_TIME);
        i_subJobId             = (String)super.i_params.get(BatchConstants.C_KEY_SUB_JOB_ID);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10110,
                this,
                BatchConstants.C_LEAVING + C_METHOD_init);
        }
        
        return;
    } // End of init()

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";

    /**
     * This method call the service defined by PpasBatchControlService. In this case for a filedriven
     * updateJobDetails should be called.
     * @param p_status The status to be updated.
     */
    protected void updateStatus(String p_status)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10600,
                this,
                BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }

        try
        {
              
            i_batchContService.updateSubJobStatus(null,
                                                  this.i_masterJsJobId,
                                                  new PpasDateTime(this.i_executionDateAndTime),
                                                  this.i_subJobId,
                                                  p_status,
                                                  C_MASTER_JOB_CONTROL_TABLE,
                                                  C_SUB_JOB_CONTROL_TABLE,
                                                  super.i_timeout);
        }
        catch (PpasServiceException p_ppasServiceExe)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10610,
                    this,
                    C_METHOD_updateStatus + " PpasServiceException from updateJobDetails");
            }
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10620,
                this,
                BatchConstants.C_LEAVING + C_METHOD_updateStatus );
        }
    } // End of updateStatus()
        
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_getKeyedControlRecord = "getKeyedControlRecord";

    /**
     * This method will return a BatchSubJobData-object with the correct key-values set (MasterJobId,
     * ExecutionDataAndTime and SubJobId). The caller can then populate the record with data needed for the
     * operation in question.
     * @return Control record details.
     */
    public BatchSubJobData getKeyedControlRecord()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10430,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedControlRecord
                                    + " ... prepare with the database keys");
        }

        BatchSubJobData l_jobData = new BatchSubJobData();

        l_jobData.setMasterJsJobId(i_masterJsJobId);
        l_jobData.setExecutionDateTime(i_executionDateAndTime.toString());
        l_jobData.setSubJobId(i_subJobId);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10450,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getKeyedControlRecord + " MasterId="
                                    + i_masterJsJobId + " ExecutionDateAndTime=" + i_executionDateAndTime
                                    + " SubJobId=" + i_subJobId);
        }

        return l_jobData;
    } // End of getKeyedControlRecord()

} // End of class AutoMsisdnRoutDelBatchController
