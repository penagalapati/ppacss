////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       MsisdnRoutingProvisioningBatchReader.java
//DATE            :       Oct 26, 2004
//AUTHOR          :       Lars Lundberg
//REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       See Javadoc
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.MsisdnRoutingBatchRecordData;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible to validate and create MsisdnRoutingBatchRecordData that are put on the in-queue.
 */
public class MsisdnRoutingProvisioningBatchReader extends BatchLineFileReader
{
    //--------------------------------------------------------------------------
    //--  Constant.                                                           --
    //--------------------------------------------------------------------------

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME                  = "MsisdnRoutingProvisioningBatchReader";

    /** Field length of the MSISDN. Value is {@value}). */
    private static final int    C_MSISDN_LENGTH               = 15;
    
    /** Field length of the SDP id (it is an optional field). Value is {@value}). */
    private static final int    C_SDPID_LENGTH                = 2;
    
    /** Max record length. Value is {@value}). */
    private static final int    C_MAX_RECORD_LEN              = C_MSISDN_LENGTH + C_SDPID_LENGTH;

    /** Min record length. Value is {@value}). */
    private static final int    C_MIN_RECORD_LEN              = C_MSISDN_LENGTH;

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT = "01";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_NON_NUMERIC_MSISDN    = "02";



    //--------------------------------------------------------------------------
    //--  Constant.                                                           --
    //--------------------------------------------------------------------------

    /** The <code>StatusChangeBatchRecordData</code> to be stored into the Queue. */
    private MsisdnRoutingBatchRecordData i_batchDataRecord = null;

    //--------------------------------------------------------------------------
    //--  Constructors.                                                       --
    //--------------------------------------------------------------------------

    /**
     * Constructs a <code>MsisdnRoutingProvisioningBatchReader</code> instance using the given 
     * parameters.
     * 
     * @param p_controller  Reference to the BatchController that creates this object.
     * @param p_ppasContext Reference to PpasContext.
     * @param p_logger      reference to the logger.
     * @param p_parameters  Reference to a Map holding information e.g. filename.
     * @param p_queue       Reference to the Queue, where all indata records will be added.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     */
    public MsisdnRoutingProvisioningBatchReader(PpasContext      p_ppasContext,
                                                Logger           p_logger,
                                                BatchController  p_controller,
                                                SizedQueue       p_queue,
                                                Map              p_parameters,
                                                PpasProperties   p_properties)
    {
        super( p_ppasContext, p_logger, p_controller, p_queue, p_parameters, p_properties );
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    } // end of public Constructor SubInstBatchReader(.....)

    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * Creates, populates and returns a <code>BatchDataRecord</code> object.
     * The <code>BatchDataRecord</code> object will be populated with the parameters given in the indata file.
     * If no more records will be created (end of file is reached) 'null' is returned.
     * 
     * @return  a <code>BatchDataRecord</code> object, or 'null' if no more records will be created.
     */
    protected BatchRecordData getRecord()
    {
        StringBuffer l_tmp      = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10400,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_getRecord);
        }

        // Get next line from input file
        i_batchDataRecord = null;

        if ( readNextLine() != null )
        {
            i_batchDataRecord = new MsisdnRoutingBatchRecordData();

            // Store input filename
            i_batchDataRecord.setInputFilename(i_inputFileName);
            
            // Store line-number form the file
            i_batchDataRecord.setRowNumber(i_lineNumber);
            
            // Store original data record
            i_batchDataRecord.setInputLine(i_inputLine);
            
            // Extract data from line
            if (extractLineAndValidate())
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10410,
                                    this,
                                    "extractLineAndValid was successful: " + i_batchDataRecord.dumpRecord());
                }
            }
            else
            {
                // Line is corrupt - invalid record.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10420,
                                    this,
                                    "extractLineAndValid failed!");
                }

                // Flag for corrupt line and set error code
                i_batchDataRecord.setCorruptLine(true);
                if ( i_batchDataRecord.getErrorLine() == null )
                {
                    // Other error than specific field errors
                    l_tmp = new StringBuffer();
                    l_tmp.append(C_ERROR_INVALID_RECORD_FORMAT);
                    l_tmp.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);
                    l_tmp.append(i_batchDataRecord.getInputLine());
                    i_batchDataRecord.setErrorLine( l_tmp.toString() );
                }
                                
            }
            
            i_batchDataRecord.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10340,
                                this,
                                "After setNumberOfSuccessfully...." + super.i_successfullyProcessed);
            }
        }
                
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10360,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_batchDataRecord;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Checks if the filename is valid.
     * Returns <code>true</code> if the filename is valid, otherwise it returns <code>false</code>.
     * 
     * @param p_fileName  the filename
     * 
     * @return <code>true</code> if the filename is valid, otherwise it returns <code>false</code>.
     */
    protected boolean isFileNameValid( String p_fileName )
    {                
        boolean  l_validFileName = true;       // Help variable - return value. Assume filename is OK
 
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10470,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_isFileNameValid);
        }

        if ( p_fileName != null ) 
        {
            if ( p_fileName.matches(BatchConstants.C_PATTERN_MSISDN_ROUTING_PROVISIONING_DAT) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_MSISDN_ROUTING_PROVISIONING_SCH) ||
                 p_fileName.matches(BatchConstants.C_PATTERN_MSISDN_ROUTING_PROVISIONING_IPG) )
            {
                // Ok, the filename has a valid format.
                // Rename the file to .IPG to indicate that processing is in progress.
                // If it is recovery mode, the file already got the "in_progress_extension"
                if ( !i_recoveryMode )
                {
                    // Flag that processing is in progress. Rename the file to *.IPG.
                    // If the batch is running in recovery mode, it already has the extension .IPG
                    if (p_fileName.matches(BatchConstants.C_PATTERN_MSISDN_ROUTING_PROVISIONING_SCH))
                    {
                        renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                    }
                    else
                    {
                        renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_INDATA_FILE,
                                         BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
                    }
                                      
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                        PpasDebug.C_APP_SERVICE,
                                        PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME,
                                        10480,
                                        this,
                                        C_METHOD_isFileNameValid + 
                                        " -- after rename, inputFullPathName = ' " + i_fullPathName + "'.");
                    }
                }

            } // End if - valid filename!
            else
            {
                // Invalid fileName!!!
                l_validFileName = false;
            }
        } // end - if fileName != null
        
        else
        {
            l_validFileName = false; 
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10490,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_isFileNameValid);
        }

        return l_validFileName;
    } // End of isFileNameValid(.)


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    

    /**
     * This method extracts information from the batch data record. The record must look like:
     * [Line number];[MSISDN][MSISDN][SDP ID][Agent][Promotion Plan][Account Group][Service Offering]
     * The line number will be used to check against the recovery file if the batch mode is
     * recovery.
     * All fields are checked to be alpha numberic.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        MsisdnFormat l_msisdnFormat = null;
        Msisdn       l_msisdn       = null;
        String       l_msisdnStr    = null;
        String       l_sdpId        = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10320,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate);
        }

        // Check record length
        if (i_inputLine.length() == C_MAX_RECORD_LEN  ||
            i_inputLine.length() == C_MIN_RECORD_LEN)
        {
            l_msisdnStr = i_inputLine.substring(0, C_MSISDN_LENGTH).trim();
            if (!l_msisdnStr.matches(BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC))
            {
                // A non-numeric MSISDN is given.
                i_batchDataRecord.setErrorLine(C_ERROR_NON_NUMERIC_MSISDN + i_inputLine);
                return false;
            }

            // Convert the MSISDN string to a Msisdn object
            // This may cause a ParseException, flag corrupt line in such a case!
            l_msisdnFormat = i_context.getMsisdnFormatInput();
            try
            {
                l_msisdn = l_msisdnFormat.parse(l_msisdnStr);
                i_batchDataRecord.setMsisdn(l_msisdn);                
            }
            catch (ParseException p_parseEx)
            {
                this.i_batchDataRecord.setErrorLine(C_ERROR_NON_NUMERIC_MSISDN);                
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10370,
                                    this,
                                    "Row: " + i_lineNumber + " ]" + i_inputLine + "[ has an invalid MSISDN");
                }
                return false;
            }

            if (i_inputLine.length() == C_MAX_RECORD_LEN)
            {
                // Get the SDP id.
                l_sdpId = i_inputLine.substring(C_MSISDN_LENGTH);
                i_batchDataRecord.setSdpId(l_sdpId);
            }

            // Got a valid record
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10350,
                                this,
                                "Row: " + i_lineNumber + " ]" + i_inputLine + "[ is a valid record." );
            }

        }
        else
        {
            // Illegal field format detected
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10360,
                                this,
                                "Row: ]" + i_inputLine + "[ is a INVALID record\n");
            }
                                                                
            // Flag for corrupt line
            if ( this.i_batchDataRecord.getErrorLine() == null)
            {
                this.i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT + i_inputLine);
            }
            return false;
        } // end else - illegal field format detected
                
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10390,
                this,
                BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate );
        }

        return true;
    } // End of method extractLineAndValidate(.)
}
