////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       RefillBatchProcessor.java
//      DATE            :       10-July-2006
//      AUTHOR          :       Yang Liu
//      REFERENCE       :       PRD_ASCS00_GEN_CA_93
//
//      COPYRIGHT       :       WM-data 2006
//
//      DESCRIPTION     :       
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                     | REFERENCE
//----------+------------+---------------------------------+--------------------
// 07/08/06 | M Erskine  | Change exception mappings. Fix  | PpacLon#2479/9483
//          |            | numerous bugs and format code   |
//----------+------------+---------------------------------+--------------------
// 07/08/06 | M Erskine  | Duplicate exception mapping     | PpacLon#2479/9757
//          |            | removed. Error code 20 is the   |
//          |            | "catch all" error, instead of 99|
//          |            | , to behave in the same way as  |
//          |            | PPAS did.                       |
//----------+------------+---------------------------------+--------------------
// 28/08/06 | Yang Liu   | Currency should be extracted    | PpacLon#2587/9845
//          |            | once at the first time the      |
//          |            | processor run.                  | 
//----------+------------+---------------------------------+--------------------
// 12.12.06 | MAGray     | Pass channel Id.                | PpacLon#2815/10635
//          |            |                                 | PRD_ASCS00_GEN_CA_105
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.RefillBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.RefillBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.Money;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasCurrency;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasPaymentService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for the Number Plan Change.
 */
public class RefillBatchProcessor extends BatchProcessor
{
    //-------------------------------------------------------------------------
    //  Private constants.
    //  -------------------------------------------------------------------------
    /** Constant holding the name of this class. Value is {@value}. */
    private static final String C_CLASS_NAME                       = "RefillBatchProcessor";
    
    /** Specific Refill header info. Value is {@value}. */
    public static final String  C_REFILL_RECOVERY_INFO = "REFILL_BATCH_HEADER";
    
    /** Error code. Invalid refill profile. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_REFILL_PROFILE = "07";
    
    /** Error code. Msisdn not install. Value is {@value}. */
    private static final String C_ERROR_CODE_MSISDN_NOT_INSTALLED   = "09";
    
    /** Error code. Invalid refill method. Value is {@value}. */
    private static final String C_ERROR_CODE_INVALID_REFILL_METHOD = "15";
    
    /** Error code. Maximum Balance exceeded. Value is {@value}. */
    private static final String C_ERROR_CODE_MAX_BAL_EXCEEDED      = "19";
    
    /** Error code. Miscellaneous processing errors (re-submit record). Value is {@value}. */
    private static final String C_ERROR_CODE_MISC_PROCESSING_ERRORS= "20";
    
    /** Error code. REfill failed as subscriber is temporarily blocked. Value is {@value}. */
    private static final String C_ERROR_CODE_SUBS_TEMP_BLOCKED     = "23";
    
    /** Error code. Service or Airtime period is too far in the future. Value is {@value}. */
    private static final String C_ERROR_CODE_SERV_OR_AIRTIME_PERIOD_TOO_LONG = "24";
    
    /** The mapping between the error codes and the corresponding 'PpasServiceException' message keys. */
    private static final String[][] C_ERROR_CODE_MAPPINGS =
    {
            // Mappings to 'PpasServiceException' message keys thrown by the IS API method itself.
        
            // Exceptions documented in the IS API
            {PpasServiceMsg.C_KEY_ACCOUNT_NOT_ACTIVE,       C_ERROR_CODE_MISC_PROCESSING_ERRORS},
            {PpasServiceMsg.C_KEY_ACCOUNT_OVERFLOW,         C_ERROR_CODE_MAX_BAL_EXCEEDED},
            {PpasServiceMsg.C_KEY_PAYMENT_TYPE_NOT_FOUND,   C_ERROR_CODE_INVALID_REFILL_METHOD},
            {PpasServiceMsg.C_KEY_AIRTIME_PERIOD_TOO_LONG,  C_ERROR_CODE_SERV_OR_AIRTIME_PERIOD_TOO_LONG},
            {PpasServiceMsg.C_KEY_CUST_IS_TEMP_BLOCKED,     C_ERROR_CODE_SUBS_TEMP_BLOCKED},
            {PpasServiceMsg.C_KEY_DEDICATED_ACCOUNT_NOT_ALLOWED, C_ERROR_CODE_MISC_PROCESSING_ERRORS},
            {PpasServiceMsg.C_KEY_INVALID_PAYMENT_PROFILE,  C_ERROR_CODE_INVALID_REFILL_PROFILE},
            {PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS,        C_ERROR_CODE_MSISDN_NOT_INSTALLED},
            {PpasServiceMsg.C_KEY_PAYMENT_PROFILE_NOT_FOUND,C_ERROR_CODE_INVALID_REFILL_PROFILE},
            {PpasServiceMsg.C_KEY_RESOURCE_UNAVAILABLE,     C_ERROR_CODE_MISC_PROCESSING_ERRORS},
            {PpasServiceMsg.C_KEY_SERVICE_PERIOD_TOO_LONG,  C_ERROR_CODE_SERV_OR_AIRTIME_PERIOD_TOO_LONG},
            {PpasServiceMsg.C_KEY_VOUCHER_GROUP_SERVICE_CLASS_ERR, C_ERROR_CODE_MISC_PROCESSING_ERRORS},
    };
    
    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------
    
    /** The <code>IS API</code> reference. */
    private PpasPaymentService i_ppasPaymentService = null;
    
    /** The <code>Session</code> object. */
    private PpasSession        i_ppasSession        = null;
    
    /** The currency from the header. */
    private PpasCurrency        i_currency          = null;
    
    /** Used to check if the first time to run processor. */
    private boolean            i_firstProcessorRun  = true;
    
    /** The oprator's Id. */
    private String             i_opId               = null;
    
    /**
     * Constructs an instance of this <code>RefillBatchProcessor</code> class using the specified
     * batch controller, input data queue and output data queue.
     *
     * @param p_ppasContext      the PpasContext reference.
     * @param p_logger           the logger.
     * @param p_batchController  the batch controller.
     * @param p_inQueue          the input data queue.
     * @param p_outQueue         the output data queue.
     * @param p_params           the start process paramters.
     * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
     */
    public RefillBatchProcessor(PpasContext           p_ppasContext,
                                Logger                p_logger,
                                RefillBatchController p_batchController,
                                SizedQueue            p_inQueue,
                                SizedQueue            p_outQueue,
                                Map                   p_params,
                                PpasProperties        p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);
        
        PpasRequest l_ppasRequest = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME, 10000, this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }
        
        i_ppasSession        = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest        = new PpasRequest(i_ppasSession);
        i_ppasPaymentService = new PpasPaymentService(l_ppasRequest, p_logger, p_ppasContext);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_LOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME, 10010, this,
                            BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    } 
    
    /** Constant holding the name of this method. Value is {@value}). */
    private static final String C_METHOD_processRecord = "processRecord";
    /**
     * Processes the given <code>BatchDataRecord</code> and returns it.
     * If the process succeeded the <code>BatchDataRecord</code> is updated with information
     * needed by the writer component before returning it.
     * If the process fails but the record shall be re-processed at the end of this process,
     * the record will be put in the retry queue and this method returns <code>null</code>.
     * 
     * @param p_record  the <code>BatchDataRecord</code>.
     * @return the given <code>BatchDataRecord</code> updated with info for the writer component,
     *         or <code>null</code>.
     * @see BatchProcessor#processRecord(BatchRecordData)
     * @throws PpasServiceException  if an unrecoverable error occurs while installing a subscriber.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        RefillBatchRecordData   l_refillRecord     = null;
        Msisdn                  l_msisdn           = null;
        PpasRequest             l_ppasRequest      = null;
        Money                   l_refillAmount     = null;
        String                  l_refillMethod     = null;
        String                  l_recoveryLine     = null;
        String                  l_refillProfile    = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10020, this,
                            "Entering " + C_METHOD_processRecord);
        }

        l_refillRecord = (RefillBatchRecordData)p_record;
        
        if (l_refillRecord == null)
        {
            return l_refillRecord;
        }
        
        if (!l_refillRecord.isCorruptLine() && ((RefillBatchRecordData)p_record).getIsRecord())
        {
            i_opId             = super.i_batchController.getSubmitterOpid();
            l_msisdn           = l_refillRecord.getMsisdn();
            l_ppasRequest      = new PpasRequest(i_ppasSession, i_opId, l_msisdn);
            l_refillProfile    = l_refillRecord.getRefillProfile();
            l_refillMethod     = l_refillRecord.getRefillMethod();
            
            if(i_firstProcessorRun)
            {
                i_currency         = l_ppasRequest.getContext().getCurrency(l_refillRecord.getCurrency());
                
                i_firstProcessorRun = false;
            }
            try
            {
                l_refillAmount     = l_ppasRequest.getContext().getMoneyFormat().parse(
                                                        "" + l_refillRecord.getRefillAmount(), i_currency);
                
                // Make payment.
                i_ppasPaymentService.makePayment(l_ppasRequest,
                                                 i_isApiTimeout,
                                                 l_refillMethod,
                                                 l_refillAmount,
                                                 l_refillProfile,  // cardGroupId
                                                 "",  // BankId
                                                 "",  // address1
                                                 "",  // address2
                                                 "",  // address3
                                                 "",  // postalCode
                                                 null); // Channel Id
                
                l_recoveryLine = l_refillRecord.getRowNumber() + 
                BatchConstants.C_DELIMITER_RECOVERY_STATUS + 
                BatchConstants.C_SUCCESSFULLY_PROCESSED;
                l_refillRecord.setRecoveryLine(l_recoveryLine);
            }
            catch (ParseException e)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME, 10148, this,
                                    "Could not create Money object for " + l_refillAmount + 
                                    " " + i_currency.getCurrencyCode() + "\n");
                }
                
                // TODO Why is this not logged ?
            }
            catch (PpasServiceException p_psExe)
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10024, this,
                                    "A PpasServiceException is caught: " + p_psExe.getMessage() + 
                                    ",  key: " + p_psExe.getMsgKey());
                }
                
                if (p_psExe.getLoggingSeverity() == PpasServiceException.C_SEVERITY_FATAL)
                {
                    // This is a fatal error, re-throw the 'PpasServiceException' in order to stop
                    // the complete process.
                    if (PpasDebug.on)
                    {
                        PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                        C_CLASS_NAME, 10028, this,
                                        "***ERROR: " +
                                        "A fatal error has occurred during batch refill -- " + 
                                        "msg key: " + p_psExe.getMsgKey());
                    }
                    throw p_psExe;
                }
                
                // Map the thrown 'PpasServiceException' to an appropriate error code.
                mapException(l_refillRecord, p_psExe);
                
                // Create and set the recovery line.
                l_recoveryLine = l_refillRecord.getRowNumber() +
                BatchConstants.C_DELIMITER_RECOVERY_STATUS +
                BatchConstants.C_NOT_PROCESSED;
                l_refillRecord.setRecoveryLine(l_recoveryLine);
            }
        }
        else
        {
            if (p_record.isCorruptLine())
            {
                // Append a suitable status to it and set the result as the recovery line for this record.
                l_recoveryLine = l_refillRecord.getRowNumber() +
                                     BatchConstants.C_DELIMITER_RECOVERY_STATUS +
                                     BatchConstants.C_NOT_PROCESSED;
                l_refillRecord.setRecoveryLine(l_recoveryLine);
            }
           
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10030, this,
                            "Leaving " + C_METHOD_processRecord);
        }

        return l_refillRecord;
    } 
    
    //----------------------------------------------------------------------
    //--  Private methods.                                                --
    //----------------------------------------------------------------------
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_mapException = "mapException";        
    /**
     * Updates the passed <code>BatchRecordData</code> object's error line with an error code that 
     * corresponds to the passed <code>PpasServiceException</code>'s message key.
     * 
     * @param p_record      the <code>BatchRecordData</code>.
     * @param p_ppasServEx  the <code>PpasServiceException</code>.
     */
    private void mapException(BatchRecordData p_record, PpasServiceException p_ppasServEx)
    {
        StringBuffer l_errorLine   = null;
        boolean      l_mappingDone = false;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 10040, this,
                            BatchConstants.C_ENTERING + C_METHOD_mapException);
        }
        
        // Set the last part of the error line.
        l_errorLine = new StringBuffer(BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                       p_record.getInputLine());
        
        // Look for matching mapping through the whole error code mapping array or until a matching
        // mapping is done.
        for (int i = 0; (i < C_ERROR_CODE_MAPPINGS.length  &&  !l_mappingDone); i++)
        {
            if (p_ppasServEx.getMsgKey().equals(C_ERROR_CODE_MAPPINGS[i][0]))
            {
                l_errorLine.insert(0, C_ERROR_CODE_MAPPINGS[i][1]);
                l_mappingDone = true;
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME, 10042, this,
                                    C_METHOD_mapException + 
                                    " -- A matching mapping is found, key = " + C_ERROR_CODE_MAPPINGS[i][0] +
                                    ",  error code = " + C_ERROR_CODE_MAPPINGS[i][1]);
                }
            }
        }
        
        if (!l_mappingDone)
        {
            // None of the expected mapping matches the given message key.
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME, 10044, this,
                            C_METHOD_mapException + 
                            " -- An unexpected PpasServiceException is detected, " +
                            "key = " + p_ppasServEx.getMsgKey() +
                            ",  Exception severity = " + p_ppasServEx.getLoggingSeverity());
            
            l_errorLine.insert(0, C_ERROR_CODE_MISC_PROCESSING_ERRORS);
        }
        
        p_record.setErrorLine(l_errorLine.toString());
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                            C_CLASS_NAME, 10050, this,
                            BatchConstants.C_LEAVING + C_METHOD_mapException);
        }
    }
}
