////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SubInstBatchWriter.java 
//      DATE            :       13-May-2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME      | DESCRIPTION                          | REFERENCE
//----------+-----------+--------------------------------------+--------------------
//18/06/08  | M Erskine | Write to temporary files and         | PpacLon#3650/13137
//          |           | rename on completion of batch job.   |
//----------+-----------+--------------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchcontroller.SubInstBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.SubInstBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/** Responsible for doing the final processing of an installation order; That is write the outcome
 *  to file.
 * Two different types of files are used for Batch Subscriber Installation; 
 * one for recovery purposes and one for rejected installation orders (report/error file).  
 * All orders (It are lines from the input file) processed will contribute with one line in 
 * the recovery file.  
 * All orders that during processing generated an error will contribute with one line in 
 * the report file. 
 */
public class SubInstBatchWriter extends BatchWriter
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME             = "SubInstBatchWriter";
    
    /** Name of report file starts with this prefix. Value is {@value}. */
    private static final String C_PREFIX_REPORT_FILENAME = "INSTALL";

    /**Gets how many records to be written. Gets from <code>p_params</code>. */    
    private int i_intervall = 0;
    /**Used to count how many successfull records that has been processed. */
    private int i_success   = 0;
    /**Used to count how many  records that has been processed. */
    private int i_processed = 0;

    /** Help variable for recovery mode. If it is the first record that is read
     * add the number of already successfully processed records to the number successfully
     * processed in this run.
     */
    private boolean i_firstRecord                          = true;
   
    /** Help variable for recovery for count the total number of successfully processed records. */
    private int     i_numberOfSuccessfullyProcessedRecords = 0;
    /** Help variable for recovery for counting the total number of failed records. */
    private int     i_numberOfFaultyRecords                = 0;

    
    /**
     * Constructs a SubInstBatchWriter object.
     * The name of the input file is used to derive the names of the two output files.
     * The filenames passed must be absolute and are constructed as a combination of 
     * property-information (path) and input filename (filename).
     * @param p_ppasContext A PPpsCOntext
     * @param p_logger      The logger used of the writer.
     * @param p_controller  The batch jobs batch controller. 
     * @param p_params      Holding parameters used of the writer. 
     * @param p_outQueue    The batch jobs out queue.
     * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
     * @throws IOException  When there is some error to write to file.
     */
    public SubInstBatchWriter(
        PpasContext p_ppasContext,
        Logger p_logger,
        BatchController p_controller,
        Map p_params,
        SizedQueue p_outQueue,
        PpasProperties p_properties)
        throws IOException
    {

        super(p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties);
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                "Constructing " + C_CLASS_NAME);
        }
        
        i_intervall = i_properties.getIntProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        
        String l_recoverPath =
            i_properties.getTrimmedProperty(BatchConstants.C_RECOVERY_FILE_DIRECTORY);

        String l_reportPath = i_properties.getTrimmedProperty(BatchConstants.C_REPORT_FILE_DIRECTORY);

        String p_inputFile = (String)i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);        

        //Create an absolute file name
        String l_reportFile   = this.getFullReportFileName(l_reportPath, 
                                                           p_inputFile );
        super.i_recoveryFileName = super.generateFileName(l_recoverPath, 
                                                          p_inputFile, 
                                                          BatchConstants.C_EXTENSION_RECOVERY_FILE );
        
        super.openFile(BatchConstants.C_KEY_REPORT_FILE, new File(l_reportFile));
        super.openFile(BatchConstants.C_KEY_RECOVERY_FILE, new File(super.i_recoveryFileName));
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10002,
                this,
                "Constructing " + C_CLASS_NAME);
        }
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";
    
    /**
     * This method will be called for all records read from the queue. 
     * Based on the content of the SubInstBatchRecordData information may be written to the error file.
     * Information is also written to the recovery file. 
     * Count number of successful and errornous records.
     * It also populates a <code>BatchJobData</code> object with information from <code>p_params</code>. 
     * 
     * @param p_record Holds the information about the error line number and the recovery line number.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void writeRecord(BatchRecordData p_record)throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10100,
                this,
                "Enter " + C_METHOD_writeRecord);
        }
        SubInstBatchRecordData l_record = (SubInstBatchRecordData)p_record;
        
        // Save number of already processed records for the trailer printout.
        if ( this.i_firstRecord )
        {
            this.i_firstRecord = false;
            this.i_numberOfSuccessfullyProcessedRecords = l_record.getNumberOfSuccessfullyProcessedRecords();
            this.i_numberOfFaultyRecords                = l_record.getNumberOfErrorRecords();
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                "noSuccessfully processed=" + this.i_numberOfSuccessfullyProcessedRecords+
                " noFailured=" + this.i_numberOfFaultyRecords);
        }

        //Check if report file info exist and write it to file.
        if (l_record.getErrorLine() != null)
        {
            super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_record.getErrorLine());
            super.i_errors++;
        }
        else
        {
            i_success++;
        }

        // If this record failed during the previous run - decrease it otherwise it will be counted
        // twice as a failure record in the baco-table
        if ( l_record.getFailureStatus())
        { 
            this.i_numberOfFaultyRecords--;
        }

        i_processed++;
        
        //Allways write to recovery file.
        super.writeToFile(BatchConstants.C_KEY_RECOVERY_FILE, l_record.getRecoveryLine());

        if ( (i_processed % i_intervall) == 0)       
        {
            updateStatus();
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10110,
                this,
                "Leaving " + C_METHOD_writeRecord);
        }
    }

    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus()throws IOException, PpasServiceException
    {
        BatchJobControlData l_jobControlData = null;
        
        super.flushFiles();
        l_jobControlData = ((SubInstBatchController)super.i_controller).getKeyedControlRecord();
        l_jobControlData.setJobType(BatchConstants.C_JOB_TYPE_BATCH_INSTALLATION);        
        l_jobControlData.setNoOfRejectedRecs(super.i_errors + i_numberOfFaultyRecords);
        l_jobControlData.setNoOfSuccessfullyRecs(i_success + i_numberOfSuccessfullyProcessedRecords);
        super.updateControlInfo(l_jobControlData);
    }

    /** Writes out the trailer record.
     * @param p_outcome Whether the batch FAILED (could not reach the end of the file) or SUCCESS
     * @throws IOException Error when writing to output file.
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        String l_count = BatchConstants.C_TRAILER_ZEROS + 
                         (i_success + i_numberOfSuccessfullyProcessedRecords);

        l_count = l_count.substring(
                  l_count.length() - BatchConstants.C_TRAILER_ZEROS.length(), l_count.length());

        super.writeToFile(BatchConstants.C_KEY_REPORT_FILE, l_count + p_outcome);
        
        // Total number of faulty record. Set to flag if the recovery file may be deleted.
        super.i_errors = super.i_errors + i_numberOfFaultyRecords;
        
        //Rename the report file from .TMP to .RPT.
        BatchFile l_reportFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_REPORT_FILE);
        
        if (l_reportFile != null)
        {
            l_reportFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    BatchConstants.C_EXTENSION_TMP_FILE,
                                    BatchConstants.C_EXTENSION_REPORT_FILE);
        }
        else
        {
            throw new IOException("Report file does not exist");
        }
    }
    
    
    
    
    /**
     * Construct full path to the report file, INSTALL_yyyymmdd_sssss.RPT.
     * Information about yyyymmdd and sssss is retrieved from the original
     * input filename. Information is picked differently depending on if it si
     * short- or long filename.
     * @param p_reportPath Path to the reportfile directory.
     * @param p_inputFileName Input file name.
     * @return full path to the report file.
     */
    private String getFullReportFileName( String p_reportPath, 
                                          String p_inputFileName )
    {
        // Create the output file name like INSTALL_yyyymmdd_sssss.RPT
        String l_fileDate       = null;
        String l_fileSequence   = null;
        String l_inputFileArr[] = 
            p_inputFileName.split(BatchConstants.C_PATTERN_FILENAME_COMPONENTS_DELIMITER);
        
        // Filename syntax already validated in SubInstBatchReader there are short or long format.
        if ( l_inputFileArr.length == BatchConstants.C_NUMBER_OF_COMPONENTS_IN_SHORT_FILENAME )
        {       
            l_fileDate     = l_inputFileArr[BatchConstants.C_INDEX_FILE_DATE_SHORT];
            l_fileSequence = l_inputFileArr[BatchConstants.C_INDEX_SEQ_NUMBER_SHORT];
        }
        else
        {
            l_fileDate     = l_inputFileArr[BatchConstants.C_INDEX_FILE_DATE_LONG];
            l_fileSequence = l_inputFileArr[BatchConstants.C_INDEX_SEQ_NUMBER_LONG];
        }
        StringBuffer l_tmp = new StringBuffer();
        l_tmp.append(p_reportPath);
        l_tmp.append("/");
        l_tmp.append(C_PREFIX_REPORT_FILENAME);
        l_tmp.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
        l_tmp.append(l_fileDate);
        l_tmp.append(BatchConstants.C_DELIMITER_FILENAME_FIELDS);
        l_tmp.append(l_fileSequence);
        l_tmp.append(BatchConstants.C_EXTENSION_REPORT_FILE);

        return l_tmp.toString();
        
    } // End of getFullReportFileName(..)
    
}
