////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       NPChBatchProcessor.java
//      DATE            :       20-Aug-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005.
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+----------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+----------------
// 15/11/05 | Lars L.       | The key MSISDN_NOT_IN_BLOCK_RANGE| PpacLon#1778/7447
//          |               | is now mapped to error code '01'.|
//          |               | A few checkstyle warnings are    |
//          |               | also fixed.                      |
//----------+---------------+----------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchElementResponseData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchResponseData;
import com.slb.sema.ppas.common.dataclass.NPChBatchRecordData;
import com.slb.sema.ppas.common.dataclass.NumberChangeData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasNumberChangeService;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
* This class is responsible for the Number Plan Change.
*/
public class NPChBatchProcessor extends BatchProcessor
{
    //--------------------------------------------------------------------------
    //--  Class level variables.                                              --
    //--------------------------------------------------------------------------

    /** The trace print out base statement number. */
    private static int c_traceStatementNumber = 12000;

    //-------------------------------------------------------------------------
    //  Private constants.
    //  -------------------------------------------------------------------------
    /** Constant holding the name of this class. Value is {@value}. */
    private static final String C_CLASS_NAME                       = "NPChBatchProcessor";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD             = "01";

    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INCORRECT_USE_OF_WILDCARDS = "02";

    /** Constant holding the error code for a not available MSISDN. Value is {@value}. */
    private static final String C_ERROR_CODE_NUMBER_PLAN_CHANGE_INPROGRESS = "03";

    /** Special delimiter for the recovery record for Number Plan Change. */
    private static final String C_DELIMITER_MSISDN = " ";

    //-------------------------------------------------------------------------
    // Private variables.
    //-------------------------------------------------------------------------

    /** The maximum chunk size. */
    private int    i_chunkSize                = 0;

    /** The chunk buffer. */
    private Vector i_chunk                    = null;
    
    /** The maximum number of parallell inner class threads. */
    private int    i_maxThreads               = 0;

    /** Signaling object. */
    private Object i_signalObj                = new Object();
    
    /** A <code>Vector</code> that contains references to the living inner class process threads. */
    private Vector i_livingIsProcessThreadVec = null;
    
    /** The inner class thread id counter. */
    private int    i_threadIdCnt              = 0;

    /**
    * Constructs an instance of this <code>SubInstBatchProcessor</code> class using the specified
    * batch controller, input data queue and output data queue.
    *
    * @param p_ppasContext      the PpasContext reference.
    * @param p_logger           the logger.
    * @param p_batchController  the batch controller.
    * @param p_inQueue          the input data queue.
    * @param p_outQueue         the output data queue.
    * @param p_params           the start process paramters.
    * @param p_properties       <code>PpasProperties </code> for the batch subsystem.
    */
    public NPChBatchProcessor(PpasContext     p_ppasContext,
                              Logger          p_logger,
                              BatchController p_batchController,
                              SizedQueue      p_inQueue,
                              SizedQueue      p_outQueue,
                              Map             p_params,
                              PpasProperties  p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10000,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        
        i_threadIdCnt = 0;
        i_chunkSize  = p_properties.getIntProperty(BatchConstants.C_MAX_CHUNK_SIZE, 200);
        i_maxThreads = p_properties.getIntProperty(BatchConstants.C_MAX_NUMBER_OF_THREADS, 10);
        trace(this, "Constructor", "chunck size = " + i_chunkSize);
        trace(this, "Constructor", "max threads = " + i_maxThreads);
        
        i_livingIsProcessThreadVec = new Vector(i_maxThreads);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_LOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
        
        return;
        
    } // End of constructor(.......)


 
    
    
    /** Constant holding the name of this method. Value is {@value}). */
    private static final String C_METHOD_processRecord = "processRecord";
    /**
    * Processes the given <code>BatchDataRecord</code> and returns it.
    * If the process succeeded the <code>BatchDataRecord</code> is updated with information
    * needed by the writer component before returning it.
    * If the process fails but the record shall be re-processed at the end of this process,
    * the record will be put in the retry queue and this method returns <code>null</code>.
    * 
    * @param p_record  the <code>BatchDataRecord</code>.
    * @return the given <code>BatchDataRecord</code> updated with info for the writer component,
    *         or <code>null</code>.
    * @see BatchProcessor#processRecord(BatchRecordData)
    * @throws PpasServiceException  if an unrecoverable error occurs while installing a subscriber.
    */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        IsProcessor             l_isProcessor       = null;
        NPChBatchRecordData     l_record            = (NPChBatchRecordData)p_record;
        String                  l_errorLine         = null;
        Hashtable               l_wildcardsRecovery = null;
        boolean                 l_doProcessing      = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10020,
                this,
                "** PROCESS RECORD ** " + BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }

        if (i_chunk == null)
        {
            i_chunk = new Vector();
        }

        trace(this, C_METHOD_processRecord, "Current i_chunk size     = " + i_chunk.size());
        trace(this, C_METHOD_processRecord, "Max allowed chunk size   = " + i_chunkSize);
        trace(this, C_METHOD_processRecord, "Number of living threads = " + i_maxThreads);
        trace(this, C_METHOD_processRecord, "Max no of threads        = " + i_maxThreads);

        l_wildcardsRecovery = l_record.getSuccessfullyProcessed();
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_TRACE,
                C_CLASS_NAME,
                10022,
                this,
                "== START PROCESSRECORD == processRecord Dump record: " + l_record.dumpRecord());
        }


        // Process record if it was OK
        if (!l_record.isCorruptLine())
        {
            
            // Check if recovery shall be performed
            if ( l_wildcardsRecovery != null )
            {
                // Running in recovery mode - check if this oldMSISDN needs recovery
                String l_tmpKey = l_record.getOldMsisdn();
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10022,
                        this,
                        "Wildcard map existsprocessor tmpKey=" + l_tmpKey);
                }
                String l_tmpStatus = (String)l_wildcardsRecovery.get(l_tmpKey);
                if ( l_tmpStatus != null )
                {
                    // Already processed successfully - pass record to the writer
                    l_doProcessing = false;
                    l_record.setRecoveryLine(createRecoveryLine(l_record.getRowNumber(),
                                                                l_tmpKey,
                                                                BatchConstants.C_SUCCESSFULLY_PROCESSED));
                }                
            }
            
                                    
            if ( l_doProcessing )
            {
                i_chunk.add(l_record);

                if (i_chunk.size() == i_chunkSize)
                {
                    if (i_livingIsProcessThreadVec.size() >= i_maxThreads)
                    {
                        // Max number of threads already started, wait before starting a new thread.
                        trace(this, C_METHOD_processRecord, "Synch. before 'Wait to start new thread'.");
                        synchronized (i_signalObj)
                        {
                            try
                            {
                                trace(this, C_METHOD_processRecord, "Wait to start new thread.");
                                i_signalObj.wait();
                            }
                            catch (InterruptedException p_intEx)
                            {
                                // Nothing to do.
                                p_intEx = null;
                            }
                        }
                    }
                    // Construct and start the inner class.
                    trace(this, C_METHOD_processRecord, "Start new thread with id = " + i_threadIdCnt);
                    l_isProcessor = new IsProcessor(i_threadIdCnt++, i_chunk);
                    l_isProcessor.start();
                    i_livingIsProcessThreadVec.add(l_isProcessor);
                    i_chunk = null;
                } // End if chunkSize... 
                
            } // End if l_doProcessing
            
        } // End - if not a corrupt line
        
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_MODERATE,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_END,
                    C_CLASS_NAME,
                    10030,
                    this,
                    "** PROCESSOR ** corrupt line " + l_record.dumpRecord());
            }

            l_record.setRecoveryLine(createRecoveryLine(l_record.getRowNumber(),
                                                        l_record.getOldMsisdn(),
                                                        BatchConstants.C_NOT_PROCESSED));
            
            l_errorLine = this.createErrorLine( l_record.getErrorLine(),
                                                l_record.getOldMsisdn(),
                                                l_record.getNewMsisdn(),
                                                l_record.getInputLine());
            l_record.setErrorLine(l_errorLine);
            
            // A corrupt line, store it directly in the output queue.
            trace(this, "processRecord", "Corrupt line, store record immediately.");
            super.storeRecord(l_record);
        } // End - else it was a corrupt line


        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_MODERATE,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10030,
                this,
                "** PROCESSOR **" + BatchConstants.C_LEAVING +
                C_METHOD_processRecord + l_record.dumpRecord());
        }

       return null;
    } // End of processRecord(.)


    
    
    
    //--------------------------------------------------------------------------
    //--  Protected methods.                                                  --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_flush = "flush";        
    /**
     * Flushes the chunk buffer.
     * This method is called from the super class ProcessorThread just before it will end, which means that
     * this method shouldn't return until all living 'IsProcessor' Threads have ended.
     */
    protected void flush()
    {
        IsProcessor l_isProcessor = null;
        boolean     l_continue    = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11020,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_flush);
        }

        // Check that there is a chunk of un-processed records.
        if (i_chunk != null  &&  i_chunk.size() > 0)
        {
            // We don't bother to check if the max number of process threads will be exceeded since we are
            // going to create only one new process thread.
            trace(this, C_METHOD_flush, "Create and start a processing thread with id = " + i_threadIdCnt);
            l_isProcessor = new IsProcessor(i_threadIdCnt++, i_chunk);
            l_isProcessor.start();
            i_livingIsProcessThreadVec.add(l_isProcessor);

            trace(this, C_METHOD_flush, "Wait for all living processing thread...");
        }

        l_continue = true;
        while (l_continue)
        {
            try
            {
                trace(this, C_METHOD_flush, "Get the first living processing thread.");
                l_isProcessor = (IsProcessor)i_livingIsProcessThreadVec.firstElement();
            }
            catch (NoSuchElementException p_nseEx)
            {
                // Ok, no more threads to join.
                trace(this, C_METHOD_flush, "No more living processing thread to join.");
                l_continue = false;
                
                // Start the loop all over again.
                continue;
            }

            try
            {
                trace(this, C_METHOD_flush, "Join the processing thread '" +
                                            l_isProcessor.getThreadName() + "'");
                l_isProcessor.join(0L);
            }
            catch (InterruptedException p_intEx)
            {
                // No specific handling.
                p_intEx = null;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_flush);
        }
        
        return;
        
    } // End of flush()
    


    
    
    //--------------------------------------------------------------------------
    //--  Protected methods.                                                  --
    //--------------------------------------------------------------------------    
    /**
     * Creates a string with recovery information, such as: row MSISDN,p_recoveryStatus.
     * 
     * For records where the MSISDN has not been successfully verified by
     * the Reader the record will look like: #row ,p_recoveryStatus.
     * 
     * @param p_rowNumber Row number.
     * @param p_key       Recovery key.
     * @param p_recoveryStatus Recovery status.
     * @return A recovery string to be written to the recovery file.
     */
    private String createRecoveryLine( long   p_rowNumber,
                                       String p_key,
                                       String p_recoveryStatus )  
    {
        StringBuffer l_recoveryInformation = new StringBuffer();
        
        l_recoveryInformation.append( p_rowNumber );
        l_recoveryInformation.append( C_DELIMITER_MSISDN );
        if ( p_key != null )
        {
            // If key is non-numeric, it is old MSISDN with wildcards for which none matching MSISDN
            // have been found in database. 
            // Only give line number in the recovery information
            if ( p_key.matches(BatchConstants.C_PATTERN_NUMERIC) )
            {
                // Specific MSISDN in key
                l_recoveryInformation.append(p_key);                 
            }
        }
        l_recoveryInformation.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);
        l_recoveryInformation.append(p_recoveryStatus);
        
        return l_recoveryInformation.toString();
    } // End createRecoveryLine(...)

    
    
    
    
    /**
     * Construct error information for the report file, the MSISDNs must be left-justified.
     * @param p_errorCode Error code.
     * @param p_oldMsisdn Old MSISDN.
     * @param p_newMsisdn New MSISDN.
     * @param p_inputLine Original input line.
     * @return String with errorline information, left-justified MSISDNs.
     */
    private String createErrorLine( String p_errorCode,
                                    String p_oldMsisdn,
                                    String p_newMsisdn,
                                    String p_inputLine )
    {
        StringBuffer l_tmpErrorLine = new StringBuffer(); // Helpvariable for creation of error line
        
        l_tmpErrorLine.append(p_errorCode);
        l_tmpErrorLine.append(BatchConstants.C_DELIMITER_REPORT_FIELDS) ;
        
        // Field or Record format error. Might also be "unknown" error...
        // and invalid use of wildcards. Then the error line should contain the
        // error code and the original input line.
        if ( p_errorCode.equals(C_ERROR_INVALID_RECORD) ||
             p_errorCode.equals(C_ERROR_INCORRECT_USE_OF_WILDCARDS))
        {
            l_tmpErrorLine.append(p_inputLine);
        }
        else
        {
            // Left justify old and new MSISDN
            l_tmpErrorLine.append(
                (p_newMsisdn + BatchConstants.C_EMPTY_MSISDN).substring(0, BatchConstants.C_LENGTH_MSISDN) );
            l_tmpErrorLine.append(
                (p_oldMsisdn + BatchConstants.C_EMPTY_MSISDN).substring(0, BatchConstants.C_LENGTH_MSISDN) );
        }
        
        return l_tmpErrorLine.toString();
        
    } // End createErrorLine()


    //--------------------------------------------------------------------------
    //--  Private methods.                                                    --
    //--------------------------------------------------------------------------

    /**
     * Prints trace print outs.
     * 
     * @param p_method         the name of th calling method.
     * @param p_callingObject  the calling <code>Object</code>.
     * @param p_message        the trace message to be printed.
     */
    private static void trace(Object p_callingObject, String p_method, String p_message)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_ALL,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            c_traceStatementNumber++,
                            p_callingObject,
                            "TRACE: " + p_method + " -- " + p_message);
        }
        
        return;
        
    } // End of trace(...)

    
    
    

    
    //--------------------------------------------------------------------------
    //--  Inner classes.                                                      --
    //--------------------------------------------------------------------------

    /**
     * The purpose of this inner class, <code>IsProcessor</code>, is to process the 
     * <code>NPChBatchRecordData</code> records stored in the chunk buffer passed in to the
     * Constructor.
     */
    private class IsProcessor extends ThreadObject
    {
        //----------------------------------------------------------------------
        //--  Class level constant.                                           --
        //----------------------------------------------------------------------

        /** Class name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_INNER_CLASS_NAME = "NPChBatchProcessor.IsProcessor";

        //----------------------------------------------------------------------
        //--  Instance variables.                                             --
        //----------------------------------------------------------------------

        /** The current thread id. */
        private int                     i_threadId     = 0;

        /** The <code>IS API</code> reference. */
        private PpasNumberChangeService i_isApi        = null;

        /** The incoming chunk buffer. */
        private Vector                  i_chunkBuffer  = null;
        
        /** The IS API output buffer. */
        private ArrayList               i_outputBuffer = null;


        //----------------------------------------------------------------------
        //--  Constructors.                                                   --
        //----------------------------------------------------------------------

        /**
         * Constructs an <code>IsProcessor</code> instance using the given chunk buffer.
         * 
         * @param p_threadId     a unique thread id.
         * @param p_chunkBuffer  a <code>Vector</code> that contains a number of 
         *                       <code>NPChBatchRecordData</code> records to be processed.
         */
        public IsProcessor(int p_threadId, Vector p_chunkBuffer)
        {
            super();

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_START,
                                C_INNER_CLASS_NAME,
                                11000,
                                this,
                                BatchConstants.C_CONSTRUCTING);
            }

            i_threadId = p_threadId;
            i_isApi    = new PpasNumberChangeService(null, 
                                                     NPChBatchProcessor.super.i_logger,
                                                     NPChBatchProcessor.super.i_ppasContext);
            this.i_chunkBuffer = p_chunkBuffer;
            i_outputBuffer     = new ArrayList(p_chunkBuffer.size());

            trace(this, "Constructor", getThreadName() + ": is constructed.");

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_END,
                                C_INNER_CLASS_NAME,
                                11000,
                                this,
                                BatchConstants.C_CONSTRUCTED);
            }
            
            return;
            
        } // End constructor, IsProcessor(..)


        
        
        
        //----------------------------------------------------------------------
        //--  Public methods.                                                 --
        //----------------------------------------------------------------------

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_doRun = "doRun";        
        /**
         * This method is called from the active <code>Thread</code> in the super class.
         */
        public void doRun()
        {
            StringBuffer               l_tmpOutputLine           = new StringBuffer();
            NPChBatchRecordData        l_record                  = null;
            NumberChangeData           l_routingData             = null; // The IS API input data object.
            BatchResponseData          l_responseData            = null; // The IS API response data object.
            Collection                 l_responseCollection      = null;
            BatchElementResponseData[] l_batchElemResponsDataArr = null;
            PpasServiceException       l_ppasServiceException    = null;
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_INNER_CLASS_NAME,
                                11020,
                                this,
                                "** DORUN ** " + BatchConstants.C_ENTERING + C_METHOD_doRun);
            }

            trace(this, C_METHOD_doRun, 
                  getThreadName() + ": Scan through passed i_chunkBuffer, size = " + i_chunkBuffer.size());

            for (int i = 0; i < this.i_chunkBuffer.size(); i++)
            {
                l_record = (NPChBatchRecordData)this.i_chunkBuffer.elementAt(i);
                
                l_routingData = new NumberChangeData(l_record.getParsedOldMsisdn(),
                                                     l_record.getParsedNewMsisdn());
                l_routingData.setSdpId(l_record.getSdpId());
                
                this.i_outputBuffer.add(l_routingData);
            }
            
            try
            {

                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_MODERATE,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10022,
                        this,
                        "== START DO PROCESSING ==");
                }
                
                trace(this, C_METHOD_doRun, getThreadName() + ": Call the IS API, timeout = " +
                      NPChBatchProcessor.super.i_isApiTimeout + " ms");

                l_responseData = i_isApi.changeNumberPlan(null,
                                                        NPChBatchProcessor.super.i_isApiTimeout,
                                                        i_outputBuffer);                
            }

            catch (PpasServiceException p_psExe)
            {
                trace(this, C_METHOD_doRun,
                      getThreadName() + ": A PpasServiceException is caught: " + p_psExe.getMessage() + 
                      ",  key = " + p_psExe.getMsgKey());

                // Any 'PpasServiceException' thrown directly by the IS API is treated as a fatal error,
                // i.e. the 'PpasServiceException' is logged and the error line will be set for all
                // 'BatchRecordData' objects in the 'i_chunkBuffer'.
                
                // Set the error line for all 'BatchRecordData' records in the 'i_chunkBuffer' and put them
                // into the output buffer.
                for (int i = 0; i < this.i_chunkBuffer.size(); i++)
                {
                    l_record = (NPChBatchRecordData)this.i_chunkBuffer.elementAt(i);
                    this.mapException(l_record, p_psExe);
                    l_record.setRecoveryLine(createRecoveryLine(l_record.getRowNumber(),
                                                                l_record.getOldMsisdn(),
                                                                BatchConstants.C_NOT_PROCESSED));
                    
                    // Store the 'BatchRecordData' in the output queue.
                    NPChBatchProcessor.super.storeRecord((BatchRecordData)this.i_chunkBuffer.elementAt(i));
                }

                // Finish up the current thread.
                trace(this, C_METHOD_doRun, getThreadName() + ": Finish up the current thread.");
                
                // Remove this instance from the 'living IsProcess Thread Vector'.
                NPChBatchProcessor.this.i_livingIsProcessThreadVec.remove(this);
                
                // Notify any waiting thread.
                synchronized (i_signalObj)
                {
                    NPChBatchProcessor.this.i_signalObj.notify();
                }

                // Return from this method.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_END,
                                    C_INNER_CLASS_NAME,
                                    11130,
                                    this,
                                    BatchConstants.C_LEAVING + C_METHOD_doRun + 
                                    ", since a 'PpasServiceException' was caught.");
                }
                return;
            }

            trace(this, C_METHOD_doRun, "Response from IS API received, all success = " + 
                                        l_responseData.getAllSuccess());
            if (l_responseData.getAllSuccess())
            {
                // Store all the processed records directly in the output queue.
                for (int i = 0; i < this.i_chunkBuffer.size(); i++)
                {
                    NPChBatchRecordData l_tmpRecord = (NPChBatchRecordData)this.i_chunkBuffer.elementAt(i);

                    // Create recovery line information
                    l_tmpRecord.setRecoveryLine(createRecoveryLine(l_tmpRecord.getRowNumber(),
                                                                   l_tmpRecord.getOldMsisdn(),
                                                                   BatchConstants.C_SUCCESSFULLY_PROCESSED));
                    
                    // Create output file information for the SDP
                    l_tmpOutputLine = new StringBuffer();
                    l_tmpOutputLine.append(BatchConstants.C_RECORD_TYPE_NUMBER_PLAN_CHANGE_DETAIL_RECORD);
                    l_tmpOutputLine.append(BatchConstants.C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2);
                    l_tmpOutputLine.append(l_tmpRecord.getOldMsisdn());
                    l_tmpOutputLine.append(BatchConstants.C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2);
                    l_tmpOutputLine.append(l_tmpRecord.getNewMsisdn());
                    l_tmpRecord.setOutputLine(l_tmpOutputLine.toString());

                    NPChBatchProcessor.super.storeRecord((BatchRecordData)this.i_chunkBuffer.elementAt(i));
                }
            }
            else
            {
                // Scan through the responses in order to update the corresponding BatchRecordData's error
                // line for an erroneous request before storing it into the output queue.
                l_responseCollection = l_responseData.getData();
                l_batchElemResponsDataArr = new BatchElementResponseData[ l_responseCollection.size() ];
                l_responseCollection.toArray(l_batchElemResponsDataArr);
                trace(this, C_METHOD_doRun,
                      "l_batchElemResponsDataArr.length = " + l_batchElemResponsDataArr.length); 
                for (int i = 0; i < l_batchElemResponsDataArr.length; i++)
                {
                    // Get the current 'BatchRecordData' object.
                    NPChBatchRecordData l_tmpRecord = (NPChBatchRecordData)this.i_chunkBuffer.elementAt(i);
                    trace(this, C_METHOD_doRun,
                          "**DORUN** before checking exception line i=" + i); 

                    // Check if any Exception was raised during the process.
                    l_ppasServiceException = l_batchElemResponsDataArr[i].getException();
                    if (l_ppasServiceException != null)
                    {
                        trace(this, C_METHOD_doRun,
                              "**DORUN** there is an exception!! i=" + i); 

                        // The current request failed, update the corresponding BatchRecordData's error line.
                        this.mapException(l_tmpRecord, l_ppasServiceException);
                        
                         // Create recovery line information
                        l_tmpRecord.setRecoveryLine(createRecoveryLine(l_tmpRecord.getRowNumber(),
                                                                       l_tmpRecord.getOldMsisdn(),
                                                                       BatchConstants.C_NOT_PROCESSED));
                    }
                    else
                    {
                        trace(this, C_METHOD_doRun,
                              "**DORUN** no exception - *SUCCESS* before create recovery i=" + i); 

                        // Create recovery line information
                        l_tmpRecord.setRecoveryLine(createRecoveryLine(l_tmpRecord.getRowNumber(),
                                                                       l_tmpRecord.getOldMsisdn(),
                                                                       BatchConstants.C_SUCCESSFULLY_PROCESSED));
                        trace(this, C_METHOD_doRun,
                              "**DORUN** no exception - *SUCCESS* before create output i=" + i); 
                        
                        // Create output file information for the SDP
                        l_tmpRecord.setOutputLine(createOutputInformation(BatchConstants.C_RECORD_TYPE_NUMBER_PLAN_CHANGE_DETAIL_RECORD,
                                                                          l_tmpRecord.getOldMsisdn(),
                                                                          l_tmpRecord.getNewMsisdn()));
                    }
                    trace(this, C_METHOD_doRun,
                          "**DORUN** store record " + l_tmpRecord.dumpRecord()); 

                    // Store the 'BatchRecordData' object into the output buffer.
                    NPChBatchProcessor.super.storeRecord(l_tmpRecord);
                }
            }
            
            // Finish up the current thread.
            trace(this, C_METHOD_doRun, getThreadName() + ": Finish up the current thread.");
            // Remove this instance from the 'living IsProcess Thread Vector'.
            NPChBatchProcessor.this.i_livingIsProcessThreadVec.remove(this);
            
            // Notify any waiting thread.
            synchronized (i_signalObj)
            {
                NPChBatchProcessor.this.i_signalObj.notify();
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_END,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                "** DORUN ** " + BatchConstants.C_LEAVING + C_METHOD_doRun);
            }
            
            return;
        } // End of doRun()


        
        
        
        //----------------------------------------------------------------------
        //--  Protected methods.                                              --
        //----------------------------------------------------------------------

        /**
         * Returns the name of this thread.
         * 
         * @return  the name of this thread.
         */
        protected String getThreadName()
        {
            return "IsProcessor: Thread-" + i_threadId;
        }
        


        //----------------------------------------------------------------------
        //--  Private methods.                                                --
        //----------------------------------------------------------------------

        /** Method name constant used in calls to middleware.  Value is {@value}. */
        private static final String C_METHOD_mapException = "mapException";        
        /**
         * Updates the passed <code>BatchRecordData</code> object's error line with an error code that 
         * corresponds to the passed <code>PpasServiceException</code>'s message key.
         * 
         * @param p_record      the <code>BatchRecordData</code>.
         * @param p_ppasServEx  the <code>PpasServiceException</code>.
         */
        private void mapException(BatchRecordData p_record, PpasServiceException p_ppasServEx)
        {
            StringBuffer l_tmpErrorLine      = null;
            boolean      l_mappingDone       = false;
            String[][]   l_errorCodeMappings = null;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                BatchConstants.C_ENTERING + C_METHOD_mapException);
            }
            
            // Set-up the message key - error code mapping array.
            l_errorCodeMappings = new String[][]{
                // Exceptions included in the 'BatchElementResponseData' object.
//              {PpasServiceMsg.C_KEY_SERVICE_RESPONSE_TIMEOUT,       C_ERROR_CODE_SERVICE_RESPONSE_TIMEOUT},                
//              {PpasServiceMsg.C_KEY_RESOURCE_UNAVAILABLE,           C_ERROR_CODE_INVALID_RECORD},
//              {PpasServiceMsg.C_KEY_ACCOUNT_NOT_EXISTS,             C_ERROR_CODE_INVALID_RECORD},
                {PpasServiceMsg.C_KEY_NUMBER_PLAN_CHANGE_IN_PROGRESS, C_ERROR_CODE_NUMBER_PLAN_CHANGE_INPROGRESS},                
                {PpasServiceMsg.C_KEY_MSISDN_NOT_AVAILABLE,           C_ERROR_INVALID_RECORD},
                {PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS,              C_ERROR_INVALID_RECORD},
                {PpasServiceMsg.C_KEY_DISCONNECTED_ACCOUNT,           C_ERROR_INVALID_RECORD},
                {PpasServiceMsg.C_KEY_TRANSIENT_ACCOUNT,              C_ERROR_INVALID_RECORD},
                {PpasServiceMsg.C_KEY_INPUT_CUSTID_AND_MSISDN_BLANK,  C_ERROR_INVALID_RECORD},
                {PpasServiceMsg.C_KEY_ACCOUNT_NOT_ACTIVE,             C_ERROR_INVALID_RECORD},
                {PpasServiceMsg.C_KEY_SUBSCRIBER_ALREADY_ASSIGNED,    C_ERROR_CODE_NUMBER_PLAN_CHANGE_INPROGRESS},
                {PpasServiceMsg.C_KEY_TRANSIENT_ACCOUNT,              C_ERROR_INVALID_RECORD},
                {PpasServiceMsg.C_KEY_MSISDN_ROUTING_ASSIGNED,        C_ERROR_CODE_NUMBER_PLAN_CHANGE_INPROGRESS},
                {PpasServiceMsg.C_KEY_MSISDN_NOT_IN_BLOCK_RANGE,      C_ERROR_INVALID_RECORD}
            };
                            
            trace(this, C_METHOD_mapException, "l_errorCodeMappings.length = " + l_errorCodeMappings.length);
            // Look for matching mapping through the whole error code mapping array or until a matching
            // mapping is done.
            for (int i = 0; (i < l_errorCodeMappings.length  &&  !l_mappingDone); i++)
            {
                if (p_ppasServEx.getMsgKey().equals(l_errorCodeMappings[i][0]))
                {
                    // Get the error line left justified
                    l_tmpErrorLine = new StringBuffer( createErrorLine( l_errorCodeMappings[i][1],
                                                                        ((NPChBatchRecordData)p_record).getOldMsisdn(),
                                                                        ((NPChBatchRecordData)p_record).getNewMsisdn(),
                                                                        p_record.getInputLine()) );

                    l_mappingDone = true;
                    trace(this, C_METHOD_mapException,
                          "matching mapping found, key = " + l_errorCodeMappings[i][0] +
                          ",  error code = " + l_errorCodeMappings[i][1]);
                }
            }
            if (!l_mappingDone)
            {
                // None of the expected mapping matches the given message key.
                trace(this, "", "An unexpected PpasServiceException is detected: " + p_ppasServEx);
                l_tmpErrorLine = new StringBuffer(" (" + p_ppasServEx.getMsgKey() + ")");
                l_tmpErrorLine.append(p_record.getInputLine());
            }

            p_record.setErrorLine(l_tmpErrorLine.toString());

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_END,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                BatchConstants.C_LEAVING + C_METHOD_mapException);
            }
            return;
        } // End of mapException(..)
        
        
        

        
        /**
         * Creates a string with output information, such as: recordType,oldMsisdn,newMsisdn.
         * @param p_recordType  the record type.
         * @param p_oldMsisdn   the old MSISDN.
         * @param p_newMsisdn   the new MSISDN.
         * @return Information to be written to the output file.
         */
        private String createOutputInformation( int    p_recordType,
                                                String p_oldMsisdn,
                                                String p_newMsisdn )  
        {
            StringBuffer l_outputInformation = new StringBuffer();
            
            l_outputInformation.append(p_recordType);
            l_outputInformation.append(BatchConstants.C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2);
            l_outputInformation.append(p_oldMsisdn);
            l_outputInformation.append(BatchConstants.C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2);
            l_outputInformation.append(p_newMsisdn);
            
            return l_outputInformation.toString();
        } // End createOutputInformation(...)
        

        
        
    } // End of inner class - IsProcessor
   
}
//End of public class NPChBatchProcessor
////////////////////////////////////////////////////////////////////////////////
//                End of file
////////////////////////////////////////////////////////////////////////////////}
