////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME :    RefillBatchProcessorUT.java
//      DATE      :    17-July-2006
//      AUTHOR    :    Yang Liu
//      REFERENCE :    PRD_ASCS00_GEN_CA_93
//
//      COPYRIGHT :    WM-data 2006
//
//      DESCRIPTION :
//
/////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
/////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME       | DESCRIPTION                           | REFERENCE
//----------+------------+---------------------------------------+---------------
// 07/08/06 | M Erskine  | Formatting and debugging.             | PpacLon#2479/9483
// //////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.util.structures.SizedQueue;
import com.slb.sema.ppas.util.structures.SizedQueueInvalidParameterException;

/**
 * Unit Test for the <code>RefillBatchProcessor</code> class.
 */
public class RefillBatchProcessorUT extends BatchTestCaseTT
{
    // ------------------------------------------------------------------------
    // Private constants
    // ------------------------------------------------------------------------
    /** Class name. Value is {@value}. */
    private static final String C_CLASS_NAME = "RefillBatchProcessorUT";

    // ------------------------------------------------------------------------
    // Constructors
    // ------------------------------------------------------------------------
    /**
     * Standard constructor specifying the name of the test.
     * @param p_title Name of test.
     */
    public RefillBatchProcessorUT(String p_title)
    {
        super(p_title, "batch_ref");
    }

    // ------------------------------------------------------------------------
    // Public instance methods
    // ------------------------------------------------------------------------

    /** Constant holding the name of this method. Value is (@value). */
    private static final String C_METHOD_testConstructor = "testConstructor";

    /**
     * Test to construct a <code>RefillBatchProcessor</code> instance.
     */
    public void testConstructor()
    {
        RefillBatchProcessor l_refillBatchProcessor = null;
        SizedQueue l_inQueue = null;
        SizedQueue l_outQueue = null;

        try
        {
            l_inQueue = new SizedQueue("RefillBatchProcessUT indata queue", 10, null);
            l_outQueue = new SizedQueue("RefillBatchProcessUT output queue", 10, null);
        }
        catch (SizedQueueInvalidParameterException p_sqInvParamExe)
        {
            p_sqInvParamExe = null;
            super.fail("Failed to create either the indata queue or the output queue or both.");
        }

        l_refillBatchProcessor = new RefillBatchProcessor(c_ppasContext,
                                                          c_logger,
                                                          null,
                                                          l_inQueue,
                                                          l_outQueue,
                                                          null,
                                                          c_properties);

        assertNotNull("Failed to create a RefillBatchProcessor instance.", l_refillBatchProcessor);

        say(C_CLASS_NAME + C_METHOD_testConstructor + "-- Completed.");
    }

    // ------------------------------------------------------------------------
    // Public static methods
    // ------------------------------------------------------------------------
    /**
     * Define test suite. This unit test uses a standard JUnit method to derive a list of test cases from the
     * class.
     * @return A <code>Test</code> that JUnit can run to thoroughly test this class.
     */
    public static Test suite()
    {
        return new TestSuite(RefillBatchProcessorUT.class);
    }

    /**
     * Allow test suite to be invoked from the command line.
     * @param p_args These are not used.
     */
    public static void main(String[] p_args)
    {
        p_args = null;
        junit.textui.TestRunner.run(suite());
    }

} // End of public class RefillBatchProcessorUT

