////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SubInstBatchControllerUT.java 
//      DATE            :       18-May-2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :       Unit test for Subscriber Install Batch.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// 03/05/07 | Lars Lundberg | One test method is removed:      | PpacLon#3033/11433
//          |               | testIsFileNameValid()            |
//          |               | One test method is modified:     |
//          |               | testAddControlInformation()      |
//----------+---------------+----------------------------------+-------------------
// 03/05/07 | M. Toernqvist | Rewrite the UT program, using the| PpacLon#3038/11189
//          |               | the end-to-end concept           |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.exceptions.BatchServiceException;
import com.slb.sema.ppas.common.exceptions.PpasException;
import com.slb.sema.ppas.common.sql.JdbcResultSet;
import com.slb.sema.ppas.common.sql.PpasSqlException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.batch.batchcommon.BatchTestDataTT;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/**
 * Class to unit test the Subscriber Install Status Change batch.
 */
/** Unit tests the Miscellaneous DataUpload batch controller. */
public class SubInstBatchControllerUT extends BatchTestCaseTT
{
    // =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                = "SubInstBatchControllerUT";

    /** The name of the Status Change batch. */
    private static final String C_SUB_INST_BATCH_NAME       = BatchConstants.C_JOB_TYPE_BATCH_INSTALLATION;

    /** The name of the test data template file to be used for the succesful insert test case. */
    private static final String C_FILENAME_SUCCESSFUL       = "SUBINST_SUCCESSFUL.DAT";

    /** Starting index for routing MSISDNs. */
    private static int          i_msisdnIndex;
    
    /** Save all created master MSISDNs. */
    private Vector              i_masterMsisdns             = new Vector();

    
    // =========================================================================
    // == Constructor(s).                                                     ==
    // =========================================================================
    /**
     * Constructs a <code>SubInstBatchControllerUT</code> instance to be used for
     * unit tests of the <code>SubInstBatchController</code> class.
     * 
     * @param p_name  the name of the current test.
     */
    public SubInstBatchControllerUT(String p_name)
    {
        super(p_name);
    }


    // =========================================================================
    // == Public unit test method(s).                                         ==
    // =========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testSubInstBatch_Successful = "testSubInstBatch_Successful";
    
    /**
     * Performs a fully functional test of a successful 'subscriber install'.
     * A test file with a valid name containing 14 valid install subscriber records is
     * created and placed in the batch indata directory. The install subscriber
     * batch is started in order to process the test file and update the CUST_MAST, CUST_COMMENTS table
     *
     * @ut.when        A test file containing 14 valid install subscriber record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        First 20 new record will be inserted in the MSIS_MSISDN table, with status 'AV',
     *                 to simulate that the routing provisioning batch have been run first.
     *                 The Subscriber install batch will update MSIS_MISISDN status to 'AS' for the
     *                 installed subscriber's MSISDNs.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There wouldn't be any recovery file.
     *                 A report file will be created which will contain only one
     *                 success trailing record.
     *                 The test file will be renamed with a new extension, DNE. 
     * 
     * @ut.attributes  +f
     */
    public void testSubInstBatch_Successful()
    {
    	final int      L_NO_SUCCESSFUL_RECORDS = 14;
    	final int      L_NO_ERRORNOUS_RECORDS  = 0;
        final String[] L_REPORT_ROWS           = {getTrailerRecord(L_NO_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_ROWS         = {};

        beginOfTest(C_METHOD_testSubInstBatch_Successful);

        SubInstTestDataTT l_testDataTT =
            new SubInstTestDataTT( CommonTestCaseTT.c_ppasRequest,
                                        UtilTestCaseTT.c_logger,
                                        CommonTestCaseTT.c_ppasContext,
                                        C_SUB_INST_BATCH_NAME,
                                        c_batchInputDataFileDir,
                                        C_FILENAME_SUCCESSFUL,
                                        JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                        BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                        L_NO_SUCCESSFUL_RECORDS,
                                        L_NO_ERRORNOUS_RECORDS );

        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows(L_RECOVERY_ROWS );

        // Starting index for MSISDNs to be installed. 
        i_msisdnIndex = 0;

        completeFileDrivenBatchTestCase( l_testDataTT );

        endOfTest();
    }


    



    // =========================================================================
    // == Public class method(s).                                             ==
    // =========================================================================
    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite( SubInstBatchControllerUT.class );
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println( "Parameters are: " + p_args );
        junit.textui.TestRunner.run( suite() );
    }
    

    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /**
     * Returns an instance of the <code>SubInstBatchController</code> class.
     * 
     * @param p_batchTestDataTT  the current batch test data object, which in this case should be
     *                           an instance of the <code>SubInstTestDataTT</code> class.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController( BatchTestDataTT p_batchTestDataTT )
    {
        SubInstTestDataTT l_testDataTT = (SubInstTestDataTT)p_batchTestDataTT;
        
        return createSubInstBatchController( l_testDataTT.getTestFilename() );
    }


    /**
     * Returns the required additional properties layers for the status change batch.
     * 
     * @return the required additional properties layers for the misc data upload batch.
     */
    protected String getPropertiesLayers()
    {
        return "batch_sta";
    }


    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();

        // Set number of processor threads to one. This is needed to ensure that the data rows are
        // processed in the same order as they are written in the input data file,
        // i.e. to avoid race conditions.
        c_ppasContext.getProperties().setProperty( BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS, "1" );
    }

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }


    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Returns an instance of the <code>SubInstBatchController</code> class.
     * 
     * @param p_testFilename  the name of the input data test file.
     * 
     * @return an instance of the <code>SubInstBatchController</code> class.
     */
    private SubInstBatchController createSubInstBatchController( String p_testFilename )
    {
        SubInstBatchController l_statusChangeBatchController = null;
        HashMap                l_batchParams                 = null;

        l_batchParams = new HashMap();
        l_batchParams.put( BatchConstants.C_KEY_INPUT_FILENAME, p_testFilename );
        
        try
        {
            l_statusChangeBatchController =
                new SubInstBatchController( CommonTestCaseTT.c_ppasContext,
                                            BatchConstants.C_JOB_TYPE_BATCH_STATUS_CHANGE,
                                            getJsJobID(BatchTestCaseTT.C_BATCH_CONTROL_TABLE_PREFIX),
                                            C_CLASS_NAME,
                                            l_batchParams );
        }
        catch (Exception l_Ex)
        {
            sayTime( "***ERROR: Failed to create a 'SubInstBatchController' instance!" );
            super.failedTestException( l_Ex );
        }

        return l_statusChangeBatchController;
    }


    //==========================================================================
    // Inner class(es).
    //==========================================================================
    /**
     * This class handles the "data" part of the UT test.
     */
    private class SubInstTestDataTT extends BatchTestDataTT
    {
        //======================================================================
        // Private constants(s).
        //======================================================================
        /** The test data filename prefix. */
        private static final String C_PREFIX_TEST_DATA_FILENAME = "INSTALL";

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_MSISDN  = "(<MSISDN>)";

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_MASTER  = "(<MASTER>)";

        /** The regular expression used to find all trailing information at the input line. */
        private static final String C_REGEXP_DATA_ANYTHING      = "(.*)";

        /** The regular expression used to find the lines that shall install a subordinate subscriber. */ 
        private static final String C_REGEXP_DATA_TAG_MASTER    = C_TEMPLATE_DATA_TAG_MSISDN + 
                                                                  C_TEMPLATE_DATA_TAG_MASTER + 
                                                                  C_REGEXP_DATA_ANYTHING;
        
        /** The regular expression used to find all lines that will install a master subscriber. */ 
        private static final String C_REGEXP_DATA_TAG_MSISDN    = C_TEMPLATE_DATA_TAG_MSISDN +
                                                                  "(" +C_REGEXP_DATA_ANYTHING + ")";    

        /** The regular expression that will find all legal lines, both defining master and subordates. */
        private static final String C_REGEXP_DATA_TAG_PATTERN   = "^" + C_REGEXP_DATA_TAG_MASTER +
                                                                  "|" + C_REGEXP_DATA_TAG_MSISDN + "$";
        
        /** SQL to perform delete the first installed MSISDN during 'database setup' before test start. */
        private static final String C_DELETE_MSIS_MSISDN        =     
            "DELETE FROM msis_msisdn WHERE msis_mobile_number = {0}";

        /** SQL to simulate Routing provisioning. */
        private static final String C_INSERT_MSIS_MSISDN        =
            "INSERT INTO msis_msisdn (msis_status, msis_mobile_number, msis_sdp_id) values ('AV', {0}, '00')";

        private static final int    C_NO_SUCCESSFULlY_INSTALLED_RECORDS = 14;


        // =====================================================================
        //  Private attribute(s).
        // =====================================================================
        /** The name of the template test file. */
        private String  i_templateTestFilename = null;

        /** The 'MSISDN' template data tag <code>Pattern</code> object. */
        private Pattern i_msisdnDataTagPattern = null;

        // =====================================================================
        //  Constructor(s).
        // =====================================================================
        /**
         * Constructs an <code>SubInstTestDataTT</code> using the given parameters.
         * 
         * @param p_ppasRequest  The <code>PpasRequest</code> object.
         * @param p_logger       The <code>Logger</code> object.
         * @param p_ppasContext  The <code>PpasContext</code> object.
         */
        private SubInstTestDataTT( PpasRequest p_ppasRequest,
                                   Logger      p_logger,
                                   PpasContext p_ppasContext,
                                   String      p_batchName,
                                   File        p_batchInputFileDir,
                                   String      p_templateTestFilename,
                                   int         p_expectedJobExitStatus,
                                   char        p_expectedBatchJobControlStatus,
                                   int         p_expectedNoOfSuccessRecords,
                                   int         p_expectedNoOfFailedRecords )
       {
            super( p_ppasRequest,
                   p_logger,
                   p_ppasContext,
                   p_batchName,
                   p_batchInputFileDir,
                   p_expectedJobExitStatus,
                   p_expectedBatchJobControlStatus,
                   p_expectedNoOfSuccessRecords,
                   p_expectedNoOfFailedRecords );

            i_templateTestFilename  = p_templateTestFilename;

            // Create 'Pattern' object for template data tags.
            i_msisdnDataTagPattern = Pattern.compile( C_REGEXP_DATA_TAG_PATTERN );
        }


        //==========================================================================
        // Protected method(s).
        //==========================================================================        

        /**
         * Creates a test file.
         * @throws PpasServiceException  if it fails to create a test file.
         */
        protected void createTestFile() throws PpasServiceException, IOException
        {
            super.createTestFile( C_PREFIX_TEST_DATA_FILENAME,
                                  BatchTestDataTT.C_SUFFIX_ORDINARY_TEST_DATA_FILENAME,
                                  i_templateTestFilename,
                                  C_CLASS_NAME );
        }


        /**
         * Replaces any found data tag in the given data string by real data and returns the resulting string.
         * 
         * @param p_dataStr  the data string.
         * 
         * @return  the resulting string after data tags have been replaced by real data.
         */
        protected String replaceDataTags( String p_dataRow )
        {
            final String L_EMPTY_MSISDN                = "               ";

            Matcher      l_matcher                      = null;
            String       l_tmpMsisdn                    = null;
            
            String       l_tmpRow                       = p_dataRow;
            StringBuffer l_tmp                          = new StringBuffer(67);
            
            
            if (p_dataRow != null)
            {
                l_matcher   = i_msisdnDataTagPattern.matcher( l_tmpRow );
                
                if (l_matcher.matches())
                {
//                	for (int i= 0; i < l_matcher.groupCount(); i++)
//                	{
//                		say("i: " + i + " :" + l_matcher.group(i));
//                	}
                	
                	for( int i= 0; i < l_matcher.groupCount(); i++ )
                	{
                	    if ( l_matcher.group(i) == null )
                	    {
                	    	continue;
                	    }
                	    
                	    
                        switch (i)
                        {
                            case 0: //Row;                            	                       
//                            	    say("### Input lineRow : ]" + l_matcher.group(i) + "[");
                                    break;
                                
                            case 1: //MSISDN
                                    l_tmpMsisdn  = L_EMPTY_MSISDN+(String)getSubscriberData( i_msisdnIndex++ );
                                    l_tmp.append(l_tmpMsisdn.substring(l_tmpMsisdn.length()-15) );
                                    break;
                                
                            case 2: //MASTER
                            	    int l_noMasters = i_masterMsisdns.size();
                            	    l_tmpMsisdn  = L_EMPTY_MSISDN+(String)i_masterMsisdns.get(l_noMasters-1);
                                    l_tmp.append(l_tmpMsisdn.substring(l_tmpMsisdn.length()-15) );
                                    break;
                                
                            case 3: //.. rest of line
                                    l_tmp.append(l_matcher.group(i));
                                    break;

                            case 4: //MSISDN - master/standalone
                                    l_tmpMsisdn  = L_EMPTY_MSISDN+(String)getSubscriberData( i_msisdnIndex++ );
                                    l_tmp.append(l_tmpMsisdn.substring(l_tmpMsisdn.length()-15) );
                        			i_masterMsisdns.add(l_tmpMsisdn);
                                    break;

                            case 5: //.. rest of line
                                    l_tmp.append(l_matcher.group(i));
                                    break;

                            default: say(" DEFAULT: "+i+" :]" + l_matcher.group(i) + "[");
                                     break;
                        }
                	
                	}
                }
                else
                {
                	say(" *** no match *** ");
                }
            }

            return l_tmp.toString();
        }


        /**
         * Verifies that the ASCS database has been properly updated.
         * 
         * @throws PpasServiceException if it fails to get the ASCS database info.
         */
        protected void verifyAscsDatabase() throws PpasServiceException
        {
        	say("\n*** start verify database ***");
        	final String L_TABLE_CUST_MAST                    = "select CUST_ID,CUST_STATUS from CUST_MAST where CUST_MOBILE_NUMBER";
        	final String L_TABLE_MSIS_MSISDN                  = "MSIS_MSISDN where MSIS_MOBILE_NUMBER";
        	final String L_TABLE_CUMS_CUSTOMER_MSISDN         = "CUMS_CUSTOMER_MSISDN where CUMS_CUST_ID";
        	final String L_TABLE_CUST_COMMENTS                = "CUST_COMMENTS where CUST_ID";
        	final String L_TABLE_CULS_CUST_LIFECYCLE_SNAPSHOT = "CULS_CUST_LIFECYCLE_SNAPSHOT where CULS_CUST_ID";
        	
            String       l_msisdn       = null;
            StringBuffer l_tmpSelect    = null;
            String       l_sql          = null;
            int          l_last         = 0;
            StringBuffer l_whereMsisdns = new StringBuffer(" in (");
            StringBuffer l_whereCustIds = new StringBuffer(" in (");
            int          l_noCustIds    = 0;
            


            // Construct where clause for all installed MSISDNs
            for ( int i = 0; i < getNumberOfTestSubscribers(); i++ )
            {
            	l_msisdn = ((String)getSubscriberData(i)).trim();
                l_whereMsisdns.append("'");
                l_whereMsisdns.append(l_msisdn);
                l_whereMsisdns.append("',");
            }
            l_last         = l_whereMsisdns.length();
            l_whereMsisdns = l_whereMsisdns.replace(l_last-1, l_last, ")");
            
            // Select from CUST_MAST - to retrieve CUST_ID and count records
            // and create a where clause with custIds
            l_tmpSelect = new StringBuffer(L_TABLE_CUST_MAST);
            l_tmpSelect.append(l_whereMsisdns);
            
            l_sql = l_tmpSelect.toString();
            try
            {
                JdbcResultSet l_resultSet = sqlQuery(new SqlString(200,0,l_sql));
                while ( l_resultSet.next(100))
                {
                	String l_tmpCustId = l_resultSet.getString(101, "CUST_ID");
                    String l_status    = l_resultSet.getString(102, "CUST_STATUS");
                    assertEquals("Unexpected status found in CUST_MAST", l_status, "A");
                    l_noCustIds++;
                    l_whereCustIds.append("'");
                    l_whereCustIds.append(l_tmpCustId);
                    l_whereCustIds.append("',");
                }
                l_last = l_whereCustIds.length();
                l_whereCustIds = l_whereCustIds.replace(l_last-1, l_last, ")");
                assertTrue("CUST_MAST had " + l_noCustIds + " expected was " + C_NO_SUCCESSFULlY_INSTALLED_RECORDS,
                		   l_noCustIds == C_NO_SUCCESSFULlY_INSTALLED_RECORDS);
            }
            catch (Exception l_Ex)
            {
            	say("exeption verifyAscsDatabase:"+l_Ex.getMessage());
            }

            // Check MSISDN
            checkNumberOfRecords( L_TABLE_MSIS_MSISDN, 
            		              l_whereMsisdns, 
            		              C_NO_SUCCESSFULlY_INSTALLED_RECORDS );
            
            // Check CUMS_CUSTOMER_MSISDN
            checkNumberOfRecords( L_TABLE_CUMS_CUSTOMER_MSISDN, 
		                          l_whereCustIds, 
		                          C_NO_SUCCESSFULlY_INSTALLED_RECORDS );
         
            // Check CUST_COMMENTS
            checkNumberOfRecords( L_TABLE_CUST_COMMENTS, 
                                  l_whereCustIds, 
                                  C_NO_SUCCESSFULlY_INSTALLED_RECORDS );
            
            // Check CULS_CUST_LIFECYCLE_SNAPSHOT
            checkNumberOfRecords( L_TABLE_CULS_CUST_LIFECYCLE_SNAPSHOT, 
                                  l_whereCustIds, 
                                  C_NO_SUCCESSFULlY_INSTALLED_RECORDS );

            say("*** end verify database ***\n");
        }

        /**
         * Verifies that the report file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyReportFile() throws IOException
        {
        	int    l_noGeneratedReportRows      = 0;
    		int    l_noExpectedRowsInReportFile = 0;
    		Vector l_reportData                 = null;
    		String l_tmpTrailer                 = null;
    		
        	say("\n*** start verifyReportFile *** ");
        	
        	l_reportData = readNewFile( c_batchReportDataFileDir, 
        			                    i_testFilename, 
 			                            "DAT", 
                                        "RPT" );
        	
        	if ( i_expectedNoOfFailedRecords == 0 )
        	{
        		// SUCCESS expected!
        		// All successful - only one row (the trailing record) in the result file
        		assertEquals( "***verifyReportFile*** Wrong number of rows in the report file.",
        				      1, l_reportData.size() );
        		
                l_tmpTrailer = i_expectedReportData[0];

          		assertTrue( "***verifyReportFile*** Trailer string not as expected : ",
                            ((String)l_reportData.firstElement()).equals(l_tmpTrailer));
        	}
        	else
        	{      		
        		say("###verifyReportFile### report file has failed records.. - *** NOT IMPLEMENTED YET ***");
        		
//        		l_noGeneratedReportRows      = l_reportData.size();  // Rows with error codes and trailing record
//        		l_noExpectedRowsInReportFile = i_expectedReportData.length;
//        		say("###verifyReportFile### noGeneratedReportRows:"+l_noGeneratedReportRows+" noExp:"+l_noExpectedRowsInReportFile);
//        		assertTrue( ("Report file has wrong number of rows expected:" + l_noExpectedRowsInReportFile +
//        				    " found:" + l_noGeneratedReportRows),
//        				    l_noGeneratedReportRows==l_noExpectedRowsInReportFile );
//        		
//        		for ( int i = 0; i < l_noExpectedRowsInReportFile; i++ )
//        		{
//        			say("###verifyReportFile### i: len="+i_expectedReportData[i].length());
//        			String l_errorCode = ((String)l_reportData.get(i)).substring(0,i_expectedReportData[i].length());
//        			say("###verifyReportFile###, expected row in reportFile :]" + l_errorCode +
//        					   "[ found : ]" + i_expectedReportData[i] + "[");
//        			assertTrue( "Unexpected row in reportFile :" + l_errorCode+
//        					   " but was :" + i_expectedReportData[i], 
//        					   l_errorCode.equals(i_expectedReportData[i]));
//        		}
        	}
        	say("***verifyReportFile*** *** end verifyReportFile ***\n");
        }

        /**
         * Verifies that the recovery file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRecoveryFile() throws IOException
        {
        	Vector l_recoveryData = null;
        	String l_found        = null;
			String l_expected     = null;
			
        	say("\n*** start verifyRecoveryFile ***");
        	
        	try
        	{
            	l_recoveryData = readNewFile( c_batchRecoveryDataFileDir, 
            			                      i_testFilename, 
            			                      "DAT", 
            			                      "RCV" );
            	
            	if ( i_expectedNoOfFailedRecords == 0 )
            	{
            		// File should not exist
            		// SUCCESS expected!
            		// All successful - only one row in the result file
            		assertEquals( "No recovery record should be found.", 
            				      0, l_recoveryData.size());            		
            	}
            	else
            	{
            		say("###verifyReportFile### failure records created");
            		for ( int i = 0; i < l_recoveryData.size(); i++)
            		{
            			l_found    = (String)l_recoveryData.get(i);
            			l_expected = i_expectedRecoveryData[i];
            			assertTrue( "VerifyRecoveryFile failed, found:" + l_found +
            					    " expected:" + l_expected, 
            					    l_found.equals(l_expected));
            		}            		
            	}
        	}
        	catch (IOException i_exep)
        	{
        		say("IOEXCEPTION caught "+i_exep.getMessage());
        		if ( l_recoveryData == null )
            	{
            		// File should not exist
            		assertEquals( "No recovery record should be found.", 
            				      0, i_expectedNoOfFailedRecords);            		
            	}
        	}
        	finally
        	{
        		say("FINALLY - recovery file has been checked");
        	}
        	say("*** end verifyRecoverfyFile ***\n");
        }


        /**
         * Verifies that the input file has been renamed.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRenaming() throws IOException
        {
        	say("\n*** start verifyRenaming *** ");

        	File   l_file             = null;
        	String l_newInputFileName = constructFileName( c_batchInputDataFileDir, 
                                                           i_testFilename, 
                                                           "DAT", 
                                                           "DNE" );        	
            l_file = new File(l_newInputFileName);
            
            if (l_file.exists())
            {
                say("FILE EXIST ***" + l_file);
            }
            else
            {
            	say( "File DOES NOT EXIST : " + l_file);
            	throw new IOException();            	
            }
        	say("*** end verifyRenaming ***\n");
        }
        
        
        
        //==========================================================================
        // Private method(s).
        //==========================================================================
        private String getTestFilename()
        {
            return super.i_testFilename;
        }
        
        /**
         * Installs test subscribers.
         * @throws PpasServiceException  if it fails to install test subscribers.
         */
        protected void installTestSubscribers() throws PpasServiceException
        {           
        	final String     L_MSISDN_FILLER   = "000";
        	final int        L_FILLER_LENGTH   = L_MSISDN_FILLER.length();

           	BasicAccountData l_subscriber      = null;
           	String           l_tmpMsisdnPrefix = null;
           	String           l_tmpLastDigits   = null;
           	int              l_endingDigits    = 0;
           	String           l_firstMsisdn     = null;
           	SqlString        l_tmpDelete       = null;
           	SqlString        l_tmpInsert       = null;
           	String           l_tmpMsisdn       = null;
           	
        	// Install one subscriber - will be used to get starting #MSISDN and
           	// to test to delete an assign customer
        	l_subscriber = installLocalTestSubscriber(null);
        	addSubscriberData(l_subscriber);	
            l_firstMsisdn = getFormattedMsisdn(0);
            
            // Store the MSISDN String instead of BasicAccountData
            removeSubscriberData(0);

            // Remove this pre-installed MSISDN from from the database
            l_tmpDelete = new SqlString( 110, 1, C_DELETE_MSIS_MSISDN );
            l_tmpDelete.setStringParam( 0, l_firstMsisdn.trim() );
            
            sqlUpdate(l_tmpDelete);

    	    // Create a MSISDN prefix - exclude the last 3 digits
            l_tmpMsisdnPrefix = l_firstMsisdn.substring(0, l_firstMsisdn.length() - L_FILLER_LENGTH );
            l_tmpLastDigits   = l_firstMsisdn.substring(l_firstMsisdn.length() - L_FILLER_LENGTH );
           	
            
           	l_endingDigits = (new Integer(l_tmpLastDigits)).intValue() + 1;
            
        	// Create MSISDN strings
        	for ( int i = l_endingDigits; i < l_endingDigits+14; i++ )
        	{
        		l_tmpMsisdn = l_tmpMsisdnPrefix + adjustString( ""+i,
        	                                                    L_FILLER_LENGTH,
        	                                                    '0',
        	                                                    true);
                addSubscriberData(l_tmpMsisdn);
        		
                l_tmpInsert = new SqlString( 110, 1, C_INSERT_MSIS_MSISDN );
                l_tmpInsert.setStringParam( 0, l_tmpMsisdn.trim() );
                sqlUpdate( l_tmpInsert );
        	}        	
        } 
        
        
        private boolean checkNumberOfRecords( String       p_table, 
        		                              StringBuffer p_whereClause, 
        		                              int          p_noRecords )
        {
        	final String L_COUNT = "select count(*) from ";

        	boolean l_tableHasValidRecords = false; // Assume NOK
        	
            // Check a table by counting the records that should have been inserted.
            StringBuffer l_tmpSelect = new StringBuffer(L_COUNT);
            l_tmpSelect.append(p_table);
            l_tmpSelect.append(p_whereClause);
            
            String l_sql = l_tmpSelect.toString();

            try
            {
                JdbcResultSet l_resultSet = sqlQuery(new SqlString(200,0,l_sql));
                while ( l_resultSet.next(100))
                {
                	int l_noRecords = l_resultSet.getInt(101, 1);
                 	assertTrue("MSIS_MSISDN had " + l_noRecords +" expected was " + C_NO_SUCCESSFULlY_INSTALLED_RECORDS,
                 			   l_noRecords == C_NO_SUCCESSFULlY_INSTALLED_RECORDS);
                }
            	l_tableHasValidRecords = true;
            }
            catch (Exception l_Ex)
            {
            	say("exeption in checkNumberOfRecords ] " + l_sql + "[" + l_Ex.getMessage());
            }
            finally
            {
            	say("Table " + (p_table.split(" "))[0] + " has been verified, " + l_tableHasValidRecords);
            }
            

            return l_tableHasValidRecords;
        }
        
    } // End of inner class 'SubInstTestDataTT'.
    
    
} // End of class 'SubInstBatchControllerUT'.