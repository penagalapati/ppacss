////////////////////////////////////////////////////////////////////////////////
//ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//FILE NAME       :       PromoAllocSynchBatchReader.java
//DATE            :       Aug 27, 2004
//AUTHOR          :       Lars Lundberg
//REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//COPYRIGHT       :       Atos Origin 2004
//
//DESCRIPTION     :       Creates and stores data records representing subscriber 
//                        accounts for promotion plan allocation syncronisation.
//
////////////////////////////////////////////////////////////////////////////////
//CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//DATE     | NAME          | DESCRIPTION                      | REFERENCE
//---------+---------------+----------------------------------+-----------------
//DD/MM/YY | <name>        | <brief description of            | <reference>
//         |               | change>                          |
//---------+---------------+----------------------------------+-----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.PromoAllocSynchBatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isil.batchisilservices.PromoAllocSynchDbBatchService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This <code>PromoAllocSynchBatchReader</code> class is responsible for providing 
 * data records representing subscriber accounts for promotion plan allocation syncronisation.
 */
public class PromoAllocSynchBatchReader extends BatchReader
{
    //-----------------------------------------------------
    //  Class level constant.
    //------------------------------------------------------ 
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME             = "PromoAllocSynchBatchReader";

    /** Number of parameters that is requested by this reader instance. Value is {@value}. */
    private static final int    C_NUMBER_OF_PARAMETERS   = 2;

    /** Log message - start range parameter is missing. Value is {@value}. */
    private static final String C_START_RANGE_IS_MISSING = "Start range value (cust. id) is missing.";

    /** Log message - start range parameter is invalid. Value is {@value}. */
    private static final String C_START_RANGE_IS_INVALID = "Start range value (cust. id) is invalid, " +
                                                           "i.e. not numerical.";

    /** Log message - end range parameter is missing. Value is {@value}. */
    private static final String C_END_RANGE_IS_MISSING   = "End range value (cust. id) is missing.";

    /** Log message - start range parameter is invalid. Value is {@value}. */
    private static final String C_END_RANGE_IS_INVALID   = "End range value (cust. id) is invalid, " +
                                                           "i.e. not numerical.";


    //-------------------------------------------------------------------------
    // Instance variables.
    //-------------------------------------------------------------------------
    /** The ISIL service. */
    private PromoAllocSynchDbBatchService i_dbService = null;

    /** A string array holding the parameters required by the Db-component. */
    private String[]                      i_paramArr  = null;


    //-------------------------------------------------------------------------
    // Constructors.
    //-------------------------------------------------------------------------
    /**
     * Constructs a <code>PromoAllocSynchBatchReader</code> instance with the specified parameters.
     * 
     * @param p_ppasContext     a <code>PpasContext</code> object that holds context info for the current
     *                          process.
     * @param p_logger          the <code>Logger</code> object used by the current process.
     * @param p_controller      the <code>BatchController</code> reference.
     * @param p_inQueue         the queue into which the data records shall be stored.
     * @param p_parameters      a <code>Map</code> object that holds the necessary parameters for this batch.
     * @param p_ppasProperties  a <code>PpasProperties</code> object that holds the properties for the current
     *                          process.
     */
    public PromoAllocSynchBatchReader(PpasContext     p_ppasContext,
                                      Logger          p_logger,
                                      BatchController p_controller,
                                      SizedQueue      p_inQueue,
                                      Map             p_parameters,
                                      PpasProperties  p_ppasProperties)
    {
        super(p_ppasContext, p_logger, p_controller, p_inQueue, p_parameters, p_ppasProperties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10000,
                            this,
                            BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
        }

        i_paramArr = this.getParameters(super.i_parameters);
        this.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10010,
                            this,
                            BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }
    }


    //-------------------------------------------------------------------------
    // Protected instance methods.
    //-------------------------------------------------------------------------
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * Returns a <code>PromoAllocSynchBatchRecordData</code> object which is retrieved from the corresponding
     * batch ISIL service <code>PromoAllocSynchDbBatchService</code>.
     * 
     * @return  a <code>PromoAllocSynchBatchRecordData</code> object which is retrieved from the corresponding
     *          batch ISIL service <code>PromoAllocSynchDbBatchService</code>.
     */
    protected BatchRecordData getRecord()
    {
        BatchRecordData l_record = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10100,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_getRecord);
        }

        // get one record by calling fetch
        try
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10110,
                    this,
                    "innan i_isApi.fetch() - getRecord. " + C_METHOD_getRecord );
            }

            l_record = (PromoAllocSynchBatchRecordData)i_dbService.fetch();
            
            // Note that fetch can return null. This indicates 'no more records to fetch'.
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10120,
                    this,
                    C_METHOD_getRecord + " -- record: '" +
                    (l_record != null  ?  l_record.dumpRecord() : "null") + "'");
            }
        }
        catch (PpasServiceException p_ppasServiceExe)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10130,
                    this,
                    C_METHOD_init + " -- PpasServiceException is caught: " + p_ppasServiceExe);

//                //Print stack trace.
//                p_ppasServiceExe.printStackTrace();
            }
            
            // Report to controller and log the Exception.
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
//            i_logger.logMessage(p_ppasServiceExe);
            l_record = null;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10140,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }
        return l_record;
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";    
    /**
     * Initialises this <code>PromoAllocSynchBatchReader</code> instance.
     */
    protected void init()
    {
        int l_fetchSize = 0;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10200,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_init);
        }

        i_dbService = new PromoAllocSynchDbBatchService(null, super.i_logger, super.i_context);
        l_fetchSize = this.getFetchSize();
        try
        {
            i_dbService.open(i_paramArr, l_fetchSize);
        }
        catch (PpasServiceException p_ppasServiceExe)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10210,
                    this,
                    C_METHOD_init + " -- PpasServiceException is caught: " + p_ppasServiceExe);
            }
            
            // Report to controller and log the Exception.
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10220,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_init);
        }
    }


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_tidyUp = "tidyUp";    
    /**
     * Initialises this <code>PromoAllocSynchBatchReader</code> instance.
     */
    protected void tidyUp()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10300,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_tidyUp);
        }

        if (i_dbService != null)
        {
            try
            {
                i_dbService.close();
            }
            catch (Exception p_exe)
            {
                // Failed to close the ISIL db service.
                // Any specific handling?
                p_exe = null;
            }
            i_dbService = null;
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10310,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_tidyUp);
        }
    }


    //-------------------------------------------------------------------------
    // Private instance methods.
    //-------------------------------------------------------------------------
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getParameters = "getParameters";    
    /**
     * Returns the Promo. Plan Alloc. Synch. batch paramters as a string array.
     * The string array contains two elements: [start_custid, end_custid].
     * The parameters are retrieved from the given <code>Map</code> object.
     * 
     * The parameters are validated and a <code>PpasConfigException</code> is thrown if any paraameter is
     * missing or is set to an invalid value.
     * 
     * @param p_parameters  a <code>Map</code> object that contains the Promo. Plan Alloc. Synch. batch 
     *                      parameters.
     * 
     * @return  the Promo. Plan Alloc. Synch. batch paramters as a string array with two elements:
     *                     [start_custid, end_custid].
     */
    private String[] getParameters(Map p_parameters)
    {
        final int L_START_IX = 0;
        final int L_END_IX   = 1;

        String[] l_paramArr = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10500,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_getParameters);
        }

        l_paramArr = new String[C_NUMBER_OF_PARAMETERS];

        // 1. Get the start customer id parameter.
        l_paramArr[L_START_IX] = (String)p_parameters.get(BatchConstants.C_KEY_START_RANGE);
        if (l_paramArr[L_START_IX] == null)
        {
            // The start customer id parameter is missing.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10510,
                                this,
                                C_METHOD_getParameters + " -- Start range parameter is missing.");
            }
            super.sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage( new LoggableEvent(C_START_RANGE_IS_MISSING,
                                                   LoggableInterface.C_SEVERITY_ERROR));

            try
            {
                Integer.parseInt(l_paramArr[L_START_IX]);
            }
            catch (NumberFormatException p_nfExe)
            {
                // The string does not contain a parsable integer, i.e. the cust id value is invalid.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME,
                                    10515,
                                    this,
                                    C_METHOD_getParameters +
                                    " -- The start range parameter is invalid, i.e. not numerical: " + 
                                    l_paramArr[L_START_IX]);
                }
                super.sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                i_logger.logMessage( new LoggableEvent(C_START_RANGE_IS_INVALID,
                                                       LoggableInterface.C_SEVERITY_ERROR));
            }
        }

        // 2. Get the end customer id parameter.
        l_paramArr[L_END_IX] = (String)p_parameters.get(BatchConstants.C_KEY_END_RANGE);
        if (l_paramArr[L_END_IX] == null)
        {
            // The end customer id parameter is missing.
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_ERROR,
                                C_CLASS_NAME,
                                10520,
                                this,
                                C_METHOD_getParameters + " -- The end range parameter is missing.");
            }
            super.sendBatchMessage(BatchMessage.C_STATUS_ERROR);
            i_logger.logMessage( new LoggableEvent(C_END_RANGE_IS_MISSING,
                                                   LoggableInterface.C_SEVERITY_ERROR));

            try
            {
                Integer.parseInt(l_paramArr[L_END_IX]);
            }
            catch (NumberFormatException p_nfExe)
            {
                // The string does not contain a parsable integer, i.e. an invalid cust id value.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_ERROR,
                                    C_CLASS_NAME,
                                    10525,
                                    this,
                                    C_METHOD_getParameters +
                                    " -- The end range parameter is invalid, i.e. not numerical: " + 
                                    l_paramArr[L_END_IX]);
                }
                super.sendBatchMessage(BatchMessage.C_STATUS_ERROR);
                i_logger.logMessage( new LoggableEvent(C_END_RANGE_IS_INVALID,
                                                       LoggableInterface.C_SEVERITY_ERROR));
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10530,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_getParameters);
        }
        return l_paramArr;
    } // End of getParameters
}
