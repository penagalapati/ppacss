////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromPlChBatchController.java 
//      DATE            :       03-Aug-2004
//      AUTHOR          :       Liam Byrne
//      REFERENCE       :       PRD_ASCS00_DEV_SS_83
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :      The entry point for "Batch Promotion Plan Change".
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.PromPlChBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.PromPlChBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.PromPlChBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;


/**
 * The entry point for "Batch Promotion Plan Change".
 */
public class PromPlChBatchController extends BatchController
{
    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "PromPlChBatchController";
    
    /** Specification of additional properties layers to load for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS  = "batch_ppc";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_INVALID_FILENAME     = "Invalid filename";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_FILENAME_NOT_DEFINED = "Filename not defined";   

    /** Creates an PromPlChBatchController, the starting process for Batch Promotion Plan
     * Change program.
     * @param p_context     A <code>PpasContext</code> object.
     * @param p_jobType     The type of job (e.g. BatchPromPlCh).
     * @param p_jobId       The unic id for this job.
     * @param p_jobRunnerName The name of the job runner in which this job is running.
     * @param p_params      Holding parameters to use when executing this job. 
     * @throws PpasConfigException - If configuration data is missing or incomplete.
     */
    public PromPlChBatchController(
        PpasContext p_context,
        String p_jobType,
        String p_jobId,
        String p_jobRunnerName,
        Map p_params)
        throws PpasConfigException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10001,
                this,
                "Constructing " + C_CLASS_NAME);
        }    

        this.init();
        i_executionDateTime = DatePatch.getDateTimeNow();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10002,
                this,
                "Constructing " + C_CLASS_NAME);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
    /**
     * Initialise.
     * @throws PpasConfigException - if configuration data is missing or incomplete.
     */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                82601,
                this,
                "Enter " + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                82602,
                this,
                "Leaving " + C_METHOD_init);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";
    /**
     * Creates a <code>PromPlChBatchReader</code>.
     * @return The created PromPlChBatchReader.
     */
    public BatchReader createReader()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10100,
                this,
                "Enter " + C_METHOD_createReader);
        }
        
        PromPlChBatchReader l_reader =
            new PromPlChBatchReader(
                super.i_ppasContext,
                super.i_logger,
                this,
                super.i_inQueue,
                super.i_params,
                super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10110,
                this,
                "Leaving " + C_METHOD_createReader);
        }
        
        return l_reader;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";

    /** 
     * Creates a <code>PromPlChBatchWriter</code>.
     * @return The created PromPlChBatchWriter.
     * @throws IOException If it was not possible to create output files.
     */
    public BatchWriter createWriter() throws IOException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10200,
                this,
                "Enter " + C_METHOD_createWriter);
        }
        PromPlChBatchWriter l_writer =
            new PromPlChBatchWriter(
                super.i_ppasContext,
                super.i_logger,
                this,
                super.i_params,
                super.i_outQueue,
                super.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10210,
                this,
                "Leaving " + C_METHOD_createWriter);
        }
        return l_writer;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";

    /** 
      * Creates a <code>PromPlChBatchProcessor</code>.
      * @return The created PromPlChBatchProcessor. 
      */
    public BatchProcessor createProcessor()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                "Enter " + C_METHOD_createProcessor);
        }

        PromPlChBatchProcessor p_processor =
            new PromPlChBatchProcessor(
                super.i_ppasContext,
                super.i_logger,
                this,
                super.i_inQueue,
                super.i_outQueue,
                super.i_params,
                this.i_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_HIGH,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                "Leaving " + C_METHOD_createProcessor);
        }
        return p_processor;
    }
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";

    /** 
    * The method is responsible to insert a record into table <code>BACO_BATCH_CONTROL</code> using the  
    * defined service method addControlInformation on PpasBatchControlService.  The method will  
    * get a <code>BatchJobData</code> object from getKeyedControlRecord() and populate it with 
    * required parameters.
    */   
    protected void addControlInformation()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                11010,
                this,
                "Enter " + C_METHOD_addControlInfo);
        }
        
        BatchJobData l_jobData    = null;
        String       l_inFileName = null;
        String       l_errorMsg   = C_INVALID_FILENAME;
   
        
        l_inFileName = (String)super.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);

        l_jobData = this.getKeyedControlRecord();
        l_jobData.setBatchType(BatchConstants.C_JOB_TYPE_BATCH_PROMOTION_PLAN_CHANGE);
        l_jobData.setOpId(super.getSubmitterOpid());
        l_jobData.setFileSeqNo(i_seqNo);
        l_jobData.setFileSubSeqNo(null);
        l_jobData.setNoOfSuccessRec("0");
        l_jobData.setNoOfRejectedRec("0");
        l_jobData.setExecutionDateTime(super.i_executionDateTime.toString());            
        l_jobData.setJsJobId(super.getJobId());
        l_jobData.setBatchDate(i_fileDate);

        l_jobData.setSubJobCnt("-1");

        if (this.isFileNameValid(l_inFileName))
        {
            l_jobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);

            l_jobData.setFileSubSeqNo(null);
            l_jobData.setExtraData1(null);
            l_jobData.setExtraData2(null);
            l_jobData.setExtraData3(null);
            l_jobData.setExtraData4(null);
        }
        else
        {
            // Invalid filename, special batch job info added.
            l_jobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_FAILED);
            if (l_inFileName != null )
            {
                if (l_errorMsg.length() + l_inFileName.length() + 2 <= 30)
                {
                    l_errorMsg += ": " + l_inFileName;
                }
            }
            else
            {
                l_errorMsg = C_FILENAME_NOT_DEFINED;
            }
            
            l_jobData.setExtraData4(l_errorMsg);
            
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;
            
            // Log 'invalid filename'.
            super.i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " -- *** ERROR: " + 
                                                        C_INVALID_FILENAME + ": " + l_inFileName,
                                                        LoggableInterface.C_SEVERITY_ERROR));
        }

        
        try
        {
            super.i_batchContService.addJobDetails(null, l_jobData, i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    11010,
                    this,
                    C_METHOD_addControlInfo + " PpasServiceException from addJobDetails: " + 
                    e.getFullMessage());
            }

            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            super.doRequestStop();
        }
       
        if (!super.i_startComponents)
        {
            super.finishDone(JobStatus.C_JOB_EXIT_STATUS_FAILURE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10390,
                this,
                BatchConstants.C_CONSTRUCTED + C_METHOD_addControlInfo);
        }
    }
    

    
    
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";

    /**
    * It updates the databse with a new status when a batch have got the status finished (c), 
    * stopped (x) or error (f). The metohd is called from the super class. 
    * The <code>BatchJobData</code>object used in the method are  
    * first fetched form getKeyedControlRecord and then the status is inserted into that object.
     * @param p_status The new batch status.
    */     
    protected void updateStatus(String p_status)
    {
        try
        {
            BatchJobData l_jobData = getKeyedControlRecord();
            l_jobData.setBatchJobStatus(p_status);
            super.i_batchContService.updateJobDetails(null, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10391,
                    this,
                    C_METHOD_updateStatus + " PpasServiceException caught " +
                    e.getMsgKey() + " " + e.getMessage());
            }
        }
    }
    
   /** 
    * This method will return a BatchJobData-object with the correct key-values set 
    * (JsJobId and executionDateTime). The caller can then populate the record with data needed 
    * for the operation in question. For example, the writer will get such record and
    * add number of successfully/faulty records processed and then update.
    * @return BatchJobData record.
    */     
    public BatchJobData getKeyedControlRecord()
    {
        BatchJobData l_jobData = new BatchJobData();
        l_jobData.setExecutionDateTime(this.i_executionDateTime.toString());
        l_jobData.setJsJobId(super.getJobId());
        return l_jobData;
    }
    
    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
  
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11300, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                11400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }

        return l_batchJobControlData;
    }
    
    /**
     * Check if the filename is valid.
     *
     * @param p_fileName  The filename
     * @return boolean : True is it's valid
     */
    public boolean isFileNameValid( String p_fileName )
    {
        // Array indexes for filenames
        final int L_FILE_DATE = 1; // Second element in a long date in the file.

        // Array indexes for sequence number
        final int L_SEQ_NO = 2; // Third element in file name sequence number.

        return isFileNameValid( p_fileName,
                         BatchConstants.C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_DAT,
                         BatchConstants.C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_IPG,
                         BatchConstants.C_PATTERN_PROMOTION_PLAN_CHANGE_FILENAME_SCH,
                         L_FILE_DATE,
                         L_SEQ_NO );
    }

}
