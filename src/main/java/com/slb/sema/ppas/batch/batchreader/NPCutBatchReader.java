////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       NPCutBatchReader.java 
//      DATE            :       Aug 17, 2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       ATOS ORIGIN 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of change>    | <reference>
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.NPCutBatchRecordData;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible to read cutof orders from flat file.
 * The actual radoperation from tile is hidden within the super class so this
 * class operates on strings that origins from the file. For each read order (line)
 * a NPCutBatchReader is created and populated.
 */
public class NPCutBatchReader extends BatchLineFileReader
{
    
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                  = "NPCutBatchReader";
    
    /** Error code. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT = "01";

    /** Field length of MSISDNto be installed. Value is {@value}. */
    private static final int    C_OLD_MSISDN_LENGTH           = 15;

    /** Field length of Master MSISDN if the first MSISDN was a subordinate one. Value is {@value}. */
    private static final int    C_NEW_MSISDN_LENGTH           = 15;

    /**The maximum allowed length of a record line in the file. */
    private static final int    C_MAX_FILE_LINE_LENGTH        = C_OLD_MSISDN_LENGTH + C_NEW_MSISDN_LENGTH; 
                                
    // Index in the batch record array
    /** Number of fields in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NUMBER_OF_FIELDS            = 2;

    /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_OLD_MSISDN                  = 0;

    /** Index for the field Master MSISDN in the batchDataRecord array. Value is {@value}. */
    private static final int    C_NEW_MSISDN                  = 1;

    /** The BatchDataRecord to be sent into the Queue. */
    private NPCutBatchRecordData i_batchRecordData            = null;
    

    /**
    * Constructor for NPCutBatchReader.
    * Information about service area, service location and service class
    * is retrieved from the indata filename.
    * @param p_controller  Reference to the BatchController that creates this object.
    * @param p_ppasContext Reference to PpasContext.
    * @param p_logger      reference to the logger.
    * @param p_parameters  Reference to a Map holding information e.g. filename.
    * @param p_inQueue     Reference to the Queue, where all indata records will be added.
    * @param p_properties  <code>PpasProperties </code> for the batch subsystem.
    */
    public NPCutBatchReader(
        PpasContext p_ppasContext,
        Logger p_logger,
        BatchController p_controller,
        SizedQueue p_inQueue,
        Map p_parameters,
        PpasProperties p_properties)
    {
        super( p_ppasContext, p_logger, p_controller, p_inQueue, p_parameters, p_properties );
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10001,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }
        
        
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10002,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }    
        
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getRecord = "getRecord";    
    /**
     * This method shall contain all logic necessary to create 
     * and populate the BatchRecordData record.
     * 
     * @return - null if "No more records to read".
     */
    protected BatchRecordData getRecord()
    {

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                BatchConstants.C_ENTERING + C_METHOD_getRecord );
        }

        // Get next line from input file
        i_batchRecordData = null;

        if ( readNextLine() != null )
        {
            i_batchRecordData = new NPCutBatchRecordData();

            // Store input filename
            i_batchRecordData.setInputFilename(i_inputFileName);
            
            // Store line-number form the file
            i_batchRecordData.setRowNumber(i_lineNumber);
            
            // Store original data record
            i_batchRecordData.setInputLine(i_inputLine);
            
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10342,
                    this,
                    "getRecord : innan extractLineAndValidate" );
            }

            // Extract data from line
            if ( extractLineAndValidate())
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10343,
                        this,
                        "extractLineAndValid was successful" );
                }
   
            }
            else
            {
                // Line is corrupt - invalid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10343,
                        this,
                        "extractLineAndValid failed!" );
                }

                i_batchRecordData.setCorruptLine( true );
                
                if ( i_batchRecordData.getErrorLine() == null )
                {
                    // Other error than specific field errors
                    i_batchRecordData.setErrorLine( C_ERROR_INVALID_RECORD_FORMAT );
                }
            }
            
            i_batchRecordData.setNumberOfSuccessfullyProcessedRecords(super.i_successfullyProcessed);
            i_batchRecordData.setNumberOfErrorRecords(super.i_notProcessed);
            if ( super.faultyRecord(i_lineNumber))
            {
                i_batchRecordData.setFailureStatus(true);
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10340,
                    this,
                    "After setNumberOfSuccessfully...."+ super.i_successfullyProcessed);
            }           
        } // end if
        
        
        if ( i_batchRecordData != null )
        {

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10350,
                    this,
                    "DumpRecord: "+i_batchRecordData.dumpRecord());
            }
        }
        else
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10350,
                    this,
                    "i_batchDataRecord is NULL!!!!!");
            }            
        }
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getRecord);
        }

        return i_batchRecordData;
        
    } // end of method getRecord()
    

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
    /**
     * Check if filename is of the valid form:
     *     NUMBER_CUTOVER_yyyymmdd_sssss.DAT /IPG.
     *     
     * @param p_fileName Input file name.
     * @return <code>true</code> if filename is valid else <code>false</code>.
     */
    protected boolean isFileNameValid(String p_fileName)
    {
        boolean l_validFileName = false;   // Help variable - return value. Assume filename is OK
 
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
        }

        if (p_fileName != null)
        {
            if (p_fileName.matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CUTOVER_FILENAME_DAT)
                || p_fileName.matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CUTOVER_FILENAME_IPG)
                || p_fileName.matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CUTOVER_FILENAME_SCH))
            {
                // valid fileName
                l_validFileName = true;
            }
            else
            {
                return l_validFileName;
            }

            // Check if recovery mode
            if (l_validFileName && !i_recoveryMode)
            {
                // Flag that processing is in progress. Rename the file to *.IPG.
                // If the batch is running in recovery mode, it already has the extension .IPG
                if (p_fileName.matches(BatchConstants.C_PATTERN_NUMBER_PLAN_CUTOVER_FILENAME_SCH))
                {
                    renameInputFile(
                        BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                        BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                        BatchConstants.C_EXTENSION_IN_PROGRESS_FILE);
                }
                else
                {
                    renameInputFile(
                        BatchConstants.C_PATTERN_INDATA_FILE,
                        BatchConstants.C_EXTENSION_INDATA_FILE,
                        BatchConstants.C_EXTENSION_IN_PROGRESS_FILE);
                }
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10410,
                        this,
                        "After rename, inputFullPathName="+i_fullPathName );
                }              
            }      
        } // end - if fileName != null

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10410,
                this,
                BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
        }

        return l_validFileName;
    }
    



    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    

    /**
     * This method extracts information from the batch data record. The record must look like:
     * [Line number];[oldMSISDN][newMSISDN]
     * The line number will be used to check against the recovery file if the batch mode is
     * recovery.
     * All fields are checked to be alpha numberic.
     * @return <code>true</code> when extracted fields are valid else <code>false</code>.
     */
    private boolean extractLineAndValidate()
    {
        MsisdnFormat l_msisdnFormat        = null;
        Msisdn       l_tmpMsisdn           = null;  // Help variable to convert from string to Msisdn object.
        String[]     l_recordFields        = new String[C_NUMBER_OF_FIELDS]; // Help array to keep the input 
                                                                             //line elements.
        boolean      l_validData           = true;        // return value - assume it is valid.
        String       l_tmpInputLine        = i_inputLine; // Initialize to inputline, if not ','-separator
        
        // Help variables to convert from ','-separated strings to fixed position
        StringBuffer l_buffer = new StringBuffer();
        String       l_tmp = null;
        int          l_tmpLen = 0;

        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate );
        }
        
        //Check if the new msisdn and old msisdn is comma separated. If so remove the comma. 
        String [] l_tempArray = i_inputLine.split("[,]");
        
        if (l_tempArray.length > 1)
        {
            l_tmp    = BatchConstants.C_EMPTY_MSISDN + l_tempArray[0];
            l_tmpLen = l_tmp.length();
            l_buffer.append(l_tmp.substring(l_tmpLen-BatchConstants.C_LENGTH_MSISDN, l_tmpLen));
            
            l_tmp    = BatchConstants.C_EMPTY_MSISDN + l_tempArray[1];
            l_tmpLen = l_tmp.length();
            l_buffer.append(l_tmp.substring(l_tmpLen-BatchConstants.C_LENGTH_MSISDN, l_tmpLen));
            
            l_tmpInputLine = l_buffer.toString();

        }
        
        //Check max record length.
        if (l_tmpInputLine.length() > C_MAX_FILE_LINE_LENGTH)
        {
            i_batchRecordData.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                                           + BatchConstants.C_DELIMITER_REPORT_FIELDS 
                                           + i_inputLine);

            l_validData = false;
            
            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_MODERATE,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10365,
                                this,
                                "Row: ]"+ l_tmpInputLine 
                                        + "[ wrong record length " + " l_lineNumber= " + i_lineNumber
                                        + " " + i_batchRecordData.getErrorLine() + " length= "
                                        + l_tmpInputLine.length());
            }
        }
        else
        {
            initializeRecordValidation(l_tmpInputLine, l_recordFields, i_batchRecordData);

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_CLASS_NAME,
                                10361,
                                this,
                                "extractLineAndValidate innan IF getfields.....");
            }

            // Get the record fields and check if it is numeric/alphanumberic
            //    Start End #Field Format check Errorcode
            if (getField(0,
                         C_OLD_MSISDN_LENGTH,
                         C_OLD_MSISDN,
                         BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC,
                         C_ERROR_INVALID_RECORD_FORMAT)
                    && getField(C_OLD_MSISDN_LENGTH,
                                C_NEW_MSISDN_LENGTH,
                                C_NEW_MSISDN,
                                BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC,
                                C_ERROR_INVALID_RECORD_FORMAT))
            {

                // Got a valid record
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10365,
                                    this,
                                    "Row: " + i_lineNumber + " ]" + l_tmpInputLine + "[ is a valid record");
                }
            }
            else
            {
                // Illegal field format detected
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10366,
                                    this,
                                    "Row: " + i_lineNumber + " ]" + l_tmpInputLine + "[ is a INVALID record"
                                            + " l_lineNumber =" + i_lineNumber + "\n" + " l_oldMsisdn  ="
                                            + l_recordFields[C_OLD_MSISDN] + "\n" + " l_newMsisdn  ="
                                            + l_recordFields[C_NEW_MSISDN] + "\n");
                }

                // Flag for corrupt line
                l_validData = false;
            }

            // Both old and new MSISDN must be specified
            if (l_recordFields[C_OLD_MSISDN] == null  || l_recordFields[C_NEW_MSISDN] == null)                                 
            {
                // Field not specified
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_START,
                                    C_CLASS_NAME,
                                    10365,
                                    this,
                                    "Row: " + i_lineNumber + " ]" + l_tmpInputLine + "[ is a INVALID record"
                                            + " l_lineNumber   =" + i_lineNumber + "\n" + " l_old_msisdn   ="
                                            + l_recordFields[C_OLD_MSISDN] + "\n" + " l_new_Msisdn   ="
                                            + l_recordFields[C_NEW_MSISDN]);
                }
                this.i_batchRecordData.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                                                    + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                    + i_inputLine);
                       
                l_validData = false;
            }
        }  
        
        // Store information into the BatchDataRecord
        // If some of the fields were invalid their value will be NULL.

        // Convert the MSISDN string (old and new) to a Msisdn object
        // This may give an ParseException - if so flag currupt line!
        if (l_validData)
        {
            l_msisdnFormat = i_context.getMsisdnFormatInput();
            try
            {
                l_tmpMsisdn = l_msisdnFormat.parse( l_recordFields[C_OLD_MSISDN] );
                i_batchRecordData.setOldMsisdn( l_tmpMsisdn );
      
                l_tmpMsisdn = l_msisdnFormat.parse(l_recordFields[C_NEW_MSISDN]);                
                i_batchRecordData.setNewMsisdn(l_tmpMsisdn);
            }
            catch (ParseException e)
            {
                l_validData = false;
                
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                            PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10366,
                            this,
                            "Row: " + i_lineNumber + " ]"+ l_tmpInputLine + "[ has invalid MSISDN" );
                }
            }
        }
            
                
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10370,
                this,
                BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate );
        }

        return l_validData;
        
    } // end of method extractLineAndValidate(.)
}
