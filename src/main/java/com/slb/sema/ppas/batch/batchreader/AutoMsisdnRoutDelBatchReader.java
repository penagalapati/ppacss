////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       AutoMsisdnRoutDelBatchReader
//  DATE            :       02 August 2006
//  AUTHOR          :       Ian James
//  REFERENCE       :       PRD_ASCS00_GEN_CA_098
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//    CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
//  DATE    | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AutoMsisdnRoutBatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isil.batchisilservices.AutoMsisdnRoutDelDbBatchService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;


/**
* This class is responsible to provide doRun() with BatchDataRecords representing customers 
* marked as disconnected on the ASCS database, excluding those that have not been
* re-connected/re-installed/re-used.
* Since this is a database driven batch process the creation and initial population of 
* AutoMsisdnRoutBatchRecords are handled by the dedicated "Batch ISIL Service". 
*/
public class AutoMsisdnRoutDelBatchReader extends BatchReader
{
  //-----------------------------------------------------
  //  Class level constant
  //  ------------------------------------------------------

  /** Class name constant used in calls to middleware. Value is {@value}. */
  private static final String  C_CLASS_NAME  = "AutoMsisdnRoutDelBatchReader";

  //-------------------------------------------------------------------------
  // Instance variables
  //-------------------------------------------------------------------------

  /** The 'batch isil' api for the disconnection batch. */
  private AutoMsisdnRoutDelDbBatchService i_isApi = null;

  /**
   * Constructor for the AutoMsisdnRoutDelBatchReader.
   * @param p_ppasContext Reference to the PpasContext.
   * @param p_logger Reference to the Logger.
   * @param p_controller Reference to the call-back Controller.
   * @param p_inQueue Reference to the in-queue.
   * @param p_parameters Reference to the start parameters.
   * @param p_properties Reference to the batch properties.
   */
  public AutoMsisdnRoutDelBatchReader(PpasContext p_ppasContext,
                                      Logger p_logger,
                                      BatchController p_controller,
                                      SizedQueue p_inQueue,
                                      Map p_parameters,
                                      PpasProperties p_properties)
  {
      super(p_ppasContext, p_logger, p_controller, p_inQueue, p_parameters, p_properties);

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW,
                          PpasDebug.C_APP_SERVICE,
                          PpasDebug.C_ST_CONFIN_START,
                          C_CLASS_NAME,
                          10010,
                          this,
                          BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
      }

      this.init();

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW,
                          PpasDebug.C_APP_SERVICE,
                          PpasDebug.C_ST_CONFIN_END,
                          C_CLASS_NAME,
                          10020,
                          this,
                          BatchConstants.C_CONSTRUCTED);
      }

  } // End of constructor

  /** Method name constant used in calls to middleware. Value is {@value}. */
  private static final String C_METHOD_getRecord = "getRecord";

  /**
   * This method call the Batch ISIL Service that returns a AutoMsisdnRoutBatchRecordData. No manipulation is
   * required since the underlying database service has knowledge of the record created.
   * @return The AutoMsisdnRoutBatchRecordData
   */
  protected BatchRecordData getRecord()
  {
      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW,
                          PpasDebug.C_APP_SERVICE,
                          PpasDebug.C_ST_START,
                          C_CLASS_NAME,
                          10030,
                          this,
                          BatchConstants.C_ENTERING + C_METHOD_getRecord);
      }

      AutoMsisdnRoutBatchRecordData l_record = null;
      
      // get one record by calling fetch
      try
      {
          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW,
                              PpasDebug.C_APP_SERVICE,
                              PpasDebug.C_ST_TRACE,
                              C_CLASS_NAME,
                              10040,
                              this,
                              "innan i_isApi.fetch() - getRecord. " + C_METHOD_getRecord);
          }

          l_record = (AutoMsisdnRoutBatchRecordData)i_isApi.fetch();

          // Note that fetch can return null. This indicates 'no more records to fetch'.
          if (l_record != null)
          {
              if (PpasDebug.on)
              {
                  PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                  PpasDebug.C_APP_SERVICE,
                                  PpasDebug.C_ST_TRACE,
                                  C_CLASS_NAME,
                                  10050,
                                  this,
                                  "after i_isApi.fetch() - getRecord. " + C_METHOD_getRecord + " record: "
                                          + l_record.dumpRecord());
              }
          }
          else
          {
              if (PpasDebug.on)
              {
                  PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                  PpasDebug.C_APP_SERVICE,
                                  PpasDebug.C_ST_TRACE,
                                  C_CLASS_NAME,
                                  10060,
                                  this,
                                  "after i_isApi.fetch() - getRecord. " + C_METHOD_getRecord
                                          + " record IS NULL - no more to read");
              }
          }
      }
      catch (PpasServiceException e)
      {
          sendBatchMessage(BatchMessage.C_STATUS_ERROR);

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW,
                              PpasDebug.C_APP_SERVICE,
                              PpasDebug.C_ST_ERROR,
                              C_CLASS_NAME,
                              10070,
                              this,
                              "SQLException - getRecord. " + C_METHOD_getRecord);
          }

          e.printStackTrace();
      }

      // Return the record to doRun() in super class
      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW,
                          PpasDebug.C_APP_SERVICE,
                          PpasDebug.C_ST_END,
                          C_CLASS_NAME,
                          10080,
                          this,
                          BatchConstants.C_LEAVING + C_METHOD_getRecord);
      }

      return l_record;

  } // End of getRecord()

  /** Method name constant used in calls to middleware. Value is {@value}. */
  private static final String C_METHOD_init = "init";

  /**
   * This method instantiate and opens the BatchISIL Service.
   */
  protected void init()
  {
      PpasRequest l_request = null;
      PpasSession l_session = null;
      String l_queryParameters[] = null;
      int l_fetchSize = 0;

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW,
                          PpasDebug.C_APP_SERVICE,
                          PpasDebug.C_ST_START,
                          C_CLASS_NAME,
                          10090,
                          this,
                          BatchConstants.C_ENTERING + C_METHOD_init);
      }

      l_fetchSize = getFetchSize();
      
      // Create and open the Batch ISIL Service Component
      l_session = new PpasSession();
      l_session.setContext(this.i_context);

      l_request = new PpasRequest(l_session);
      i_isApi = new AutoMsisdnRoutDelDbBatchService(l_request, i_logger, i_context);

      // Call open on the API component
      try
      {
          i_isApi.open(l_queryParameters, l_fetchSize);
      }
      catch (PpasServiceException e)
      {
          sendBatchMessage(BatchMessage.C_STATUS_ERROR);

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW,
                              PpasDebug.C_APP_SERVICE,
                              PpasDebug.C_ST_ERROR,
                              C_CLASS_NAME,
                              10100,
                              this,
                              "PpasSqlException! - getRecord. " + C_METHOD_init);
          }

          e.printStackTrace();
      }
      catch (SecurityException e)
      {
          sendBatchMessage(BatchMessage.C_STATUS_ERROR);
          i_logger.logMessage(new LoggableEvent("SecurityException", LoggableInterface.C_SEVERITY_ERROR));

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW,
                              PpasDebug.C_APP_SERVICE,
                              PpasDebug.C_ST_ERROR,
                              C_CLASS_NAME,
                              10110,
                              this,
                              "SecurityException - getRecord. " + C_METHOD_getRecord);
          }

          e.printStackTrace();
      }
      catch (IllegalArgumentException e)
      {
          sendBatchMessage(BatchMessage.C_STATUS_ERROR);
          i_logger.logMessage(new LoggableEvent("IllegalArgumentException",
                                                LoggableInterface.C_SEVERITY_ERROR));

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW,
                              PpasDebug.C_APP_SERVICE,
                              PpasDebug.C_ST_ERROR,
                              C_CLASS_NAME,
                              10120,
                              this,
                              "IllegalArgumentException - getRecord. " + C_METHOD_getRecord);
          }

          e.printStackTrace();
      }

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW,
                          PpasDebug.C_APP_SERVICE,
                          PpasDebug.C_ST_END,
                          C_CLASS_NAME,
                          10130,
                          this,
                          BatchConstants.C_LEAVING + C_METHOD_init);
      }

      return;
  } // End of init()

  /** Method name constant used in calls to middleware. Value is {@value}. */
  private static final String C_METHOD_tidyUp = "tidyUp";

  /**
   * @see com.slb.sema.ppas.batch.batchreader.BatchReader#tidyUp() .
   */
  protected void tidyUp()
  {
      // Clean up Clean up and closes isapi
      try
      {
          if (this.i_isApi != null)
          {
              this.i_isApi.close();
              this.i_isApi = null;

          }
      }
      catch (PpasServiceException e)
      {
          sendBatchMessage(BatchMessage.C_STATUS_ERROR);
          i_logger.logMessage(e);

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW,
                              PpasDebug.C_APP_SERVICE,
                              PpasDebug.C_ST_ERROR,
                              C_CLASS_NAME,
                              10140,
                              this,
                              "PpasServiceException - " + C_METHOD_tidyUp);
          }

          e.printStackTrace();
      }

      return;
  } // End of tidyUp()
} // End of AutoMsisdnRoutDelBatchReader
