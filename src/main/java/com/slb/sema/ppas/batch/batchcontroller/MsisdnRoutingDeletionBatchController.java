////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       MsisdnRoutingDeletionBatchController
//      DATE            :       27-October-2004
//      AUTHOR          :       Marianne Tornqvist
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       This class is the entry point for Batch Routing
//                              and will be initiated by the Job Scheduler. It is 
//                              responsible to create and start all underlying components
//                              and will also be used for "call-back" to the initiating 
//                              client. It extends the generic batch controller class 
//                              BatchCOntroller and containts no specific logic.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of change>    | <reference>
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchprocessing.BatchProcessor;
import com.slb.sema.ppas.batch.batchprocessing.MsisdnRoutingDeletionBatchProcessor;
import com.slb.sema.ppas.batch.batchreader.BatchReader;
import com.slb.sema.ppas.batch.batchreader.MsisdnRoutingDeletionBatchReader;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.batch.batchwriter.BatchWriter;
import com.slb.sema.ppas.batch.batchwriter.MsisdnRoutingDeletionBatchWriter;
import com.slb.sema.ppas.common.dataclass.BatchJobControlData;
import com.slb.sema.ppas.common.dataclass.BatchJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasConfigException;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;


/**
* This <code>MsisdnRoutingDeletionBatchController</code> class is the entry point for the Msisdn deletion
* batch. It is responsible to create and start all underlying components such as the reader, the processor
* and the writer components.
*/
public class MsisdnRoutingDeletionBatchController extends BatchController
{
    //--------------------------------------------------------
    //  Class level constant
    //  ------------------------------------------------------ 

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME          = "MsisdnRoutingDeletionBatchController";

    /** Specification of additional properties layers to load for this batch job type. */
    private static final String C_ADDED_CONFIG_LAYERS = "batch_mrd";

    /** Array index to find the date from the filename.  Value is {@value}. */
    private static final int C_INDEX_FILE_DATE        = 2; // 3rd element in the filename is the date

    /** Array index to fine the sequence number from the filename.  Value is {@value}. */
    private static final int C_NO_SEQUENCE_NUMBER_PRESENT_IN_FILENAME  = -1; 

//    /** Execution date used in the control block. */
//    private String i_executionDateTime                = null;
//
//    /** Job id from the job scheduler, used in the control block. */
//    private String i_jsJobId                          = null;

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_INVALID_FILENAME    = "Invalid filename";

    /** Log message - invalid filename for input file. Value is {@value}. */
    private static final String C_FILENAME_NOT_DEFINED = "Filename not defined";

   /**
    * Constructs a new MsisdnRoutingDeletionBatchController object.
    * @param p_context     A <code>PpasContext</code> object.
    * @param p_jobType     The type of job (e.g. BatchInstall).
    * @param p_jobId       The unic id for this job.
    * @param p_jobRunnerName The name of the job runner in which this job is running.
    * @param p_params      Holding parameters to use when executing this job. 
    * @throws PpasConfigException - If configuration data is missing or incomplete.
    */
    public MsisdnRoutingDeletionBatchController( PpasContext p_context,
                                  String      p_jobType,
                                  String      p_jobId,
                                  String      p_jobRunnerName,
                                  Map         p_params)
        throws PpasConfigException
    {
        super(p_context, p_jobType, p_jobId, p_jobRunnerName, p_params);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10300,
                this,
                BatchConstants.C_CONSTRUCTING );
        }

        this.init();

//        i_jsJobId           = p_jobId;
        i_executionDateTime = DatePatch.getDateTimeNow();

        // Make this batch neither pausible nor stoppable.
        super.setPausable(false);
        super.setStoppable(false);

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10310,
                this,
                BatchConstants.C_CONSTRUCTED );
        }

    } // End of constructor

   /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_init = "init";
   /**
    * Initialise.
    * @throws PpasConfigException - if configuration data is missing or incomplete.
    */
    protected void init() throws PpasConfigException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                82601,
                this,
                BatchConstants.C_ENTERING + C_METHOD_init);
        }       

        super.i_properties = i_properties.getPropertiesWithAddedLayers(C_ADDED_CONFIG_LAYERS);
        super.init();

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                82602,
                this,
                BatchConstants.C_LEAVING + C_METHOD_init);
        }
    }

   /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createReader = "createReader";        
   /**
    * This method will instantiate the correct reader-class and passes on
    * the required parameters and also a reference to itself. The later is used
    * for the reader-component to report back the controller-component. The method
    * is called from the super-class inner class method doRun() when start is ordered.
    * 
    * @return Reference to a MsisdnRoutingDeletionBatchReader.
    */
    public BatchReader createReader()
    {
        MsisdnRoutingDeletionBatchReader l_reader = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10320,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createReader );
        }

        if (super.i_startComponents)
        {
            l_reader = new MsisdnRoutingDeletionBatchReader(super.i_ppasContext,
                                                            super.i_logger,
                                                            this,
                                                            super.i_inQueue,
                                                            super.i_params,
                                                            super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10330,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createReader );
        }

        return l_reader;

    } // End of createReader()

   /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createWriter = "createWriter";        
   /**
    * This method will instantiate the correct writer-class and passes on
    * the required parameters and also a reference to itself. The later is used
    * for the writer-component to report back the controller-component. The method
    * is called from the super-class inner class method doRun() when start is ordered.
    * 
    * @return Reference to a MsisdnRoutingDeletionBatchWriter.
    * @throws IOException  if an error occurs when opening (creating) any of the output files.
    */
    public BatchWriter createWriter() throws IOException
    {
        MsisdnRoutingDeletionBatchWriter l_writer = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10340,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createWriter );
        }

        if (super.i_startComponents)
        {
            l_writer = new MsisdnRoutingDeletionBatchWriter(super.i_ppasContext,
                                                   super.i_logger,
                                                   this,
                                                   super.i_params,
                                                   super.i_outQueue,
                                                   super.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10350,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createWriter );
        }

        return l_writer;

    } // End of createWriter()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_createProcessor = "createProcessor";        
    /**
    * This method will instantiate the correct processor-class and passes on
    * the required parameters and also a reference to itself. The later is used
    * for the processor-component to report back the controller-component. The method
    * is called from the super-class inner class method doRun() when start is ordered.
    * 
    * @return Reference to a MsisdnRoutingDeletionBatchProcessor.
    */
    public BatchProcessor createProcessor()
    {
        MsisdnRoutingDeletionBatchProcessor l_processor = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10360,
                this,
                BatchConstants.C_ENTERING + C_METHOD_createProcessor );
        }

        if (super.i_startComponents)
        {
            l_processor = new MsisdnRoutingDeletionBatchProcessor(super.i_ppasContext,
                                                         super.i_logger,
                                                         this,
                                                         super.i_inQueue,
                                                         super.i_outQueue,
                                                         super.i_params,
                                                         this.i_properties);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10370,
                this,
                BatchConstants.C_LEAVING + C_METHOD_createProcessor );
        }

        return l_processor;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_addControlInfo = "addControlInfo";
    /**
    * This method is responsible to insert a record into table BACO using the defined
    * service method PpasBatchCOntrolService.addControlInformation(). The method will
    * get a BatchJobData object and populate it with required parameters.
    * Updates the control information record for each batch process.
    */
    protected void addControlInformation()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10380,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_addControlInfo);
        }

        BatchJobData l_jobData            = null;
        String       l_inFileName         = null;   
        String       l_errorMsg           = C_INVALID_FILENAME;
        String       l_inputFileDirectory = null;

        l_inFileName = (String)super.i_params.get(BatchConstants.C_KEY_INPUT_FILENAME);
        
        // If the given input file has the 'in progress file' extension (i.e. this is a recovery run) rename
        // it to an ordinary indata file, i.e. change the extension to the 'indata file' extension.
        // NOTE: This is a work-around to make recovery run work for this batch.
        //       When (if) the complete filename check is re-designed this might be handled in some other way.
        if ( l_inFileName != null && l_inFileName.endsWith(BatchConstants.C_EXTENSION_IN_PROGRESS_FILE))
        {
            l_inputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
            l_inFileName = renameFile(l_inFileName,
                                      l_inputFileDirectory,
                                      BatchConstants.C_EXTENSION_IN_PROGRESS_FILE,
                                      BatchConstants.C_EXTENSION_INDATA_FILE);
            
            // Put the new filename into the parameter map since this is passed as an argument when 
            // constructing the underlying components (reader, writer and processor).
            super.i_params.put(BatchConstants.C_KEY_INPUT_FILENAME, l_inFileName);
        }

        l_jobData = this.getKeyedControlRecord();
        l_jobData.setBatchType(BatchConstants.C_JOB_TYPE_MSISDN_ROUTING_DELETION);
        l_jobData.setSubJobCnt("-1");
        l_jobData.setOpId(super.getSubmitterOpid());
        l_jobData.setFileSubSeqNo(null);
        l_jobData.setExtraData1(null);
        l_jobData.setExtraData2(null);
        l_jobData.setExtraData3(null);
        l_jobData.setExtraData4(null);
        if (this.isFileNameValid(l_inFileName,
                                  BatchConstants.C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_DAT,
                                  BatchConstants.C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_IPG,
                                  BatchConstants.C_PATTERN_MSISDN_ROUTING_DELETION_FILENAME_SCH,
                                  C_INDEX_FILE_DATE, 
                                  C_NO_SEQUENCE_NUMBER_PRESENT_IN_FILENAME))
        {
            l_jobData.setBatchJobStatus(BatchConstants.C_BATCH_STATUS_INPROGRESS);
            l_jobData.setBatchDate(convertDateString(i_fileDate));
            l_jobData.setFileSeqNo(i_seqNo);
            l_jobData.setNoOfSuccessRec("0");
            l_jobData.setNoOfRejectedRec("0");
        }
        else
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;
            
            // Invalid filename, special batch job info added.
            l_jobData.setBatchJobStatus("F");
            if (l_inFileName != null )
            {
                if (l_errorMsg.length() + l_inFileName.length() + 2 <= 30)
                {
                    l_errorMsg += ": " + l_inFileName;
                }
            }
            else
            {
                l_errorMsg = C_FILENAME_NOT_DEFINED;
            }

            l_jobData.setExtraData4(l_errorMsg);
            
            // Log 'invalid filename'.
            super.i_logger.logMessage(new LoggableEvent(C_CLASS_NAME + " -- *** ERROR: " + 
                                                        C_INVALID_FILENAME + ": " + l_inFileName,
                                                        LoggableInterface.C_SEVERITY_ERROR));
        }

        try
        {
            super.i_batchContService.addJobDetails(null, l_jobData, super.i_timeout);
        }
        catch (PpasServiceException e)
        {
            // Do not start any reader, writer or processor component.
            super.i_startComponents = false;

            super.doRequestStop();
        }

        if (!super.i_startComponents)
        {
            super.finishDone(JobStatus.C_JOB_EXIT_STATUS_FAILURE);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10390,
                this,
                BatchConstants.C_LEAVING + C_METHOD_addControlInfo);
        }
    } // End of addControlInformation()


    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus"; 
    /**
     * This method call the service defined by PpasBatchControlService. In this case
     * for a filedriven  updateJobDetails should be called.
     * @param p_status The status to be updated.
     */
    protected void updateStatus(String p_status)
    {
        BatchJobData l_jobData = null;

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_START,
                C_CLASS_NAME,
                10400,
                this,
                BatchConstants.C_ENTERING + C_METHOD_updateStatus);
        }
        l_jobData = getKeyedControlRecord();
        l_jobData.setBatchJobStatus( p_status );

        try
        {
            super.i_batchContService.updateJobDetails( null, l_jobData, super.i_timeout );
        }
        catch (PpasServiceException e)
        {
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10410,
                    this,
                    C_METHOD_addControlInfo + " PpasServiceException from updateJobDetails");
            }
        }                

        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                10420,
                this,
                BatchConstants.C_LEAVING + C_METHOD_updateStatus );
        }

        return;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedControlRecord = "getKeyedControlRecord";        
    /**
     * This method will return a BatchJobData-object with the correct key-values set 
     *  (JsJobId and executionDateTime).  The caller can then populate the record with data needed 
     * for the operation in question.  For example, the writer will get such record and
     *   add number of successfully/faulty records processed and then update. 
     * @return BatchJobData job control block
     */    
    public BatchJobData getKeyedControlRecord()
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10430,
                            this,
                            C_METHOD_getKeyedControlRecord + " -- " + BatchConstants.C_ENTERING);
        }

        BatchJobData l_jobData = new BatchJobData();
        l_jobData.setExecutionDateTime(i_executionDateTime.toString());
        l_jobData.setJsJobId(getJobId());
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10450,
                            this,
                            C_METHOD_getKeyedControlRecord + " -- " + BatchConstants.C_LEAVING);
        }

        return l_jobData;

    } // End of getKeyedControlRecord()

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getKeyedBatchJobControlData = "getKeyedBatchJobControlData";        
    /** 
     * This method will return a BatchJobData-object with the correct key-values 
     * set (JsJobId and executionDateTime).
     * @return a BatchJobData object with JsJobId and executionDateTime set
     */
    public BatchJobControlData getKeyedBatchJobControlData()
    {
        BatchJobControlData l_batchJobControlData = null;
        long                l_jsJobId             = -1;          

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                            C_CLASS_NAME, 11300, this,
                            BatchConstants.C_ENTERING + C_METHOD_getKeyedBatchJobControlData);
        }

        l_jsJobId             = Long.parseLong(super.getJobId());
        l_batchJobControlData = new BatchJobControlData(super.i_executionDateTime, l_jsJobId);
                        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_END,
                C_CLASS_NAME,
                11400,
                this,
                BatchConstants.C_LEAVING + C_METHOD_getKeyedBatchJobControlData);
        }

        return l_batchJobControlData;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_renameFile = "renameFile";

    /**
     * Renames the file with the given filename at the given path. That is, change the current file extension
     * to the specified new extension.
     * If the rename succeeds the new filename is returned, otherwise the given filename will be returned.
     * 
     * @param p_filename      the name of the file that will be renamed.
     * @param p_path          the full directory path in which the given file is stored.
     * @param p_extension     the current filename extension to be replaced, includiing the leading dot (".").
     * @param p_newExtension  the new filename extension includiing the leading dot (e.g. ".DAT").
     * 
     * @return  the new filename is returned if the rename succeeded, otherwise the given filename will be 
     *          returned.
     */
    protected String renameFile(String p_filename, String p_path, String p_extension, String p_newExtension)
    {
        String l_filename        = null;
        File   l_file            = null;
        String l_fullPathName    = null;
        String l_newFullPathName = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10700,
                            this,
                            C_METHOD_renameFile + " -- " + BatchConstants.C_ENTERING);
        }

        l_filename     = p_filename;
        l_fullPathName = p_path + File.separator + p_filename;
        l_file         = new File(l_fullPathName);

        if (l_file.exists())
        {
            l_newFullPathName = l_fullPathName.replaceFirst(p_extension, p_newExtension);
            
            if (l_file.renameTo(new File(l_newFullPathName)))
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10710,
                                    this,
                                    C_METHOD_renameFile + " -- SUCCEEDED: " + l_newFullPathName);
                }
                l_filename = p_filename.replaceFirst(p_extension, p_newExtension);
            }      
            else
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_TRACE,
                                    C_CLASS_NAME,
                                    10720,
                                    this,
                                    C_METHOD_renameFile + " -- ***FAILED: " + l_newFullPathName);
                }                
            }
        }            

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10730,
                            this,
                            C_METHOD_renameFile + " -- " + BatchConstants.C_LEAVING + 
                            ", returning filename: " + l_filename);
        }
        return l_filename;
        
    } // End of renameFile(..)
} // End of class MsisdnRoutingDeletionBatchController
