////////////////////////////////////////////////////////////////////////////////
//
//  FILE NAME       :       AutoMsisdnRoutDelBatchWriter
//  DATE            :       03 August 2006
//  AUTHOR          :       Ian James
//  REFERENCE       :       PRD_ASCS00_GEN_CA_098
//
//  COPYRIGHT       :       WM-data 2006
//
//  DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//  CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME      | DESCRIPTION                            | REFERENCE
//----------+-----------+----------------------------------------+--------------
//18/06/08  | M Erskine | Write to temporary files and           | PpacLon#3650/13137
//          |           | rename on completion of batch job      |
//----------+-----------+----------------------------------------+-------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchwriter;

import java.io.File;
import java.io.IOException;
import java.util.Map;
//import java.util.NoSuchElementException;


import com.slb.sema.ppas.batch.batchcommon.BatchFile;
import com.slb.sema.ppas.batch.batchcommon.BatchMessage;
import com.slb.sema.ppas.batch.batchcontroller.AutoMsisdnRoutDelBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AutoMsisdnRoutBatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchSubJobData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
//import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.DatePatch;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for doing the final processing of Automated MSISDN Routing Deletion which
 * writes an input file containing all desconnected subscribers ready to be processed by the exiting 
 * batch MSSIDN Routing Deletion.
 */
public class AutoMsisdnRoutDelBatchWriter extends BatchWriter
{
    //------------------------------------------------------------------------
    // Public class constants
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private class constants
    //------------------------------------------------------------------------
    /** Class name constant used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                                        = "AutoMsisdnRoutDelBatchWriter";

    /** Logger printout - missing property for Recovery File Directory.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY          =
        "Property for OUTPUT_FILE_DIRECTORY not found";

    /** Logger printout - missing property for Control Table Update Frequency.  Value is {@value}. */
    private static final String C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY =
        "Property for CONTROL_TABLE_UPDATE_FREQUENCY not found";

    /** Logger printout - cannot open error- or recovery-file.  Value is {@value}. */
    private static final String C_CANNOT_OPEN_OUTPUT_FILE     = "Cannot open output-file";

    /** File prefix for the output filename. */
    private static final String C_PREFIX_OUTPUT_FILE_NAME     = "MSISDN_DELETE_";
    
    /** File suffix for the output filename. */
    private static final String C_SUFFIX_OUTPUT_FILE_NAME     = ".DAT";
    
    /** File suffix for the temporary output filename. */
    private static final String C_SUFFIX_TEMP_OUTPUT_FILE_NAME = ".TMP";
    
    //------------------------------------------------------------------------
    // Private instance attributes
    //------------------------------------------------------------------------
    /** Gets how many records to be written. Gets from <code>p_params</code>. */
    private int                 i_interval                                          = 0;

    /** Counter for number of precessed records. */
    private int                 i_processed                                         = 0;
    
    /** Output file directory. */
    private String i_outputFileDirectory = null;
    
    //------------------------------------------------------------------------
    // Public constructors
    //------------------------------------------------------------------------
    /**
     * @param p_ppasContext the reference to PpasContext.
     * @param p_logger the reference to Logger.
     * @param p_controller the callback reference to BatchController.
     * @param p_params Start parameters.
     * @param p_outQueue Output queue.
     * @param p_properties The properties defined for this batch.
     */
    public AutoMsisdnRoutDelBatchWriter(PpasContext p_ppasContext,
                                        Logger p_logger,
                                        AutoMsisdnRoutDelBatchController p_controller,
                                        Map p_params,
                                        SizedQueue p_outQueue,
                                        PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_controller, p_params, p_outQueue, p_properties);

        String       l_outputFileFullPath = null;
        StringBuffer l_tmp                = null;
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_START,
                C_CLASS_NAME,
                10010,
                this,
                BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        if ( this.getProperties() )
        {
            //Get the full path filename for the output-file 
            l_tmp = new StringBuffer();
            l_tmp.append(this.i_outputFileDirectory);
            l_tmp.append("/");
            l_tmp.append(C_PREFIX_OUTPUT_FILE_NAME);        
            l_tmp.append(DatePatch.getDateTimeNow().toString_yyyyMMddHHmmss());
            l_tmp.append(C_SUFFIX_TEMP_OUTPUT_FILE_NAME);

            l_outputFileFullPath = l_tmp.toString();

            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_TRACE,
                    C_CLASS_NAME,
                    10020,
                    this,
                    "Output file full path: '" + l_outputFileFullPath + "'" );
            }

            try
            {
                super.openFile(BatchConstants.C_KEY_OUTPUT_FILE_DB_DRIVEN, new File(l_outputFileFullPath));
            }
            catch (IOException e)
            {
                
                sendBatchMessage( BatchMessage.C_STATUS_ERROR );
                i_logger.logMessage( new LoggableEvent( C_CANNOT_OPEN_OUTPUT_FILE,
                                     LoggableInterface.C_SEVERITY_ERROR) );
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_ERROR,
                        C_CLASS_NAME,
                        10030,
                        this,
                        C_CANNOT_OPEN_OUTPUT_FILE + " " + C_METHOD_getProperties );
                }
                e.printStackTrace();
            }
            finally
            {
                if (PpasDebug.on)
                {
                    PpasDebug.print(
                        PpasDebug.C_LVL_VLOW,
                        PpasDebug.C_APP_SERVICE,
                        PpasDebug.C_ST_TRACE,
                        C_CLASS_NAME,
                        10040,
                        this,
                        "Finally - efter openFile..." );
                }
            }    
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(
                PpasDebug.C_LVL_VLOW,
                PpasDebug.C_APP_SERVICE,
                PpasDebug.C_ST_CONFIN_END,
                C_CLASS_NAME,
                10050,
                this,
                BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
        }

    }

    //------------------------------------------------------------------------
    // Public instance methods
    //------------------------------------------------------------------------

    //------------------------------------------------------------------------
    // Private instance methods
    //------------------------------------------------------------------------
    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_writeRecord = "writeRecord";

    /**
     * Does all necessary final processing of the passed record, that is writing to file(s) and/or database.
     * @param p_record The record to process.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException An error was raised during update of Information data.
     */
    protected void writeRecord(BatchRecordData p_record)
            throws IOException, PpasServiceException
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            10360,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_writeRecord);
        }

        AutoMsisdnRoutBatchRecordData l_record = (AutoMsisdnRoutBatchRecordData)p_record;

        //Check if error file info exist and write it to file.
        if (l_record.getOutputLine() != null)
        {
            //Write to output file.
            super.writeToFile(BatchConstants.C_KEY_OUTPUT_FILE_DB_DRIVEN, l_record.getOutputLine());
        }
      
        i_processed++;
        
        if ( (i_processed % i_interval) == 0)
        {
            updateStatus();
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            10370,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_writeRecord);
        }

        return;
    } // End of writeRecord(.)

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_updateStatus = "updateStatus";

    /**
     * Does all necessary final processing that is writing to file(s) and/or database.
     * @throws IOException If it is not possible to write to the file.
     * @throws PpasServiceException No specific keys are anticipated.
     */
    protected void updateStatus()
            throws IOException, PpasServiceException
    {
        BatchSubJobData l_jobData;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            11065,
                            this,
                            "Enter " + C_METHOD_updateStatus);
        }

        super.flushFiles();

        l_jobData = ((AutoMsisdnRoutDelBatchController)super.i_controller).getKeyedControlRecord();
        l_jobData.setLastProcessedCustId("-1");

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            11066,
                            this,
                            "Leaving " + C_METHOD_updateStatus);
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getProperties = "getProperties";    
    /**
     * This method tries to read the properies for:
     * Table update frequency and directories for the input-, error- and recovery-files.
     * @return true if the properties were defined.
     */
    private boolean getProperties()
    {
        String  l_interval    = null;  // Help variable - finding the update frequency intervall
        boolean l_returnValue = true;  // Assume all found
        
        // Get the "update frequency interval"
        l_interval = i_properties.getTrimmedProperty(BatchConstants.C_CONTROL_TABLE_UPDATE_FREQUENCY);
        if ( l_interval != null )
        {
            i_interval = Integer.parseInt(l_interval);
        }
        else
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10390,
                    this,
                    C_PROPERTY_NOT_FOUND_CONTROL_TABLE_UPDATE_FREQUENCY + " " + C_METHOD_getProperties );
            }
        }

        // Get the output file directory
        i_outputFileDirectory = i_properties.getTrimmedProperty(BatchConstants.C_INPUT_FILE_DIRECTORY);
        if ( i_outputFileDirectory == null )
        {
            l_returnValue = false;
            sendBatchMessage( BatchMessage.C_STATUS_ERROR );
            i_logger.logMessage( new LoggableEvent( C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY,
                                 LoggableInterface.C_SEVERITY_ERROR) );
            if (PpasDebug.on)
            {
                PpasDebug.print(
                    PpasDebug.C_LVL_VLOW,
                    PpasDebug.C_APP_SERVICE,
                    PpasDebug.C_ST_ERROR,
                    C_CLASS_NAME,
                    10420,
                    this,
                    C_PROPERTY_NOT_FOUND_OUTPUT_FILE_DIRECTORY + " " + C_METHOD_getProperties );
            }
        }
              
        return l_returnValue;
        
     } // End of method getProperties()

    /** 
     * This batch does not write out trailer records.
     * @see com.slb.sema.ppas.batch.batchwriter.BatchWriter#writeTrailerRecord(java.lang.String)
     * @throws IOException If there is any problem with file operations.
     */
    protected void writeTrailerRecord(String p_outcome) throws IOException
    {
        //This batch does not write out trailer records
        
        //Rename the output file from .TMP to .DAT.
        BatchFile l_outputFile = (BatchFile)i_filePointers.get(BatchConstants.C_KEY_OUTPUT_FILE_DB_DRIVEN);
        
        if (l_outputFile != null)
        {
            l_outputFile.renameFile(BatchConstants.C_PATTERN_TEMP_OUTFILE,
                                    C_SUFFIX_TEMP_OUTPUT_FILE_NAME,
                                    C_SUFFIX_OUTPUT_FILE_NAME);
        }
        else
        {
            throw new IOException("Output file does not exist");
        }
    }
}