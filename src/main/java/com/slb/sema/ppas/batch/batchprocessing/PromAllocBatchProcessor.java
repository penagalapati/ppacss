////////////////////////////////////////////////////////////////////////////////
//      ASCS            :       9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       PromAllocBatchProcessor
//      DATE            :       9-Aug-2004
//      AUTHOR          :       Emmanuel-Pierre Hebe
//      REFERENCE       :       PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT       :       Atos Origin 2004
//
//      DESCRIPTION     :       See javadoc
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.PromAllocBatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AccountDetailBatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasAccountService;
import com.slb.sema.ppas.is.isapi.PpasCustPromoAllocService;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * This class is responsible for identifying accounts with expired promotion allocations and invoking the
 * relevant service to ensure the SDP is updated with the latest allocations.
 */
public class PromAllocBatchProcessor extends BatchProcessor
{
    //---------------------------------------------------------------
    //  Class level constant
    //  -------------------------------------------------------------

    /** Class name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_CLASS_NAME = "PromAllocBatchProcessor";

    /** Field length of the Identifier in the output file record. Value is {@value}. */
    private static final int C_LENGTH_ID     = 15;

    /** Error code from isapi, for nonexisting account, non-master, invalid custid and General error. Value is {@value}. */
    private static final String C_MSISDN_NOT_FOUND                    = "01";

    /** Error code from isapi, for invalid promotion plan and date outside promotion limits. Value is {@value}. */
    private static final String C_PROMOTION_PLAN_ID_NOT_VALID         = "02";
    
    /** Error code from isapi for Promotion allocation overlap, date before today's date, 
     * end date before startdate and date cannot be blank. Value is {@value}. */
    private static final String C_PROMOTION_PLAN_ALLOCATION_NOT_FOUND = "03";
    
    /** Error code from isapi, for Promotion Allocation Synchronisation required. Value is {@value}. */
    private static final String C_PROMOTION_SYNCHRONIZATION_REQUIRED  = "04";
    //-------------------------------------------------------------------------
    // Instance variables
    //-------------------------------------------------------------------------
    /** The <code>Session</code> object. */
    private PpasSession               i_ppasSession = null;
    
    /** The <code>isApi</code> object. */
    private PpasCustPromoAllocService i_isApi       = null;

    /** The <code>BatchController</code> object. */
    private PromAllocBatchController  i_controller  = null;
    
    /**
     * Constructor for the class PromAllocBatchProcessor.
     * 
     * @param p_ppasContext  Session information.
     * @param p_logger       Logger for directing loggable messages.
     * @param p_batchController Batch Controller.
     * @param p_inQueue      Data 'in' queue.
     * @param p_outQueue     Data 'out' queue.
     * @param p_parameters   Additional parameters.
     * @param p_properties   Properties.
     */
    public PromAllocBatchProcessor( PpasContext               p_ppasContext,
                                    Logger                    p_logger,
                                    PromAllocBatchController  p_batchController,
                                    SizedQueue                p_inQueue,
                                    SizedQueue                p_outQueue,
                                    Map                       p_parameters,
                                    PpasProperties            p_properties )
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_parameters, p_properties);
        
        PpasRequest l_ppasRequest;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_START,
                             C_CLASS_NAME,
                             10100,
                             this,
                             BatchConstants.C_CONSTRUCTING + C_CLASS_NAME );
        }

        i_ppasSession  = new PpasSession();
        i_ppasSession.setContext(p_ppasContext);
        l_ppasRequest  = new PpasRequest(i_ppasSession);

        i_isApi        = new PpasCustPromoAllocService(l_ppasRequest, p_logger, p_ppasContext);

        i_controller   = p_batchController;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_CONFIN_END,
                             C_CLASS_NAME,
                             10200,
                             this,
                             BatchConstants.C_CONSTRUCTED );
        }
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";
    /**
     * This method will be called from doRun() passed a record to process. It will call the method
     * addPromo() in the IS API with paramters extraced from the record.
     * 
     * @param   p_record Record to process.
     * @return  BatchRecordData.
     * @throws  PpasServiceException If the request fails.
     */
    protected BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        AccountDetailBatchRecordData  l_record        = null;
        PpasRequest                   l_request       = null;
        String                        l_tmpErrorLine  = null;

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             20100,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }

        // Cast the record into the batch specific record type
        l_record     = (AccountDetailBatchRecordData) p_record;
        if ( l_record == null )
        {
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME,
                                 20199,
                                 this,
                                 "** PROCESSOR** " + BatchConstants.C_LEAVING + C_METHOD_processRecord
                                 + " record was NULL!!!");
            }

            return null;
        }
        
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_TRACE,
                             C_CLASS_NAME,
                             20199,
                             this,
                             "** PROCESSOR** Before try-block with isapi, customer=" +
                             l_record.getCustomerId() + l_record.dumpRecord());
        }

        // Create PpasRequest object
        try
        {
            l_request  = new PpasRequest(i_ppasSession,
                                         i_controller.getSubmitterOpid(),
                                         l_record.getCustomerId());

            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME,
                                 20199,
                                 this,
                                 "** PROCESSOR** Before isApi.provisionBatchPromo, request=" + l_request);
            }

            i_isApi.provisionBatchPromo( l_request, i_isApiTimeout);
            
            if (PpasDebug.on)
            {
                PpasDebug.print( PpasDebug.C_LVL_VLOW,
                                 PpasDebug.C_APP_SERVICE,
                                 PpasDebug.C_ST_TRACE,
                                 C_CLASS_NAME,
                                 20199,
                                 this,
                                 "** PROCESSOR** after calling isapi.provisionBatchPromo");
            }            

        }
        catch ( PpasServiceException p_ppasServiceEx )
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            10030,
                            this,
                            "** PROCESSOR** =====PpasServiceException is caught: ===== " +
                            p_ppasServiceEx.getMessage() + ",  key: " + p_ppasServiceEx.getMsgKey());
                       
            if ( p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_ACCOUNT_NOT_EXISTS) ||
                 p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_NOT_MASTER) ||
                 p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_INVALID_INPUT_CUSTID) ||
                 p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_GENERAL_FAILURE))
            {
                l_tmpErrorLine = getErrorLineInformation( l_request, 
                                                          C_MSISDN_NOT_FOUND );
                
            }
            else if (p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_DATE_OUTSIDE_PROMOTION_LIMITS) ||
                     p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_INVALID_VALUE_PROMO_PLAN))
            {
                l_tmpErrorLine = getErrorLineInformation( l_request,  
                                                          C_PROMOTION_PLAN_ID_NOT_VALID );
            }
            else if (p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_PROMO_ALLOC_OVERLAP_NOT_ALLOWED) ||
                     p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_DATE_BEFORE_TODAYS_DATE) ||
                     p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_END_DATE_BEFORE_START_DATE) ||
                     p_ppasServiceEx.getMsgKey().equals(PpasServiceMsg.C_KEY_DATE_CANNOT_BE_BLANK) )
            {
                l_tmpErrorLine = getErrorLineInformation( l_request,  
                                                          C_PROMOTION_PLAN_ALLOCATION_NOT_FOUND );
            }
            
            else if (p_ppasServiceEx.getMsgKey().equals(
                                     PpasServiceMsg.C_KEY_PROMOTION_SYNCHRONIZATION_REQUIRED))
            {
                l_tmpErrorLine = getErrorLineInformation( l_request,  
                                                          C_PROMOTION_SYNCHRONIZATION_REQUIRED );
            }
            else
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_TRACE,
                                C_CLASS_NAME,
                                10030,
                                this,
                                "** PROCESSOR** PpasServiceException UNKNOWN ERROR !!!");
                
                l_tmpErrorLine = getErrorLineInformation( l_request,  
                                                          p_ppasServiceEx.getMsgKey() );
                l_record.setErrorLine(l_tmpErrorLine );
            }
        }

        l_record.setErrorLine(l_tmpErrorLine );

//        // Store recovery information
//        i_controller.addCustomerInProgress( l_record.getCustomerId() );

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             20300,
                             this,
                             "** PROCESSOR** "+BatchConstants.C_LEAVING + C_METHOD_processRecord +
                             l_record.dumpRecord());
        }
        
        return l_record;
    }

    /** Method name constant used in calls to middleware.  Value is {@value}. */
    private static final String C_METHOD_getErrorLineInformation = "getErrorLineInformation";   
    /** Format a line of output.
     * @param p_request         PpasRequest initialized with the Customer Identifier of this account.
     * @param p_errorCode       Error code.
     * @return String representing the error information.
     * @throws PpasServiceException Error from PpasAccountService.
     */
    private String getErrorLineInformation( PpasRequest p_request, 
                                            String p_errorCode ) throws PpasServiceException
    {
        PpasAccountService i_accountService = new PpasAccountService(p_request,
                                                                     i_logger,
                                                                     i_ppasContext);
        Msisdn l_msisdn    = i_accountService.getBasicData(p_request,0L,0L).getMsisdn();
        String l_tmpMsisdn = l_msisdn.toString();
        
        // Gave a string like:  CC: 44 N(S)N: 0832501001
        // Get the last part. index = 2       
        String[] l_tmpMsisdnParts = l_tmpMsisdn.split(":");
        String   l_currentMsisdn  = l_tmpMsisdnParts[2].trim();

        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_START,
                             C_CLASS_NAME,
                             20100,
                             this,
                             BatchConstants.C_ENTERING + C_METHOD_getErrorLineInformation + 
                             "\n_msisdn=" + l_currentMsisdn + " errorCode+" + p_errorCode);
        }

        // Construct errorLine information.
        // CustomerIdentifier  ErrorCode
        // all fields are fixed length specified in XI_001
        StringBuffer l_tmp = new StringBuffer();
        l_tmp.append(l_currentMsisdn);
        l_tmp.append(BatchConstants.C_EMPTY_MSISDN.substring(0, C_LENGTH_ID - l_currentMsisdn.length()));
        
        l_tmp.append(p_errorCode);
                
        if (PpasDebug.on)
        {
            PpasDebug.print( PpasDebug.C_LVL_VLOW,
                             PpasDebug.C_APP_SERVICE,
                             PpasDebug.C_ST_END,
                             C_CLASS_NAME,
                             20100,
                             this,
                             BatchConstants.C_LEAVING + C_METHOD_getErrorLineInformation+ 
                             " errorline=]" + l_tmp.toString() + "[");
        }

        return l_tmp.toString();
    }
}
