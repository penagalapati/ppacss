////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SCChangeBatchControllerUT.java 
//      DATE            :       Jun 23, 2004
//      AUTHOR          :       Urban Wigstrom
//      REFERENCE       :       PRD_ASCS00_DEV_SS_
//
//      COPYRIGHT       :       WM-data 2005
//
//      DESCRIPTION     :      A unit test of SCChangeBatchController.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of            | <reference>
//          |               | change>                          |
//----------+---------------+----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchcontroller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.slb.sema.ppas.batch.batchcommon.BatchTestCaseTT;
import com.slb.sema.ppas.batch.batchcommon.BatchTestDataTT;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AdditionalInfoData;
import com.slb.sema.ppas.common.dataclass.BasicAccountData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.sql.SqlString;
import com.slb.sema.ppas.common.support.CommonTestCaseTT;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.is.isapi.PpasAdditionalInfoService;
import com.slb.sema.ppas.js.jscommon.JobStatus;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.test.UtilTestCaseTT;

/** A unit test of SCChangeBatchController. */
public class SCChangeBatchControllerUT extends BatchTestCaseTT
{
//  =========================================================================
    // == Private constant(s).                                                ==
    // =========================================================================
    /** The name of the current class to be used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME                = "SCChangeBatchControllerUT";

    /** The name of the Service Class Change batch. */
    private static final String C_SC_CHANGE_BATCH_NAME      = "BatchServiceClassChange";

    /** The name of the test data template file to be used for the succesful insert test case. */
    private static final String C_FILENAME_SUCCESSFUL       = "SC_CHANGE_SUCCESSFUL.DAT";

    /** The name of the test data template file to be used for the test case with an errornous inputfile. */
    private static final String C_FILENAME_ERROR            = "SC_CHANGE_ERRORS.DAT";
    
    /**
     * @param p_name This unit test's class name.
     */
    public SCChangeBatchControllerUT(String p_name)
    {
        super(p_name);
    }
    
    /**
     * Test suite anabling the execution of multiple tests automatically.
     * @return Test.
     */
    public static Test suite()
    {
        return new TestSuite(SCChangeBatchControllerUT.class);
    }

    /**
     * Main method provided for convenience to get the JUnit test framework to
     * run all the tests in this class. 
     *
     * @param p_args not used.
     */
    public static void main(String[] p_args)
    {
        System.out.println("Parameters are: " + p_args);
        junit.textui.TestRunner.run(suite());
    }
    
    // =========================================================================
    // == Public unit test method(s).                                         ==
    // =========================================================================
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testSCChangeBatch_Successful =
        "testSCChangeBatch_Successful";
    /**
     * Performs a fully functional test of a successful 'Service Class change'.
     * A test file with a valid name containing 2 valid Service Class change record is
     * created and placed in the batch indata directory. The Service Class change
     * batch is started in order to process the test file and update the MSISDN table
     *  table, CUST_COMMENTS table
     *
     * @ut.when        A test file containing 2 valid Service Class change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        A new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There wouldn't be any recovery file.
     *                 A report file will be created which will contain a
     *                 success trailing record.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testSCChangeBatch_Successful()
    {
        final int      L_NUMBER_OF_SUCCESSFUL_RECORDS = 2;
        final int      L_NUMBER_OF_ERRORNOUS_RECORDS  = 0;
        final String[] L_REPORT_ROWS = {getTrailerRecord(L_NUMBER_OF_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_ROWS = {};

        beginOfTest(C_METHOD_testSCChangeBatch_Successful);

        SCChangeTestDataTT l_testDataTT =
            new SCChangeTestDataTT(CommonTestCaseTT.c_ppasRequest,
                                       UtilTestCaseTT.c_logger,
                                       CommonTestCaseTT.c_ppasContext,
                                       C_SC_CHANGE_BATCH_NAME,
                                       c_batchInputDataFileDir,
                                       C_FILENAME_SUCCESSFUL,
                                       JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                       BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                       L_NUMBER_OF_SUCCESSFUL_RECORDS,
                                       L_NUMBER_OF_ERRORNOUS_RECORDS);

        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows(L_RECOVERY_ROWS );

        completeFileDrivenBatchTestCase(l_testDataTT);

        endOfTest();
    }


    
    /** The name of the following method. Used for calls to middleware. Value is (@value). */
    private static final String C_METHOD_testSCChangeBatch_Error =
        "testSCChangeBatch_Error";
    /**
     * Performs a fully functional test of a failuer 'service class change'.
     * A test file with a valid name containing 6 invalid service class change record is
     * created and placed in the batch indata directory. The service class change
     * batch is started in order to process the test file and update the MSISDN table,
     * CUST_COMMENTS table
     *
     * @ut.when        A test file containing 6 invalid status change record is
     *                 created and placed in the batch indata directory.
     *
     * @ut.then        2 new record will be inserted in the MSISDN table.
     *                 A new record will be inserted in the BACO_BATCH_CONTROL
     *                 table.
     *                 There will be a recovery file, 6 rows indicating errors.
     *                 A report file will be created which will contain 6 error code rows and
     *                 a trailing record saying that no successful statusChanges have been made.
     *                 The test file will be renamed with a new extension. 
     * 
     * @ut.attributes  +f
     */
    public void testSCChangeBatch_Error()
    {
        final int      L_NUMBER_OF_SUCCESSFUL_RECORDS = 0;
        final int      L_NUMBER_OF_ERRORNOUS_RECORDS  = 6;
        final String[] L_REPORT_ROWS = {"01",   //Invalid record format
                                        "01", 
                                        "02",   //Non-numeric MSISDN
                                        "06",   //Invalid old service class
                                        "07",   //Invalid new service class
                                        "03",   //MSISDN not installed on ASCS
                                        getTrailerRecord(L_NUMBER_OF_SUCCESSFUL_RECORDS)};
        final String[] L_RECOVERY_ROWS = {"1,ERR",
                                          "2,ERR",
                                          "3,ERR",
                                          "4,ERR",
                                          "5,ERR",
                                          "6,ERR"};

        beginOfTest(C_METHOD_testSCChangeBatch_Error);

        SCChangeTestDataTT l_testDataTT =
            new SCChangeTestDataTT(CommonTestCaseTT.c_ppasRequest,
                                       UtilTestCaseTT.c_logger,
                                       CommonTestCaseTT.c_ppasContext,
                                       C_SC_CHANGE_BATCH_NAME,
                                       c_batchInputDataFileDir,
                                       C_FILENAME_ERROR,
                                       JobStatus.C_JOB_EXIT_STATUS_SUCCESS,
                                       BatchConstants.C_BATCH_STATUS_COMPLETED.charAt(0),
                                       L_NUMBER_OF_SUCCESSFUL_RECORDS,
                                       L_NUMBER_OF_ERRORNOUS_RECORDS);
        
        l_testDataTT.defineReportRows( L_REPORT_ROWS );
        l_testDataTT.defineRecoveryRows(L_RECOVERY_ROWS );

        completeFileDrivenBatchTestCase(l_testDataTT);

        endOfTest();
    }
    
    // =========================================================================
    // == Protected method(s).                                                ==
    // =========================================================================
    /**
     * Returns an instance of the <code>SCChangeBatchController</code> class.
     * 
     * @param p_batchTestDataTT  the current batch test data object, which in this case should be
     *                           an instance of the <code>SCChangeTestDataTT</code> class.
     * 
     * @return the actual <code>BatchController</code> object.
     */
    protected BatchController createBatchController(BatchTestDataTT p_batchTestDataTT)
    {
        SCChangeTestDataTT l_testDataTT = null;
        
        l_testDataTT = (SCChangeTestDataTT)p_batchTestDataTT;
        
        return createSCChangeBatchController(l_testDataTT.getTestFilename());
    }
    
    /** Sets up any batch specific requirements. */
    protected void setUp()
    {
        super.setUp();
        
        // Set number of processor threads to one. This is needed to ensure that the data rows are
        // processed in the same order as they are written in the input data file,
        // i.e. to avoid race conditions.
        c_ppasContext.getProperties().setProperty(BatchConstants.C_NUMBER_OF_PROCESSOR_THREADS, "1");
}

    /** Perform standard activities at end of a test. */
    protected void tearDown()
    {
        super.tearDown();
    }    
    
    /**
     * Returns the required additional properties layers for the Service Class change batch.
     * 
     * @return the required additional properties layers for the Service Class change batch.
     */
    protected String getPropertiesLayers()
    {
        return "batch_scc";
    }
    
    // =========================================================================
    // == Private method(s).                                                  ==
    // =========================================================================
    /**
     * Returns an instance of the <code>StatusChangeBatchController</code> class.
     * 
     * @param p_testFilename  the name of the input data test file.
     * 
     * @return an instance of the <code>StatusChangeBatchController</code> class.
     */
    private SCChangeBatchController createSCChangeBatchController(String p_testFilename)
    {
        SCChangeBatchController     l_scChangeBatchController = null;
        HashMap                     l_batchParams                 = null;

        l_batchParams = new HashMap();
        l_batchParams.put(BatchConstants.C_KEY_INPUT_FILENAME, p_testFilename);
        
        try
        {
            l_scChangeBatchController =
                new SCChangeBatchController(CommonTestCaseTT.c_ppasContext,
                                        BatchConstants.C_JOB_TYPE_BATCH_SERVICE_CLASS_CHANGE,
                                        super.getJsJobID(BatchTestCaseTT.C_BATCH_CONTROL_TABLE_PREFIX),
                                        C_CLASS_NAME,
                                        l_batchParams);
        }
        catch (Exception l_Ex)
        {
            sayTime("***ERROR: Failed to create a 'SCChangeBatchController' instance!");
            super.failedTestException(l_Ex);
        }

        return l_scChangeBatchController;
    }
    
    //==========================================================================
    // Inner class(es).
    //==========================================================================
    private class SCChangeTestDataTT extends BatchTestDataTT
    {
        //======================================================================
        // Private constants(s).
        //======================================================================
        /** The test data filename prefix. */
        private static final String C_PREFIX_TEST_DATA_FILENAME = "BATCH_CHG_SERV_CLASS";

        /** The 'MSISDN' template data tag to be replaced by a real (and formatted) MSISDN number. */
        private static final String C_TEMPLATE_DATA_TAG_MSISDN  = "<MSISDN>";

        /** The regular expression used to find the 'MSISDN' template data tag. */
        private static final String C_REGEXP_DATA_TAG_MSISDN    = "^(" + C_TEMPLATE_DATA_TAG_MSISDN + ").*$";

        /** SQL to perform a "false" disconnection - to get errorcode 03. */
        private static final String C_UPDATE_MSISDN_TO_DISCONNECT_IN_PROGRESS =
            "UPDATE cust_mast SET cust_status = 'P' WHERE cust_mobile_number = '<MSISDN>'";

        
       /* *//** SQL to perform a "false" temp blocked - to get errorcode 05. *//*
        private static final String C_UPDATE_MSISDN_TO_TEMP_BLOCKED_IN_PROGRESS =
            "update CULS_CUST_LIFECYCLE_SNAPSHOT set CULS_TEMP_BLOCK_STATUS = 'Y' " +
            "where CULS_CUST_LIFECYCLE_SNAPSHOT.CULS_TEMP_BLOCK_STATUS in " +
            "(select CULS_CUST_LIFECYCLE_SNAPSHOT.CULS_TEMP_BLOCK_STATUS from CULS_CUST_LIFECYCLE_SNAPSHOT, " +
            "cust_mast where cust_mast.cust_id = CULS_CUST_LIFECYCLE_SNAPSHOT.culs_cust_id and " +
            "cust_mast.cust_mobile_number = '<MSISDN>')";*/

        // =====================================================================
        //  Private attribute(s).
        // =====================================================================
        /** The name of the template test file. */
        private String  i_templateTestFilename = null;

        /** The 'MSISDN' template data tag <code>Pattern</code> object. */
        private Pattern i_msisdnDataTagPattern = null;

        // =====================================================================
        //  Constructor(s).
        // =====================================================================
        /**
         * Constructs an <code>SCChangeTestDataTT</code> using the given parameters.
         * 
         * @param p_ppasRequest  The <code>PpasRequest</code> object.
         * @param p_logger       The <code>Logger</code> object.
         * @param p_ppasContext  The <code>PpasContext</code> object.
         */
        private SCChangeTestDataTT(PpasRequest p_ppasRequest,
                                         Logger      p_logger,
                                         PpasContext p_ppasContext,
                                         String      p_batchName,
                                         File        p_batchInputFileDir,
                                         String      p_templateTestFilename,
                                         int         p_expectedJobExitStatus,
                                         char        p_expectedBatchJobControlStatus,
                                         int         p_expectedNoOfSuccessRecords,
                                         int         p_expectedNoOfFailedRecords)
        {
            super(p_ppasRequest,
                  p_logger,
                  p_ppasContext,
                  p_batchName,
                  p_batchInputFileDir,
                  p_expectedJobExitStatus,
                  p_expectedBatchJobControlStatus,
                  p_expectedNoOfSuccessRecords,
                  p_expectedNoOfFailedRecords);

            i_templateTestFilename  = p_templateTestFilename;

            // Create 'Pattern' object for template data tags.
            // NOTE: The data tag will be placed in matcher group 1 if it is found in the template record.
            //       See method '' for the usage of the 'Pattern' object.
            i_msisdnDataTagPattern = Pattern.compile(C_REGEXP_DATA_TAG_MSISDN);
            
        }


        //==========================================================================
        // Protected method(s).
        //==========================================================================
        

        /**
         * Creates a test file.
         * @throws PpasServiceException  if it fails to create a test file.
         */
        protected void createTestFile() throws PpasServiceException, IOException
        {
            super.createTestFile(C_PREFIX_TEST_DATA_FILENAME,
                                 BatchTestDataTT.C_SUFFIX_ORDINARY_TEST_DATA_FILENAME,
                                 i_templateTestFilename,
                                 C_CLASS_NAME);
        }


        /**
         * Replaces any found data tag in the given data string by real data and returns the resulting string.
         * 
         * @param p_dataRow  the data string.
         * 
         * @return  the resulting string after data tags have been replaced by real data.
         */
        protected String replaceDataTags(String p_dataRow)
        {
            final int    L_DATA_TAG_GROUP_NUMBER = 1;
            final String L_SPECIAL_MSISDN_TAG    = "<MSISDN2>";

            StringBuffer l_dataRowSB             = null;
            Matcher      l_matcher               = null;
            String       l_formattedMsisdn       = null;
            int          l_tagBeginIx            = 0;
            int          l_tagEndIx              = 0;
            
            String       l_tmpRow                = p_dataRow;
            int          l_index                 = 0;
            
            
            say("**START replaceDataTags : ]"+p_dataRow+"[");
            if (p_dataRow != null)
            {
                if ( p_dataRow.indexOf( L_SPECIAL_MSISDN_TAG )==0 )
                {
                    l_tmpRow = p_dataRow.replaceAll( "2", "" );
                    l_index  = 1;
                    say("**MTR** special row: ]"+l_tmpRow+"[");
                }
                l_dataRowSB = new StringBuffer(l_tmpRow);
                l_matcher   = i_msisdnDataTagPattern.matcher(l_tmpRow);
                if (l_matcher.matches())
                {
                    l_tagBeginIx      = l_matcher.start(L_DATA_TAG_GROUP_NUMBER);
                    l_tagEndIx        = l_matcher.end(L_DATA_TAG_GROUP_NUMBER);
                    l_formattedMsisdn = getFormattedMsisdn(l_index);
                    l_dataRowSB.replace(l_tagBeginIx, l_tagEndIx, l_formattedMsisdn);
                }
            }

            return l_dataRowSB.toString();
        }


        /**
         * Verifies that the ASCS database has been properly updated.
         * 
         * @throws PpasServiceException if it fails to get the ASCS database info.
         */
        protected void verifyAscsDatabase() throws PpasServiceException
        {
            say("*** start verify database ***");
            PpasAdditionalInfoService l_additionalInfoServ = null;
            AdditionalInfoData        l_additionalInfoData = null;
            BasicAccountData          l_basicAccountData   = null;

            l_additionalInfoServ = new PpasAdditionalInfoService( super.i_ppasRequest, 
                                                                  super.i_logger, 
                                                                  super.i_ppasRequest.getContext() );
            for ( int i = 0; i < getNumberOfTestSubscribers(); i++ )
            {
                l_basicAccountData = (BasicAccountData)super.getSubscriberData(i);
                say("########### MTR - verifyAscsDatabase ############### i: " + i + " , l_basicAccountData=" + l_basicAccountData);
                super.i_ppasRequest.setBasicAccountData( l_basicAccountData );
                l_additionalInfoData = l_additionalInfoServ.getAdditionalInfo( super.i_ppasRequest, 30000L );
                
                sayTime("l_additionalInfoData = [" + l_additionalInfoData + "]");
            }
            
            say("*** end verify database ***");
        }

        /**
         * Verifies that the report file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyReportFile() throws IOException
        {
            say("*** start verifyReportFile *** ");
            
            Vector l_reportData = readNewFile(c_batchReportDataFileDir, i_testFilename, "DAT", "RPT");
            
            say("***verifyReportFile*** expected no success= " + i_expectedNoOfSuccessRecords + "expected no failed= " + i_expectedNoOfFailedRecords);
            if ( i_expectedNoOfFailedRecords == 0 )
            {
                // SUCCESS expected!
                // All successful - only one row (the trailing record) in the result file
                assertEquals("***verifyReportFile*** Wrong number of rows in the report file.", 1, l_reportData.size());
                
                String l_tmpTrailer = i_expectedReportData[0];
                say("***verifyReportFile*** **MTR** first element: ]"+ (((String)l_reportData.firstElement())+"[ expected : ]"+(l_tmpTrailer))+"[") ;

                assertTrue("***verifyReportFile*** Trailer string not as expected : ", ((String)l_reportData.firstElement()).equals(l_tmpTrailer));
            }
            else
            {
                say("###verifyReportFile### report file has failed records..");
                
                int l_noGeneratedReportRows      = l_reportData.size();  // Rows with error codes and trailing record
                int l_noExpectedRowsInReportFile = i_expectedReportData.length;
                assertTrue("Report file has wrong number of rows expected:"+l_noExpectedRowsInReportFile+
                           " found:"+l_noGeneratedReportRows, l_noGeneratedReportRows==l_noExpectedRowsInReportFile);
                for ( int i = 0; i < l_noExpectedRowsInReportFile-1; i++ )
                {
                    String l_errorCode = ((String)l_reportData.get(i)).substring(0,2);
                    say("###verifyReportFile### i:"+i+"row in reportFile :"+l_errorCode+
                               " found :"+i_expectedReportData[i]);
                    assertTrue("Unexpected row in reportFile :"+l_errorCode+
                               " but was :"+i_expectedReportData[i], l_errorCode.equals(i_expectedReportData[i]));
                }
            }
            say("***verifyReportFile*** *** end verifyReportFile ***");
        }
        
        /**
         * Verifies that the recovery file has been created and contains the right information.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */

        protected void verifyRecoveryFile() throws IOException
        {
            Vector l_recoveryData = null;
            say("*** start verifyRecoveryFile *** ");
            try
            {
                l_recoveryData = readNewFile(c_batchRecoveryDataFileDir, i_testFilename, "DAT", "RCV");
                
                say("expected no success= " + i_expectedNoOfSuccessRecords + "expected no failed= " + i_expectedNoOfFailedRecords);
                if ( i_expectedNoOfFailedRecords == 0 )
                {
                    // File should not exist
                    // SUCCESS expected!
                    // All successful - only one row in the result file
                    say("**MTR no recovery file should be found size:"+l_recoveryData.size());
                    assertEquals("No recovery record should be found.", 0, l_recoveryData.size());                  
                }
                else
                {
                    say("**MTR** failure records created");
                    for ( int i = 0; i < l_recoveryData.size(); i++)
                    {
                        String l_found    = (String)l_recoveryData.get(i);
                        String l_expected = i_expectedRecoveryData[i];
                        say("found :    "+l_found);
                        say("expected : "+l_expected);
                        
                        assertTrue("VerifyRecoveryFile failed, found:"+l_found+
                                   " expected:"+l_expected, l_found.equals(l_expected));
                    }                   
                }
            }
            catch (IOException i_exep)
            {
                say("IOEXCEPTION caught "+i_exep.getMessage());
            }
            finally
            {
                say("FINALLY - recovery file has been checked");
            }
            say("*** end verifyRecoverfyFile ***");
        }

        /**
         * Verifies that the input file has been renamed.
         * 
         * @throws IOException if it fails to get the ASCS database info.
         */
        protected void verifyRenaming() throws IOException
        {
            say("*** start verifyRenaming *** ");

            File   l_file             = null;
            String l_newInputFileName = constructFileName( c_batchInputDataFileDir, 
                                                           i_testFilename, 
                                                           "DAT", 
                                                           "DNE" );         
            l_file = new File(l_newInputFileName);
            
            if (l_file.exists())
            {
                say("FILE EXIST ***" + l_file);
            }
            else
            {
                say( "File DOES NOT EXIST : " + l_file);
                throw new IOException();                
            }
            say("*** end verifyRenaming ***");
        }
        

        //==========================================================================
        // Private method(s).
        //==========================================================================
        private String getTestFilename()
        {
            return super.i_testFilename;
        }
        
        /**
         * Installs test subscribers.
         * @throws PpasServiceException  if it fails to install test subscribers.
         */
        protected void installTestSubscribers() throws PpasServiceException
        {
            BasicAccountData l_subscriber = null;
            Msisdn           l_msisdn     = null;
            String           l_delete     = null;
            String           l_tmpMsisdn  = null;
            
            // Create 2 standalone subscribers 
            l_subscriber = installGlobalTestSubscriber(null);
            addSubscriberData(l_subscriber);
            l_subscriber = installGlobalTestSubscriber(null);
            addSubscriberData(l_subscriber);
            
            // Update the second MSISDN so that the batch treat it as an already disconnected one.
            l_msisdn     = l_subscriber.getMsisdn();
            l_delete = C_UPDATE_MSISDN_TO_DISCONNECT_IN_PROGRESS.replaceAll(C_TEMPLATE_DATA_TAG_MSISDN,
                                                                                l_msisdn.toString());
            say("**MTR** SQL: ]" + l_delete + "[");
            
            l_tmpMsisdn  = getFormattedMsisdn(1).trim();
            l_delete = C_UPDATE_MSISDN_TO_DISCONNECT_IN_PROGRESS.replaceAll(C_TEMPLATE_DATA_TAG_MSISDN, 
                                                                                l_tmpMsisdn);
            say("**MTR** SQL using getFormattedMsisdn: ]" + l_delete + "[");
            
            int noRowsUpdated = sqlUpdate(new SqlString(l_delete.length(),0,l_delete));
            say("noRowsUpdated: " + noRowsUpdated);
        }

    } // End of inner class 'SCChangeTestDataTT'.
}
