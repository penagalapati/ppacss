////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME       :       SubSegmentationChgBatchReader.java
//      DATE            :       21 June 2007
//      AUTHOR          :       Ian James
//      REFERENCE       :       PRD_ASCS00_GEN_CA_129
//
//      COPYRIGHT       :       WM-data 2007
//
//      DESCRIPTION     :       Creates and stores data records representing subscriber 
//                              accounts for Subscriber Segmentation Change.
//
////////////////////////////////////////////////////////////////////////////////
//                              CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                      | REFERENCE
//----------+---------------+----------------------------------+-----------------
// 04/09/07 | Ian James     | Review comment changes.          | PpacsLon#3329/12047
//----------+---------------+----------------------------------+-----------------
// 24/06/08 | M Erskine | Change i_timeout to read the JDBC    | PpacLon#3650/13158
//          |           | connection timeout from the JsContext|
//          |           |  instead of just hard-coding it.     |
//----------+-----------+--------------------------------------+----------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchreader;

import java.text.ParseException;
import java.util.Map;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.AccountGroupId;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.ServiceOfferings;
import com.slb.sema.ppas.common.dataclass.SubSegChgBatchRecordData;
import com.slb.sema.ppas.common.dataclass.SubscriberLifecycleSnapshotData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceFailedException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.common.support.PpasRequest;
import com.slb.sema.ppas.common.support.PpasSession;
import com.slb.sema.ppas.is.isapi.PpasSnapshotService;
import com.slb.sema.ppas.is.isil.batchisilservices.SubSegChgDbBatchService;
import com.slb.sema.ppas.util.logging.LoggableEvent;
import com.slb.sema.ppas.util.logging.LoggableInterface;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
* This <code>SubSegmentationChgBatchReader</code> class is responsible for providing 
* data records representing subscriber accounts for subscriber segmentation change.
*/
public class SubSegmentationChgBatchReader extends BatchLineFileReader
{
  //-----------------------------------------------------
  //  Class level constant.
  //------------------------------------------------------ 

  /** Class name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_CLASS_NAME                              = "SubSegmentationChgBatchReader";

  /** Error code. Value is {@value}. */
  private static final String C_ERROR_INVALID_RECORD_FORMAT             = "01";

  /** Error code. Value is {@value}. */
  private static final String C_ERROR_NON_NUMERIC_MSISDN                = "02";

  /** Constant holding the error code for inactive account. Value is {@value}. */
  private static final String   C_ERROR_CODE_INACTIVE_ACCOUNT           = "03";
  
  /** Constant holding the error code for invalid new account group id. Value is {@value}. */
  private static final String C_ERROR_CODE_INVALID_NEW_ACCOUNT_GROUP_ID = "05";

  /** Constant holding the error code for invalid new service offering. Value is {@value}. */
  private static final String C_ERROR_CODE_INVALID_NEW_SERV_OFFERINGS   = "06";

  /** Constant holding the error code for invalid old account group id. Value is {@value}. */
  private static final String C_ERROR_CODE_INVALID_OLD_ACCOUNT_GROUP_ID = "08";

  /** Constant holding the error code for invalid old service offering. Value is {@value}. */
  private static final String C_ERROR_CODE_INVALID_OLD_SERV_OFFERINGS   = "09";

    /** Field length of MSISDN to be installed. Value is {@value}. */
  private static final int    C_MSISDN_LENGTH                           = 15;

  /** Field length of old service class to be installed. Value is {@value}. */
  private static final int    C_OLD_ACC_GROUP_ID_LENGTH                 = 10;   

  /** Field length of new service class to be installed. Value is {@value}. */
  private static final int    C_NEW_ACC_GROUP_ID_LENGTH                 = 10;
  
  /** Field length of old service class to be installed. Value is {@value}. */
  private static final int    C_OLD_SERV_OFFERINGS_LENGTH               = 10;   

  /** Field length of new service class to be installed. Value is {@value}. */
  private static final int    C_NEW_SERV_OFFERINGS_LENGTH               = 10;
  
  /**The maximum allowed length of a record line in the file. */
  private static final int    C_EXPECTED_RECORD_LENGTH                  = C_MSISDN_LENGTH + 
                                                                          C_OLD_ACC_GROUP_ID_LENGTH +
                                                                          C_NEW_ACC_GROUP_ID_LENGTH +  
                                                                          C_OLD_SERV_OFFERINGS_LENGTH +
                                                                          C_NEW_SERV_OFFERINGS_LENGTH;

  /** Index for the field MSISDN in the batchDataRecord array. Value is {@value}. */
  private static final int    C_MSISDN                                  = 0;

  /** Index for the field Old Account Group ID in the batchDataRecord array. Value is {@value}. */
  private static final int    C_OLD_ACC_GRP_ID                          = 1;

  /** Index for the field New Account Group ID in the batchDataRecord array. Value is {@value}. */
  private static final int    C_NEW_ACC_GRP_ID                          = 2;

  /** Index for the field Old Service Offering in the batchDataRecord array. Value is {@value}. */
  private static final int    C_OLD_SERV_OFF                            = 3;

  /** Index for the field New Service Offering in the batchDataRecord array. Value is {@value}. */
  private static final int    C_NEW_SERV_OFF                            = 4;
  
  // Index in the batch record array
  /** Number of fields in the batchDataRecord array. Value is {@value}. */
  private static final int    C_NUMBER_OF_FIELDS                        = 5;

  //-------------------------------------------------------------------------
  // Instance variables.
  //-------------------------------------------------------------------------

  /** The ISIL service. */
  private SubSegChgDbBatchService  i_dbService                          = null;

  /** The BatchDataRecord to be sent into the Queue. */
  private SubSegChgBatchRecordData i_batchDataRecord                    = null;

  /** IS API service to retrieve subscriber lifecycle snapshots. */
  private PpasSnapshotService      i_ppasSnapshotService                 = null;

  /** The batch controller object. */
  protected BatchController        i_batchController                     = null;

  /** The current <code>Session</code> object reference. */
  private PpasSession              i_ppasSession                         = null;

  //-------------------------------------------------------------------------
  // Constructors.
  //-------------------------------------------------------------------------
  /**
   * Constructs a <code>SubSegmentationChgBatchReader</code> instance with the specified parameters.
   * 
   * @param p_ppasContext     a <code>PpasContext</code> object that holds context info for the current
   *                          process.
   * @param p_logger          the <code>Logger</code> object used by the current process.
   * @param p_controller      the <code>BatchController</code> reference.
   * @param p_inQueue         the queue into which the data records shall be stored.
   * @param p_parameters      a <code>Map</code> object that holds the necessary parameters for this batch.
   * @param p_ppasProperties  a <code>PpasProperties</code> object that holds the properties for the current
   *                          process.
   */
  public SubSegmentationChgBatchReader(PpasContext     p_ppasContext,
                                       Logger          p_logger,
                                       BatchController p_controller,
                                       SizedQueue      p_inQueue,
                                       Map             p_parameters,
                                       PpasProperties  p_ppasProperties)
  {
      super(p_ppasContext, p_logger, p_controller, p_inQueue, p_parameters, p_ppasProperties);

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_START,
                          C_CLASS_NAME, 10000, this, BatchConstants.C_CONSTRUCTING + C_CLASS_NAME);
      }

      i_ppasSession = new PpasSession();
      i_ppasSession.setContext(p_ppasContext);
      PpasRequest l_ppasRequest = new PpasRequest(i_ppasSession);
      i_ppasSnapshotService = new PpasSnapshotService(l_ppasRequest, i_logger, p_ppasContext);

      i_batchController = p_controller;
      
      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_CONFIN_END,
                          C_CLASS_NAME, 10010, this, BatchConstants.C_CONSTRUCTED + C_CLASS_NAME);
      }
  }

  /**
   * Get method for the created BatchDataRecord.
   * @return BatchDataRecord
   */
  public BatchRecordData getBatchDataRecord()
  {
      return i_batchDataRecord;
  }

  //-------------------------------------------------------------------------
  // Protected instance methods.
  //-------------------------------------------------------------------------
  
  /** Method name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_METHOD_getRecord = "getRecord";    
  /**
   * This method shall contain all logic necessary to create 
   * and populate the BatchDataRecord record.
   * 
   * @return - null if "No more records to read"
   */
  protected BatchRecordData getRecord()
  {
      i_batchDataRecord = null;

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                          C_CLASS_NAME, 10020, this, BatchConstants.C_ENTERING + C_METHOD_getRecord );
      }

      // Get next line from input file
      if ( readNextLine() != null )
      {
          i_batchDataRecord = new SubSegChgBatchRecordData();

          // Store input filename
          i_batchDataRecord.setInputFilename(this.i_inputFileName);
          
          // Store line-number from the file
          i_batchDataRecord.setRowNumber(this.i_lineNumber);

          // Store original data record
          i_batchDataRecord.setInputLine(this.i_inputLine);

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                              C_CLASS_NAME, 10030, this, "getRecord : extractLineAndValidate" );
          }

          // Extract data from line
          if (!extractLineAndValidate())
          {
             // Line is corrupt - invalid record
              if (PpasDebug.on)
              {
                  PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                  C_CLASS_NAME, 10040, this, "extractLineAndValidate failed!" );
              }

              i_batchDataRecord.setCorruptLine(true);
              
              if ( i_batchDataRecord.getErrorLine() == null )
              {
                  // Other error than specific field errors
                  i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT +
                                                 BatchConstants.C_DELIMITER_REPORT_FIELDS + 
                                                 i_batchDataRecord.getInputLine());
              }
          }
          
          i_batchDataRecord.setNumberOfSuccessfullyProcessedRecords(i_successfullyProcessed);
          i_batchDataRecord.setNumberOfErrorRecords(i_notProcessed);

          if (faultyRecord(i_lineNumber))
          {
              i_batchDataRecord.setFailureStatus(true);
          }

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                              C_CLASS_NAME, 10050, this, "After setNumberOfSuccessfully...." + i_successfullyProcessed);
          }
      } // end if ... nextLine != null

      if (PpasDebug.on)
      {
          if (i_batchDataRecord != null )
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                              C_CLASS_NAME, 10060, this, "DumpRecord: " + i_batchDataRecord.dumpRecord());
          }
          else
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                              C_CLASS_NAME, 10070, this,  "i_batchDataRecord is NULL!!!!!");
          }            

          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                          C_CLASS_NAME, 10080, this, BatchConstants.C_LEAVING + C_METHOD_getRecord);
      }

      return i_batchDataRecord;
      
  } // end of method getRecord()  
  
  /** Method name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_METHOD_tidyUp = "tidyUp";    
  /**
   * Initialises this <code>SubSegmentationBatchReader</code> instance.
   */
  protected void tidyUp()
  {
      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                          C_CLASS_NAME, 10090, this, BatchConstants.C_ENTERING + C_METHOD_tidyUp);
      }
      
      super.tidyUp();

      if (i_dbService != null)
      {
          try
          {
              i_dbService.close();
          }
          catch (Exception p_exe)
          {
              // Failed to close the ISIL db service.
              // Any specific handling?
              p_exe = null;
          }

          i_dbService = null;
      }

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                          C_CLASS_NAME, 10100, this, BatchConstants.C_LEAVING + C_METHOD_tidyUp);
      }
  }

  //-------------------------------------------------------------------------
  // Private instance methods.
  //-------------------------------------------------------------------------
  
   /** Method name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_METHOD_isFileNameValid = "isFileNameValid";            
  /**
   * Check if filename is of the valid form.
   * @param p_fileName Filename to be tested.
   * @return <code>true</code> if filename is valid else <code>false</code>.
   */
  protected boolean isFileNameValid( String p_fileName )
  {
      boolean  l_validFileName          = false;   // Help variable - return value. Assume filename is OK.

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                          C_CLASS_NAME, 10110, this, BatchConstants.C_ENTERING + C_METHOD_isFileNameValid );
      }

      if (p_fileName != null && 
         (p_fileName.matches(BatchConstants.C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_DAT) ||
          p_fileName.matches(BatchConstants.C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_SCH) ||
          p_fileName.matches(BatchConstants.C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_IPG)))
      {
          l_validFileName = true;
      }
      
      // Check if recovery mode
      if ( l_validFileName && !this.i_recoveryMode )
      {
          // Flag that processing is in progress. Rename the file to *.IPG.
          // If the batch is running in recovery mode, it already has the extension .IPG
          if (p_fileName.matches(BatchConstants.C_PATTERN_SUBSCRIBER_SEGMENTATION_FILENAME_SCH))
          {
              renameInputFile( BatchConstants.C_PATTERN_BOI_INDATA_FILE,
                               BatchConstants.C_EXTENSION_BOI_INDATA_FILE,
                               BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
          }
          else
          {
              renameInputFile( BatchConstants.C_PATTERN_INDATA_FILE,
                               BatchConstants.C_EXTENSION_INDATA_FILE,
                               BatchConstants.C_EXTENSION_IN_PROGRESS_FILE );
          }
          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                              C_CLASS_NAME, 10120, this, "After rename, inputFullPathName=" + this.i_fullPathName );
          }              
      }

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                          C_CLASS_NAME, 10130, this, BatchConstants.C_LEAVING + C_METHOD_isFileNameValid );
      }

      return l_validFileName;
  }
  
  /** Method name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_METHOD_extractLineAndValidate = "extractLineAndValidate";    

  /**
   * This method extracts information from the batch data record. The record must look like:
   * [MSISDN][MSISDN][SDP ID][Agent][Promotion Plan][Account Group][Service Offering]
   * The line number will be used to check against the recovery file if the batch mode is
   * recovery.
   * All fields are checked to be alpha numberic.
   * @return <code>true</code> when extracted fields are valid else <code>false</code>.
   */
  private boolean extractLineAndValidate()
  {
      MsisdnFormat l_msisdnFormat = null;
      Msisdn       l_tmpMsisdn    = null; // Help variable to convert from string to Msisdn object.
      String[]     l_recordFields = new String[C_NUMBER_OF_FIELDS]; // Help array to keep the input line elements
      SubscriberLifecycleSnapshotData l_subLifeSnapshotData = null;
      boolean l_validData = true; // return value - assume it is valid

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                          C_CLASS_NAME, 10140, this, BatchConstants.C_ENTERING + C_METHOD_extractLineAndValidate);
      }

      //Check max record length.
      if (i_inputLine.length() != C_EXPECTED_RECORD_LENGTH)
      {
          i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                  + BatchConstants.C_DELIMITER_REPORT_FIELDS + i_inputLine);

          l_validData = false;

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                              C_CLASS_NAME, 10150, this,
                              "Row: ]" + i_inputLine 
                                       + "[ wrong record length " + " l_lineNumber= " + i_lineNumber
                                       + " " + i_batchDataRecord.getErrorLine() + " length= "
                                       + i_inputLine.length());
          }
      }
      else
      {
          initializeRecordValidation(i_inputLine, l_recordFields, i_batchDataRecord);

          if (PpasDebug.on)
          {
              PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                              C_CLASS_NAME, 10160, this, "ExtractLineAndValidate IF getfields.....]"+i_inputLine+"[");
          }

          // Get the record fields and check if it is numeric/alphanumeric
          // Start End #Field Format check Errorcode
          //           Start                              End                          #Field            Format check                                     Errorcode
          if (getField(0,                                 C_MSISDN_LENGTH,             C_MSISDN,         BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_NON_NUMERIC_MSISDN )   &&
              getFieldNoTrim(C_MSISDN_LENGTH,             C_OLD_ACC_GROUP_ID_LENGTH,   C_OLD_ACC_GRP_ID, BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_NON_NUMERIC_MSISDN )   &&
              getFieldNoTrim(C_OLD_ACC_GROUP_ID_LENGTH,   C_NEW_ACC_GROUP_ID_LENGTH,   C_NEW_ACC_GRP_ID, BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_INVALID_RECORD_FORMAT) &&
              getFieldNoTrim(C_NEW_ACC_GROUP_ID_LENGTH,   C_OLD_SERV_OFFERINGS_LENGTH, C_OLD_SERV_OFF,   BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_INVALID_RECORD_FORMAT) &&
              getFieldNoTrim(C_OLD_SERV_OFFERINGS_LENGTH, C_NEW_SERV_OFFERINGS_LENGTH, C_NEW_SERV_OFF,   BatchConstants.C_PATTERN_RIGHT_ADJUSTED_NUMERIC, C_ERROR_INVALID_RECORD_FORMAT))
          {
              // MSISDN is mandatory
              if (l_recordFields[C_MSISDN] == null)
              {
                  // Field not specified
                  i_batchDataRecord.setErrorLine(C_ERROR_INVALID_RECORD_FORMAT
                                                 + BatchConstants.C_DELIMITER_REPORT_FIELDS + i_inputLine);
                                                 i_batchDataRecord.setCorruptLine(true);
                                           
                  l_validData = false;
                                       
                  if (PpasDebug.on)
                  {
                      PpasDebug.print(PpasDebug.C_LVL_MODERATE, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                      C_CLASS_NAME, 10170, this,
                                      "Row: ]" + i_inputLine 
                                               + "[ is an INVALID record" + " l_lineNumber   ="
                                               + i_lineNumber + "\n" + " l_msisdn   ="
                                               + l_recordFields[C_MSISDN] + "\n"
                                               + " l_oldAccountGroup   ="
                                               + l_recordFields[C_OLD_ACC_GRP_ID] + "\n"
                                               + " l_newAccountGroup   ="
                                               + l_recordFields[C_NEW_ACC_GRP_ID] + "\n"
                                               + " l_oldServiceOffering   ="
                                               + l_recordFields[C_OLD_SERV_OFF] + "\n"
                                               + " l_newServiceOffering   ="
                                               + l_recordFields[C_NEW_SERV_OFF]);
                  }
              }
          }
          else
          {
              // Illegal field format detected
              if (PpasDebug.on)
              {
                  PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                  C_CLASS_NAME, 10180, this,
                                  "Row: ]" + i_inputLine 
                                           + "[ is an INVALID record" + " l_lineNumber   ="
                                           + i_lineNumber + "\n" + " l_msisdn   ="
                                           + l_recordFields[C_MSISDN] + "\n"
                                           + " l_oldAccountGroup   ="
                                           + l_recordFields[C_OLD_ACC_GRP_ID] + "\n"
                                           + " l_newAccountGroup   ="
                                           + l_recordFields[C_NEW_ACC_GRP_ID] + "\n"
                                           + " l_oldServiceOffering   ="
                                           + l_recordFields[C_OLD_SERV_OFF] + "\n"
                                           + " l_newServiceOffering   ="
                                           + l_recordFields[C_NEW_SERV_OFF]);
              }

              // Flag for corrupt line
              l_validData = false;
          }
      }
      // Store information into the BatchDataRecord
      // If some of the fields were invalid their value will be NULL.

      // Convert the MSISDN string (master and subordinate) to a Msisdn object
      // This may give an ParseException - if so flag currupt line!
      if (l_validData)
      {
          l_msisdnFormat = this.i_context.getMsisdnFormatInput();
          try
          {
              l_tmpMsisdn = l_msisdnFormat.parse(l_recordFields[C_MSISDN]);
              i_batchDataRecord.setMsisdn(l_tmpMsisdn);
          }
          catch (ParseException e)
          {
              l_validData = false;

              i_logger.logMessage( new LoggableEvent("Could convert MSISDN string to a MSISDN object" + e.getMessage(),
            		                                 LoggableInterface.C_SEVERITY_ERROR) );

              if (PpasDebug.on)
              {
                  PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                  C_CLASS_NAME, 10190, this, "Row: " + i_lineNumber
                                                                     + " ]"
                                                                     + i_inputLine
                                                                     + "[ has invalid MSISDN");
              }
          }
      }

      if ((l_validData) &&
          (l_recordFields[C_OLD_ACC_GRP_ID] != null ||
           l_recordFields[C_OLD_SERV_OFF] != null ))

      {
          String l_opId = i_batchController.getSubmitterOpid();

          PpasRequest l_ppasRequest = new PpasRequest(i_ppasSession, l_opId, l_tmpMsisdn);

          try
          {
              l_subLifeSnapshotData = i_ppasSnapshotService.readSnapshot(l_ppasRequest, i_timeout);

          }
          catch (PpasServiceException Se)
          {
              l_validData = false;

              //If not available/non-existent MSISDN, set error line in batch record.
              if (Se.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_AVAILABLE)
                      || Se.getMsgKey().equals(PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS)
                      || Se.getMsgKey().equals(PpasServiceMsg.C_KEY_DISCONNECTED_ACCOUNT))
              {
                  i_batchDataRecord.setErrorLine(C_ERROR_CODE_INACTIVE_ACCOUNT +
                                                 BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                                 i_inputLine);
                  
              }
              
              i_logger.logMessage( new LoggableEvent("Unable to read snapshot for the requested subscriber"
                                                     + Se.getMessage(), LoggableInterface.C_SEVERITY_ERROR) );

              if (PpasDebug.on)
              {
                  PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                                  C_CLASS_NAME, 10195, this, "Row: " + i_lineNumber
                                                                     + " ]"
                                                                     + i_inputLine
                                                                     + "[ Unable to read "
                                                                     + "Old Group ID/Service Offerings ");
              }
          }
      }
     
      if ((l_validData) &&
          (l_recordFields[C_OLD_ACC_GRP_ID] !=  null))
      {
          if (!validateOldAccountGroup(l_recordFields, l_subLifeSnapshotData))
          {
              l_validData = false;
          }
      }    

      if ((l_validData) &&
          (l_recordFields[C_NEW_ACC_GRP_ID] !=  null))
      {
          if (!validateNewAccountGroup(l_recordFields))
          {
              l_validData = false;
          }
      }

      if ((l_validData) &&
          (l_recordFields[C_OLD_SERV_OFF] != null))
      {
          if (!validateOldServiceOfferings(l_recordFields, l_subLifeSnapshotData))
          {
              l_validData = false;
          }
      }

      if ((l_validData) &&
          (l_recordFields[C_NEW_SERV_OFF] != null))
      {
          if (!validateNewServiceOfferings(l_recordFields))
          {
              l_validData = false;
          }
      }

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                          C_CLASS_NAME, 10200, this, BatchConstants.C_LEAVING + C_METHOD_extractLineAndValidate);
      }

      return l_validData;

  } // end of method extractLineAndValidate(.)

  /** Method name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_METHOD_validateOldAccountGroup = "validateOldAccountGroup";    

  /**
   * This method validates the Old Account Group Id supplied in the input file.
   * @param p_recordFields The array to keep the input line elements.
   * @param p_subLifeSnapshotData The MSISDN being processed.
   * @return <code>true</code> when extracted fields are valid else <code>false</code>.
   */
  private boolean validateOldAccountGroup(String [] p_recordFields, SubscriberLifecycleSnapshotData p_subLifeSnapshotData)
  {
      boolean l_validOldGroup= true;

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                          C_CLASS_NAME, 10210, this, BatchConstants.C_ENTERING + C_METHOD_validateOldAccountGroup);
      }

      // If we have an old account group supplied in the file then we must 
      // also have a new account group supplied.
      if (p_recordFields[C_NEW_ACC_GRP_ID] == null ||
          p_recordFields[C_NEW_ACC_GRP_ID].trim().equals(""))
      {
          i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_NEW_ACCOUNT_GROUP_ID +
                                         BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                         i_inputLine);
          l_validOldGroup = false;
      }

      // If we have a new account group supplied, then validate the old account group.
      if ((l_validOldGroup) &&
          (p_recordFields[C_OLD_ACC_GRP_ID].length() < C_OLD_ACC_GROUP_ID_LENGTH))
      {
          i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_OLD_ACCOUNT_GROUP_ID +
                                         BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                         i_inputLine);

          l_validOldGroup = false;
      }
      else
      {
          //Compare old account group supplied in the file with account group in database.
          if (p_subLifeSnapshotData != null)
          {
              AccountGroupId l_accountGrpId = null;
              try
              {
                  l_accountGrpId = new AccountGroupId(p_recordFields[C_OLD_ACC_GRP_ID].trim());
              }
              catch (PpasServiceFailedException e)
              {
                  i_logger.logMessage( new LoggableEvent("Invalid OldAccountGroupId: " + p_recordFields[C_OLD_ACC_GRP_ID] + e.getMessage(),
                                                         LoggableInterface.C_SEVERITY_ERROR) );

                  if (PpasDebug.on)
                  {
                      PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                      C_CLASS_NAME, 10215, this,
                                      "Invalid OldAccountGroupId: " + p_recordFields[C_OLD_ACC_GRP_ID]);
                  }
              }
   
              if (!p_subLifeSnapshotData.getAccountGroupId().equals(l_accountGrpId))
              {
                  i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_OLD_ACCOUNT_GROUP_ID +
                                                 BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                                 i_inputLine);
                  l_validOldGroup = false;
              }
              else
              {
                  try
                  {
                      i_batchDataRecord.setOldAccountGroupId(p_recordFields[C_OLD_ACC_GRP_ID].trim());
                  }
                  catch (PpasServiceFailedException Le)
                  {
                      i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_OLD_ACCOUNT_GROUP_ID
                                                     + BatchConstants.C_DELIMITER_REPORT_FIELDS
                                                     + i_inputLine);

                      i_logger.logMessage( new LoggableEvent("Unable to set OldAccountGroupId: " + p_recordFields[C_OLD_ACC_GRP_ID] + Le.getMessage(),
                                                             LoggableInterface.C_SEVERITY_ERROR) );

                      if (PpasDebug.on)
                      {
                          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                          C_CLASS_NAME, 10220, this,
                                          "Invalid OldAccountGroupId: " + p_recordFields[C_OLD_ACC_GRP_ID]);
                      }
                      l_validOldGroup = false;
                  } 
              }
          }
      }    

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                          C_CLASS_NAME, 10230, this, BatchConstants.C_LEAVING + C_METHOD_validateOldAccountGroup);
      }
      return l_validOldGroup;
  }
  
  /** Method name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_METHOD_validateNewAccountGroup = "validateNewAccountGroup";    

  /**
   * This method validates the New Account Group Id supplied in the input file.
   * @param p_recordFields The array to keep the input line elements.
   * @return <code>true</code> when extracted fields are valid else <code>false</code>.
   */
  private boolean validateNewAccountGroup(String [] p_recordFields)
  {
      boolean l_validNewGroup= true;

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                          C_CLASS_NAME, 10240, this, BatchConstants.C_ENTERING + C_METHOD_validateNewAccountGroup);
      }

      if (p_recordFields[C_NEW_ACC_GRP_ID].length() < C_NEW_ACC_GROUP_ID_LENGTH)
      {
          i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_NEW_ACCOUNT_GROUP_ID +
                                         BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                         i_inputLine);

          l_validNewGroup = false;
      }
      else
      {
          try
          {
              i_batchDataRecord.setNewAccountGroupId(p_recordFields[C_NEW_ACC_GRP_ID].trim());
          }
          catch (PpasServiceFailedException e1)
          {
              i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_NEW_ACCOUNT_GROUP_ID +
                                             BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                             i_inputLine);

              i_logger.logMessage( new LoggableEvent("Unable to set NewAccountGroupId: " + p_recordFields[C_NEW_ACC_GRP_ID] + e1.getMessage(),
                                                     LoggableInterface.C_SEVERITY_ERROR) );

              if (PpasDebug.on)
              {
                  PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                  C_CLASS_NAME, 10250, this,
                                  "Invalid NewAccountGroupId: " + p_recordFields[C_NEW_ACC_GRP_ID]);
              }
              l_validNewGroup = false;
          }                 
      }

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                          C_CLASS_NAME, 10260, this, BatchConstants.C_LEAVING + C_METHOD_validateNewAccountGroup);
      }
      return l_validNewGroup;
  }
  
  /** Method name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_METHOD_validateOldServiceOfferings = "validateOldServiceOfferings";    

  /**
   * This method validates the Old Service Offerings supplied in the input file.
   * @param p_recordFields The array to keep the input line elements.
   * @param p_subLifeSnapshotData The MSISDN being processed.
   * @return <code>true</code> when extracted fields are valid else <code>false</code>.
   */
  private boolean validateOldServiceOfferings(String [] p_recordFields, SubscriberLifecycleSnapshotData p_subLifeSnapshotData)
  {
      boolean l_validOldOfferings = true;

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                          C_CLASS_NAME, 10270, this, BatchConstants.C_ENTERING + C_METHOD_validateOldServiceOfferings);
      }

      if (p_recordFields[C_NEW_SERV_OFF] ==  null ||
          p_recordFields[C_NEW_SERV_OFF].trim().equals(""))
      {
          i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_NEW_SERV_OFFERINGS +
                                         BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                         i_inputLine);
          l_validOldOfferings = false;
      }

      if ((l_validOldOfferings) &&
          (p_recordFields[C_OLD_SERV_OFF].length() < C_OLD_SERV_OFFERINGS_LENGTH))
      {
          i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_OLD_SERV_OFFERINGS +
                                         BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                         i_inputLine);

          l_validOldOfferings = false;
      }
      else
      {
          //Compare old service offerings supplied in the file with account group in database.
          if (p_subLifeSnapshotData != null)
          {
              ServiceOfferings l_servOfferings = null;
              try
              {
                  l_servOfferings = new ServiceOfferings(p_recordFields[C_OLD_SERV_OFF].trim());
              }
              catch (PpasServiceFailedException e)
              {
            	  i_logger.logMessage( new LoggableEvent("Invalid OldServiceOfferings: " + p_recordFields[C_OLD_SERV_OFF] + e.getMessage(),
                                                         LoggableInterface.C_SEVERITY_ERROR) );

            	  if (PpasDebug.on)
                  {
                      PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                      C_CLASS_NAME, 10275, this,
                                      "The OldServiceOfferings: " + p_recordFields[C_OLD_SERV_OFF] + " for " +
                                      " MSISDN " + p_recordFields[C_MSISDN] + " does not match CULS row");
                  }
              }
   
              if (p_subLifeSnapshotData != null && 
                      p_subLifeSnapshotData.getServiceOfferings() != null &&
                      !p_subLifeSnapshotData.getServiceOfferings().equals(l_servOfferings))
              {
                  i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_OLD_SERV_OFFERINGS +
                                                 BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                                 i_inputLine);
                  l_validOldOfferings = false;
              }
              else
              {
                  try
                  {
                      i_batchDataRecord.setOldServiceOfferings(p_recordFields[C_OLD_SERV_OFF].trim());
                  }
                  catch (PpasServiceFailedException e)
                  {
                      i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_OLD_SERV_OFFERINGS +
                                                     BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                                     i_inputLine);

                	  i_logger.logMessage( new LoggableEvent("Unable to set OldServiceOfferings: " + p_recordFields[C_OLD_SERV_OFF] + e.getMessage(),
                              LoggableInterface.C_SEVERITY_ERROR) );

                	  if (PpasDebug.on)
                      {
                          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                          C_CLASS_NAME, 10280, this,
                                          "The OldServiceOfferings: " + p_recordFields[C_OLD_SERV_OFF] + " for " +
                                          " MSISDN " + p_recordFields[C_MSISDN] + " does not match CULS row");
                      }
                      l_validOldOfferings = false;
                  } 
              }
          }
      }

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                          C_CLASS_NAME, 10290, this, BatchConstants.C_LEAVING + C_METHOD_validateOldServiceOfferings);
      }
      return l_validOldOfferings;
  }
  
  /** Method name constant used in calls to middleware.  Value is {@value}. */
  private static final String C_METHOD_validateNewServiceOfferings = "validateNewServiceOfferings";    

  /**
   * This method validates the New Service Offerings supplied in the input file.
   * @param p_recordFields The array to keep the input line elements.
   * @return <code>true</code> when extracted fields are valid else <code>false</code>.
   */
  private boolean validateNewServiceOfferings(String [] p_recordFields)
  {
      boolean l_validNewOfferings = true;

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_START,
                          C_CLASS_NAME, 10300, this, BatchConstants.C_ENTERING + C_METHOD_validateNewServiceOfferings);
      }

      if (p_recordFields[C_NEW_SERV_OFF].length() < C_NEW_SERV_OFFERINGS_LENGTH)
      {
          i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_NEW_SERV_OFFERINGS +
                                         BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                         i_inputLine);

          l_validNewOfferings = false;
      }
      else
      {
          try
          {
              i_batchDataRecord.setNewServiceOfferings(p_recordFields[C_NEW_SERV_OFF].trim());
          }
          catch (PpasServiceFailedException e)
          {
              i_batchDataRecord.setErrorLine(C_ERROR_CODE_INVALID_NEW_SERV_OFFERINGS +
                                             BatchConstants.C_DELIMITER_REPORT_FIELDS +
                                             i_inputLine);

        	  i_logger.logMessage( new LoggableEvent("Unable to set NewServiceOfferings: " + p_recordFields[C_NEW_SERV_OFF] + e.getMessage(),
                      LoggableInterface.C_SEVERITY_ERROR) );
        	  
              if (PpasDebug.on)
              {
                  PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_TRACE,
                                  C_CLASS_NAME, 10310, this,
                                  "Invalid NewServiceOfferings: " + p_recordFields[C_NEW_SERV_OFF]);
              }
              l_validNewOfferings = false;
          }
      }

      if (PpasDebug.on)
      {
          PpasDebug.print(PpasDebug.C_LVL_VLOW, PpasDebug.C_APP_SERVICE, PpasDebug.C_ST_END,
                          C_CLASS_NAME, 10320, this, BatchConstants.C_LEAVING + C_METHOD_validateNewServiceOfferings);
      }
      return l_validNewOfferings;
  }
}
