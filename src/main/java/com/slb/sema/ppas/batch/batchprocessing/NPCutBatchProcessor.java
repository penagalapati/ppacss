// //////////////////////////////////////////////////////////////////////////////
//      ASCS        : 9500
////////////////////////////////////////////////////////////////////////////////
//
//      FILE NAME   : NPCutBatchProcessor.java
//      DATE        : Aug 17, 2004
//      AUTHOR      : Urban Wigstrom
//      REFERENCE   : PRD_ASCS00_DEV_SS_083
//
//      COPYRIGHT   : ATOS ORIGIN 2004
//
//      DESCRIPTION : Performs the actuall cutover based on NPCutoverRecordsData retrived
//                    from the (in)queue.
//
////////////////////////////////////////////////////////////////////////////////
//      CHANGE HISTORY
////////////////////////////////////////////////////////////////////////////////
// DATE     | NAME          | DESCRIPTION                       | REFERENCE
//----------+---------------+-----------------------------------+--------------------
// DD/MM/YY | <name>        | <brief description of             | <reference>
//          |               | change>                           |
//----------+---------------+-----------------------------------+-------------------
////////////////////////////////////////////////////////////////////////////////
package com.slb.sema.ppas.batch.batchprocessing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Vector;

import com.slb.sema.ppas.batch.batchcontroller.BatchController;
import com.slb.sema.ppas.batch.batchutil.BatchConstants;
import com.slb.sema.ppas.common.dataclass.BatchRecordData;
import com.slb.sema.ppas.common.dataclass.BatchResponseData;
import com.slb.sema.ppas.common.dataclass.Msisdn;
import com.slb.sema.ppas.common.dataclass.MsisdnFormat;
import com.slb.sema.ppas.common.dataclass.NPCutBatchRecordData;
import com.slb.sema.ppas.common.dataclass.NumberChangeData;
import com.slb.sema.ppas.common.dataclass.NumberChangeResponseData;
import com.slb.sema.ppas.common.exceptions.PpasServiceException;
import com.slb.sema.ppas.common.exceptions.PpasServiceMsg;
import com.slb.sema.ppas.common.support.PpasContext;
import com.slb.sema.ppas.common.support.PpasDebug;
import com.slb.sema.ppas.common.support.PpasProperties;
import com.slb.sema.ppas.is.isapi.PpasNumberChangeService;
import com.slb.sema.ppas.util.lang.ThreadObject;
import com.slb.sema.ppas.util.logging.Logger;
import com.slb.sema.ppas.util.structures.SizedQueue;

/**
 * Performs the actual cutover based on <code>NPCutoverRecordsData</code> retrived from the (in)queue.
 */

public class NPCutBatchProcessor extends BatchProcessor
{
    /** Name of class. Used in calls to middleware. Value is {@value}. */
    private static final String C_CLASS_NAME               = "NPCutBatchProcessor";

    /** Error code - line or record format. Value is {@value}. */
    private static final String C_ERROR_INVALID_RECORD_FORMAT = "01";

    //--------------------------------------------------------------------------
    //-- Class level variables. --
    //--------------------------------------------------------------------------

    /** The trace print out base statement number. */
    private static int          c_traceStatementNumber     = 12000;

    //--------------------------------------------------------------------------
    //-- Instance variables. --
    //--------------------------------------------------------------------------

    /** The maximum chunk size. */
    private int                 i_chunkSize                = 0;

    /** The chunk buffer. */
    private Vector              i_chunk                    = null;

    /** The maximum number of parallell inner class threads. */
    private int                 i_maxThreads               = 0;

    /** Signaling object. */
    private Object              i_signalObj                = new Object();

    /** A <code>Vector</code> that contains references to the living inner class process threads. */
    private Vector              i_livingIsProcessThreadVec = null;

    /** The inner class thread id counter. */
    private int                 i_threadIdCnt              = 0;

    /**
     * Creates an instance of <code>NPCutBatchProcessor</code>.
     * @param p_ppasContext the <code>PpasContext</code> reference.
     * @param p_logger the logger.
     * @param p_batchController the batch controller.
     * @param p_inQueue the input data queue.
     * @param p_outQueue the output data queue.
     * @param p_params the start process paramters.
     * @param p_properties <code>PpasProperties </code> for the batch subsystem.
     */
    public NPCutBatchProcessor(PpasContext p_ppasContext,
            Logger p_logger,
            BatchController p_batchController,
            SizedQueue p_inQueue,
            SizedQueue p_outQueue,
            Map p_params,
            PpasProperties p_properties)
    {
        super(p_ppasContext, p_logger, p_batchController, p_inQueue, p_outQueue, p_params, p_properties);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_START,
                            C_CLASS_NAME,
                            10001,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }

        i_threadIdCnt = 0;
        i_chunkSize = p_properties.getIntProperty(BatchConstants.C_MAX_CHUNK_SIZE, 200);
        i_maxThreads = p_properties.getIntProperty(BatchConstants.C_MAX_NUMBER_OF_THREADS, 10);
        trace(this, "Constructor", "chunck size = " + i_chunkSize);
        trace(this, "Constructor", "max threads = " + i_maxThreads);

        i_livingIsProcessThreadVec = new Vector(i_maxThreads);

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_CONFIN_END,
                            C_CLASS_NAME,
                            10002,
                            this,
                            "Constructing " + C_CLASS_NAME);
        }
    }

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_processRecord = "processRecord";

    /**
     * Processes the given <code>BatchDataRecord</code> but will always return a 'null' object. The passed
     * <code>BatchDataRecord</code> will instead be stored in an internal buffer in order to be processed
     * when the buffer reaches a configurable number of stored records.
     * @param p_record the <code>BatchDataRecord</code>.
     * @return always returns a <code>null</code> object.
     * @throws PpasServiceException if a fatal error occurs.
     */
    public BatchRecordData processRecord(BatchRecordData p_record) throws PpasServiceException
    {
        IsProcessor     l_isProcessor       = null;
        StringBuffer    l_tmpRecoveryLine   = null;
        NPCutBatchRecordData l_recordData   = (NPCutBatchRecordData)p_record;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11020,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_processRecord);
        }

        //Prepare recovery line - shall always be created
        l_tmpRecoveryLine = new StringBuffer();
        l_tmpRecoveryLine.append(l_recordData.getRowNumber());
        l_tmpRecoveryLine.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);

        if (i_chunk == null)
        {
            i_chunk = new Vector();
        }

        trace(this, C_METHOD_processRecord, "Current i_chunk size     = " + i_chunk.size());
        trace(this, C_METHOD_processRecord, "Max allowed chunk size   = " + i_chunkSize);
        trace(this, C_METHOD_processRecord, "Number of living threads = " + i_maxThreads);
        trace(this, C_METHOD_processRecord, "Max no of threads        = " + i_maxThreads);

        if (!p_record.isCorruptLine())
        {
            i_chunk.add(l_recordData);
            trace(this, C_METHOD_processRecord, "Current i_chunk size2     = " + i_chunk.size());
            trace(this, C_METHOD_processRecord, "Record no " + i_chunk.size() + " oldMsisdn "
                    + l_recordData.getOldMsisdn());
            trace(this, C_METHOD_processRecord, "Record no " + i_chunk.size() + " sdp id "
                  + l_recordData.getSdpId());

            if (i_chunk.size() == i_chunkSize)
            {
                trace(this, C_METHOD_processRecord, "Inside i_chunk.size() == i_chunkSize");
                if (i_livingIsProcessThreadVec.size() >= i_maxThreads)
                {
                    // Max number of threads already started, wait before starting a new thread.
                    trace(this, C_METHOD_processRecord, "Synch. before 'Wait to start new thread'.");

                    synchronized (i_signalObj)
                    {
                        try
                        {
                            trace(this, C_METHOD_processRecord, "Wait to start new thread.");
                            i_signalObj.wait();
                        }
                        catch (InterruptedException p_intEx)
                        {
                            // Nothing to do.
                            p_intEx = null;
                        }
                    }
                }
                // Construct and start the inner class.
                trace(this, C_METHOD_processRecord, "Start new thread with id = " + i_threadIdCnt);

                l_isProcessor = new IsProcessor(i_threadIdCnt++, i_chunk);
                l_isProcessor.start();
                i_livingIsProcessThreadVec.add(l_isProcessor);
                i_chunk = null;
            }
        }
        else
        {
            // A corrupt line, store it directly in the output queue.
            trace(this, "processRecord", "Corrupt line, store record immediately.");
            
            l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);
            p_record.setRecoveryLine(l_tmpRecoveryLine.toString());
            super.storeRecord(l_recordData);
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_processRecord);
        }

        return null;
    } // End of processRecord()

    //--------------------------------------------------------------------------
    //-- Protected methods. --
    //--------------------------------------------------------------------------

    /** Method name constant used in calls to middleware. Value is {@value}. */
    private static final String C_METHOD_flush = "flush";

    /**
     * Flushes the chunk buffer. This method is called from the super class ProcessorThread just before it
     * will end, which means that this method shouldn't return until all living 'IsProcessor' Threads have
     * ended.
     */
    protected void flush()
    {
        IsProcessor l_isProcessor = null;
        boolean l_continue = true;

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_START,
                            C_CLASS_NAME,
                            11020,
                            this,
                            BatchConstants.C_ENTERING + C_METHOD_flush);
        }

        // Check that there is a chunk of un-processed records.
        if (i_chunk != null && i_chunk.size() > 0)
        {
            // We don't bother to check if the max number of process threads will be exceeded since we are
            // going to create only one new process thread.
            trace(this, C_METHOD_flush, "Create and start a processing thread with id = " + i_threadIdCnt);
            l_isProcessor = new IsProcessor(i_threadIdCnt++, i_chunk);
            l_isProcessor.start();
            i_livingIsProcessThreadVec.add(l_isProcessor);
        }
        
        trace(this, C_METHOD_flush, "Wait for all living processing thread...");
        l_continue = true;

        while (l_continue)
        {
            try
            {
                trace(this, C_METHOD_flush, "Get the first living processing thread.");
                l_isProcessor = (IsProcessor)i_livingIsProcessThreadVec.firstElement();
            }
            catch (NoSuchElementException p_nseEx)
            {
                // Ok, no more threads to join.
                trace(this, C_METHOD_flush, "No more living processing thread to join.");
                l_continue = false;

                // Start the loop all over again.
                continue;
            }

            try
            {
                trace(this, C_METHOD_flush, "Join the processing thread '" + l_isProcessor.getThreadName()
                        + "'");
                l_isProcessor.join(0L);
            }
            catch (InterruptedException p_intEx)
            {
                // No specific handling.
                p_intEx = null;
            }
        }

        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_SERVICE,
                            PpasDebug.C_ST_END,
                            C_CLASS_NAME,
                            11130,
                            this,
                            BatchConstants.C_LEAVING + C_METHOD_flush);
        }
    }
    //--------------------------------------------------------------------------
    //-- Private methods. --
    //--------------------------------------------------------------------------

    /**
     * Construct error information for the report file, the MSISDNs must be left-justified.
     * @param p_oldMsisdn The old msisdn.
     * @param p_errorCode The error code.
     * @param p_inputLine The original input line.
     * @return String with errorline information, left-justified msisdns
     */
    private String createErrorLine(Msisdn p_oldMsisdn, String p_errorCode, String p_inputLine)
    {
        StringBuffer l_tmpErrorLine = new StringBuffer(); // Helpvariable for creation of error line.
        StringBuffer l_tmp          = new StringBuffer();
        MsisdnFormat l_msisdnFormat = null;
        String       l_oldMsisdn    = null;

        l_tmpErrorLine.append(p_errorCode);
        l_tmpErrorLine.append(BatchConstants.C_DELIMITER_REPORT_FIELDS);

        if ( p_errorCode.equals(C_ERROR_INVALID_RECORD_FORMAT) )
        {
            // Line or record format error. Show inputline in report file
            l_tmpErrorLine.append(p_inputLine);
        }
        else
        {
            // Left justify the MSISDN number
            l_msisdnFormat = NPCutBatchProcessor.super.i_ppasContext.getMsisdnFormatInput();
            l_oldMsisdn    = l_msisdnFormat.format(p_oldMsisdn);

            l_tmp.append(l_oldMsisdn);
            l_tmp.append(BatchConstants.C_EMPTY_MSISDN);
            l_tmpErrorLine.append(l_tmp.substring(0, BatchConstants.C_LENGTH_MSISDN));
        }
        
        return l_tmpErrorLine.toString();

    } // End createErrorLine()
    
    /**
     * Prints trace print outs.
     * @param p_method the name of th calling method.
     * @param p_callingObject the calling <code>Object</code>.
     * @param p_message the trace message to be printed.
     */
    private static void trace(Object p_callingObject, String p_method, String p_message)
    {
        if (PpasDebug.on)
        {
            PpasDebug.print(PpasDebug.C_LVL_VLOW,
                            PpasDebug.C_APP_ALL,
                            PpasDebug.C_ST_TRACE,
                            C_CLASS_NAME,
                            c_traceStatementNumber++,
                            p_callingObject,
                            "TRACE: " + p_method + " -- " + p_message);
        }
    }

    //--------------------------------------------------------------------------
    //-- Inner classes. --
    //--------------------------------------------------------------------------

    /**
     * The purpose of this inner class, <code>IsProcessor</code>, is to process the
     * <code>MsisdnRoutingBatchRecordData</code> records stored in the chunk buffer passed in to the
     * Constructor.
     */
    private class IsProcessor extends ThreadObject
    {
        //----------------------------------------------------------------------
        //-- Class level constant. --
        //----------------------------------------------------------------------

        /** Class name constant used in calls to middleware. Value is {@value}. */
        private static final String     C_INNER_CLASS_NAME             =
            "MsisdnRoutingProvisioningBatchProcessor.IsProcessor";

        /** The error code for a service response timeout. Value is {@value}. */
        //        private static final String C_ERROR_CODE_SERVICE_RESPONSE_TIMEOUT = "03";

        /** Valid record but error during processing. */
        private static final String     C_ERROR_DURING_PROCESSING      = "02";

        /**
         * Subscriber has no number plan in progress or the new subscriber identifier is not the same as the
         * one defined for the subscriber.
         */
        private static final String     C_ERROR_SUBSCRIBER_IN_PROGRESS = "03";

        /** Old Subscriber identifier does not exist in the db. */
        private static final String     C_MSISDN_DONT_EXIST            = "04";

        /** The error code for an unexpected PpasServiceException. Value is {@value}. */
        //        private static final String C_ERROR_CODE_UNEXPECTED_EXCEPTION = "99";

        //----------------------------------------------------------------------
        //-- Instance variables. --
        //----------------------------------------------------------------------
        /** The current thread id. */
        private int                     i_threadId                     = 0;

        /** An instance of PpasNumberChangeService. */
        private PpasNumberChangeService i_numberChangeServ             = null;

        /** The incoming chunk buffer. */
        private Vector                  i_chunkBuffer                  = null;

        /** The IS API output buffer. */
        private ArrayList               i_outputBuffer                 = null;

        //----------------------------------------------------------------------
        //-- Constructors. --
        //----------------------------------------------------------------------

        /**
         * Constructs an <code>IsProcessor</code> instance using the given chunk buffer.
         * @param p_threadId a unique thread id.
         * @param p_chunkBuffer a <code>Vector</code> that contains a number of
         * <code>MsisdnRoutingBatchRecordData</code> records to be processed.
         */
        public IsProcessor(int p_threadId, Vector p_chunkBuffer)
        {
            super();

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_START,
                                C_INNER_CLASS_NAME,
                                11000,
                                this,
                                BatchConstants.C_CONSTRUCTING);
            }

            i_threadId = p_threadId;

            i_numberChangeServ = new PpasNumberChangeService(null,
                                                             NPCutBatchProcessor.super.i_logger,
                                                             NPCutBatchProcessor.super.i_ppasContext);
            this.i_chunkBuffer = p_chunkBuffer;
            i_outputBuffer = new ArrayList(p_chunkBuffer.size());

            trace(this, "Constructor", getThreadName() + ": is constructed.");

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_CONFIN_END,
                                C_INNER_CLASS_NAME,
                                11000,
                                this,
                                BatchConstants.C_CONSTRUCTED);
            }
        }

        //----------------------------------------------------------------------
        //-- Public methods. --
        //----------------------------------------------------------------------

        /** Method name constant used in calls to middleware. Value is {@value}. */
        private static final String C_METHOD_doRun = "doRun";

        /**
         * This method is called from the active <code>Thread</code> in the super class.
         */
        public void doRun()
        {
            NPCutBatchRecordData l_record = null;
            NumberChangeData l_numberChangeData = null;
            BatchResponseData l_responseData = null; // The IS API response data object.
            Collection l_responseCollection = null;
            NumberChangeResponseData[] l_numberChangeDataArr = null;
            PpasServiceException l_ppasServiceException = null;
            String l_sdpId      =  null;
            MsisdnFormat l_msisdnFormat = NPCutBatchProcessor.super.i_ppasContext.getMsisdnFormatInput();

            StringBuffer l_tmpRecoveryLine = null;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_INNER_CLASS_NAME,
                                11020,
                                this,
                                BatchConstants.C_ENTERING + C_METHOD_doRun);
            }

            trace(this, C_METHOD_doRun, getThreadName() + ": Scan through passed i_chunkBuffer, size = "
                    + i_chunkBuffer.size());

            for (int i = 0; i < this.i_chunkBuffer.size(); i++)
            {
                l_record = (NPCutBatchRecordData)this.i_chunkBuffer.elementAt(i);
                l_numberChangeData = new NumberChangeData(l_record.getOldMsisdn(), l_record.getNewMsisdn());
                this.i_outputBuffer.add(l_numberChangeData);
            }

            try
            {
                trace(this, C_METHOD_doRun, getThreadName() + ": Call the IS API, timeout = "
                        + NPCutBatchProcessor.super.i_isApiTimeout + " ms");

                l_responseData = this.i_numberChangeServ.cutoverNumberPlan(null,
                                                                           NPCutBatchProcessor.super.i_isApiTimeout,
                                                                           this.i_outputBuffer);
                
    
//                l_responseData = simulateIsAPI(this.i_outputBuffer);

            }
            catch (PpasServiceException p_psExe)
            {
                trace(this, C_METHOD_doRun, getThreadName() + ": A PpasServiceException is caught: "
                        + p_psExe.getMessage() + ",  key = " + p_psExe.getMsgKey());

                // Set the error line for all 'BatchRecordData' records in the 'i_chunkBuffer' and put them
                // into the output buffer.
                for (int i = 0; i < this.i_chunkBuffer.size(); i++)
                {
                    l_record = (NPCutBatchRecordData)this.i_chunkBuffer.elementAt(i);
                    this.mapException(l_record, p_psExe);

                    //Create a recovery line.
                    l_tmpRecoveryLine = new StringBuffer();
                    l_tmpRecoveryLine.append(l_record.getRowNumber());
                    l_tmpRecoveryLine.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);
                    l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);

                    l_record.setRecoveryLine(l_tmpRecoveryLine.toString());

                    // Store the 'BatchRecordData' in the output queue.
                    NPCutBatchProcessor.super.storeRecord(l_record);
                }

                // Finish up the current thread.
                trace(this, C_METHOD_doRun, getThreadName() + ": Finish up the current thread.");
                // Remove this instance from the 'living IsProcess Thread Vector'.
                NPCutBatchProcessor.this.i_livingIsProcessThreadVec.remove(this);

                // Notify any waiting thread.
                synchronized (i_signalObj)
                {
                    NPCutBatchProcessor.this.i_signalObj.notify();
                }

                // Return from this method.
                if (PpasDebug.on)
                {
                    PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                    PpasDebug.C_APP_SERVICE,
                                    PpasDebug.C_ST_END,
                                    C_INNER_CLASS_NAME,
                                    11130,
                                    this,
                                    BatchConstants.C_LEAVING + C_METHOD_doRun
                                            + ", since a 'PpasServiceException' was caught.");
                }
                return;
            }

            trace(this, C_METHOD_doRun, "Response from IS API received, all success = "
                    + l_responseData.getAllSuccess());
            
            
            l_responseCollection = l_responseData.getData();
            l_numberChangeDataArr = new NumberChangeResponseData[l_responseCollection.size()];
            l_responseCollection.toArray(l_numberChangeDataArr);

            if (l_responseData.getAllSuccess())
            {
                // Store all the processed records directly in the output queue.
                for (int i = 0; i < this.i_chunkBuffer.size(); i++)
                {
                    l_record = (NPCutBatchRecordData)this.i_chunkBuffer.elementAt(i);
                    l_sdpId = l_numberChangeDataArr[i].getScpId();
                    // Always create a recovery line.
                    l_tmpRecoveryLine = new StringBuffer();
                    l_tmpRecoveryLine.append(l_record.getRowNumber());
                    l_tmpRecoveryLine.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);
                    l_tmpRecoveryLine.append(BatchConstants.C_SUCCESSFULLY_PROCESSED);
                    l_record.setRecoveryLine(l_tmpRecoveryLine.toString());

                    l_record.setOutputLine("2" + BatchConstants.C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2
                            + l_msisdnFormat.format(l_record.getOldMsisdn()));
                    
                    l_record.setSdpId(l_sdpId);

                    NPCutBatchProcessor.super.storeRecord(l_record);
                }
            }
            else
            {
                // Scan through the responses in order to update the corresponding BatchRecordData's error
                // line for an erroneous request before storing it into the output queue.
                

                trace(this, C_METHOD_doRun, "l_numberChangeDataArr.length = "
                        + l_numberChangeDataArr.length);

                for (int i = 0; i < l_numberChangeDataArr.length; i++)
                {
                    // Get the current 'BatchRecordData' object.
                    l_record = (NPCutBatchRecordData)this.i_chunkBuffer.elementAt(i);
                    l_sdpId = l_numberChangeDataArr[i].getScpId();
                    
                    //Always create a recovery line.   
                    l_tmpRecoveryLine = new StringBuffer();
                    l_tmpRecoveryLine.append(l_record.getRowNumber());
                    l_tmpRecoveryLine.append(BatchConstants.C_DELIMITER_RECOVERY_STATUS);

                    // Check if any Exception was raised during the process.
                    l_ppasServiceException = l_numberChangeDataArr[i].getException();

                    if (l_ppasServiceException != null)
                    {
                        l_tmpRecoveryLine.append(BatchConstants.C_NOT_PROCESSED);
                        // The current request failed, update the corresponding BatchRecordData's error line.
                        this.mapException(l_record, l_ppasServiceException);

                    }
                    else
                    {
                        l_record.setOutputLine("2"
                                + BatchConstants.C_DELIMITER_NUMBER_PLAN_CHANGE_FORMAT_2
                                + l_msisdnFormat.format((l_record).getOldMsisdn()));
                                        
                        
                        l_record.setSdpId(l_sdpId);
                        
                        l_tmpRecoveryLine.append(BatchConstants.C_SUCCESSFULLY_PROCESSED);
                    }

                    l_record.setRecoveryLine(l_tmpRecoveryLine.toString());
                    // Store the 'BatchRecordData' object into the output buffer.
                    NPCutBatchProcessor.super.storeRecord(l_record);
                }
            }

            // Finish up the current thread.
            trace(this, C_METHOD_doRun, getThreadName() + ": Finish up the current thread.");
            // Remove this instance from the 'living IsProcess Thread Vector'.
            NPCutBatchProcessor.this.i_livingIsProcessThreadVec.remove(this);

            // Notify any waiting thread.
            synchronized (i_signalObj)
            {
                NPCutBatchProcessor.this.i_signalObj.notify();
            }

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_END,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                BatchConstants.C_LEAVING + C_METHOD_doRun);
            }
        }

        //----------------------------------------------------------------------
        //-- Protected methods. --
        //----------------------------------------------------------------------

        /**
         * Returns the name of this thread.
         * @return the name of this thread.
         */
        protected String getThreadName()
        {
            return "IsProcessor: Thread-" + i_threadId;
        }

        //----------------------------------------------------------------------
        //-- Private methods. --
        //----------------------------------------------------------------------

        /** Method name constant used in calls to middleware. Value is {@value}. */
        private static final String C_METHOD_mapException = "mapException";

        /**
         * Updates the passed <code>BatchRecordData</code> object's error line with an error code that
         * corresponds to the passed <code>PpasServiceException</code>'s message key.
         * @param p_record the <code>BatchRecordData</code>.
         * @param p_ppasServEx the <code>PpasServiceException</code>.
         */
        private void mapException(BatchRecordData p_record, PpasServiceException p_ppasServEx)
        {
            String l_errorLine = null;
            boolean l_mappingDone = false;
            String[][] l_errorCodeMappings = null;

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_START,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                BatchConstants.C_ENTERING + C_METHOD_mapException);
            }

            // Set-up the message key - error code mapping array.
            l_errorCodeMappings = new String[][] {
                // Exceptions included in the 'BatchElementResponseData' object.
                {PpasServiceMsg.C_KEY_MSISDN_NOT_EXISTS, C_MSISDN_DONT_EXIST},                  
                {PpasServiceMsg.C_KEY_NO_NUMBER_PLAN_CHANGE_IN_PROGRESS, C_ERROR_SUBSCRIBER_IN_PROGRESS},
                {PpasServiceMsg.C_KEY_ACCOUNT_NOT_ACTIVE, C_ERROR_DURING_PROCESSING},
                {PpasServiceMsg.C_KEY_DISCONNECTED_ACCOUNT, C_ERROR_DURING_PROCESSING},
                {PpasServiceMsg.C_KEY_TRANSIENT_ACCOUNT, C_ERROR_DURING_PROCESSING},
                {PpasServiceMsg.C_KEY_RESOURCE_UNAVAILABLE, C_ERROR_DURING_PROCESSING},
                {PpasServiceMsg.C_KEY_INPUT_CUSTID_AND_MSISDN_BLANK, C_ERROR_DURING_PROCESSING}};
                                  

            trace(this, C_METHOD_mapException, "l_errorCodeMappings.length = " + l_errorCodeMappings.length);
            // Look for matching mapping through the whole error code mapping array or until a matching
            // mapping is done.
            for (int i = 0; (i < l_errorCodeMappings.length && !l_mappingDone); i++)
            {
                if (p_ppasServEx.getMsgKey().equals(l_errorCodeMappings[i][0]))
                {
                    l_errorLine = createErrorLine(((NPCutBatchRecordData)p_record).getOldMsisdn(),
                                                  l_errorCodeMappings[i][1],
                                                  ((NPCutBatchRecordData)p_record).getInputLine());
                    l_mappingDone = true;
                    trace(this, C_METHOD_mapException, "matching mapping found, key = "
                            + l_errorCodeMappings[i][0] + ",  error code = " + l_errorCodeMappings[i][1]);
                }
            }
            if (!l_mappingDone)
            {
                // None of the expected mapping matches the given message key.
                trace(this, "", "An unexpected PpasServiceException is detected: " + p_ppasServEx);

                l_errorLine = createErrorLine(((NPCutBatchRecordData)p_record).getOldMsisdn(),
                                              C_ERROR_DURING_PROCESSING,
                                              ((NPCutBatchRecordData)p_record).getInputLine());
            }

            p_record.setErrorLine(l_errorLine.toString());

            if (PpasDebug.on)
            {
                PpasDebug.print(PpasDebug.C_LVL_VLOW,
                                PpasDebug.C_APP_SERVICE,
                                PpasDebug.C_ST_END,
                                C_INNER_CLASS_NAME,
                                11130,
                                this,
                                BatchConstants.C_LEAVING + C_METHOD_mapException);
            }
        }
    }

    /**
     * Simulates a call to the IS API. NOTE: Only for test purposes!!!!
     * @param p_arr an <code>ArrayList</code> which contains <code>MsisdnRoutingData</code> objects.
     * @return a <code>BatchResponseData</code>.
     * @throws PpasServiceException
     */
//    private BatchResponseData simulateIsAPI(ArrayList p_arr) throws PpasServiceException
//    {
//        BatchResponseData l_responseData = null;
//        BatchElementResponseData l_msisdnResponseData = null;
//        NPCutBatchRecordData l_npCutBatchData = null;
//        ArrayList l_responseCollection = new ArrayList(p_arr.size());
//
//        trace(this, "simulateIsAPI", "Scan through passed ArrayList, size = " + p_arr.size());
//
//        for (int i = 0; i < p_arr.size(); i++)
//        {
//            l_npCutBatchData = (NPCutBatchRecordData)p_arr.get(i);
//            l_msisdnResponseData = new BatchElementResponseData(l_npCutBatchData.getNewMsisdn(), null);
//            l_responseCollection.add(l_msisdnResponseData);
//
//            // Simulate a processing time.
//            trace(this, "simulateIsAPI", "Simulate a processing time, sleep 2 sec.");
//
//            try
//            {
//                Thread.sleep(2000);
//            }
//            catch (InterruptedException p_intEx)
//            {
//                p_intEx = null;
//            }
//        }
//
//        l_responseData = new BatchResponseData(l_responseCollection, true);
//
//        return l_responseData;
//    }

}